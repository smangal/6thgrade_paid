package com.mathfriendzy.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter extends SQLiteOpenHelper
{
	private static String dbPath= "/data/data/com.sixthgradepaid/databases/"; 
	private static String dbName = "LeapAhead.sqlite"; 
	private SQLiteDatabase applicationDatabase;  
	private final Context applicationContext;


	private static int VERSION_NUMBER = 1;

	public DatabaseAdapter(Context context)
	{
		super(context,  dbName , null, VERSION_NUMBER);
		this.applicationContext  = context;
	}//END Constructor

	public void createDatabase(){
		boolean dbexist = checkDataBase();
		if (!dbexist){
			SharedPreferences sheredPreference = this.applicationContext
					.getSharedPreferences("SHARED_FLAG", 0);
			SharedPreferences.Editor editor = sheredPreference.edit();
			editor.putBoolean("isGetLanguageFromServer", false);
			editor.putBoolean("isTranselation", false);
			editor.commit();
			try {
				createdatabase();
			} catch (IOException e) {
				e.printStackTrace();
			}//End try catch block
		}//ENd if else
	}

	public void deleteDataBaseIfExist(){
		try{
			File dbFile = new File(dbPath +  dbName);
			if(dbFile.exists()){
				dbFile.delete();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db){

	}

	@Override
	public synchronized void close()
	{
		if( applicationDatabase != null)
			applicationDatabase .close();
		super.close();
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2){
	}

	public boolean checkDataBase(){
		File dbFile = new File(dbPath +  dbName);
		return dbFile.exists();
	}

	public void copyDataBase() throws IOException{
		PackageManager pm = applicationContext.getPackageManager();
		String apkFile;
		try	{
			apkFile = pm.getApplicationInfo(applicationContext.getPackageName(), 0).sourceDir;
			ZipFile zipFile = new ZipFile(apkFile);
			ZipEntry entry = zipFile.getEntry("assets/" + dbName);
			InputStream myInput = zipFile.getInputStream(entry);
			FileOutputStream myOutput = new FileOutputStream(dbPath + dbName);
			byte[] buffer = new byte[1024*8];
			int length;
			while ((length = myInput.read(buffer)) > 0){
				myOutput.write(buffer, 0, length);
			}
			myInput.close();
			myOutput.flush();
		}catch (NameNotFoundException e){
			e.printStackTrace();
		}
	}


	public void openDataBase() throws SQLException{
		String fullDbPath= dbPath + dbName;
		applicationDatabase = SQLiteDatabase.openDatabase( fullDbPath,
				null,SQLiteDatabase.OPEN_READONLY);
	}


	public void createdatabase() throws IOException{
		boolean dbexist = checkDataBase();
		if (!dbexist){
			this.getReadableDatabase();
			try{
				copyDataBase();
			} catch (IOException e) {
				e.printStackTrace();
				throw new Error("Error copying database");
			}
			this.close();
		}
	}
}
