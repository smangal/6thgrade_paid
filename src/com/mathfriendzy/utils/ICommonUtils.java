package com.mathfriendzy.utils;

public interface ICommonUtils 
{
	boolean MAIN_ACTIVITY_LOG	= false;
	boolean SPLASH_ACTIVITY_LOG = false;
	boolean LANGUAGE_LOG		= false;
	boolean DATABASE_LOG		= false;
	boolean PLAYER_ACTIVITY_LOG	= false;
	boolean CHOOSE_AVTAR_LOG	= false;

	boolean CREATE_TEMP_PLAYER_FLAG 	= false;
	boolean ADD_TEMP_PLAYER_STEP1_FLAG 	= false;
	boolean ADD_TEMP_PLAYER_STEP2_FLAG 	= false;
	boolean SCHOOL_INFO_SUB_FLAG		= false;
	boolean LOGIN_USER_PLAYER_FLAG		= false;
	boolean REGISTRATION_STEP_1_FALG	= false;
	boolean REGISTRATION_STEP_2_FALG    = false;
	boolean EDIT_REG_USER_PLAYER_FLAG   = false;
	boolean LOGIN_USER_CREATE_PLAYER    = false;
	boolean MODIFY_USER_FLAG			= false;
	boolean ADD_OR_CREATE_USER_PLAYER_FLAG = false;
	boolean SEARCH_YOUR_SCHOOL_FLAG		= false;
	boolean SEACRCH_YOUR_TEACHER		= false;
	boolean TAECHER_STUDENT_FLAG        = false;
	boolean LOGIN_ACTIVITY_FLAG         = false;
	boolean SEE_ANSWER_FLAG             = false;

	boolean LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER = false;
	boolean MULTI_FRIENDZY_MAIN_FLAG	= false;

	boolean TEMP_PLAYER_OPERATION_FLAG = false;
	boolean FACEBOOK_CONNECT_LOG       = false;
	boolean CHHOSEAVTAR_OPERATION_LOG  = false; 
	boolean COUNTRY_FLAG			   = false;	
	boolean GRADE_FLAG				   = false;

	boolean IS_DATABASE_LOAD           = false;
	boolean IS_LANGUAGE_LOAD		   = false;
	boolean IS_TRANSELATION_LOAD	   = false;

	//single friendzy

	boolean SINGLE_FRIENDZY_MAIN        = false;
	boolean CHOOSE_A_CHALLENGER         = false;
	boolean CHOOSE_A_CHALLENGER_LIST    = false;
	boolean SINGLE_FRIENDZY_EQUATION_SOLVE = false;

	//learning center flag

	boolean LEARNING_CENTER_MAIN_FLAG 		= false;
	boolean LEARNING_CENTER_OPR       		= false;
	boolean LEARNING_CENTER_CHOOSE_EQUATION = false;
	boolean LEARNING_CENTER_EQUATION_SOLVE  = false;

	//coins distribution flag
	boolean COINS_DISTRIBUTION_FLAG 		= false;

	//multifriendzy flag
	boolean MULTIFRIENDZY_ROUND_FLAG        = false;
	boolean MULTIFRIENDZY_PROBLEM_TYPE      = false;
	boolean MULTIFRIENDZY_WINNER_SCREEN     = false;

	String LOGIN_SHARED_PREFF          = "loginPreff";
	String  IS_LOGIN			       = "isLogin";
	String PLAYER_ID				   = "playerId";
	String PLAYER_INFO                 = "playerInfo";
	String REG_USER_INFO_PREFF		   = "reguserinfoPreff";
	String IS_CHECKED_PREFF            = "isCheckedPreff";
	String LEARNING_CENTER_BG_INFO     = "backgroundInfo";
	String IS_FACEBOOK_LOGIN		   = "facebookLogin";//name of the shared preference
	String IS_LOGIN_FROM_FACEBOOK	   = "isLoginFromFacebook";//for holding true or false

	String HOST_NAME 			= "http://api.letsleapahead.com/";
	String FILE_PATH_ON_HOST 	= "TriviaFriendzy/index.php?";
	String FILE_PATH_ON_HOST_FOR_NOTIFICATION	= "6thGradeFriendzy/index.php?";
	String CONPLETE_URL_FOR_NOTIFICATION  = HOST_NAME + FILE_PATH_ON_HOST_FOR_NOTIFICATION;
	//String HOST_NAME 			= "http://www.chromeinfotech.com/";
	//	String HOST_NAME 			= "http://api.letsleapahead.com/";
	//	String FILE_PATH_ON_HOST 	= "6thGradeFriendzy/index.php?";
	String COMPLETE_URL         =  HOST_NAME + FILE_PATH_ON_HOST;
	String IMG_AVTAR_URL		=  HOST_NAME + "LeapAheadMultiFreindzy/images/avatars/";

	String FACEBOOK_HOST_NAME 	= "http://graph.facebook.com/";
	String BTN_COM_LINK_URL     = "http://www.letsleapahead.com/";

	enum OPERATION_NAME { READING, MATH, LANGUAGE, TIME, MONEY, PHONICS, MEASUREMENT, GEOGRAPHY, SOCIAL, SCIENCE }

	String PROPERTY_REG_ID      = "GCM_REG_ID";
	String REG_ID_PREFF         = "Reg_Id";
	String DEVICE_ID_PREFF      = "deviceIdPreff";
	String DEVICE_ID            = "deviceId";
	String COMPLETE_URL_FOR_REG_DEVICE_NITFICATION = HOST_NAME + "LeapAheadMultiFreindzy/androidNotification/" +
			"register.php?";

	String FACEBOOK_GETMORE_COINS = "https://www.facebook.com/pages/Math-Friendzy/418520078185325?ref=ts";

	//inn app
	boolean GET_MORE_COINS_FLAG             = false;

	//client in app API key
	String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnDd5" +
			"GVJqnPFFqe6/y34Dlkil2uaKMX5HyK87cf2KXhgUk7ofQ5cnnjYSOD5yawHTEmm8u6y0m5o2" +
			"yUD2MBjJUr07uaqgfIRLxLu5DHyOQvx3iMQfBcroScc4XAVEznbeFAJ4Ve9TYBGFg2ciJ4RDBe7uj" +
			"h8tjQJKHEJN8kkK9qGbvJ3Pq4GhgAez0Nh90VBnfWOVror9cX6UDX1fJCYr3qu6wCI4wAhM4yybbA" +
			"SF9RSHBlAdn7dTBde+lX7m5DEfsdqcuhbLtVXCqDJj6cHD35Gxj8QbXWVzFjS5Loy+jO3KGz4QY3O89" +
			"kzrHxnwxsKzot+cwg0JhtvBtltBll65nwIDAQAB";

	String ADS_FREQUENCIES_PREFF  	     = "adsFrequenciesPreff";
	String ADS_FREQUENCIES_DATE   	     = "getFrequenciesDate";
	String ADS_FREQUENCIES_HOUSE_DATE    = "ADS_FREQUENCIES_HOUSE_DATE";
	String ADS_FREQUENCIES			     = "adsFrequencies";
	String ADS_timeIntervalFullAd	     = "adstimeIntervalFullAd";
	String ADS_timeIntervalFullAdForPaid = "adstimeIntervalFullAdForPaid";
	int FREQUENCY_ADS_DAY = 1;
	String HOUSE_AD_ID 					 = "ca-app-pub-4866053470899641/6736383810";
	String ADS_IS_ADS_DISABLE 			= "ADS_IS_ADS_DISABLE";
	String DOWNLOAD_APP_ICON_URL = HOST_NAME + "LeapAheadMultiFreindzy/images/";
	String HOUSE_ADS_IMAGE_URL = "http://api.letsleapahead.com/LeapAheadMultiFreindzy/AdImage/";

	String REGISTARION_URL = COMPLETE_URL;
	String MORE_APP_URL = "https://play.google.com/store/apps/developer?id=WS+Publishing+Group";

	String COMPLETE_URL_FROM_MATH = COMPLETE_URL;//HOST_NAME + "LeapAheadMultiFreindzy/index.php?";//For Math app live url
	//String GOORU_SEARCH_RESOURCES_URL = HOST_NAME + "LeapAheadMultiFreindzy/gooruapi/index.php/gooru/search?";
	String GOORU_SEARCH_RESOURCES_URL = HOST_NAME + "LeapAheadMultiFreindzy/khanapi/?";
	String  GET_KHAN_VIDEO_LINK = HOST_NAME + "LeapAheadMultiFreindzy/khanapi/video_link.php?";
	String PRIVACY_POLICY_URL   = "http://letsleapahead.com/privacy-policy-app";
}
