package com.mathfriendzy.utils;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.coinsdistribution.CoinsDistributionActivity;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.controller.singlefriendzy.ChooseChallengerAdapter;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyEquationActivity;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.listener.LoginRegisterPopUpListener;
import com.mathfriendzy.model.coinsdistribution.CoinsDistributionServerOperation;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;

public class DialogGenerator implements OnClickListener
{
	Context context;
	Dialog dialog;
	Translation transeletion = null;
	String playerId = null;
	String userId   = null;
	String callingActivity = null;

	public DialogGenerator(Context context) 
	{
		this.context = context;
		transeletion = new Translation(context);
	}

	/**
	 * Generate Register or ligin Dialog
	 * @param msg
	 */
	public void generateRegisterOrLogInDialog(String msg)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_register_login);
		dialog.show();

		Button btnNoThanks = (Button)dialog.findViewById(R.id.btnAlertBtnTitleNoThanks);
		Button btnRegister = (Button)dialog.findViewById(R.id.btnAlertBtnTitleRegister);
		Button btnLogin    = (Button)dialog.findViewById(R.id.btnAlertBtnTitleLogIn);
		TextView txtRegisterLogIn = (TextView)dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnRegister.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnTitleRegister"));
		btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
		txtRegisterLogIn.setText(msg);
		transeletion.closeConnection();
		btnNoThanks.setOnClickListener(this);
		btnRegister.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
	}

	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateWarningDialog(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_warning);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener(this);
	}



	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateFriendzyWarningDialog(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_friendzy);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener(this);
	}

	/**
	 * Generate Dialog for check ChallengerId
	 * @param msg
	 */
	public void generateFriendzyCheckDialog(final ImageButton img, final String challengerId, final FriendzyDTO friendzyDTO) 
	{		
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialogdeleteconfirmation);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));		
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallengeWeek")+
				"\n\n"+transeletion.getTranselationTextByTextIdentifier("lblSpellAppYouAreNowActivelyParticipating"));

		transeletion.closeConnection();

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{	
				//Log.e("Dailog", "endDate : "+endDate);
				SharedPreferences sharedPreffPlayerInfo	 = context.getSharedPreferences(IS_CHECKED_PREFF, 0);

				//SharedPreferences sharedPreffriendzy	 = context.getSharedPreferences(ITextIds.FRIENDZY, 0);
				String playerId			= sharedPreffPlayerInfo.getString("playerId", "");
				//String userId			= sharedPreffPlayerInfo.getString("userId", "");	

				FriendzyUtils.remeoveChallenge(playerId);

				FriendzyDTO friendzyDto = new FriendzyDTO();
				friendzyDto = friendzyDTO;
				friendzyDto.setChallengerId(challengerId);
				friendzyDto.setPid(playerId);
				FriendzyUtils.activeFriendzyPlayerList.add(friendzyDto);

				if(img != null)
				{		
					if(MainActivity.isTab)
					{
						img.setBackgroundResource(R.drawable.tab_checkbox_checked_ipad);
					}
					else
					{
						img.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					}
				}
				//img.setTag("1");
				context.startActivity(new Intent(context, MainActivity.class));
				((Activity)context).finish();
				dialog.dismiss();
			}
		});
		btnNoThanks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{	
				//img.setTag("0");
				dialog.dismiss();
			}
		});

	}



	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateWarningDialogForEmptyCoisTransfer(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_empty_coinstransfer_warning);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();			
			}
		});
	}

	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateWarningDialogForSingleFriendzy(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_internet_warning_for_single_friendzy);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		TextView mfAlertMsgWouldYouLikeToPlayWithComputer = (TextView) dialog.findViewById(R.id.mfAlertMsgWouldYouLikeToPlayWithComputer);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOkForSingleFriendzy);
		Button btnTahnks = (Button) dialog.findViewById(R.id.btnNoThanks);

		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		mfAlertMsgWouldYouLikeToPlayWithComputer.setText(transeletion.getTranselationTextByTextIdentifier("mfAlertMsgWouldYouLikeToPlayWithComputer"));
		btnTahnks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				ChallengerTransferObj challengerObj = new ChallengerTransferObj();

				challengerObj.setFirst("Computer");
				challengerObj.setLast("Player");
				challengerObj.setIsFakePlayer(1);
				challengerObj.setPlayerId("");
				challengerObj.setUserId("");
				challengerObj.setSum("");
				challengerObj.setCountryIso("");

				ChooseChallengerAdapter.challengerDataObj = challengerObj;

				context.startActivity(new Intent(context , SingleFriendzyEquationActivity.class));
			}
		});

		btnTahnks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{				
				dialog.dismiss();
			}
		});
	}

	/**
	 * Generate Internet Warning dialog
	 * @param msg
	 * @param okMsg
	 */
	public void generateInternetWarningDialog(String msg , String okMsg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_warning);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		btnOk.setText(okMsg);
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener( new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				((Activity)context).finish();		
			}
		});
	}

	/**
	 * Genarate School Dialog
	 * @param msg
	 * @param schoolInfo
	 * @param result
	 */
	public void generateSchoolDialog(String msg,final ArrayList<String> schoolInfo,final int result)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialogschoolfromusorcanada);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnSchoolNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnSchoolOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtMsg.setText(msg);

		//btnNoThanks.setOnClickListener(this);
		btnNoThanks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				Intent returnIntent = new Intent();
				returnIntent.putExtra("schoolInfo",schoolInfo);
				((Activity)context).setResult(result,returnIntent);     
				((Activity)context).finish();
			}
		});

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);  
				emailIntent.setType("plain/text");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[]{"info@letsleapahead.com"});
				emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Please add my school.");
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Enter your school's name,city,state,and zip code here...");
				context.startActivity(emailIntent); 

				Intent returnIntent = new Intent();
				returnIntent.putExtra("schoolInfo",schoolInfo);
				((Activity)context).setResult(result,returnIntent);     
				((Activity)context).finish();
				/*Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.android.email");
				context.startActivity(intent);*/
			}
		});
	}


	/**
	 * This dalog open when user play with application and back without saving your data
	 * @param msg
	 */
	public void generatetimeSpentDialog()
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_time_spent_on_this_application);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnNoThanks);
		Button btnYes 		= (Button)dialog.findViewById(R.id.btnYes);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtTimeSpentMessage);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnYes.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes"));
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgWouldYouLikeToRecordYourTimeSpentOnThisApp"));
		transeletion.closeConnection();

		btnNoThanks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
			}
		});

		btnYes.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				transeletion.openConnection();
				String msg = transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin");
				transeletion.closeConnection();
				//generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
				generateRegisterOrLogInDialog(msg);
			}
		});
	}

	/**
	 * This dalog open when user play with application and back without saving your data
	 * @param msg
	 */
	public void generateDialogUserEnterGraterCoinsThenAvalableCoins(String msg , final int userCoins)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_enter_transfercoins_greater_available_coins);
		dialog.show();

		Button btnGetMoreCoins 	= (Button)dialog.findViewById(R.id.btnNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnYes);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtTimeSpentMessage);

		transeletion.openConnection();
		btnGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		//txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgSorryYouDoNotHaveThatManyCoins"));
		txtMsg.setText(msg);
		transeletion.closeConnection();

		btnGetMoreCoins.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				context.startActivity(new Intent(context , GetMoreCoins.class));//chANGES
			}
		});

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();

				if(userCoins == 0)
					context.startActivity(new Intent(context , MainActivity.class));
			}
		});
	}


	/**
	 * Dialog when user click on proceed button , when user coins is less then the required coins
	 * @param msg
	 */
	public void generateDialogWhenUserClickOnProceedButtonForCoins(int earnCoins , int requiredCoins, final String userCoins)
	{ 
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_click_on_proceed_for_coins);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnYes);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtYouneedMoreCoins);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouNeedMoreCoinsForThatItem"));
		transeletion.closeConnection();

		btnNoThanks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
			}
		});

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				if(userCoins.equals("0"))
					context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
				else
				{
					Intent intent = new Intent(context, CoinsDistributionActivity.class);
					context.startActivity(intent);
				}
			}
		});
	}


	/**
	 * Generate Delete confirmation dialog
	 * @param msg
	 */
	public void generateDeleteConfirmationDialog(String msg)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialogdeleteconfirmation);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtMsg.setText(msg);

		btnNoThanks.setOnClickListener(this);
		btnOk.setOnClickListener(this);
	}

	/**
	 * Generate Delete confirmation dialog
	 * @param msg
	 */
	public void generateResignDialogForMultifriendzy(final String friendzyId , final String winnerId)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_resign_for_multifriendzy);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtIfYouResign);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgIfYouResignYouWillLose"));
		transeletion.closeConnection();
		btnNoThanks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();			
			}
		});

		btnOk.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				new ResignAndSaveFriendzy(friendzyId , winnerId , context).execute(null,null,null);
				dialog.dismiss();
			}
		});
	}

	/**
	 * De;ete Confirmation User Player dialog
	 * @param msg
	 * @param playerId
	 * @param userId
	 * @param callingActivity
	 */
	public void generateDeleteConfirmationUserPlayerDialog(String msg,String playerId,String userId,String callingActivity)
	{
		this.playerId = playerId;
		this.userId   = userId;
		this.callingActivity = callingActivity;

		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.deleteuserplayerdialog);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelUserPlayerOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtMsg.setText(msg);

		btnNoThanks.setOnClickListener(this);
		btnOk.setOnClickListener(this);
	}


	/**
	 * Generate email existence dialog
	 * @param msg
	 */
	public void generateDialogEmailExistDialog(String msg)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialogalreadyemailexist);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnemailAlreadyExistThanks);
		Button btnLogin 		= (Button)dialog.findViewById(R.id.btnLogin);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtemailAlreadyExist);

		transeletion.openConnection();
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
		txtMsg.setText(msg);
		transeletion.closeConnection();

		btnNoThanks.setOnClickListener(this);
		btnLogin.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(context,LoginActivity.class);
				intent.putExtra("callingActivity", "RegistrationStep1");
				context.startActivity(intent);
			}
		});
	}



	/**
	 * Generate Forgate password dialog 
	 * 
	 */
	public void generateDialogForgatePassword()
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialogforgatepasswordlayout);
		dialog.show();

		TextView txtMsg 	= (TextView)dialog.findViewById(R.id.txtAlertMsgForgatePass);
		TextView txtEmail 	= (TextView) dialog.findViewById(R.id.txtEmail);
		final EditText edtEmail 	= (EditText) dialog.findViewById(R.id.edtEnterEmail);
		Button btnOk      	= (Button) dialog.findViewById(R.id.dialogBtnOk);

		transeletion.openConnection();
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgEnterYourEmailUsedDuringRegistrationSoWeCanSendYouYourPassword"));
		txtEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + ":");
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();

		btnOk.setOnClickListener( new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if(edtEmail.getText().toString().equals(""))
				{
					dialog.dismiss();
				}
				else
				{
					if(CommonUtils.isEmailValid(edtEmail.getText().toString()))
					{
						if(CommonUtils.isInternetConnectionAvailable(context))
						{
							new ForgetPass(edtEmail.getText().toString()).execute(null,null,null);
						}
						else
						{		
							transeletion.openConnection();
							generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();
						}
					}
					else
					{
						dialog.dismiss();
					}
				}
			}
		});
	}

	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateDateWarningDialog(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_warning);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener( new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				((Activity)context).finish();		
			}
		});
	}


	/**
	 * This dialog open when user get coins from server
	 * @param currentCoins
	 * @param requiredCoins
	 */
	public void generateLevelWarningDialogForLearnignCenter(final int currentCoins,CoinsFromServerObj coinsFromServer,
			final int itemId,String msg){	

		boolean isTrue = false;
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			isTrue= true;
		}

		final boolean isTemp = isTrue;

		final int requiredCoins 	= coinsFromServer.getCoinsRequired();
		final int monthlyCoins		= coinsFromServer.getMonthlyCoins();//Change them in string format like 30,000
		final int yearlyCoins		= coinsFromServer.getYearlyCoins();
		String userCoins			= coinsFromServer.getCoinsPurchase()+"";

		dialog = new Dialog(context, R.style.CustomDialogTheme);
		if(isTemp)
		{
			userCoins = "0";
			dialog.setContentView(R.layout.dialog_for_lock_level_for_temp_player);
		}
		else
		{
			dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center);
		}

		final String coins = userCoins;

		dialog.show();

		TextView txtUnlock 			= (TextView) dialog.findViewById(R.id.txtUlockAllRemaining);
		TextView txtCurrentCoins 	= (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
		Button btnAssign 			= (Button) dialog.findViewById(R.id.btnNoThanks);
		Button btnProceeds			= (Button) dialog.findViewById(R.id.btnProceeds);
		Button btnPurchaseCoins 		= (Button) dialog.findViewById(R.id.btnGetMoreCoins);

		transeletion.openConnection();
		DateTimeOperation numberformat = new DateTimeOperation();
		String reqcoins = numberformat.setNumberString(requiredCoins + "");
		String currCoins= numberformat.setNumberString(currentCoins + "");

		//Changes For Subscription
		if(!isTemp)
		{
			String msgForUserCoins	= "";			
			if(userCoins.equals("0"))
			{
				btnAssign.setVisibility(View.GONE);
				btnProceeds.setVisibility(View.GONE);
			}				
			else
			{
				msgForUserCoins = "\n\n"+transeletion.getTranselationTextByTextIdentifier("lblYouAlsoHave") + " " +
						CommonUtils.setNumberString(coinsFromServer.getCoinsPurchase() + " ") 
						+ " "+ transeletion.getTranselationTextByTextIdentifier("lblUnassignedCoins");
			}
			if(currentCoins >= requiredCoins)
			{
				btnProceeds.setVisibility(View.VISIBLE);
			}
			TextView txtSubscription	= (TextView) dialog.findViewById(R.id.txtSubscription);
			Button	 btnCancel			= (Button) dialog.findViewById(R.id.btncancel);
			Button	 btnGoForYear		= (Button) dialog.findViewById(R.id.btnGoForYear);
			Button	 btnGoForMonth		= (Button) dialog.findViewById(R.id.btnGoForMonth);

			TextView txtMonthly			= (TextView) dialog.findViewById(R.id.txtMonthly);
			TextView txtYearly			= (TextView) dialog.findViewById(R.id.txtYearly);

			if(MainActivity.isTab)
			{
				btnCancel.setBackgroundResource(R.drawable.cross_ipad);
			}

			btnCancel.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					dialog.dismiss();			
				}
			});

			btnGoForMonth.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
					goForSubscription(true, currentCoins, monthlyCoins, coins, 1);
				}
			});
			btnGoForYear.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
					goForSubscription(false, currentCoins, yearlyCoins, coins, 12);
				}
			});			

			if(msg == null){
				txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
						reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
			}
			else{

				txtUnlock.setText(msg);
			}

			txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " +
					currentCoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins")+
					" assigned to your account."	+ msgForUserCoins);

			btnAssign.setText("Assign Coins");
			btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
			btnPurchaseCoins.setText("Purchase Coins");
			btnGoForMonth.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
			btnGoForYear.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
			txtMonthly.setText("1 Month = "+CommonUtils.setNumberString(monthlyCoins +"")+" coins");
			txtYearly.setText("1 Year = "+CommonUtils.setNumberString(yearlyCoins +"")+" coins");
			//txtSubscription.setText(transeletion.getTranselationTextByTextIdentifier("lblOrYouCanPurchaseSchoolSubscription"));
			txtSubscription.setText(transeletion.getTranselationTextByTextIdentifier("lblYouCanPurchaseSubscriptionToUnlock"));
		}
		else
		{
			txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
					reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
			txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave") + " " +
					currCoins + " " + transeletion.getTranselationTextByTextIdentifier("alertMsgPointsInYourAccount"));

			btnAssign.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
			btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
			btnPurchaseCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
		}

		transeletion.closeConnection();		

		btnAssign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();
				if(!isTemp)
				{
					getUserDataFromServer();
					/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
					context.startActivity(intent);*/
				}
			}
		});

		btnProceeds.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();

				if(currentCoins < requiredCoins)
				{
					generateDialogForNeedCoinsForUlock(currentCoins , requiredCoins, coins, "");
				}
				else
				{				
					SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
					String userId   = sharedPreffPlayerInfo.getString("userId", "") ; 
					String playerId = sharedPreffPlayerInfo.getString("playerId", "");

					if(userId.equals("0") && playerId.equals("0"))
					{
						int playerCoins = currentCoins - requiredCoins ;

						ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

						PurchaseItemObj itemObj = new PurchaseItemObj();
						itemObj.setUserId(userId);
						itemObj.setItemId(itemId);
						itemObj.setStatus(1);
						purchseItems.add(itemObj);

						LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
						lrarningImpl.openConn();
						lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
						lrarningImpl.insertIntoPurchaseItem(purchseItems);
						lrarningImpl.closeConn();

						Intent intent = new Intent(context,context.getClass());
						context.startActivity(intent);
					}
					else
					{
						UserRegistrationOperation userObj = new UserRegistrationOperation(context);
						RegistereUserDto regUserObj = userObj.getUserData();

						int userCoins  = 0 ;
						int playerCoins = currentCoins - requiredCoins ;

						if(regUserObj.getCoins().length() > 0)
							userCoins = Integer.parseInt(regUserObj.getCoins());

						if(CommonUtils.isInternetConnectionAvailable(context))
						{
							new SaveCoinsAndItemStatus(itemId , userId , userCoins , playerId , playerCoins , requiredCoins).execute(null , null,null);
						}
						else
						{
							DialogGenerator dg = new DialogGenerator(context);
							Translation transeletion = new Translation(context);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();
						}
					}
				}
				/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
				context.startActivity(intent);*/
			}
		});

		btnPurchaseCoins.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
			}
		});

	}

	private void goForSubscription(boolean isMonth, int currentCoins, int requiredCoins, String userCoins, int duration)
	{
		if(currentCoins < requiredCoins)
		{
			generateDialogForNeedCoinsForUlock(currentCoins , requiredCoins, userCoins, "all");
		}
		else
		{
			if(CommonUtils.isInternetConnectionAvailable(context))
			{
				new SaveSubscriptionUser(context, (currentCoins-requiredCoins), duration).execute(null,null,null);
			}
			else
			{
				CommonUtils.showInternetDialog(context);
			}
		}
	}


	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateSubsriptionDialog(String msg) 
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_friendzy);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Intent intent = new Intent(context, context.getClass());
				context.startActivity(intent);

			}
		});
	}


	/**
	 * Generate Warning Dialog
	 * @param msg
	 */
	public void generateDialogForNeedCoinsForUlock(int earnCoins,int requiredCoins,
			String userCoins, String all) {

		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_need_coins_for_unlock);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		String textAlert = null;
		if(all.equals("all"))
			textAlert = transeletion.getTranselationTextByTextIdentifier("lblCoinsToUnlockAllFeaturesOfApp");
		else{
			textAlert = transeletion.getTranselationTextByTextIdentifier("lblCoins")+" " 
					+transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockThisCategory");
		}

		txtAlertMsgRegisterLogIn.setText(transeletion.getTranselationTextByTextIdentifier("lblYouNeed")
				+ " " + CommonUtils.setNumberString((requiredCoins - earnCoins)+"") + " "
				+ textAlert);
		transeletion.closeConnection();

		btnOk.setOnClickListener( new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();

				getUserDataFromServer();

			}
		});
	}	


	/**
	 * This method get data from server 
	 */
	private void getUserDataFromServer()
	{
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(context);
			dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
			transeletion.closeConnection();	
		}
		else
		{	
			if(CommonUtils.isInternetConnectionAvailable(context))
			{
				UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
				RegistereUserDto regUserObj = userOprObj.getUserData();

				if(regUserObj.getEmail().length() > 1)
				{
					new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
				}
				else
				{
					new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
				}
			}
			else
			{
				UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
				RegistereUserDto regUserObj = userOprObj.getUserData();
				if(regUserObj.getCoins().equals("0"))
					context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
				else
				{
					Intent intent = new Intent(context, CoinsDistributionActivity.class);
					context.startActivity(intent);
				}
			}
		}
	}


	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnAlertBtnTitleNoThanks:
			dialog.dismiss();
			break;
		case R.id.dialogBtnOk:
			dialog.dismiss();
			break;

		case R.id.btnAlertBtnTitleRegister:
			dialog.dismiss();
			context.startActivity(new Intent(context, RegistrationStep1.class));
			break;

		case R.id.btnAlertBtnTitleLogIn:
			dialog.dismiss();
			Intent intent = new Intent(context, LoginActivity.class);
			intent.putExtra("callingActivity", ((Activity)context).getClass().getSimpleName());
			((Activity)context).startActivity(intent);
			break;
		case R.id.btnSchoolNoThanks:
			dialog.dismiss();

			break;
		case R.id.btnemailAlreadyExistThanks:
			dialog.dismiss();
			break;
		case R.id.btnLogin:
			/*Intent intentLogin = new Intent(context,LoginActivity.class);
			context.startActivity(intentLogin);*/
			break;
		case R.id.btnDeletelOk:
			TempPlayerOperation tempObj = new TempPlayerOperation(context);
			tempObj.deleteFromTempPlayer();
			tempObj.closeConn();

			LearningCenterimpl learningCenterImpl = new LearningCenterimpl(context);
			learningCenterImpl.openConn();
			learningCenterImpl.deleteFromMathResult();
			learningCenterImpl.deleteFromPlayerEruationLevel();
			learningCenterImpl.deleteFromPlayerTotalPoints();
			learningCenterImpl.closeConn();

			SharedPreferences sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
			SharedPreferences.Editor editor = sheredPreference.edit();
			editor.clear();
			editor.commit();
			Intent intentCreate = new Intent(context,MainActivity.class);
			context.startActivity(intentCreate);
			break;
		case R.id.btnDeleteNoThanks:
			dialog.dismiss();
			break;
		case R.id.btnDeletelUserPlayerOk:
			dialog.dismiss();
			this.deleteUserPlayer();
			/*UserPlayerOperation userOprObj = new UserPlayerOperation(context);	
			userOprObj.deleteFromUserPlayerByPlayerId(playerId);
			userOprObj.closeConn();
			Intent intentCreateUserPlayer = new Intent(context,MainActivity.class);
			context.startActivity(intentCreateUserPlayer);*/
			break;

		case R.id.btnGoForMonth:
		case R.id.btnGoForYear:
			break;
		}
	}


	/**
	 * Generate Dialog for cannot find my Teacher
	 * @param msg
	 * @param teacherobj
	 * @param result
	 */
	public void canNotFindTeacherDialog(String msg , final TeacherDTO teacherobj , final int result)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_warning);
		dialog.show();

		TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
		Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();
		txtAlertMsgRegisterLogIn.setText(msg);
		btnOk.setOnClickListener( new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				ArrayList<String> teacherIdAndName = new ArrayList<String>();
				teacherIdAndName.add(teacherobj.getfName());//add teacher fname at 0th position
				teacherIdAndName.add(teacherobj.getlName());//add teacher lname at 1st position
				teacherIdAndName.add(teacherobj.getTeacherUserId());//add teacher id at 2nd position

				Intent returnIntent = new Intent();
				returnIntent.putExtra("schoolInfo",teacherIdAndName);
				((Activity)context).setResult(result,returnIntent);     
				((Activity)context).finish();
			}
		});
	}

	/**
	 * This method delete user player 
	 */
	private void deleteUserPlayer() 
	{	
		if(CommonUtils.isInternetConnectionAvailable(context))
			new DeletePlayerAsynkTask().execute(null,null,null);
		else
		{
			DialogGenerator dg = new DialogGenerator(context);
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**
	 * AsynckTask For delete user player from server
	 * @author Yashwant Singh
	 *
	 */
	class DeletePlayerAsynkTask extends AsyncTask<Void, Void, Void>
	{
		String resultValue = null;
		ProgressDialog progressDialog = CommonUtils.getProgressDialog(context);

		@Override
		protected void onPreExecute() 
		{
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register registerObj = new Register(context);
			resultValue     	 = registerObj.deleteRegisteredUser(userId, playerId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			progressDialog.cancel();

			if(resultValue.equals("success"))
			{
				int noOfUserPlayereRemaining = 0;

				UserPlayerOperation userOprObj = new UserPlayerOperation(context);	
				userOprObj.deleteFromUserPlayerByPlayerId(playerId);
				noOfUserPlayereRemaining = userOprObj.getUserPlayerData().size();
				userOprObj.closeConn();

				SharedPreferences sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
				SharedPreferences.Editor editor = sheredPreference.edit();
				editor.clear();
				editor.commit();

				if(noOfUserPlayereRemaining > 0)
				{
					if(callingActivity.equals("LoginUserPlayerActivity"))
					{
						Intent intentCreateUserPlayer = new Intent(context,LoginUserPlayerActivity.class);
						context.startActivity(intentCreateUserPlayer);
					}
					else if(callingActivity.equals("ModifyRegistration") || callingActivity.equals("RegisterationStep2"))
					{
						Intent intent = new Intent(context,ModifyRegistration.class);
						context.startActivity(intent);
					}
					else if(callingActivity.equals("TeacherPlayer"))
					{
						Intent intent = new Intent(context,TeacherPlayer.class);
						context.startActivity(intent);
					}
				}
				else
				{
					Intent intentCreateUserPlayer = new Intent(context,LoginUserCreatePlayer.class);
					context.startActivity(intentCreateUserPlayer);
				}
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(context);
				dg.generateWarningDialog("Proble In Deletion");
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * This aynckTask send the pass. to user email id
	 * @author Yashwant Singh
	 *
	 */
	class ForgetPass extends AsyncTask<Void, Void, Void>
	{
		private String email 		= null;
		private String resultValue  = null;

		ForgetPass(String email)
		{
			this.email = email;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(context);
			resultValue = login.forgetPassword(email);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			dialog.dismiss();
			DialogGenerator dg = new DialogGenerator(context);
			Translation transeletion = new Translation(context);
			transeletion.openConnection();

			if(resultValue.equals("1"))
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYourPasswordHasBeenSentToYourEmail"));
			}
			else if(resultValue.equals("-9001"))
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailUsernameNotInSystem"));
			}

			transeletion.closeConnection();
		}
	}

	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByEmail extends AsyncTask<Void, Void, Void>
	{
		private ProgressDialog pd 	= null;
		private String email 		= null;
		private String pass 		= null;

		GetUserDetailByEmail(String email, String pass)
		{
			this.email = email;
			this.pass  = pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(context);
			login.getUserDetailByEmail(email, pass);

			return null;

		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
			RegistereUserDto regUserObj = userOprObj.getUserData();
			if(regUserObj.getCoins().equals("0"))
				context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
			else
			{
				Intent intent = new Intent(context, CoinsDistributionActivity.class);
				context.startActivity(intent);
			}

			super.onPostExecute(result);
		}
	}


	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByUserId extends AsyncTask<Void, Void, Void>
	{
		private String userId 	= null;
		//private String pass     	= null;
		private ProgressDialog pd 	= null;

		GetUserDetailByUserId(String userId , String pass)
		{
			this.userId = userId;
			//this.pass     =  pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(context);
			login.getUserDetailByUserId(userId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
			RegistereUserDto regUserObj = userOprObj.getUserData();
			if(regUserObj.getCoins().equals("0"))
				context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
			else
			{
				Intent intent = new Intent(context, CoinsDistributionActivity.class);
				context.startActivity(intent);
			}

			super.onPostExecute(result);
		}
	}


	/**
	 * This class save itm status on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveCoinsAndItemStatus extends AsyncTask<Void, Void, Void>
	{
		private int itemId 		= 0;
		private String userId 	= null;
		private int userCoins 	= 0;
		private String playerId = null;
		private int playerCoins = 0;

		private ProgressDialog pd = null;

		SaveCoinsAndItemStatus(int itemId , String userId , int userCoins , String playerId , int playerCoins ,int requiredCoins)
		{
			this.itemId 	= itemId;
			this.userId 	= userId;
			this.userCoins  = userCoins;
			this.playerId 	= playerId;
			this.playerCoins = playerCoins;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			CoinsDistributionServerOperation coinsOperation = new CoinsDistributionServerOperation();
			coinsOperation.saveCoinsAndItemStatus(itemId, userId, userCoins, playerId, playerCoins);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

			PurchaseItemObj itemObj = new PurchaseItemObj();
			itemObj.setUserId(userId);
			itemObj.setItemId(itemId);
			itemObj.setStatus(1);
			purchseItems.add(itemObj);

			LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
			lrarningImpl.openConn();
			lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
			lrarningImpl.insertIntoPurchaseItem(purchseItems);
			lrarningImpl.closeConn();


			Intent intent = new Intent(context,context.getClass());
			context.startActivity(intent);

			super.onPostExecute(result);
		}
	}

	/**
	 * This method save and resign friendzy
	 * @author Yashwant Singh
	 *
	 */
	class ResignAndSaveFriendzy extends AsyncTask<Void, Void, Void>
	{
		private String friendzyId;
		private String winnerId;
		private Context context;

		ResignAndSaveFriendzy(String friendzyId , String winnerId , Context context)
		{
			this.friendzyId = friendzyId ; 
			this.winnerId   = winnerId;
			this.context 	= context;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			serverObj.resignAndSaveFriendzy(friendzyId, winnerId);
			return null;
		}


		@Override
		protected void onPostExecute(Void result) 
		{
			context.startActivity(new Intent(context , MultiFriendzyMain.class));
			super.onPostExecute(result);
		}

	}

	//Change by Shilpi Date April2014......
	/**
	 * This dailog open when user click to play with school curriculum for 
	 * learning center or for the assessment test and also for friendzy chaalange
	 */
	public void generateDialogForFriendzyUnlock(final int currentCoins, final CoinsFromServerObj coinsFromServer){

		//final int currentCoins		= coinsFromServer.getCoinsEarned();
		final int monthlyCoins		= coinsFromServer.getMonthlyCoins();
		final int yearlyCoins		= coinsFromServer.getYearlyCoins();
		String userCoins			= coinsFromServer.getCoinsPurchase()+"";
		final String coins = userCoins;

		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center_school_curriculum);
		dialog.show();

		TextView txtScoolCurriculumTop = (TextView) dialog.findViewById(R.id.txtScoolCurriculumTop);
		TextView txtIncludeTheFollowing = (TextView) dialog.findViewById(R.id.txtIncludeTheFollowing);
		TextView txtExpire = (TextView) dialog.findViewById(R.id.txtExpire);
		//TextView txtB = (TextView) dialog.findViewById(R.id.txtB);
		//TextView txtC = (TextView) dialog.findViewById(R.id.txtC);
		TextView txtAssignCoinsYouHave = (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
		TextView txtUnAssignCoinsYouHave = (TextView) dialog.findViewById(R.id.txtUserCoins);

		Button btnAssignCons = (Button) dialog.findViewById(R.id.btnAssignCons);
		Button btnPurchaseCoins = (Button) dialog.findViewById(R.id.btnPurchaseCoins);

		TextView txtMonth = (TextView) dialog.findViewById(R.id.txtMonth);
		TextView txtYear = (TextView) dialog.findViewById(R.id.txtYear);

		Button btnGoForMonth = (Button) dialog.findViewById(R.id.btnGoForMonth);
		Button btnGoForYear = (Button) dialog.findViewById(R.id.btnGoForYear);

		//TextView txtSpecialPrice = (TextView) dialog.findViewById(R.id.txtSpecialPrice);

		Button btncancel = (Button) dialog.findViewById(R.id.btncancel);



		//set visibility for view
		if(!(userCoins.equals("0") && userCoins.equals("")) ){
			txtUnAssignCoinsYouHave.setVisibility(TextView.VISIBLE);
			btnAssignCons.setVisibility(Button.VISIBLE);
		}

		transeletion.openConnection();
		//lblAssignCoins, lblPurchaseCoins
		if(coinsFromServer.getAppStatus() == 0)
			txtExpire.setText(transeletion.getTranselationTextByTextIdentifier("lblYourSubscriptionExpired"));
		else
			txtExpire.setVisibility(View.GONE);
		
		
		
		btnAssignCons.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignCoins"));
		btnPurchaseCoins.setText(transeletion.getTranselationTextByTextIdentifier("lblPurchaseCoins"));
		//set text value lblSchoolsCurriculum  lblIncludesTheFollowing
		txtIncludeTheFollowing.setText(transeletion.getTranselationTextByTextIdentifier("lblYouCanPurchaseSubscriptionToUnlock"));
		//txtIncludeTheFollowing.setText(transeletion.getTranselationTextByTextIdentifier("lblIncludesTheFollowing"));
		txtScoolCurriculumTop.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));
		//txtSpecialPrice.setText(transeletion.getTranselationTextByTextIdentifier("lblWeHaveSpecialPricesForSchools"));


		txtAssignCoinsYouHave.setText(transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " +
				CommonUtils.setNumberString(currentCoins+"") + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins")+
				" assigned to your account.");

		txtUnAssignCoinsYouHave.setText(transeletion.getTranselationTextByTextIdentifier("lblYouAlsoHave") + " " +
				CommonUtils.setNumberString(coinsFromServer.getCoinsPurchase() + " ")+ " " 
				+ transeletion.getTranselationTextByTextIdentifier("lblUnassignedCoins"));

		txtMonth.setText("1 Month = " + CommonUtils.setNumberString(coinsFromServer.getMonthlyCoins() + "") 
				+ " Coins");
		txtYear.setText("1 Year = " + CommonUtils.setNumberString(coinsFromServer.getYearlyCoins() + "") 
				+ " Coins");

		/*txtA.setText("a. "+transeletion.getTranselationTextByTextIdentifier("lblCompleteMathCurriculumFor"));
		txtB.setText("b. "+transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
		txtC.setText("c. "+transeletion.getTranselationTextByTextIdentifier("lblSchoolChallenge"));*/

		transeletion.closeConnection();

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		btnAssignCons.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				getUserDataFromServer();
				//context.startActivity(new Intent(context, CoinsDistributionActivity.class));  
			}
		});

		btnPurchaseCoins.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				context.startActivity(new Intent(context , GetMoreCoins.class));    
			}
		});

		btnGoForMonth.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				goForSubscription(true, currentCoins, monthlyCoins, coins, 1);   
			}
		});

		btnGoForYear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				goForSubscription(false, currentCoins, yearlyCoins, coins, 12);   
			}
		});
	}//End generateDailogForSchoolCurriculumUnlock
	

	/**
	 * This dailog open when user click to play with school curriculum for 
	 * learning center or for the assessment test and also for friendzy chaalange
	 */
	public void generateDailogAfterRegistration(String msg){
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_for_confirmation_email_after_registration);
		dialog.show();

		TextView txtLastStepOfRegistration = (TextView) dialog.findViewById(R.id.txtLastStepOfRegistration);
		Button btncancel = (Button) dialog.findViewById(R.id.btncancel);

		txtLastStepOfRegistration.setText(msg);

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}

	
	//-----------------------------------New Change Jan 2016-------------//
	/**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateCommonWarningDialog(String msg , final HttpServerRequest request){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.common_warning_dialog);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);

        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                request.onRequestComplete();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                request.onRequestComplete();
            }
        });
    }
    
    /**
     * Generate Register or login Dialog
     * @param msg
     */
    public void generateRegisterOrLogInDialog(String msg , final LoginRegisterPopUpListener listener)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_register_login);
        dialog.show();

        Button btnNoThanks = (Button)dialog.findViewById(R.id.btnAlertBtnTitleNoThanks);
        Button btnRegister = (Button)dialog.findViewById(R.id.btnAlertBtnTitleRegister);
        Button btnLogin    = (Button)dialog.findViewById(R.id.btnAlertBtnTitleLogIn);
        TextView txtRegisterLogIn = (TextView)dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        
        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnRegister.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnTitleRegister"));
        btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
        txtRegisterLogIn.setText(msg);
        transeletion.closeConnection();     

        btnNoThanks.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onNoThanks();
            }
        });

        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onRegister();
            }
        });

        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onLogin();
            }
        });
    }
}
