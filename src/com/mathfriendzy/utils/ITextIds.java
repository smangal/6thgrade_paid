package com.mathfriendzy.utils;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

public interface ITextIds
{
	String LBL_RATE 				= "lblRememberToBeKind";
	String BTN_RATE 				= "btnTitleRateUs";
	String LBL_FEEDBACK 			= "lblWeWantToHearFromYouSoWeCanDoOurBest";
	String BTN_FEEDBACK 			= "btnTitleSendUsFeedback";
	String LBL_SHARE 				= "lblGoAheadSpreadTheWorld";
	String BTN_SHARE 				= "btnTitleShareThisApp";
	String LBL_MORE_BOOKS 			= "lblLetsLeapAheadBooksAreHoursOfLearningFun";
	String BTN_MORE_BOOKS 			= "btnTitleViewTheBooks";
	String LBL_COPY_RIGHT 			= "lblCopyrightMessage";
	String LBL_INFO					= "infoTitle";
	String EMAIL_BODY				= "infoEmailMessage";
	String EMAIL_SUB				= "inviteEmailSubject";
	String FEED_SUB					= "mfTitleHomeScreen";
	
	String FEEDBACK_ID				= "info@letsleapahead.com";
	String MORE_BOOKS_URL			= "http://www.letsleapahead.com/books/";
	//String RATE_URL					= "https://play.google.com/store?hl=en";
	String RATE_URL					= "https://play.google.com/store/apps/details?id=com.sixthgradepaid";
	
	String MF_TEACHER				= "mfBtnTitleTeachers";
	String MF_STUDENT				= "mfBtnTitleStudents";
	String MF_SCHOOL				= "mfBtnTitleSchools";
	String MF_TOP100				= "btnTitleTop100";
	String LBL_COUNTDOWN			= "lblCountdownToReset";
	String LBL_POS					= "mfLblPos";
	String LBL_POINTS				= "btnTitlePoints";
	String LBL_STUDENT_NAME			= "mfLblStudentsName";
	String LBL_TEACHER_NAME			= "mfLblTeacherName";
	String LBL_SCHOOL_NAME			= "mfLblSchoolsName";
	//String MF_HOMESCREEN			= "homeTitleFriendzy";
	String MF_RESULT				= "btnTitleResults";
	String LBL_LIFETIME				= "lblLifetimeTotal";
	String LBL_TIME					= "lblTime";
	String LBL_SELECTDATE			= "btnTitleSelectDate";
	String LBL_SCORE				= "mfBtnTitleScore";
	
	String LBL_READING				= "lblGeography";//1
	String LBL_MATH					= "lblGeometry_and_Fractions";//2
	String LBL_LANGUAGE				= "lblLanguage_Arts";//3
	String LBL_VOCABULARY			= "lblMath";//4
	String LBL_MONEY				= "lblMeasurement_and_Data";//5
	String LBL_PHONICS				= "lblReading";//6
	String LBL_MEASUREMENT			= "lblScience";//7
	String LBL_GEOGRAPHY			= "lblVocabulary";//8
	String LBL_SOCIAL				= "lblWorld_Cultures";//9
	String LBL_SCIENCE				= "lblMiscellaneous";//10
	
	String LBL_FIND_INVITE			= "lblChooseYourOpponent";
	String LBL_FIND_FRIEND			= "lblFindPlayers";
	String LBL_INVITE				= "lblInvite";
	String LBL_FRIEND				= "lblFriends";
	String LBL_MESSAGE				= "lblTextMessage";
	String LBL_EMAIL				= "lblInviteFriendsViaEmail";
	String LBL_TOP100				= "btnTitleTop100";
	String LBL_FB					= "lblFacebookFriends";
	String LBL_USER					= "lblPlayerSearch";
	
	String ALERT_LOGIN_REGISTER		= "alertMsgYouMustLoginOrRegisterToViewAndParticipate";
	String ALERT_NO_NUMBER			= "alertMsgNoPhoneNumberAssociated";/*
	 Message to invite Friend
	String MSG_INVITE_1				= "txtDownloadMathFriendzyOnItunes";
	String MSG_INVITE_2				= "lblToVisitiTunesAndDownload";
	String MSG_INVITE_3				= "msgBodyDownloadMathFriendzyOnItunes";
	String MSG_INVITE_4				= "txtAndInviteMeToStartFriendzy";	
	String MSG_INVITE_5				= "txtLikeUsOnOfficialPage";*/
	
	/*String MSG_CLICK				= "lblClickHere";
	String MSG_LIKE					= "lblLike";*/
	String LIKE_URL					= "https://www.facebook.com/friendzyapps";
	
	String EMAIL_SUBJECT			= "lblCheckOut";
	//String EMAIL_SUBJECT			= "inviteEmailSubject";
	String LBL_SEARCH_USER			= "lblEnterYourOpponents";
	String LBL_USERNAME				= "lblUserName";
	String BTN_GO					= "btnTitleGo";
	String LBL_FOUND				= "lblFound";
	String LBL_ROUND				= "lblRound";
	String LBL_PLAYERS				= "btnTitlePlayers";
	
	String LBL_GARDE				= "lblAddPlayerGrade";	
	String LEVEL					= "6th";
	String TOP_100_URL				= COMPLETE_URL+"action=top100";
	
	String LBL_MEMBER				= "lblMembers";
	String URL_ICON_IMG				= "http://api.letsleapahead.com/images/icon_6thGrade.png";
}
