package com.mathfriendzy.model.schools;

public class TeacherDTO 
{
	private String fName = null;
	private String lName = null;
	private String teacherUserId = null;
	
	public String getTeacherUserId() {
		return teacherUserId;
	}
	public void setTeacherUserId(String teacherUserId) {
		this.teacherUserId = teacherUserId;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
}
