package com.mathfriendzy.model.learningcenter.triviaEquation;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;

public class TriviaQuestion
{
	private SQLiteDatabase dbConn 	  	 	 	    = null;
	private Database database               		= null;

	private String TBL_TRIVIA_QUE_OPERATION			= "questions_levels_categories";
	private String TBL_TRIVIA_EQUATION		        = "trivia_questions";

	private String OPERATION_CATEGORY_ID 			= "operations_categories_id";	
	private String LEVEL_ID				            = "levels_id";
	private String EQUATION_ID                      = "equations_id";	

	private String QUESTION_ID                      = "question_id";
	private String QUESTION	                      	= "question";
	private String ANSWER	                      	= "answer";
	private String OPTION1	                      	= "option1";
	private String OPTION2	                      	= "option2";
	private String OPTION3  	                    = "option3";
	private String CATEGORY_ID						= "category_id";



	/**
	 * Constructor
	 * @param context
	 */
	public TriviaQuestion(Context context)
	{
		database = new Database(context);
		database.open();		
	}

	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}

	/**
	 * This method close the database connection
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}


	/**
	 * use to join two tables for taking equation.
	 * @param levelId takes levelId for questionId
	 * @param operationId takes operationId that is category of Equation
	 * @return list of Question with their anwer and options
	 */
	public ArrayList<LearningCenterTransferObj> getQuestionList(int levelId, int operationId)
	{	
		openConn();
		ArrayList<LearningCenterTransferObj> categoryObj = new ArrayList<LearningCenterTransferObj>();
		String query = null;
		if(levelId != 0)
		{
			query = "SELECT q."+EQUATION_ID+", equation."+QUESTION+", equation."+ANSWER
					+", equation."+OPTION1+", equation."+OPTION2	+", equation."+OPTION3 
					+"  from "+TBL_TRIVIA_QUE_OPERATION+" q, "+TBL_TRIVIA_EQUATION+"  equation "
					+"where q."+LEVEL_ID+" = '" +levelId+"' "
					+"AND q."+OPERATION_CATEGORY_ID+" = "+operationId
					+" AND equation."+ QUESTION_ID+" =  q."+ EQUATION_ID
					+" order by random()";
		}
		else
		{
			query = "SELECT q."+EQUATION_ID+", equation."+QUESTION+", equation."+ANSWER
					+", equation."+OPTION1+", equation."+OPTION2	+", equation."+OPTION3 
					+"  from "+TBL_TRIVIA_QUE_OPERATION+" q, "+TBL_TRIVIA_EQUATION+"  equation "
					+"where  q."+OPERATION_CATEGORY_ID+" = "+operationId
					+" AND equation."+ QUESTION_ID+" =  q."+ EQUATION_ID
					+" order by random()";
		}
		
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{	
			LearningCenterTransferObj obj = new LearningCenterTransferObj();
			obj.setQuestion(cursor.getString(cursor.getColumnIndex(QUESTION)));
			obj.setEquationId(cursor.getInt(cursor.getColumnIndex(EQUATION_ID)));
			obj.setAnswer(cursor.getString(cursor.getColumnIndex(ANSWER)));
			obj.setOption1(cursor.getString(cursor.getColumnIndex(OPTION1)));
			obj.setOption2(cursor.getString(cursor.getColumnIndex(OPTION2)));
			obj.setOption3(cursor.getString(cursor.getColumnIndex(OPTION3)));		
			categoryObj.add(obj);
		}

		if(cursor != null)
			cursor.close();
		closeConn();

		return categoryObj;
	}



	/**
	 * use to join two tables for taking equation.
	 * @param levelId takes levelId for questionId
	 * @return list of Question with their anwer and options
	 */
	public ArrayList<LearningCenterTransferObj> getQuestionListForChallenger(int levelId, String userId, String playerId)
	{
		ArrayList<LearningCenterTransferObj> categoryObj = new ArrayList<LearningCenterTransferObj>();
		String query = null;

		if(levelId <= 10)
		{
			query = "SELECT q."+EQUATION_ID+",equation."+CATEGORY_ID +", equation."+QUESTION+", equation."+ANSWER
					+", equation."+OPTION1+", equation."+OPTION2	+", equation."+OPTION3 
					+"  from "+TBL_TRIVIA_QUE_OPERATION+" q, "+TBL_TRIVIA_EQUATION+"  equation "
					+"where q."+LEVEL_ID+" = '" +levelId+"' "							
					+" AND equation."+ QUESTION_ID+" =  q."+ EQUATION_ID
					+" AND q."+EQUATION_ID+" NOT IN (Select QuestionId FROM triviaQuestionsPlayed" +
					" WHERE User_id = '"+userId+"' AND Player_id = '"+playerId+"') order by random()";			


		}
		else
		{
			query = "SELECT q."+EQUATION_ID+",equation."+CATEGORY_ID +", equation."+QUESTION+", equation."+ANSWER
					+", equation."+OPTION1+", equation."+OPTION2	+", equation."+OPTION3 
					+" from "+TBL_TRIVIA_QUE_OPERATION+" q, "+TBL_TRIVIA_EQUATION+"  equation "
					+" where equation."+ QUESTION_ID+" =  q."+ EQUATION_ID 
					+" AND q."+EQUATION_ID+" NOT IN (Select QuestionId FROM triviaQuestionsPlayed" +
					"  WHERE User_id = '"+userId+"' AND Player_id = '"+playerId+"') order by random()";
		}


		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{	
			LearningCenterTransferObj obj = new LearningCenterTransferObj();
			obj.setQuestion(cursor.getString(cursor.getColumnIndex(QUESTION)));
			obj.setEquationId(cursor.getInt(cursor.getColumnIndex(EQUATION_ID)));
			obj.setOperationId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));
			obj.setAnswer(cursor.getString(cursor.getColumnIndex(ANSWER)));
			obj.setOption1(cursor.getString(cursor.getColumnIndex(OPTION1)));
			obj.setOption2(cursor.getString(cursor.getColumnIndex(OPTION2)));
			obj.setOption3(cursor.getString(cursor.getColumnIndex(OPTION3)));		
			categoryObj.add(obj);
		}

		if(cursor != null)
			cursor.close();

		return categoryObj;
	}

	/**
	 * use to select question list where equationId is given.
	 * @param equationId takes questionId
	 * @return list of Question with their anwer and options
	 */
	public ArrayList<LearningCenterTransferObj> getQuestionListForEquationId(ArrayList<Integer> equationId)
	{
		ArrayList<LearningCenterTransferObj> categoryObj = new ArrayList<LearningCenterTransferObj>();
		String query = null;

		for(int i = 0; i < equationId.size(); i++)
		{
			query = "SELECT "+CATEGORY_ID +", "+QUESTION+", "+ANSWER
					+", "+OPTION1+", "+OPTION2	+", "+OPTION3 
					+"  from "+TBL_TRIVIA_EQUATION
					+" where "+ QUESTION_ID+" = " +equationId.get(i)
					+" order by random()";
			
			Cursor cursor = dbConn.rawQuery(query, null);

			while(cursor.moveToNext())
			{	
				LearningCenterTransferObj obj = new LearningCenterTransferObj();
				obj.setQuestion(cursor.getString(cursor.getColumnIndex(QUESTION)));
				obj.setOperationId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));
				obj.setAnswer(cursor.getString(cursor.getColumnIndex(ANSWER)));
				obj.setOption1(cursor.getString(cursor.getColumnIndex(OPTION1)));
				obj.setOption2(cursor.getString(cursor.getColumnIndex(OPTION2)));
				obj.setOption3(cursor.getString(cursor.getColumnIndex(OPTION3)));		

				categoryObj.add(obj);
			}

			if(cursor != null)
				cursor.close();
		}

		return categoryObj;
	}



	public boolean insertIntotriviaQuestionsPlayed(String userId, String playerId, int equId)
	{		
		ContentValues values = new ContentValues();
		values.put("QuestionId", equId);
		values.put("User_id", userId);
		values.put("Player_id", playerId);

		return (dbConn.insert("triviaQuestionsPlayed", null, values) > -1) ? true : false;

	}

	public boolean deleteRecordFromTriviaQuestionsPlayed()
	{
		dbConn.delete("triviaQuestionsPlayed", null, null);

		return false;
	}
}
