package com.mathfriendzy.model.learningcenter;

public class MathResultTransferObj 
{
	private int roundId;
	private String isFirstRound;
	private String gameType;
	private int isWin;
	private String userId;
	private String playerId;
	private int isFakePlayer;
	private String problems;
	//private String isFirstPlay;
	private int totalScore;
	
	private int equationTypeId;
	private int level;
	private int stars;
	private int totalPoints;
	private int coins;
	
	private String challengerId;
	
	public String getChallengerId() {
		return challengerId;
	}
	public void setChallengerId(String challengerId) {
		this.challengerId = challengerId;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public int getEquationTypeId() {
		return equationTypeId;
	}
	public void setEquationTypeId(int equationTypeId) {
		this.equationTypeId = equationTypeId;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public int getTotalPoints() {
		return totalPoints;
	}
	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}
		
	public int getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
	public int getRoundId() {
		return roundId;
	}
	public void setRoundId(int roundId) {
		this.roundId = roundId;
	}
	public String getIsFirstRound() {
		return isFirstRound;
	}
	public void setIsFirstRound(String isFirstRound) {
		this.isFirstRound = isFirstRound;
	}
	public String getGameType() {
		return gameType;
	}
	public void setGameType(String gameType) {
		this.gameType = gameType;
	}
	public int getIsWin() {
		return isWin;
	}
	public void setIsWin(int isWin) {
		this.isWin = isWin;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getIsFakePlayer() {
		return isFakePlayer;
	}
	public void setIsFakePlayer(int isFakePlayer) {
		this.isFakePlayer = isFakePlayer;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	
}
