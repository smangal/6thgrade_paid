package com.mathfriendzy.model.player.temp;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.TEMP_PLAYER_OPERATION_FLAG;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.controller.learningcenter.LearningCenterMain;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.database.Database;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.sixthgradepaid.R;

/**
 * This class for holding the temp player record
 * @author Yashwant Singh
 *
 */
public class TempPlayerOperation 
{
	private final String  TEMP_PLAYER_USERNAME_URL 	= COMPLETE_URL + "action=getTempUserName";
	private final String TEMP_PLAYER_TABLE_NAME 	= "tempplayer";
	private final String TEMP_PLAYER_ID				= "playerid";
	private final String FIRST_NAME					= "firstname";
	private final String LAST_NAME					= "lastname";
	private final String USER_NAME					= "username";
	private final String CITY						= "city";
	private final String COIN						= "coin";
	private final String COMPLETE_LEVEL				= "completelavel";
	private final String GRADE						= "grade";
	private final String PARENT_USER_ID				= "parentUserId";
	private final String POINTS						= "points";
	private final String PROFILE_IMAGE				= "profileImage";
	private final String SCHOOL_ID					= "schoolId";
	private final String SCHOOL_NEME				= "schoolName";
	private final String TEACHER_FIRST_NEME			= "teacherFirstName";
	private final String TEACHER_LAST_NEME			= "teacheLastName";
	private final String TEACHER_USER_ID			= "teacherUserId";
	private final String ZIP_CODE			 		= "zipCode";
	private final String STATE_ID			 		= "stateId";
	private final String COUNTRY_ID			 		= "countryId";
	private final String COUNTRY_NAME				= "countryName";
	private final String STATE_NAME					= "stateName";
	private final String PROFILE_IMAGE_NAME         = "profileImageName";
	
	//private String userName = null;
	
	private SQLiteDatabase dbConn 					= null;
	private Context        context					= null;
	private String openActivityAftertempPlayercreate = null;
	
	/**
	 * Constructor
	 * @param context
	 */
	public TempPlayerOperation(Context context)
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside constructor(");
		this.context = context;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside constructor(");
	}
	
	/**
	 * This method create a default temp player with default value
	 */
	public void createTempPlayer(String openActivityAfter)
	{	
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside createTempPlayer(");
		
		openActivityAftertempPlayercreate = openActivityAfter;		
			
		if(CommonUtils.isInternetConnectionAvailable(context))
		{
			new GetUserName().execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(context);
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside createTempPlayer(");
	}
	
	/**
	 * This method get the user name from server 
	 * @return
	 */
	private String getUserNameFromServer()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside getUserNameFromServer(");
		
		String userName = "";
		String jsonString = CommonUtils.readFromURL(TEMP_PLAYER_USERNAME_URL);
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			userName = jObject.getString("data");
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside getUserNameFromServer(");
		
		return userName;
	}
	
	/**
	 * This method check for player exist or not on the bases of table existence return true if exist otherwise false
	 * @param context
	 * @return
	 */
	public boolean isTemparyPlayerExist()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside isTemparyPlayerExist(");
		
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
	    											new String[] {"table", TEMP_PLAYER_TABLE_NAME});
	    if(cursor.moveToFirst())
	    {
	    	cursor.close();
	    	return true;
	    }
	    else
	    {
	    	cursor.close();
	    	return false;
	    }
	}
	
	/**
	 * This method create tempPlayer table for saving tempplayer information
	 * @param tempPlayer
	 */
	public void createTempPlayerTable(TempPlayer tempPlayer)
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside createTempPlayerTable(");
		
		/*String query = " create table " + TEMP_PLAYER_TABLE_NAME + " (" + TEMP_PLAYER_ID + " int, " + FIRST_NAME + " text , "
						+ LAST_NAME + " text , " + USER_NAME + "  text , " +  CITY + "  text , " + COIN + "  int , " 
						+ COMPLETE_LEVEL + " int , " + GRADE + " int , " + PARENT_USER_ID + " int , " + POINTS + "  int , "
						+ PROFILE_IMAGE + " BLOB , " + SCHOOL_ID + " int , " + SCHOOL_NEME + " text , " + TEACHER_FIRST_NEME 
						+ " text , " + TEACHER_LAST_NEME + " text , " + TEACHER_USER_ID + " int , " + ZIP_CODE + " text," 
						+ STATE_NAME + "  text , " + COUNTRY_NAME + " text )";*/
		String query = " create table " + TEMP_PLAYER_TABLE_NAME + " (" + TEMP_PLAYER_ID + " int, " + FIRST_NAME + " text , "
				+ LAST_NAME + " text , " + USER_NAME + "  text , " +  CITY + "  text , " + COIN + "  int , " 
				+ COMPLETE_LEVEL + " int , " + GRADE + " int , " + PARENT_USER_ID + " int , " + POINTS + "  int , "
				+ PROFILE_IMAGE + " BLOB , " + SCHOOL_ID + " int , " + SCHOOL_NEME + " text , " + TEACHER_FIRST_NEME 
				+ " text , " + TEACHER_LAST_NEME + " text , " + TEACHER_USER_ID + " int , " + ZIP_CODE + " text," 
				+ STATE_NAME + "  text , " + COUNTRY_NAME + " text ," + PROFILE_IMAGE_NAME + " text )";
		
		dbConn.execSQL(query);
		
		this.insertTempPlayer(tempPlayer);
		
		if(dbConn != null)
			dbConn.close();
		
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside createTempPlayerTable(");
	}
	
	
	/**
	 * This method will create a blank temp table
	 */
	public void createTempPlayerTable()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside createTempPlayerTable(");
		
		String query = " create table " + TEMP_PLAYER_TABLE_NAME + " (" + TEMP_PLAYER_ID + " int, " + FIRST_NAME + " text , "
				+ LAST_NAME + " text , " + USER_NAME + "  text , " +  CITY + "  text , " + COIN + "  int , " 
				+ COMPLETE_LEVEL + " int , " + GRADE + " int , " + PARENT_USER_ID + " int , " + POINTS + "  int , "
				+ PROFILE_IMAGE + " BLOB , " + SCHOOL_ID + " int , " + SCHOOL_NEME + " text , " + TEACHER_FIRST_NEME 
				+ " text , " + TEACHER_LAST_NEME + " text , " + TEACHER_USER_ID + " int , " + ZIP_CODE + " text," 
				+ STATE_NAME + "  text , " + COUNTRY_NAME + " text ," + PROFILE_IMAGE_NAME + " text )";
		
		dbConn.execSQL(query);
				
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside createTempPlayerTable(");
	}
	
	/**
	 * This method insert the data into the temp player table
	 * @param tempPlayer
	 * @return
	 */
	public boolean insertTempPlayer(TempPlayer tempPlayer)
	{		
		boolean isInsert = false;
		
		ContentValues contentValues = new ContentValues();
		contentValues.put("playerid", tempPlayer.getPlayerId());
		contentValues.put("firstname", tempPlayer.getFirstName());
		contentValues.put("lastname", tempPlayer.getLastName());
		contentValues.put("username", tempPlayer.getUserName());
		contentValues.put("city", tempPlayer.getCity());
		contentValues.put("coin", tempPlayer.getCoins());
		contentValues.put("completelavel", tempPlayer.getCompeteLevel());
		contentValues.put("grade", tempPlayer.getGrade());
		contentValues.put("parentUserId", tempPlayer.getParentUserId());
		contentValues.put("points", tempPlayer.getPoints());
		contentValues.put("profileImage", tempPlayer.getProfileImage());//changes
		contentValues.put("profileImageName", tempPlayer.getProfileImageName());//changes
		contentValues.put("schoolId", tempPlayer.getSchoolId());
		contentValues.put("schoolName", tempPlayer.getSchoolName());
		contentValues.put("teacherFirstName", tempPlayer.getTeacherFirstName());
		contentValues.put("teacheLastName", tempPlayer.getTeacherLastName());
		contentValues.put("teacherUserId", tempPlayer.getTeacherUserId());
		contentValues.put("zipCode", tempPlayer.getZipCode());
		contentValues.put("stateName", tempPlayer.getState());
		contentValues.put("countryName", tempPlayer.getCountry());
		
		isInsert = dbConn.insert(TEMP_PLAYER_TABLE_NAME, null, contentValues) > 0 ? true : false;
		
		if(dbConn != null)
			dbConn.close();
		return isInsert;
	}
	
	/**
	 * Delate the data from the temp player table
	 */
	public void deleteFromTempPlayer()
	{
		//Log.e("TempPlayerOperation", "Temp Player Deleted");
		dbConn.delete(TEMP_PLAYER_TABLE_NAME , null , null);
	}
	
	
	/**
	 * Check for is temp player deleted or not whenever it was created at first time
	 * @return
	 */
	public boolean isTempPlayerDeleted()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside isTempPlayerDeleted(");
		
		String query = " select * from " + TEMP_PLAYER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		//Log.e("", "Number of temp player "+cursor.getCount());
		if(cursor.moveToFirst())
	    {
	    	cursor.close();
	    	dbConn.close();
	      	return false;
	    }
	    else
	    {
	    	cursor.close();
	    	dbConn.close();
	     	return true;
	    }
	}
	
	/**
	 * This method return the temp player data
	 * @param context
	 * @return
	 */
	public ArrayList<TempPlayer> getTempPlayerData()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside getTempPlayerData(");
				
		ArrayList<TempPlayer> tempPlayeList = new ArrayList<TempPlayer>();
		String query = " select * from " + TEMP_PLAYER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{	
			TempPlayer tempPlayeObj = new TempPlayer();
			
			tempPlayeObj.setCity(cursor.getString(cursor.getColumnIndex("city")));
			tempPlayeObj.setCoins(cursor.getInt(cursor.getColumnIndex("coin")));
			tempPlayeObj.setCompeteLevel(cursor.getInt(cursor.getColumnIndex("completelavel")));
			tempPlayeObj.setFirstName(cursor.getString(cursor.getColumnIndex("firstname")));
			tempPlayeObj.setGrade(cursor.getInt(cursor.getColumnIndex("grade")));
			
			tempPlayeObj.setLastName(cursor.getString(cursor.getColumnIndex("lastname")));
			tempPlayeObj.setParentUserId(cursor.getInt(cursor.getColumnIndex("parentUserId")));
			tempPlayeObj.setPlayerId(cursor.getInt(cursor.getColumnIndex("playerid")));
			tempPlayeObj.setPoints(cursor.getInt(cursor.getColumnIndex("points")));
			tempPlayeObj.setProfileImage(cursor.getBlob(cursor.getColumnIndex("profileImage")));
			
			tempPlayeObj.setSchoolId(cursor.getInt(cursor.getColumnIndex("schoolId")));
			tempPlayeObj.setSchoolName(cursor.getString(cursor.getColumnIndex("schoolName")));
			tempPlayeObj.setTeacherFirstName(cursor.getString(cursor.getColumnIndex("teacherFirstName")));
			tempPlayeObj.setTeacherLastName(cursor.getString(cursor.getColumnIndex("teacheLastName")));
			tempPlayeObj.setTeacherUserId(cursor.getInt(cursor.getColumnIndex("teacherUserId")));
			
			tempPlayeObj.setUserName(cursor.getString(cursor.getColumnIndex("username")));
			tempPlayeObj.setZipCode(cursor.getString(cursor.getColumnIndex("zipCode")));
			tempPlayeObj.setState(cursor.getString(cursor.getColumnIndex("stateName")));
			tempPlayeObj.setCountry(cursor.getString(cursor.getColumnIndex("countryName")));
			
			tempPlayeObj.setProfileImageName(cursor.getString(cursor.getColumnIndex("profileImageName")));//Changes
			
			//Log.e("TempPlayerOperation", "temp level : "+tempPlayeObj.getCompeteLevel());
			
			tempPlayeList.add(tempPlayeObj);
		}
		cursor.close();
		dbConn.close();
		
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside getTempPlayerData(");
		
		return tempPlayeList;
	}
	
	/**
	 * This method close the connection with the database
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
	
	
	/**
	 * This method update the coins of the user
	 * @param coins
	 */
	public void updatePlayerCoins(int coins , String playerId)
	{
		Log.e("TempPlayeroperation", "inside updatePlayerCoins");
		
		String where = "playerid" + " = '" + playerId + "'";
		ContentValues cv = new ContentValues();
		cv.put("coin", coins + "");
				
		dbConn.update(TEMP_PLAYER_TABLE_NAME, cv, where, null);
	}
	
	/**
	 * This method get Default user name from the server when the user use this appliaction at first time
	 * then it will create temp player 
	 * @author Yashwant Singh
	 *
	 */
	class GetUserName extends AsyncTask<Void, Void, String>
	{
		
		private ProgressDialog pd = null;
		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) 
		{			
			pd.cancel();
		
			// TODO Auto-generated method stub
			TempPlayer tempPlayer = new TempPlayer();
			
			tempPlayer.setCity("");
			tempPlayer.setCoins(0);
			tempPlayer.setCompeteLevel(1);
			tempPlayer.setFirstName(MathFriendzyHelper.TEMP_PLAYER_F_NAME);
			tempPlayer.setGrade(1);
			
			tempPlayer.setLastName(MathFriendzyHelper.TEMP_PLAYER_L_NAME);
			tempPlayer.setParentUserId(0);
			tempPlayer.setPlayerId(0);
			tempPlayer.setPoints(0);
			
			tempPlayer.setProfileImageName("Smiley");//changes
			
			Resources res = context.getResources();
			Drawable drawable = res.getDrawable(R.drawable.smiley_ipad);
			Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			byte[] bitMapData = stream.toByteArray();
			tempPlayer.setProfileImage(bitMapData);
						
			tempPlayer.setSchoolId(1);
			tempPlayer.setSchoolName("Home School");
			tempPlayer.setTeacherFirstName("NA");
			tempPlayer.setTeacherLastName("");
			tempPlayer.setTeacherUserId(-2);
				
			tempPlayer.setUserName(result);
			tempPlayer.setZipCode("");
			tempPlayer.setState("");
			tempPlayer.setCountry("United States");
			createTempPlayerTable(tempPlayer);//create tempplayer table for save temp player information
			
			if(openActivityAftertempPlayercreate.equals("LearningCenterMain") || 
					openActivityAftertempPlayercreate.equals("SingleFriendzyMain")
					||openActivityAftertempPlayercreate.equals("MultiFriendzyMain"))//changes for single friendzy
			{
				Intent intent = null;
				SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
				SharedPreferences.Editor editor =        sharedPrefPlayerInfo.edit();
				
				TempPlayerOperation tempPlayer1 = new TempPlayerOperation(context);
				ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();
				
				if(openActivityAftertempPlayercreate.equals("LearningCenterMain"))
					intent  = new Intent(context,LearningCenterMain.class);
				else if(openActivityAftertempPlayercreate.equals("SingleFriendzyMain"))
					intent  = new Intent(context,SingleFriendzyMain.class);
				else
					intent  = new Intent(context,MultiFriendzyMain.class);
				
				editor.clear();
				editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
				editor.putString("city", tempPlayerData.get(0).getCity());
				editor.putString("state", tempPlayerData.get(0).getState());
				editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
				editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
				editor.putInt("grade", tempPlayerData.get(0).getGrade());
				
				//for score update
				editor.putString("userId",  "0");
				editor.putString("playerId",  "0");
				
				editor.putString("countryName",tempPlayerData.get(0).getCountry());
				editor.putInt("completeLevel", tempPlayerData.get(0).getCompeteLevel());
				//Log.e("TempPlayerOperation", "temp level : "+tempPlayerData.get(0).getCompeteLevel());
				
				editor.commit();	
				
				context.startActivity(intent);
			}
			else
			{
				Intent intent  = new Intent(context,PlayersActivity.class);
				context.startActivity(intent);
			}
			
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(Void... params) 
		{
			Log.e("", "inside do in background()");
			
			String userName = getUserNameFromServer();
			return userName;
		}
	}
	
	
	/**
	 * This method return the User Data
	 * @return
	 *//*
	public RegistereUserDto getUserData()
	{
		RegistereUserDto regUserObj = new RegistereUserDto();
		String query = " select * from " + TEMP_PLAYER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
				
		if(cursor.moveToNext())
		{
			regUserObj.setUserId(cursor.getString(cursor.getColumnIndex(TEMP_PLAYER_ID)));
			regUserObj.setFirstName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
			regUserObj.setLastName(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
			regUserObj.setSchoolId(cursor.getString(cursor.getColumnIndex(SCHOOL_ID)));;
			regUserObj.setCountryId(cursor.getString(cursor.getColumnIndex(COUNTRY_ID)));
			regUserObj.setStateId(cursor.getString(cursor.getColumnIndex(STATE_ID)));
			regUserObj.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
			regUserObj.setCoins(cursor.getString(cursor.getColumnIndex(COIN)));
		}
		
		if(cursor != null)
			cursor.close();
		if(dbConn != null)
			dbConn.close();
		
		return regUserObj;
	}*/
}
