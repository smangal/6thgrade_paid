package com.mathfriendzy.model.friendzy;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.mathfriendzy.utils.FriendzyUtils;

public class SaveTimePointsForFriendzyChallenge extends AsyncTask<Void, Void, Void>
{
	int time;
	int points;
	Context context;
	SharedPreferences sharedPreffPlayerInfo;

	SharedPreferences sharedPreffriendzy;
	String playerId	;
	String userId	;
	String freindPlayer;
	String friendUser;


	public SaveTimePointsForFriendzyChallenge(int time, int points, Context context)
	{
		this.points = points;
		this.time	= time;
		this.context	= context;

		sharedPreffPlayerInfo	 	= context.getSharedPreferences(IS_CHECKED_PREFF, 0);/*
		sharedPreffriendzy	 		= context.getSharedPreferences(ITextIds.FRIENDZY, 0);*/

		playerId					= sharedPreffPlayerInfo.getString("playerId", "");
		userId						= sharedPreffPlayerInfo.getString("userId", "");/*

		freindPlayer				= sharedPreffriendzy.getString("playerId", "");
		friendUser					= sharedPreffriendzy.getString("userId", "");		*/
	}

	@Override
	protected Void doInBackground(Void... params) 
	{
		/*if(playerId.equals(freindPlayer) && userId.equals(friendUser))
		{
			FriendzyServerOperation frndzy = new FriendzyServerOperation();
			frndzy.saveTimeForFriendzy(sharedPreffriendzy.getString("userId", ""), sharedPreffriendzy.getString("playerId", ""),
					sharedPreffriendzy.getString("challengerId", ""), time, points);
		}*/
		if(FriendzyUtils.isActivePlayer(userId, playerId))
		{
			FriendzyServerOperation frndzy = new FriendzyServerOperation();
			frndzy.saveTimeForFriendzy(userId, playerId,
					FriendzyUtils.getActivePlayerChallengerId(userId , playerId), time, points);
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) 
	{
		//pd.cancel();
		super.onPostExecute(result);
	}


}
