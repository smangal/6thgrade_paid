package com.mathfriendzy.model.multifriendzy;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;

public class MultiFriendzyImpl 
{

	//private Context context 				= null;
		private SQLiteDatabase dbConn 	  	    = null;
		private Database database               = null;
		
		/**
		 * Constructor
		 * @param context
		 */
		public MultiFriendzyImpl(Context context)
		{
			database = new Database(context);
			database.open();		
		}
		
		/**
		 * This method open connection with database
		 */
		public void openConn()
		{
			dbConn = database.getConnection();
		}
		
		/**
		 * This method close the database conneciton
		 */
		public void closeConn()
		{
			if(dbConn != null)
				dbConn.close();
		}
		
		
		/**
		 * This method return the equation type id 
		 * @param operationIdList
		 * @param difficultyId
		 */
		public ArrayList<Integer>  getEquationCategoryId(ArrayList<Integer> catagoryIdList , int difficultyId)
		{
			ArrayList<Integer> equationsIdList = new ArrayList<Integer>();
			
			String stringFromArray = "";
			
			for(  int  i = 0 ; i < catagoryIdList.size() ; i ++)
			{
				if(i == catagoryIdList.size() - 1)
					stringFromArray = stringFromArray + catagoryIdList.get(i);
				else
					stringFromArray = stringFromArray + catagoryIdList.get(i) + ",";
			}
			
			String query = "SELECT Equation_Category_id from MultiFriendzyEquationsType WHERE Difficulty_Level ="
							+ "'" + difficultyId + "'" 
							 + "AND Equation_Type IN(" + stringFromArray + ")";
	
			Cursor cursor = dbConn.rawQuery(query, null);
			while(cursor.moveToNext())
			{
				equationsIdList.add(cursor.getInt(0));
			}
			
			if(cursor != null)
				cursor.close();
			return equationsIdList;
		}
}
