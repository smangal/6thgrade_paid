package com.mathfriendzy.gcm;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyWinnerScreen;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.multifriendzy.MultiFriendzysFromServerDTO;
import com.mathfriendzy.notification.PopUpActivityForNotification;
import com.mathfriendzy.notification.PopUpForFriendzyChallenge;
import com.mathfriendzy.notification.SendNotificationTransferObj;
import com.sixthgradepaid.R;

/**
 * This class process notification
 * @author Yashwant Singh
 *
 */
public class ProcessNotification extends IntentService
{
	private final String TAG = this.getClass().getSimpleName();
	public static SendNotificationTransferObj sentNotificationData = null;

	public static FriendzyDTO dto				= null;
	public static boolean isFromNotification = false;
	private boolean isParseJson   = false;
	private boolean isShowMessage = false;
	private boolean isChallenge	  = false;

	public ProcessNotification() 
	{
		super("Process Notification");
	}

	
	@Override
	protected void onHandleIntent(Intent intent) 
	{
		CharSequence messageFromNotification = intent.getExtras().getString("Message");//message from notification

		//parse notification data
		if(!isParseJson)
		{
			isParseJson = true;
			sentNotificationData = this.parsingNotificationData(messageFromNotification.toString());
			if(!isChallenge)
			{
				isFromNotification = true;
				new FindMultiFriendzyRound(ProcessNotification.sentNotificationData.getOppUserId(), 
						ProcessNotification.sentNotificationData.getOppPlayerId(), 
						ProcessNotification.sentNotificationData.getFriendzyId()).execute(null,null,null);
			}
		}

	}

	/**
	 * This method parse notification messagejson String 
	 * @param jsonString
	 */
	private SendNotificationTransferObj parsingNotificationData(String jsonStringMessage)
	{
		Log.e(TAG, "inside parsingNotificationData jsonString : " + jsonStringMessage);

		SendNotificationTransferObj notificationData = new SendNotificationTransferObj();

		try 
		{
			JSONObject jObject = new JSONObject(jsonStringMessage);
			notificationData.setMessage(jObject.getString("msg"));
			notificationData.setUserId(jObject.getString("uid"));
			notificationData.setPlayerId(jObject.getString("pid"));
			notificationData.setCountry(jObject.getString("iso"));
			notificationData.setOppUserId(jObject.getString("oppUserId"));
			notificationData.setOppPlayerId(jObject.getString("oppPlrId"));
			notificationData.setFriendzyId(jObject.getString("fid"));
			notificationData.setProfileImageId(jObject.getString("img"));
			notificationData.setShareMessage(jObject.getString("optMsg"));
			notificationData.setSenderDeviceId(jObject.getString("oppPid"));//sender device pid
		}
		catch (JSONException e) 
		{
			isChallenge = true;
			//Log.e(TAG, "inside parsingNotificationData Error while parsing : " + e.toString());
			dto = setChallengerNotification(jsonStringMessage);

			displayNotification();

			//FriendzyNotificationPlayerActivity.class
		}

		return notificationData;
	}


	@SuppressWarnings("deprecation")
	private void displayNotification()
	{		
		String myPackageName = getPackageName();

		ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
		String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();

		if(myPackageName.equals(packageName))
		{
			Intent newIntent =  new Intent(ProcessNotification.this , PopUpForFriendzyChallenge.class);
			newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(newIntent);		
		}
		else
		{			
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			int icon = R.drawable.ic_launcher;
			CharSequence notiText = MathFriendzyHelper.getAppNameForNotification();
			long meow = System.currentTimeMillis();

			Notification notification = new Notification(icon, notiText, meow);
			notification.flags     |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults  |= Notification.DEFAULT_LIGHTS;
			notification.defaults  |= Notification.DEFAULT_SOUND;
			notification.defaults  |= Notification.DEFAULT_VIBRATE;

			CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotification();
			Intent notificationIntent = new Intent(ProcessNotification.this, PopUpForFriendzyChallenge.class);			
			/*notificationIntent.putExtra("message", sentNotificationData.getMessage());*/

			PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this, 0, notificationIntent, 0);

			notification.setLatestEventInfo(ProcessNotification.this, contentTitle, 
					dto.getClassName(), contentIntent);


			int SERVER_DATA_RECEIVED = 1;

			notificationManager.notify(SERVER_DATA_RECEIVED, notification);

		}//End if else

	}//END displayNotification method

	/**
	 * This class find multi friendzy rounds when user from notification
	 * @author Yashwant Singh
	 *
	 */
	class FindMultiFriendzyRound extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private String playerId;
		private String friendzyId;
		MultiFriendzysFromServerDTO multifriendzyDataFromSerevr = null;
		//private ProgressDialog pd = null;

		FindMultiFriendzyRound(String userId ,String playerId , String friendzyId)
		{
			MultiFriendzyRound.roundList = new ArrayList<MathFriendzysRoundDTO>();
			multifriendzyDataFromSerevr  = new MultiFriendzysFromServerDTO();

			this.userId 	= userId;
			this.playerId 	= playerId;
			this.friendzyId = friendzyId;
		}

		@Override
		protected void onPreExecute() 
		{
			//pd = CommonUtils.getProgressDialog(this);
			//pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			multifriendzyDataFromSerevr = serverObj.getRoundList(userId, playerId, friendzyId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			if(multifriendzyDataFromSerevr.getRoundList().size() != 0)
			{
				MultiFriendzyMain.isClickOnYourTurn = true;
				String myPackageName = getPackageName();

				ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
				String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();

				if(myPackageName.equals(packageName))
				{
					if(!isShowMessage)
					{
						isShowMessage = true;

						//ProcessNotification.isFromNotification = false;

						if(multifriendzyDataFromSerevr.getWinner() == -1)
						{
							sentNotificationData.setWinnerId("-1");
							MultiFriendzyRound.roundList = multifriendzyDataFromSerevr.getRoundList();
						}
						else
						{
							sentNotificationData.setWinnerId(multifriendzyDataFromSerevr.getWinner() + "");
							MultiFriendzyWinnerScreen.roundList = multifriendzyDataFromSerevr.getRoundList();
						}

						Intent newIntent =  new Intent(ProcessNotification.this , PopUpActivityForNotification.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
					}
				}
				else
				{
					NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
					int icon = R.drawable.ic_launcher;
					CharSequence notiText = MathFriendzyHelper.getAppNameForNotification();
					long meow = System.currentTimeMillis();

					@SuppressWarnings("deprecation")
					Notification notification = new Notification(icon, notiText, meow);
					notification.flags     |= Notification.FLAG_AUTO_CANCEL;
					notification.defaults  |= Notification.DEFAULT_LIGHTS;
					notification.defaults  |= Notification.DEFAULT_SOUND;
					notification.defaults  |= Notification.DEFAULT_VIBRATE;

					CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotification();
					Intent notificationIntent = null;

					if(multifriendzyDataFromSerevr.getWinner() == -1)
					{
						sentNotificationData.setWinnerId("-1");
						MultiFriendzyRound.roundList = multifriendzyDataFromSerevr.getRoundList();
						notificationIntent = new Intent(ProcessNotification.this, MultiFriendzyRound.class);//sending notification activity
					}
					else
					{
						sentNotificationData.setWinnerId(multifriendzyDataFromSerevr.getWinner() + "");
						MultiFriendzyWinnerScreen.roundList = multifriendzyDataFromSerevr.getRoundList();
						notificationIntent = new Intent(ProcessNotification.this, MultiFriendzyWinnerScreen.class);//sending notification activity
					}

					/*notificationIntent.putExtra("message", sentNotificationData.getMessage());*/
					PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this, 0, notificationIntent, 0);

					notification.setLatestEventInfo(ProcessNotification.this, contentTitle, sentNotificationData.getMessage(), contentIntent);

					int SERVER_DATA_RECEIVED = 1;

					notificationManager.notify(SERVER_DATA_RECEIVED, notification);
				}
			}
			else
			{
				isParseJson = false;
			}
			super.onPostExecute(result);
		}
	}


	private FriendzyDTO setChallengerNotification(String jsonStringMessage)
	{
		FriendzyDTO dto = new FriendzyDTO();
		try 
		{
			JSONObject jObject = new JSONObject(jsonStringMessage);
			dto.setRewards(jObject.getString("msg"));
			if(!dto.getRewards().contains("has been removed"))
			{
				dto.setTeacherId(jObject.getString("teacherId"));
				dto.setSchoolId(jObject.getString("schoolId"));
				dto.setSchoolName(jObject.getString("school"));
				dto.setStatus(jObject.getString("status"));
				dto.setType(jObject.getString("type"));
				dto.setGrade(jObject.getString("grade"));
			}
			else
			{

				PopUpForFriendzyChallenge.isDelete = true;
				dto.setUid(jObject.getString("uId"));
				dto.setPid(jObject.getString("pid"));
			}						
			dto.setChallengerId(jObject.getString("chId"));			
			dto.setTeacherName(jObject.getString("teacherName"));
			dto.setStartDate(jObject.getString("startDate"));			
			dto.setEndDate(jObject.getString("endDate"));
			dto.setClassName(jObject.getString("class"));

		}
		catch (JSONException e) 
		{
			Log.e(TAG, ""+e.toString());
		}	

		return dto;
	}

}
