package com.mathfriendzy.newinappclasses;

public interface YesNoListenerInterface {
	void onYes();
	void onNo();
}
