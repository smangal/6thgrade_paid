package com.mathfriendzy.customview;

import static com.mathfriendzy.utils.ICommonUtils.BTN_COM_LINK_URL;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mathfriendzy.controller.information.InformationActivity;
import com.mathfriendzy.controller.moreapp.ActMoreApp;
import com.sixthgradepaid.R;

public class ShareBottomBar extends LinearLayout implements OnClickListener
{	
	Context context;
	public static final int idBtnDotCom		 		 = 9001;
	public static final int idBtnCustomMoreGreatApps = 9002;
	public static final int idBtnCustomInfo   		 = 9003;
	
	public ShareBottomBar(Context context) 
	{
		super(context);
		this.context = context;
		final LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		this.setOrientation(LinearLayout.VERTICAL);
		this.setLayoutParams(paramsLayout);
	}// END TiltleBar( , )



	// Constructor which called in starting
	public ShareBottomBar(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		this.context = context;
		this.setOrientation(LinearLayout.HORIZONTAL);
		
		
		//*************Add Relative Layout For Account Dynamically********************
		RelativeLayout relativeLayoutAccount = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutAccount = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutAccount.weight = 1.0f;
		this.addView(relativeLayoutAccount , paramsRelativeLayoutAccount);
	
		// Add Account button view dynamically
		Button btnDotCom = new Button(context);
		btnDotCom.setId(idBtnDotCom);
		btnDotCom.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnAccount = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		paramsBtnAccount.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnAccount.addRule(RelativeLayout.CENTER_VERTICAL);
		btnDotCom.setBackgroundResource(R.drawable.btn_background_green);
		btnDotCom.setText(".com");
		btnDotCom.setTextColor(Color.WHITE);
		btnDotCom.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		btnDotCom.setTypeface(null, Typeface.BOLD);
		relativeLayoutAccount.addView(btnDotCom, paramsBtnAccount);
		
		//*************Add Relative Layout For More Great App Dynamically********************
		RelativeLayout relativeLayoutMoreGreatApp = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutMoreGreatApp = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutMoreGreatApp.weight = 1.0f;
		this.addView(relativeLayoutMoreGreatApp , paramsRelativeLayoutMoreGreatApp);
			
		// Add More Great App  button view dynamically
		Button btnMoreGreatApp = new Button(context);
		btnMoreGreatApp.setId(idBtnCustomMoreGreatApps);
		btnMoreGreatApp.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnMoreGreatApp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_VERTICAL);
		btnMoreGreatApp.setTextColor(Color.WHITE);
		btnMoreGreatApp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		btnMoreGreatApp.setTypeface(null, Typeface.BOLD);
		btnMoreGreatApp.setText("+");
		btnMoreGreatApp.setBackgroundResource(R.drawable.btn_background_green);
		relativeLayoutMoreGreatApp.addView(btnMoreGreatApp, paramsBtnMoreGreatApp);
		
		//*************Add Relative Layout For Information Dynamically********************
		RelativeLayout relativeLayoutInformation = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutInformation = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutInformation.weight = 1.0f;
		this.addView(relativeLayoutInformation , paramsRelativeLayoutInformation);
			
		// Add Information  button view dynamically
		Button btnInformation = new Button(context);
		btnInformation.setId(idBtnCustomInfo);
		btnInformation.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnInformation = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsBtnInformation.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnInformation.addRule(RelativeLayout.CENTER_VERTICAL);
		btnInformation.setTextColor(Color.WHITE);
		btnInformation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		btnInformation.setTypeface(null, Typeface.BOLD);
		btnInformation.setText("i");
		btnInformation.setBackgroundResource(R.drawable.btn_background_green);
		relativeLayoutInformation.addView(btnInformation, paramsBtnInformation);
		
		//*************Add Relative Layout For Player Dynamically********************
		RelativeLayout relativeLayoutPlayer = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutPlayer = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutPlayer.weight = 1.0f;
		this.addView(relativeLayoutPlayer , paramsRelativeLayoutPlayer);			
		
		this.setBackgroundResource(R.drawable.topbar);
	}

	
	@Override
	public void onClick(View view) 
	{
		switch (view.getId()) 
		{
		case idBtnCustomInfo:
			context.startActivity(new Intent(context, InformationActivity.class));
			break;
			
		case idBtnCustomMoreGreatApps:
			Intent intent1 = new Intent(context, ActMoreApp.class);
            context.startActivity(intent1);
			break;
		
		case idBtnDotCom:
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BTN_COM_LINK_URL)));
			break;
			
		default:
			break;
		}
	}
	
	
}