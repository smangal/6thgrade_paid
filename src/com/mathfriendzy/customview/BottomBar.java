package com.mathfriendzy.customview;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.friendzy.TeacherChallengeActivity;
import com.mathfriendzy.controller.information.InformationActivity;
import com.mathfriendzy.controller.moreapp.ActMoreApp;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This class Creates bottom bar, Which is used in whole Applicaition
 * @author Yashwant Singh
 *
 */
public class BottomBar extends LinearLayout implements OnClickListener
{
	Context context;
	public static final int idBtnCustomAccount 		 = 9001;
	public static final int idBtnCustomMoreGreatApps = 9002;
	public static final int idBtnCustomInfo   		 = 9003;
	public static final int idBtnCustomPlayer 		 = 9004;

	private boolean isClickOnAccount = false;

	public BottomBar(Context context) 
	{
		super(context);
		this.context = context;
		final LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);

		this.setOrientation(LinearLayout.VERTICAL);
		this.setLayoutParams(paramsLayout);
	}// END TiltleBar( , )



	// Constructor which called in starting
	public BottomBar(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		this.context = context;
		this.setOrientation(LinearLayout.HORIZONTAL);


		//*************Add Relative Layout For Account Dynamically********************
		RelativeLayout relativeLayoutAccount = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutAccount = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutAccount.weight = 1.0f;
		this.addView(relativeLayoutAccount , paramsRelativeLayoutAccount);

		// Add Account button view dynamically
		Button btnAccount = new Button(context);
		btnAccount.setId(idBtnCustomAccount);
		btnAccount.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnAccount = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		paramsBtnAccount.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnAccount.addRule(RelativeLayout.CENTER_VERTICAL);
		btnAccount.setBackgroundResource(R.drawable.setting);
		relativeLayoutAccount.addView(btnAccount, paramsBtnAccount);

		//*************Add Relative Layout For More Great App Dynamically********************
		RelativeLayout relativeLayoutMoreGreatApp = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutMoreGreatApp = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutMoreGreatApp.weight = 1.0f;
		this.addView(relativeLayoutMoreGreatApp , paramsRelativeLayoutMoreGreatApp);

		// Add More Great App  button view dynamically
		Button btnMoreGreatApp = new Button(context);
		btnMoreGreatApp.setId(idBtnCustomMoreGreatApps);
		btnMoreGreatApp.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnMoreGreatApp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_VERTICAL);
		btnMoreGreatApp.setTextColor(Color.WHITE);
		btnMoreGreatApp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		btnMoreGreatApp.setTypeface(null, Typeface.BOLD);
		btnMoreGreatApp.setText("+");
		btnMoreGreatApp.setBackgroundResource(R.drawable.btn_background_green);
		relativeLayoutMoreGreatApp.addView(btnMoreGreatApp, paramsBtnMoreGreatApp);

		//*************Add Relative Layout For Information Dynamically********************
		RelativeLayout relativeLayoutInformation = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutInformation = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutInformation.weight = 1.0f;
		this.addView(relativeLayoutInformation , paramsRelativeLayoutInformation);

		// Add Information  button view dynamically
		Button btnInformation = new Button(context);
		btnInformation.setId(idBtnCustomInfo);
		btnInformation.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnInformation = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsBtnInformation.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnInformation.addRule(RelativeLayout.CENTER_VERTICAL);
		btnInformation.setTextColor(Color.WHITE);
		btnInformation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		btnInformation.setTypeface(null, Typeface.BOLD);
		btnInformation.setText("i");
		btnInformation.setBackgroundResource(R.drawable.btn_background_green);
		relativeLayoutInformation.addView(btnInformation, paramsBtnInformation);

		//*************Add Relative Layout For Player Dynamically********************
		RelativeLayout relativeLayoutPlayer = new RelativeLayout(context);
		final LinearLayout.LayoutParams paramsRelativeLayoutPlayer = 
				new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
		paramsRelativeLayoutPlayer.weight = 1.0f;
		this.addView(relativeLayoutPlayer , paramsRelativeLayoutPlayer);

		// Add Player  button view dynamically
		Button btnPlayer = new Button(context);
		btnPlayer.setId(idBtnCustomPlayer);
		btnPlayer.setOnClickListener(this);
		// Set properties for dynamically generated button
		final RelativeLayout.LayoutParams paramsBtnPlayer = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsBtnPlayer.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsBtnPlayer.addRule(RelativeLayout.CENTER_VERTICAL);
		btnPlayer.setBackgroundResource(R.drawable.share);
		relativeLayoutPlayer.addView(btnPlayer, paramsBtnPlayer);


		//Check for there is need to show player or not
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.custom_bottom_bar);
		boolean isShowPlayer = a.getBoolean(R.styleable.custom_bottom_bar_show_player , true);

		if(!isShowPlayer){
			btnPlayer.setVisibility(View.INVISIBLE);
		}
		a.recycle();
		this.setBackgroundResource(R.drawable.topbar);

		this.setPlayerButtonVisibility(btnPlayer);
	}

	@SuppressWarnings({ "unused", "deprecation" })
	private void setPlayerButtonVisibility(Button btnPlayer){
		if(true){            
			btnPlayer.setVisibility(View.INVISIBLE);            
			return;
		}		
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false)){
			UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
			RegistereUserDto regUserObj = userOprObj.getUserData();
			ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);			
			String topActivityName =  am.getRunningTasks(1).get(0).topActivity.getClassName();
			if(topActivityName.equals("com.mathfriendzy.controller.main.MainActivity")
					&& !regUserObj.getIsParent().equals("0")){//0 for teacher
				btnPlayer.setVisibility(View.INVISIBLE);
			}
		}		
	}

	@Override
	public void onClick(View view) 
	{
		switch (view.getId()) 
		{
		case idBtnCustomInfo:
			context.startActivity(new Intent(context, InformationActivity.class));
			break;

		case idBtnCustomMoreGreatApps:
			//context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ITextIds.RATE_URL)));
			/*Intent intent1 = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(ICommonUtils.MORE_APP_URL));
            context.startActivity(intent1);*/

			Intent intent1 = new Intent(context, ActMoreApp.class);
			context.startActivity(intent1);

			break;

		case idBtnCustomPlayer:

			if(!MathFriendzyHelper.isUserLogin(context)){
				MathFriendzyHelper.showLoginRegistrationDialog(context);
				return;
			}

			//isClickOnAccount = false;
			//////////////////////////////
			SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);  
			if(sheredPreference.getBoolean(IS_LOGIN, false))
			{
				UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
				RegistereUserDto regUserObj = userOprObj.getUserData();

				ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
				//Log.e("hjg", "activity " + am.getRunningTasks(1).get(0).topActivity.getClassName());
				String topActivityName =  am.getRunningTasks(1).get(0).topActivity.getClassName();
				if(topActivityName.equals("com.mathfriendzy.controller.main.MainActivity")
						&& regUserObj.getIsParent().equals("0")){//0 for teacher
					//this.clickOnForFriendzyChallenge();
					Intent intent = new Intent(context,TeacherChallengeActivity.class);
					context.startActivity(intent);

				}else{
					isClickOnAccount = false;
					this.checkForTempPlayer();
				}
			}else{
				isClickOnAccount = false;
				this.checkForTempPlayer();
			}

			//this.checkForTempPlayer();

			break;

		case idBtnCustomAccount:
			if(!CommonUtils.isInternetConnectionAvailable(context))
			{
				CommonUtils.showInternetDialog(context);			
			}
			else
			{
				isClickOnAccount = true;
				this.chekValidLogin();
			}
			break;
		default:
			break;
		}
	}




	/**
	 * This method check for temp player is exist or not
	 */
	private void checkForTempPlayer()
	{
		TempPlayerOperation tempObj = new TempPlayerOperation(context);
		if(tempObj.isTemparyPlayerExist())
		{
			SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(sheredPreference.getBoolean(IS_LOGIN, false))
			{				
				UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
				RegistereUserDto regUserObj = userOprObj.getUserData();

				if(regUserObj.getEmail().length() > 1)
				{
					new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
				}
				else
				{
					new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
				}
			}
			else
			{
				if(tempObj.isTempPlayerDeleted())//if temp player exist then check for temp player is deleted or not from the table
				{
					/*Intent intentCreate = new Intent(context,CreateTempPlayerActivity.class);
					context.startActivity(intentCreate);*/
					MathFriendzyHelper.openCreateTempPlayerActivity(context);
				}
				else
				{
					Intent intent  = new Intent(context,PlayersActivity.class);
					context.startActivity(intent);
				}
			}
			tempObj.closeConn();
		}
		else
		{
			tempObj.createTempPlayer("");//create temp player if not exists
		}

		//tempObj.closeConn();
	}

	/**
	 * This method check for valid login
	 */
	public void chekValidLogin()
	{	
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(context);
			dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
			transeletion.closeConnection();	
		}
		else
		{			
			UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
			RegistereUserDto regUserObj = userOprObj.getUserData();

			if(regUserObj.getEmail().length() > 1)
			{
				new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
			}
			else
			{
				new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
			}

		}
	}


	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByEmail extends AsyncTask<Void, Void, Void>
	{
		private ProgressDialog pd 	= null;
		private String email 		= null;
		private String pass 		= null;

		GetUserDetailByEmail(String email, String pass)
		{
			this.email = email;
			this.pass  = pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(context);
			login.getUserDetailByEmail(email, pass);

			return null;

		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			if(isClickOnAccount)
			{
				Intent intent = new Intent(context,ModifyRegistration.class);
				context.startActivity(intent);
			}
			else
			{
				UserPlayerOperation userPlayer = new UserPlayerOperation(context);
				if(userPlayer.isUserPlayersExist())
				{

					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,CreateTeacherPlayerActivity.class);
						context.startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(context,LoginUserCreatePlayer.class);
						context.startActivity(intent);
					}
				}
				else
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,TeacherPlayer.class);
						context.startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(context,LoginUserPlayerActivity.class);
						context.startActivity(intent);
					}
				}
			}

			super.onPostExecute(result);
		}
	}


	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByUserId extends AsyncTask<Void, Void, Void>
	{
		private String userId 	= null;
		//private String pass     	= null;
		private ProgressDialog pd 	= null;

		GetUserDetailByUserId(String userId , String pass)
		{
			this.userId = userId;
			//this.pass     =  pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(context);
			login.getUserDetailByUserId(userId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			if(isClickOnAccount)
			{
				Intent intent = new Intent(context,ModifyRegistration.class);
				context.startActivity(intent);
			}
			else
			{
				UserPlayerOperation userPlayer = new UserPlayerOperation(context);
				if(userPlayer.isUserPlayersExist())
				{

					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,CreateTeacherPlayerActivity.class);
						context.startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(context,LoginUserCreatePlayer.class);
						context.startActivity(intent);
					}
				}
				else
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,TeacherPlayer.class);
						context.startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(context,LoginUserPlayerActivity.class);
						context.startActivity(intent);
					}
				}
			}
			super.onPostExecute(result);
		}
	}


	//Change For Friendzy Challenge in Player button on 6th March 2014
	/**
	 * use to check player for challenge
	 */
	private void clickOnForFriendzyChallenge()
	{
		/*Intent intent = new Intent(this,FriendzyNotificationPlayerActivity.class);
		startActivity(intent);*/
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(context))
			{
				new GetRequiredCoinsForPurchaseItem().execute(null,null,null);
			}
			else
			{
				CommonUtils.showInternetDialog(context);
			}
			/*UserPlayerOperation userPlayer = new UserPlayerOperation(context);
			if(!userPlayer.isUserPlayersExist())
			{
				SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0); 
				if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
				{		
					Intent intent = new Intent(this,LoginUserPlayerActivity.class);
				startActivity(intent);

					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,TeacherPlayer.class);
						context.startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(context,LoginUserPlayerActivity.class);
						context.startActivity(intent);
					}
				}
				else
				{
					if(getApplicationUnLockStatus(MAX_ITEM_ID_FOR_ALL_APP, userId) == 1)
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(this);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(this,TeacherChallengeActivity.class);
						startActivity(intent);
					}
					else
					{		 
						Intent intent = new Intent(this,StudentChallengeActivity.class);
						startActivity(intent);										
					}
				}
				else
				{
					if(CommonUtils.isInternetConnectionAvailable(context))
					{
						new GetRequiredCoinsForPurchaseItem().execute(null,null,null);
					}
					else
					{
						CommonUtils.showInternetDialog(context);
					}
					//}
				}
			}
			else
			{
				Intent intent = new Intent(context,LoginUserCreatePlayer.class);
				context.startActivity(intent);
			}*/


		}
		else
		{
			DialogGenerator dg = new DialogGenerator(context);
			Translation translate = new Translation(context);
			translate.openConnection();
			dg.generateWarningDialog(translate.getTranselationTextByTextIdentifier
					("alertMsgYouMustRegisterLoginToAccessThisFeature")+"");
			translate.closeConnection();
		}

	}


	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;
		String userId;
		String playerId;

		GetRequiredCoinsForPurchaseItem()
		{			
			SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
			userId = sharedPreffPlayerInfo.getString("userId", "");
			playerId = sharedPreffPlayerInfo.getString("playerId", "");

			apiString = "userId="+userId+"&playerId="+playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(context);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();	

			if(coindFromServer.getAppStatus() == 1)
			{
				ArrayList<PurchaseItemObj> purchaseItem	= new ArrayList<PurchaseItemObj>();
				PurchaseItemObj obj						= new PurchaseItemObj();
				obj.setItemId(100);
				obj.setStatus(1);
				obj.setUserId(userId);
				purchaseItem.add(obj);
				LearningCenterimpl learnObj = new LearningCenterimpl(context);
				learnObj.openConn();
				learnObj.insertIntoPurchaseItem(purchaseItem);
				UserRegistrationOperation userObj = new UserRegistrationOperation(context);
				/*if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
				{*/
				Intent intent = new Intent(context,TeacherChallengeActivity.class);
				context.startActivity(intent);
				/*}
				else
				{		 
					Intent intent = new Intent(context,StudentChallengeActivity.class);
					context.startActivity(intent);										
				}*/
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(context);
				dg.generateDialogForFriendzyUnlock(coindFromServer.getCoinsEarned(),coindFromServer);	

				/*DialogGenerator dg = new DialogGenerator(MainActivity.this);
			    dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId);*/
			}					

			super.onPostExecute(result);
		}
	}
}