package com.mathfriendzy.controller.resources;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sixthgradepaid.R;

/**
 * Created by root on 11/8/15.
 */
public class ResourceAdapter extends BaseAdapter{
    public ArrayList<ResourceResponse> resourceList = null;
    private LayoutInflater mInflater = null;
    private ViewHolder vHolder = null;
    private DisplayImageOptions options;
    private ImageLoader loader = null;
    private Context context = null;
    private String collection = "Collection";
    private String format = "Format";
    private String iPadFriendzy = "iPad_friendly";
    private String notIpadFriendzy = "not_iPad_friendly";
    private String mobileFriendzy = "Mobile friendly";
    private String notMobileFriendzy = "Not Mobile friendly";
    private boolean isAssignHomework = false;

    private final String TAG = this.getClass().getSimpleName();
    private ResourceAdapterCallback callback = null;
    private int pageNumber = 0;
    private final int FIRST_PAGE = 0;
    private final int NUMBER_OF_UNLOCK_VIDEO = 3;//index start with 0 that means for three video we put 2
    private boolean isResourceUnlock;

    private boolean isDeleteVisible = false;
    private String btnTitleDelete = "";
    private String lblAddToHomeowrk = "";
    public ResourceAdapter(Context context ,
                           ArrayList<ResourceResponse> resourceList ,
                           boolean isAssignHomework ,
                           ResourceAdapterCallback callback
            , int pageNumber , boolean isResourceUnlock){
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.resourceList = resourceList;
        this.isAssignHomework = isAssignHomework;
        this.callback = callback;
        loader = MathFriendzyHelper.getImageLoaderInstance();
        options = MathFriendzyHelper.getDisplayOption();
        this.pageNumber = pageNumber;
        this.isResourceUnlock = isResourceUnlock;

        String[] textIds = MathFriendzyHelper.getTreanslationTextById(context , "btnTitleDelete" , "lblAddToHomeowrk");
        btnTitleDelete = textIds[0];
        lblAddToHomeowrk = textIds[1];
    }

    public void updateNewList(ArrayList<ResourceResponse> resourceList
            , int pageNumber){
        this.pageNumber = pageNumber;
        this.resourceList = resourceList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return resourceList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            vHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.gooru_search_result_item , null);
            vHolder.resourceLayout = (RelativeLayout) view.findViewById(R.id.resourceLayout);
            vHolder.title = (TextView) view.findViewById(R.id.title);
            vHolder.mediaType = (TextView) view.findViewById(R.id.mediatype);
            vHolder.discription = (TextView) view	.findViewById(R.id.description);
            vHolder.image = (ImageView) view.findViewById(R.id.image);
            vHolder.imageType = (ImageView) view.findViewById(R.id.imageType);
            vHolder.btnCollection = (Button) view.findViewById(R.id.btncollections);
            vHolder.imgResourceLock = (ImageView) view.findViewById(R.id.imgResourceLock);
            vHolder.curriculumCodeScroleviewLayout =(LinearLayout)
                    view.findViewById(R.id.collectionsscroleviewlayout);
            vHolder.addtohome = (CheckBox) view.findViewById(R.id.addtohome);
            vHolder.txtDelete = (TextView) view.findViewById(R.id.txtDelete);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }

        final ResourceResponse resources = resourceList.get(position);

        vHolder.txtDelete.setText(Html.fromHtml(MathFriendzyHelper.convertStringToUnderLineString(btnTitleDelete)));
        vHolder.addtohome.setText(lblAddToHomeowrk);

        vHolder.title.setText(resources.getTitle());
        vHolder.mediaType.setText(format + ": " + resources.getValue());
        this.displayImage(resources.getUrl() , vHolder.image);
        this.setIcon(resources.getValue() , vHolder.imageType);

        if(resources.getScollectionCount().length() == 0 )
            vHolder.btnCollection.setText(collection + "(0)");
        else
            vHolder.btnCollection.setText(collection + "(" +
                    resources.getScollectionCount() + ")");

        if(vHolder.curriculumCodeScroleviewLayout.getChildCount() > 0)
            vHolder.curriculumCodeScroleviewLayout.removeAllViews();
        if(resources.getCurriculumCode() != null && resources.getCurriculumCode().size() > 0){
            int curriculumSize = resources.getCurriculumCode().size();
            for(int i = 0 ; i < curriculumSize;  i ++){
                View collectionView = mInflater.inflate(R.layout.resource_collection_text , null);
                TextView txtCollectionText = (TextView) collectionView.findViewById(R.id.txtCollectionText);
                txtCollectionText.setText(resources.getCurriculumCode().get(i).toString());
                vHolder.curriculumCodeScroleviewLayout.addView(collectionView);
            }
        }

        if (resources.getMediaType() != null) {
            String mediaSupport = resources.getMediaType();
            if (mediaSupport.equals(iPadFriendzy))
                vHolder.mediaType.setText(vHolder.mediaType.getText() + "; " + mobileFriendzy);
            else if (mediaSupport.equals(notIpadFriendzy))
                vHolder.mediaType.setText(vHolder.mediaType.getText() +  "; " + notMobileFriendzy);
            else
                vHolder.mediaType.setText(vHolder.mediaType.getText() + "; " + mobileFriendzy);
        }

        this.setAddToHomeworkVisibilityAndChecked(vHolder , resources , position);
        this.setVisibilityOfDelete(vHolder , resources , position);
        this.setLockButtonVisibility(position , vHolder.imgResourceLock);
        vHolder.discription.setText(resources.getDescription());
        vHolder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLockButtonVisible(position)){
                    callback.onResourceLock(position);
                    return ;
                }
                openUrl(resources.getResourceUrl());
            }
        });
        vHolder.imageType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLockButtonVisible(position)){
                    callback.onResourceLock(position);
                    return ;
                }
                openUrl(resources.getResourceUrl());
            }
        });

        vHolder.imgResourceLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLockButtonVisible(position)){
                    callback.onResourceLock(position);
                    return ;
                }
            }
        });

        vHolder.resourceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLockButtonVisible(position)){
                    callback.onResourceLock(position);
                    return ;
                }
                openUrl(resources.getResourceUrl());
            }
        });

        this.setVisibilityOfMediaType(vHolder , false);
        return view;
    }

    private void setAddToHomeworkVisibilityAndChecked(ViewHolder vHolder ,
                                                      final ResourceResponse resources ,
                                                      final int position){
        if(isAssignHomework){
            vHolder.addtohome.setVisibility(CheckBox.VISIBLE);
            vHolder.addtohome.setChecked(resources.isAddedToHomework());
            vHolder.addtohome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isLockButtonVisible(position)){
                        callback.onResourceLock(position);
                        ((CheckBox)v).setChecked(false);
                        return ;
                    }
                    resources.setAddedToHomework(!resources.isAddedToHomework());
                    resources.setPageNumber(pageNumber);
                    ResourceAdapter.this.notifyDataSetChanged();
                    callback.onAddToHomework(resources);
                }
            });
        }else{
            vHolder.addtohome.setVisibility(CheckBox.GONE);
        }
    }

    private void setVisibilityOfMediaType(ViewHolder vHolder , boolean isVisible){
        if(isVisible){
            vHolder.mediaType.setVisibility(View.VISIBLE);
        }else{
            vHolder.mediaType.setVisibility(View.GONE);
        }
    }

    private class ViewHolder{
        RelativeLayout resourceLayout;
        TextView title;
        TextView mediaType;
        TextView discription;
        ImageView image;
        ImageView imageType;
        LinearLayout curriculumCodeScroleviewLayout;
        Button btnCollection;
        CheckBox addtohome;
        ImageView imgResourceLock;
        TextView txtDelete ;
    }

    private void displayImage(String url , ImageView imgView){
        loader.displayImage(url, imgView, options, null);
    }

    private void setIcon(String value , ImageView imgType){
        if (value.equals("video"))
            imgType.setImageResource(R.drawable.video_play_icon);
        else if (value.equals("webpage"))
            imgType.setImageResource(R.drawable.web_icon);
        else if (value.equals("image"))
            imgType.setImageResource(R.drawable.gallery_icon);
        else if (value.equals("text"))
            imgType.setImageResource(R.drawable.text_icon);
    }

    public  ArrayList<ResourceResponse> getSelectedResourceList(){
        ArrayList<ResourceResponse> selectedResourceList = new ArrayList<ResourceResponse>();
        try {
            if (resourceList != null && resourceList.size() > 0) {
                for(int i = 0 ; i < resourceList.size() ; i ++ ){
                    if(resourceList.get(i).isAddedToHomework()){
                        selectedResourceList.add(resourceList.get(i));
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return selectedResourceList;
    }

    private void setLockButtonVisibility(int index , ImageView imgView){
        if(this.isLockButtonVisible(index)){
            imgView.setVisibility(ImageView.VISIBLE);
        }else{
            imgView.setVisibility(ImageView.GONE);
        }
    }

    /**
     * Return true if want to show the lock button
     * @param index
     * @return
     */
    private boolean isLockButtonVisible(int index){
        if(this.isResourceUnlock ){
            return false;
        }else{
            if(this.pageNumber == FIRST_PAGE
                    && index < NUMBER_OF_UNLOCK_VIDEO){//lock first three video for first page
                return false;
            }else{
                return true;
            }
        }
    }

    public void initializeResourceUnlockStatusAndNotifyAdapter(boolean isResourceUnlock){
        this.isResourceUnlock = isResourceUnlock;
        this.notifyDataSetChanged();
    }

    private void setVisibilityOfDelete(ViewHolder vHolder ,
                                       final ResourceResponse resources ,
                                       final int position){
        if(this.isDeleteVisible){
            vHolder.txtDelete.setVisibility(TextView.VISIBLE);
            vHolder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onResourceLock(position);
                    deleteAndNotify(position);//don't move anywhere to callback otherwise create problem
                }
            });
        }else{
            vHolder.txtDelete.setVisibility(TextView.GONE);
        }
    }

    public void setDeleteVisibility(boolean isDeleteVisible){
        this.isDeleteVisible = isDeleteVisible;
    }

    private void deleteAndNotify(int index){
        try {
            if (this.resourceList != null
                    && this.resourceList.size() > 0) {
                this.resourceList.remove(index);
                this.notifyDataSetChanged();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Open the url in webview
     * @param resourceUrl
     */
    private void openUrl(String resourceUrl){
        MathFriendzyHelper.getResourceUrlFromLink(context , resourceUrl , new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                try {
                    GetKhanVideoLinkResponse response = (GetKhanVideoLinkResponse) httpResponseBase;
                    if (response != null) {
                        if (MathFriendzyHelper.isEmpty(response.getLink())) {
                            MathFriendzyHelper.showWarningDialog(context, "No Video exit for this resource!");
                            return;
                        }
                        MathFriendzyHelper.openUrlInWebView(context, response.getLink());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
