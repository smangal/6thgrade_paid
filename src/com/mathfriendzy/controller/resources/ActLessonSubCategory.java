package com.mathfriendzy.controller.resources;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

public class ActLessonSubCategory extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private LinearLayout goLayout = null;
    private TextView txtSearchOverLessons = null;
    private EditText edtEnterTopic = null;
    private Button btnGo = null;
    private TextView txtChooseCategory = null;
    private ListView lstLessonCategories = null;
    private String alerMessageSearchTopicRequired = null;
    private ResourceCategory selectedResourceCat  = null;
    private TextView txtSearchTopic = null;
    private TextView txtsubject = null;
    private Spinner spinnerSubject = null;
    private RelativeLayout selectGradeLayut = null;
    private RelativeLayout rlSubjectLayout = null;

    private int selectedLanguage = 1;//1 default for english

    //category added to add resourcse from teacher assign homework
    private boolean isAssignHomework = false;
    private String hwTitle = null;
    private ResourceParam searchParam = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_lesson_sub_categories);

        CommonUtils.printLog(TAG, "inside onCreate()");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setCategoryAdapter(selectedResourceCat.getSubCatList());
        this.setVisibilityOfLayoutForSelectedSubject(true);

        CommonUtils.printLog(TAG, "outside onCreate()");
    }

    private void getIntentValues() {
        selectedResourceCat = (ResourceCategory) this.getIntent().getSerializableExtra("selectedCat");
        selectedLanguage = this.getIntent().getIntExtra("selectedLanguage" ,1);

        isAssignHomework = this.getIntent().getBooleanExtra("isAssignHomework" , false);
        if(isAssignHomework){
            hwTitle = this.getIntent().getStringExtra("hwTitle");
            searchParam = (ResourceParam) this.getIntent().getSerializableExtra("searchParam");
        }
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG, "inside setWidgetsReferences()");
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        goLayout = (LinearLayout) findViewById(R.id.goLayout);
        edtEnterTopic = (EditText) findViewById(R.id.edtEnterTopic);
        btnGo = (Button) findViewById(R.id.btnGo);
        txtChooseCategory = (TextView) findViewById(R.id.txtChooseCategory);
        lstLessonCategories = (ListView) findViewById(R.id.lstLessonCategories);
        txtSearchOverLessons = (TextView) findViewById(R.id.txtSearchOverLessons);
        txtSearchTopic = (TextView) findViewById(R.id.txtSearchTopic);
        txtsubject = (TextView) findViewById(R.id.txtsubject);
        spinnerSubject = (Spinner) findViewById(R.id.spinnerSubject);
        selectGradeLayut = (RelativeLayout) findViewById(R.id.selectGradeLayut);
        rlSubjectLayout = (RelativeLayout) findViewById(R.id.rlSubjectLayout);
        rlSubjectLayout.setVisibility(RelativeLayout.GONE);

        CommonUtils.printLog(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG, "inside setListenerOnWidgets()");
        btnGo.setOnClickListener(this);
        CommonUtils.printLog(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG, "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(selectedResourceCat.getCatName());
        txtSearchOverLessons.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblHeaderBrowseAndAddResources"));
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        alerMessageSearchTopicRequired = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageSearchTopicRequired");
        txtSearchTopic.setText(transeletion.getTranselationTextByTextIdentifier
                ("txtFieldPlaceHolderSearchTopic"));
        txtsubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader"));
        transeletion.closeConnection();
        CommonUtils.printLog(TAG, "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnGo:
                String searchTerm = this.getSearchTerm();
                this.openResourceScreen(searchTerm);
                break;
        }
    }

    private void setCategoryAdapter(final ArrayList<ResourceSubCat> catList) {
        ResourceSubCetAdapter adapter = new ResourceSubCetAdapter
                (this , catList);
        lstLessonCategories.setAdapter(adapter);
        lstLessonCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openResourceScreen(catList.get(position).getSubCatName());
            }
        });
    }

    private String getSearchTerm(){
        return edtEnterTopic.getText().toString();
    }

    private void openResourceScreen(String searchTerm){
        if(isAssignHomework){
            if(CommonUtils.isInternetConnectionAvailable(this)) {
                searchParam.setQuery(searchTerm);
                new MyAsyckTask(ServerOperation.CreatePostRequestForGetResources(searchParam)
                        , null, ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST, this,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST) {
                                    SearchResourceResponse response = (SearchResourceResponse) httpResponseBase;
                                    goFoResourceResultActivity(response);
                                }
                            }
                        }, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                CommonUtils.showInternetDialog(this);
            }
            return;
        }
        ResourceSearchTermParam searchTermParam = new ResourceSearchTermParam();
        if(MathFriendzyHelper.isEmpty(searchTerm)){
            MathFriendzyHelper.showWarningDialog(this , alerMessageSearchTopicRequired);
            return ;
        }
        searchTermParam.setSearchTerm(searchTerm);
        searchTermParam.setSelectedLang(selectedLanguage);
        MathFriendzyHelper.openResourceScreen(this, searchTermParam);
    }

    private void setVisibilityOfLayoutForSelectedSubject(boolean isShowList){
        if(isShowList){
            lstLessonCategories.setVisibility(ListView.VISIBLE);
            txtChooseCategory.setVisibility(TextView.VISIBLE);
            selectGradeLayut.setVisibility(RelativeLayout.GONE);
        }else{
            lstLessonCategories.setVisibility(ListView.GONE);
            txtChooseCategory.setVisibility(TextView.GONE);
            selectGradeLayut.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    private void goFoResourceResultActivity(SearchResourceResponse initialResponse){
        if(initialResponse.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
            Intent intent = new Intent(this, ActResourceSearchResult.class);
            intent.putExtra("isAssignHomework", isAssignHomework);
            intent.putExtra("hwTitle" , hwTitle);
            intent.putExtra("searchParam", searchParam);
            intent.putExtra("initialResponse", initialResponse);
            startActivity(intent);
            finish();
            ActResourceHome.getCurrentObj().finish();
        }else{
            MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                            .getTreanslationTextById(this , "alerMessageNoResultReceived") ,
                    new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            finish();
                        }
                    });
        }
    }
}
