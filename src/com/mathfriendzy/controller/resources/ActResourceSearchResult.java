package com.mathfriendzy.controller.resources;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.LoginRegisterPopUpListener;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusResponse;
import com.mathfriendzy.newinappclasses.OnPurchaseDone;
import com.mathfriendzy.newinappclasses.YesNoDialogMessagesForInAppPopUp;
import com.mathfriendzy.newinappclasses.YesNoListenerInterface;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

public class ActResourceSearchResult extends ActBase {
    private final String TAG = this.getClass().getSimpleName();

    private TextView txtsearchlabel = null;
    private TextView tvsearchquery = null;
    private TextView txtGrades = null;
    private TextView tvresourceformat = null;

    private TextView grade = null;
    private CheckBox chkvideo = null;
    private CheckBox chkWebpage = null;
    private CheckBox chkimages = null;
    private CheckBox chkText = null;

    private ListView resourcelist = null;
    private Button btnprevious = null;
    private Button btnnext = null;
    private LinearLayout pagenationscroleviewlayout = null;
    private HorizontalScrollView pagenationscroleview = null;
    private ResourceParam searchParam = null;

    private final int VIDEO = 1;
    private final int WEBPAGE = 2;
    private final int IMAGES = 3;
    private final int TEXT = 4;
    private int selectedType = 1;//default video
    private SearchResourceResponse initialResponse = null;
    private float pageLimit = 20.0f;
    private int selectedTabIndex = 0;
    private int newTabIndex = 0;
    private int totalHitCount = 105;
    private int totalTabs = 0;
    private int MAX_TAB_LIMIT_TO_DISPLAY = 7;
    private HashMap<String , SearchResourceResponse> dataList = null;
    private final int PAGE_LIMIT = 20;
    private String alerMessageNoResultReceived = null;

    private ResourceAdapter adapter = null;
    private int lastSelectedType = 0;

    private boolean isAssignHomework = false;
    private HashMap<String , ArrayList<ResourceResponse>> selectedListResources = null;
    //private String lastSelectedKey = "";
    private final String VIDEO_TEXT = "video";
    private final String WEBPAGE_TEXT = "webpage";
    private final String IMAGE_TEXT = "image";
    private final String TEXT_TEXT = "text";
    private ArrayList<ResourceResponse> finalSelectedResourceList = null;
    private String hwTitle = "";
    private boolean isAddMoreResource = false;
    private boolean isTab = false;

    //for lesson app change
    private String lblUnlockAllVideo = null;
    private String btnTitleYes = null;
    private String lblNo = null;
    private String lblYouMustRegisterLoginToAccess = null;

    //New InApp Changes
    private ProgressDialog progressDialogForNewInApp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_resource_search_result);

        CommonUtils.printLog(TAG , "inside onCreate()");

        isTab = getResources().getBoolean(R.bool.isTablet);

        this.init();
        this.getIntentValues();
        this.saveInitialData();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setSearchData();
        //this.createBottomNavigationTab(initialResponse);

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void init() {
        try {
            dataList = new LinkedHashMap<String, SearchResourceResponse>();
            selectedListResources = new LinkedHashMap<String, ArrayList<ResourceResponse>>();
            finalSelectedResourceList = new ArrayList<ResourceResponse>();

            if (!isTab) {
                MAX_TAB_LIMIT_TO_DISPLAY = 3;
            }
            progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this, "");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void saveDataIntoDataList(String key , SearchResourceResponse response){
        if(dataList != null){
            dataList.put(key , response);
        }
    }

    private void createKeyAndSaveData(SearchResourceResponse response){
        if(searchParam != null){
            String key = searchParam.getResourceFormat()
                    + "_" + searchParam.getPageNumber();
            this.saveDataIntoDataList(key , response);
        }
    }

    private void saveInitialData() {
        if(!isAddMoreResource) {
            this.createKeyAndSaveData(initialResponse);
        }
    }

    private void setSearchData() {
        try {
            if (searchParam != null) {
                tvsearchquery.setText(searchParam.getQuery());
                grade.setText(searchParam.getGrade());
                this.setResourcesTypeChecked(searchParam.getResourceFormateType());
                tvresourceformat.setText(searchParam.getQuery());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
	private void getIntentValues() {
        isAddMoreResource = this.getIntent().getBooleanExtra("isAddMoreResource" , false);
        isAssignHomework = this.getIntent().getBooleanExtra("isAssignHomework" , false);
        searchParam = (ResourceParam) this.getIntent().getSerializableExtra("searchParam");
        initialResponse = (SearchResourceResponse) this.getIntent().getSerializableExtra("initialResponse");
        if(isAssignHomework)
            hwTitle = this.getIntent().getStringExtra("hwTitle");

        if(isAddMoreResource){
            dataList = (HashMap<String, SearchResourceResponse>) this.getIntent()
                    .getSerializableExtra("dataList");
            selectedListResources = (HashMap<String, ArrayList<ResourceResponse>>)
                    this.getIntent().getSerializableExtra("selectedListResources");
        }
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtsearchlabel = (TextView) findViewById(R.id.txtsearchlabel);
        tvsearchquery = (TextView) findViewById(R.id.tvsearchquery);
        txtGrades = (TextView) findViewById(R.id.txtGrades);
        grade = (TextView) findViewById(R.id.grade);
        chkvideo = (CheckBox) findViewById(R.id.chkvideo);
        chkWebpage = (CheckBox) findViewById(R.id.chkwebpage);
        chkimages = (CheckBox) findViewById(R.id.chkimages);
        chkText = (CheckBox) findViewById(R.id.chktext);
        resourcelist = (ListView) findViewById(R.id.resourcelist);
        btnprevious = (Button) findViewById(R.id.btnprevious);
        btnnext = (Button) findViewById(R.id.btnnext);
        pagenationscroleviewlayout = (LinearLayout) findViewById(R.id.pagenationscroleviewlayout);
        pagenationscroleview = (HorizontalScrollView) findViewById(R.id.pagenationscroleview);
        tvresourceformat = (TextView) findViewById(R.id.tvresourceformat);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        chkvideo.setOnClickListener(this);
        chkimages.setOnClickListener(this);
        chkText.setOnClickListener(this);
        chkWebpage.setOnClickListener(this);
        btnprevious.setOnClickListener(this);
        btnnext.setOnClickListener(this);

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

        CommonUtils.printLog(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        txtsearchlabel.setText(transeletion.getTranselationTextByTextIdentifier("lblSearchResultFor") + ": ");
        txtGrades.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectedGradeHeader") + ": ");
        tvresourceformat.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormat"));
        chkvideo.setText(transeletion.getTranselationTextByTextIdentifier("lblLesson"));
        chkWebpage.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatWebPage"));
        chkimages.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatImages"));
        chkText.setText(transeletion.getTranselationTextByTextIdentifier("lblText"));
        btnprevious.setText(transeletion.getTranselationTextByTextIdentifier("btnPrevious"));
        btnnext.setText(transeletion.getTranselationTextByTextIdentifier("btnNext"));
        alerMessageNoResultReceived = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageNoResultReceived");
        lblUnlockAllVideo = transeletion.getTranselationTextByTextIdentifier("lblUpgradeToViewVideo");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleGo");
        lblNo = transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks");
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblWouldLikeToLoginBeforePurchase");
        transeletion.closeConnection();

        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    /**
     * Set the selected type
     * @param type
     */
    private void setResourcesTypeChecked(int type){
        this.setSelectedCheck(type);
        selectedTabIndex = 0;
        this.fatchData(new FatchSearchResourcesCallback() {
            @Override
            public void onFatchDone
                    (SearchResourceResponse response) {
                createBottomNavigationTab(response);
            }
        } , selectedTabIndex);
    }

    private void setSelectedCheck(int type){
        selectedType = type;
        switch (type) {
            case VIDEO:
                chkvideo.setChecked(true);
                chkimages.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case IMAGES:
                chkimages.setChecked(true);
                chkvideo.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case WEBPAGE:
                chkWebpage.setChecked(true);
                chkvideo.setChecked(false);
                chkimages.setChecked(false);
                chkText.setChecked(false);
                break;
            case TEXT:
                chkText.setChecked(true);
                chkvideo.setChecked(false);
                chkimages.setChecked(false);
                chkWebpage.setChecked(false);
        }
    }

    /**
     * Create bottom navigation tab
     */
    private void createBottomNavigationTab(final SearchResourceResponse resultServer) {
        totalTabs = (int) Math.ceil(Integer.valueOf(resultServer.getTotalHitCount()) / pageLimit);
        pagenationscroleviewlayout.removeAllViews();
        selectedTabIndex = 0;
        newTabIndex = 0;
        //totalTabs = (int) Math.ceil(Integer.valueOf(totalHitCount) / pageLimit);
        this.setLayoutWidth();

        new AsyncTask<Void,Void,Void>(){
            ProgressDialog pd = null;
            @Override
            protected void onPreExecute() {
                pd = MathFriendzyHelper.getProgressDialog(getCurrentActivityObj() , "");
                MathFriendzyHelper.showProgressDialog(pd);
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for(int i = 0 ; i < totalTabs ; i ++ ){
                            final int pageNumber = i;
                            View view = LayoutInflater.from(getCurrentActivityObj()).
                                    inflate(R.layout.resoure_pagination_item_layout, null);
                            TextView txtPageNumber = (TextView) view.findViewById(R.id.txtPageNumber);
                            RelativeLayout pageNumberLayout = (RelativeLayout) view.findViewById(R.id.pageNumberLayout);
                            txtPageNumber.setText((pageNumber + 1 ) + "");
                            pageNumberLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fatchData(new FatchSearchResourcesCallback() {
                                        @Override
                                        public void onFatchDone(SearchResourceResponse response) {
                                            newTabIndex = pageNumber;
                                            highLightCurrentTabAndButtonVisibility();
                                        }
                                    } , pageNumber);
                                }
                            });
                            pagenationscroleviewlayout.addView(view);
                        }
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MathFriendzyHelper.hideDialog(pd);
                highLightCurrentTabAndButtonVisibility();
                super.onPostExecute(aVoid);
            }
        }.execute();

        /*for(int i = 0 ; i < totalTabs ; i ++ ){
            final int pageNumber = i;
            View view = LayoutInflater.from(this).
                    inflate(R.layout.resoure_pagination_item_layout, null);
            TextView txtPageNumber = (TextView) view.findViewById(R.id.txtPageNumber);
            RelativeLayout pageNumberLayout = (RelativeLayout) view.findViewById(R.id.pageNumberLayout);
            txtPageNumber.setText((pageNumber + 1 ) + "");
            pageNumberLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fatchData(new FatchSearchResourcesCallback() {
                        @Override
                        public void onFatchDone(SearchResourceResponse response) {
                            newTabIndex = pageNumber;
                            highLightCurrentTabAndButtonVisibility();
                        }
                    } , pageNumber);
                }
            });
            pagenationscroleviewlayout.addView(view);
        }*/

        //this.highLightCurrentTabAndButtonVisibility();
    }

    /**
     * Highlight the selected tab and set the previous and next button visibility
     */
    private void highLightCurrentTabAndButtonVisibility(){
        this.highLightCurrentPage();
        this.setPrevButtonVisibility();
        this.setNextButtonVisibility();
        //this.fatchData();
    }

    /**
     * Highlight the current tab
     */
    private void highLightCurrentPage(){
        try {
            RelativeLayout selectedTabLayout = (RelativeLayout) pagenationscroleviewlayout
                    .getChildAt(selectedTabIndex).findViewById(R.id.pageNumberLayout);
            RelativeLayout newTabLayout = (RelativeLayout) pagenationscroleviewlayout
                    .getChildAt(newTabIndex).findViewById(R.id.pageNumberLayout);
            selectedTabLayout.setBackgroundColor(getResources().getColor(R.color.WHITE));
            newTabLayout.setBackgroundColor(getResources().getColor(R.color.LIGHT_BLUE));
            selectedTabIndex = newTabIndex;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set previous button visibility
     */
    private void setPrevButtonVisibility(){
        if(selectedTabIndex == 0){
            btnprevious.setVisibility(Button.INVISIBLE);
        }else{
            btnprevious.setVisibility(Button.VISIBLE);
        }
    }

    /**
     * Set Next button visibility
     */
    private void setNextButtonVisibility(){
        if(selectedTabIndex == (totalTabs - 1)){
            btnnext.setVisibility(Button.INVISIBLE);
        }else{
            btnnext.setVisibility(Button.VISIBLE);
        }
    }

    /**
     * Set the number of tabs in the bottom navigation tab height
     */
    private void setLayoutWidth(){
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)
                pagenationscroleview.getLayoutParams();
        if(totalTabs > MAX_TAB_LIMIT_TO_DISPLAY){
            lp.width = (int) getResources().getDimension(R.dimen.bottom_navigate_layout_width);
        }else{
            lp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        pagenationscroleview.setLayoutParams(lp);
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    private void createAndUpdateAdapter(ArrayList<ResourceResponse> listResources , int pageNumber){
        if(listResources == null)
            return ;
        if(adapter == null){
            adapter = new ResourceAdapter(this , listResources , isAssignHomework ,
                    new ResourceAdapterCallback() {
                        @Override
                        public void onAddToHomework(ResourceResponse resources) {
                            addOrRemoveResource(resources);
                        }

                        @Override
                        public void onResourceLock(int index) {
                            showUnlockDialog();
                        }

                    } , pageNumber , MathFriendzyHelper.isAppUnlockOrResourcePurchased(this));
            resourcelist.setAdapter(adapter);
        }else{
            adapter.updateNewList(listResources , pageNumber);
        }
    }

    private void addOrRemoveResource(ResourceResponse resources){
        String selectedKey = this.getResourceFormate();
        ArrayList<ResourceResponse> list =
                selectedListResources.get(selectedKey);
        if(list == null){
            list = new ArrayList<ResourceResponse>();
        }
        if(resources.isAddedToHomework()){
            list.add(resources);
        }else{
            //list.remove(resources);
            this.removeObjFromList(list , resources);
        }
        selectedListResources.put(selectedKey , list);
    }

    /**
     * Remove object from list based on the title
     * @param list
     * @param resources
     */
    private void removeObjFromList(ArrayList<ResourceResponse> list ,
                                   ResourceResponse resources){
        for(int i = 0 ; i < list.size() ; i ++ ){
            if(resources.getTitle().equals(list.get(i).getTitle())){
                list.remove(i);
                break;
            }
        }
    }

    /**
     *
     * @param callback
     * @param pageNumber -  start with 0
     */
    private void fatchData(final FatchSearchResourcesCallback callback , final int pageNumber){
        //lesson app changes
        /*if(!this.isAppUnlockOrResourcePurchased(pageNumber)){//this condition check and show the message dialog
            return;
        }*/

        String key = this.getResourceFormate() + "_" + (pageNumber + 1);
        SearchResourceResponse result = dataList.get(key);
        if(result != null){
            callback.onFatchDone(result);
            this.createAndUpdateAdapter(result.getListOfresource() , pageNumber);
        }else{
            searchParam = this.getResourceParam(pageNumber);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResources(searchParam)
                    , null, ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST) {
                                SearchResourceResponse resultServer =
                                        (SearchResourceResponse) httpResponseBase;
                                if(resultServer.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                    createKeyAndSaveData(resultServer);
                                    callback.onFatchDone(resultServer);
                                    createAndUpdateAdapter(resultServer.getListOfresource() , pageNumber);
                                }else{
                                    setSelectedCheck(lastSelectedType);
                                    MathFriendzyHelper.showWarningDialog(ActResourceSearchResult.this
                                            ,alerMessageNoResultReceived);
                                }
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private ResourceParam getResourceParam(int pageNumber){
        ResourceParam resourceParam = new ResourceParam();
        resourceParam.setGrade(searchParam.getGrade());
        resourceParam.setPageNumber((pageNumber + 1) + "");
        resourceParam.setPageSize(PAGE_LIMIT + "");
        resourceParam.setQuery(searchParam.getQuery());
        resourceParam.setResourceFormat(this.getResourceFormate());
        resourceParam.setSubjectName(searchParam.getSubjectName());
        resourceParam.setSelectedLang(searchParam.getSelectedLang());
        return resourceParam;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.chkvideo:
                lastSelectedType = selectedType;
                this.setResourcesTypeChecked(VIDEO);
                break;
            case R.id.chkimages:
                lastSelectedType = selectedType;
                this.setResourcesTypeChecked(IMAGES);
                break;
            case R.id.chkwebpage:
                lastSelectedType = selectedType;
                this.setResourcesTypeChecked(WEBPAGE);
                break;
            case R.id.chktext:
                lastSelectedType = selectedType;
                this.setResourcesTypeChecked(TEXT);
                break;
            case R.id.btnnext:
                if(!(selectedTabIndex < totalTabs - 1))
                    return;

                this.fatchData(new FatchSearchResourcesCallback() {
                    @Override
                    public void onFatchDone
                            (SearchResourceResponse response) {
                        newTabIndex = selectedTabIndex + 1;
                        highLightCurrentTabAndButtonVisibility();
                    }
                } , selectedTabIndex + 1);
                break;
            case R.id.btnprevious:
                if(selectedTabIndex == 0)
                    return;

                this.fatchData(new FatchSearchResourcesCallback() {
                    @Override
                    public void onFatchDone
                            (SearchResourceResponse response) {
                        newTabIndex = selectedTabIndex - 1;
                        highLightCurrentTabAndButtonVisibility();
                    }
                } , selectedTabIndex - 1);
                break;
        }
    }

    /**
     * This method check which resources are marked.
     * @return Resource name ( video , text , image , webpage )  or null string.
     */
    private String getResourceFormate() {
        if(chkvideo.isChecked())
            return "video";
        else if(chkText.isChecked())
            return "text";
        else if(chkimages.isChecked())
            return "image";
        else if(chkWebpage.isChecked())
            return "webpage";
        return null;
    }

    private void addToFinalList(ArrayList<ResourceResponse> list){
        if (list != null && list.size() > 0) {
            finalSelectedResourceList.addAll(list);
        }
    }

    /**
     * OPen the selected resource activity
     *//*
    private void openSelectedResourceActivity(){
        Intent intent = new Intent(this , ActSelectedResource.class);
        intent.putExtra("selectedResourceList" ,finalSelectedResourceList);
        intent.putExtra("hwTitle" , hwTitle);
        searchParam.setResourceFormat(this.getResourceFormate());
        searchParam.setResourceFormateType(selectedType);
        intent.putExtra("searchParam" , searchParam);
        intent.putExtra("dataList" , dataList);
        intent.putExtra("selectedListResources" , selectedListResources);
        startActivity(intent);
        finish();
    }*/

    /*private void manegeSelectedResources(){
        if(isAssignHomework) {
            if (selectedListResources != null) {
                this.addToFinalList(selectedListResources.get(VIDEO_TEXT));
                this.addToFinalList(selectedListResources.get(WEBPAGE_TEXT));
                this.addToFinalList(selectedListResources.get(IMAGE_TEXT));
                this.addToFinalList(selectedListResources.get(TEXT_TEXT));
            }
            if(finalSelectedResourceList != null
                    && finalSelectedResourceList.size() > 0){
                this.openSelectedResourceActivity();
            }else{
                super.onBackPressed();
            }
        }else{
            super.onBackPressed();
        }
    }*/


    @Override
    public void onBackPressed() {
        //this.manegeSelectedResources();
    	super.onBackPressed();
    }


    //lesson app changes , page number 0 for the first page
    /*private boolean isAppUnlockOrResourcePurchased(int pageNumber){
        if(pageNumber > 0 ) {
            if(!MathFriendzyHelper
                    .isAppUnlockOrResourcePurchased(this)) {
                this.showUnlockDialog();
                return false;
            }else {
                return true;
            }
        }else{
            return true;//return true for non-lesson apps
        }
    }*/

    private void openGetMoreCoinsScreen(Context context){
        MathFriendzyHelper.openInAppScreen(context,
                GetMoreCoins.VIDEO_PURCHASED, GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST);
    }

    private Activity getCurrentActivityObj(){
        return this;
    }

    private void showUnlockDialog(){

        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                MathFriendzyHelper.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            updateAdapterAfterPurchase();
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_PURCHASE_RESOURCE_VIDEO ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            showInAppDialog();
                                            return ;
                                        }
                                        updateAdapterAfterPurchase();
                                    }
                                } , false , response , new OnPurchaseDone() {
                                    @Override
                                    public void onPurchaseDone(boolean isDone) {

                                    }
                                });
                    }
                });
    }
    //end lesson app change

    private void showInAppDialog(){
        String[] textArray = MathFriendzyHelper.getTreanslationTextById(this , "lblLimitedTimeOffer" ,
                "lblUpgradeNowToViewVedio" , "lblUpgradeVedioOnly");
        YesNoDialogMessagesForInAppPopUp message = new YesNoDialogMessagesForInAppPopUp();
        message.setMainText(textArray[1]);
        message.setLimitedTimeOffer(textArray[0]);
        message.setUpgradeText(textArray[2]);
        MathFriendzyHelper.yesNoConfirmationDialogForLongMessageForUnlockInAppPopUp(this, lblUnlockAllVideo
                , btnTitleYes, lblNo, new YesNoListenerInterface() {
            @Override
            public void onYes() {
                if (!MathFriendzyHelper.isUserLogin(ActResourceSearchResult.this)) {
                    MathFriendzyHelper.showLoginRegistrationDialog(ActResourceSearchResult.this
                            , lblYouMustRegisterLoginToAccess , new LoginRegisterPopUpListener() {
                        @Override
                        public void onRegister() {
                            MathFriendzyHelper.openRegistration(ActResourceSearchResult.this , true ,
                                    MathFriendzyHelper.OPEN_REGISTRATION_SCREEN_REQUEST_CODE);
                        }

                        @Override
                        public void onLogin() {
                            MathFriendzyHelper.openLogin(ActResourceSearchResult.this , true ,
                                    MathFriendzyHelper.OPEN_LOGIN_SCREEN_REQUEST_CODE);
                        }

                        @Override
                        public void onNoThanks() {
                            openGetMoreCoinsScreen(ActResourceSearchResult.this);
                        }
                    });
                    return;
                }
                openGetMoreCoinsScreen(ActResourceSearchResult.this);
            }

            @Override
            public void onNo() {
                //nothing to do
            }
        } , message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
                    this.updateAdapterAfterPurchase();
                    break;
                case MathFriendzyHelper.OPEN_LOGIN_SCREEN_REQUEST_CODE:
                    this.updateAdapterAfterPurchase();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateAdapterAfterPurchase(){
        if(adapter != null){
            adapter.initializeResourceUnlockStatusAndNotifyAdapter
                    (MathFriendzyHelper.isAppUnlockOrResourcePurchased(this));
        }
    }
}
