package com.mathfriendzy.controller.top100;

import static com.mathfriendzy.utils.ITextIds.MF_SCHOOL;
import static com.mathfriendzy.utils.ITextIds.MF_STUDENT;
import static com.mathfriendzy.utils.ITextIds.MF_TEACHER;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.top100.JsonAsynTask;
import com.mathfriendzy.model.top100.TopListDatabase;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.sixthgrade.facebookconnect.ShareActivity;
import com.sixthgradepaid.R;

public class Top100Activity extends AdBaseActivity implements OnClickListener 
{
	private Button btnStudent				= null;
	private Button btnTeacher				= null;
	private Button btnSchool				= null;
	private TextView txtTitleScreen			= null;
	private Button btnShare					= null;
	private Button btnCloseShareTool		= null;
	private LinearLayout layoutShare		= null;
	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;

	private String subject					= null;
	private String body						= null;
	private String screenText				= null;

	private boolean student_flag;
	private boolean school_flag;
	private boolean teacher_flag;

	public static Bitmap b;

	@SuppressLint("CommitPrefEdits")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top100);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		if ((getResources().getConfiguration().screenLayout &
				Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
				metrics.densityDpi >= 320){
			setContentView(R.layout.activity_top100_activity_large);

		}
		else if ((getResources().getConfiguration().screenLayout &
				Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
				metrics.densityDpi > 160 && metrics.densityDpi < 320){				
			setContentView(R.layout.activity_top100);				
		}

		getWidgetId();
		setWidgetText();
		setListener();	
		
		TopListDatabase db 	= new TopListDatabase();
		db.createTableTop100(this);		
	}//END onClick Method



	private void getWidgetId() 
	{
		btnSchool			= (Button) findViewById(R.id.btnSchool);
		btnStudent			= (Button) findViewById(R.id.btnStudent);
		btnTeacher			= (Button) findViewById(R.id.btnTeacher);
		txtTitleScreen		= (TextView) findViewById(R.id.txtTitleScreen);
		layoutShare			= (LinearLayout) findViewById(R.id.layoutShare);
		btnShare			= (Button) findViewById(R.id.btnShare);
		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);
		btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);

	}//END getWidgetId method



	private void setWidgetText()
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(MF_SCHOOL);
		btnSchool.setText(text);

		text = translate.getTranselationTextByTextIdentifier(MF_STUDENT);
		btnStudent.setText(text);

		text = translate.getTranselationTextByTextIdentifier(MF_TEACHER);
		btnTeacher.setText(text);

		text = MathFriendzyHelper.getAppName(translate);
		txtTitleScreen.setText(text);

		screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");

		translate.closeConnection();
	}//END setWidgetText method



	private void setListener() 
	{
		btnSchool.setOnClickListener(this);		
		btnStudent.setOnClickListener(this);
		btnTeacher.setOnClickListener(this);
		btnCloseShareTool.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);
	}//END setListener method


	private Bitmap getSceenShot(){
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap b = view.getDrawingCache();
		return b;
	}

	@SuppressLint("ShowToast")
	@Override
	public void onClick(View v) 
	{
		Intent intent = new Intent(this, Top100ListActivity.class);	
		Translation transeletion = null;		
		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		b = getSceenShot();

		switch(v.getId())
		{
		case R.id.btnStudent:
			intent.putExtra("id", 1);

			if(!CommonUtils.isInternetConnectionAvailable(this) && !student_flag)				
				CommonUtils.showInternetDialog(this);											
			else if(student_flag || !CommonUtils.isInternetConnectionAvailable(this)){
				startActivity(intent);
				finish();
			}else if(!student_flag && CommonUtils.isInternetConnectionAvailable(this))		
				new JsonAsynTask(this, 1).execute(null,null,null);

			break;

		case R.id.btnTeacher:
			intent.putExtra("id", 2);

			if(!CommonUtils.isInternetConnectionAvailable(this) && !teacher_flag)				
				CommonUtils.showInternetDialog(this);				
			else if(teacher_flag || !CommonUtils.isInternetConnectionAvailable(this))
				startActivity(intent);				
			else if(!teacher_flag && CommonUtils.isInternetConnectionAvailable(this))		
				new JsonAsynTask(this, 2).execute(null,null,null);	

			break;

		case R.id.btnSchool:
			intent.putExtra("id", 3);

			if(!CommonUtils.isInternetConnectionAvailable(this) && !school_flag)				
				CommonUtils.showInternetDialog(this);				
			else if(school_flag || !CommonUtils.isInternetConnectionAvailable(this))
				startActivity(intent);
			else if(!school_flag && CommonUtils.isInternetConnectionAvailable(this))		
				new JsonAsynTask(this, 3).execute(null,null,null);	

			break;

		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);			
			break;

		case R.id.btnCloseShareToolbar:
			break;

		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+RATE_URL);
			emailIntent.setType("image/png");
			try 
			{
				startActivity(Intent.createChooser(emailIntent, "Send email using"));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}
			//startActivity(Intent.createChooser(emailIntent, "Send email using"));

			break;

		case R.id.btnFbSahre:				
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);

			break;

		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;

		}


	}//END onClick method




	@Override
	protected void onResume() 
	{		
		SharedPreferences prefs = getSharedPreferences("name",Context.MODE_PRIVATE);

		long date1 					= new Date().getTime();
		long date2 					= prefs.getLong("date",date1);		
		long startTime				= prefs.getLong("start",0);	
		student_flag				= prefs.getBoolean("student_flag", false);
		school_flag					= prefs.getBoolean("school_flag", false);
		teacher_flag				= prefs.getBoolean("teacher_flag", false);
		long diff 					= Math.abs(date1 - date2);		
		startTime 					= startTime - diff;

		if(startTime/1000 <= 0)
		{
			student_flag = false;
			school_flag = false;
			teacher_flag = false;
		}

		//perform click at the starting when user click on top 100
		btnStudent.performClick();
		super.onResume();
	}
}
