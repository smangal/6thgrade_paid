package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_USER_CREATE_PLAYER;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

/**
 * This Acitivity is open when user is login and and no player exist in his under,
 * and want to create player
 * @author Yashwant Singh
 *
 */
public class LoginUserCreatePlayer extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen 		= null;
	private Button   btnTitleTop100         = null;
	private TextView btnTitlePlayers        = null;
	private Button   lblCreatePlayer   		= null;
	private Button   btnTitleBack      		= null;
	
	private String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_user_create_player);
		
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "inside oncreate()");
		
		this.setWidgetsReferences();
		this.setWidgetsTextValue();
		this.setListenerOnWidgets();
		
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "outside oncreate()");
	}
	
	/**
	 * This method set Listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "inside setListenerOnWidgets()");
			
		lblCreatePlayer.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
				
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	/**
	 * This methos set the widgets references from the layout to the widgets object
	 */
	private void setWidgetsReferences()
	{	
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblCreatePlayer         = (Button)   findViewById(R.id.lblCreatePlayer);	
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		btnTitlePlayers         = (TextView) findViewById(R.id.btnTitlePlayers);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);
		
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * this method set the widgets text values from the transelation
	 */
	private void setWidgetsTextValue()
	{
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "inside setWidgetsTextValue()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("lblThankYouForCreatingAnAccount"));
		lblCreatePlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblCreatePlayer"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		
		if(LOGIN_USER_CREATE_PLAYER)
			Log.e(TAG, "outside setWidgetsTextValue()");
	}

	@Override
	public void onClick(View v) 
	{
	
		switch(v.getId())
		{
		case R.id.lblCreatePlayer : 
			startActivity(new Intent(this,AddPlayer.class).putExtra("callingActivity", "LoginUserCreatePlayer"));
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
			   startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
