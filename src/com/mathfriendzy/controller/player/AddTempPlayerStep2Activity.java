package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.ADD_TEMP_PLAYER_STEP2_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PRIVACY_POLICY_URL;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.school.SearchTeacherActivity;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.controller.webview.PrivacyPolicyActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

/**
 * This is 2nd setp for creating temp player
 * Here user give the detail about the school and teacher etc
 * @author Yashwant Singh
 *
 */
public class AddTempPlayerStep2Activity extends BasePlayer implements OnClickListener
{
	private TextView lblSchoolInformation 	= null;
	private Button   btnTitleDone       	= null;
	private TextView lblBySelectingDoneYouAgreeToPolicy = null;
	private TextView lblPrivacyPolicyAndTermsOfService  = null;
	
	public static String schoolName				= null;
	public static boolean IS_REGISTER_SCHOOL	= false;
	private String schoolIdFromFind  			= null;
	private  ArrayList<String> teacherInfo 		= null;
	private String teacherFirstName 			= null;
	private String teacherLastName  			= null;
	private String teacherId        			= null;
	
	//private Button   btnTitleBack      = null;
	
	private final String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_temp_player_step2);
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.getGrade(this,"");
		this.setWidgetsTextValue();
		this.setListenerOnWidgets();
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		spinnerGrade 	   	= (Spinner) findViewById(R.id.spinnerGrade);
		edtSchool 			= (EditText) findViewById(R.id.spinnerSchool);
		edtTeacher 		    = (EditText) findViewById(R.id.spinnerTeacher);
		pickerTitleSchool   = (TextView) findViewById(R.id.pickerTitleSchool);
		lblAddPlayerGrade 	= (TextView) findViewById(R.id.lblAddPlayerGrade);
		lblRegTeacher 		= (TextView) findViewById(R.id.lblRegTeacher);
		btnTitleDone		= (Button)   findViewById(R.id.btnTitleDone); 
		lblSchoolInformation= (TextView) findViewById(R.id.lblSchoolInformation);
		lblBySelectingDoneYouAgreeToPolicy = (TextView) findViewById(R.id.lblBySelectingDoneYouAgreeToPolicy);
		lblPrivacyPolicyAndTermsOfService = (TextView) findViewById(R.id.lblPrivacyPolicyAndTermsOfService);
		//btnTitleBack        = (Button)   findViewById(R.id.btnTitleBack);
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the widgets text valuees from the transelation
	 */
	private void setWidgetsTextValue()
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside setWidgetsTextValue()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool") + " :");
		lblAddPlayerGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + " :");
		lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher") + " :");
		lblSchoolInformation.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInformation") + " :");
		btnTitleDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));
		lblBySelectingDoneYouAgreeToPolicy.setText(transeletion.getTranselationTextByTextIdentifier("lblBySelectingDoneYouAgreeToPolicy"));
		lblPrivacyPolicyAndTermsOfService.setText(transeletion.getTranselationTextByTextIdentifier("lblPrivacyPolicyAndTermsOfService"));
		//btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside setWidgetsTextValue()");
	}
	
	
	@Override
	protected void onRestart() 
	{
		/*if(IS_REGISTER_SCHOOL)
		{
			if(schoolName != null)
			{
				schoolNameList.add(schoolName);
				schoolAdapter = new ArrayAdapter<String>(AddTempPlayerStep2Activity.this, android.R.layout.simple_list_item_1, schoolNameList);
				schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerSchool.setAdapter(schoolAdapter);
				spinnerSchool.setSelection(schoolAdapter.getPosition(schoolName));
			}
		}*/
		super.onRestart();
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		btnTitleDone.setOnClickListener(this);
		lblPrivacyPolicyAndTermsOfService.setOnClickListener(this);
		
		edtSchool.setOnClickListener(this);//changes
		edtTeacher.setOnClickListener(this);//changes
		//btnTitleBack.setOnClickListener(this);
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}
	
	@Override
	public void onClick(View v) 
	{
	
		switch(v.getId())
		{
		case R.id.btnTitleDone : 
			 checkEmptyValidation();
			 break;
		case R.id.lblPrivacyPolicyAndTermsOfService:
			Intent intentWeb = new Intent(this,PrivacyPolicyActivity.class);
			intentWeb.putExtra("url", PRIVACY_POLICY_URL);
			startActivity(intentWeb);
			break;
		case R.id.spinnerSchool:
			this.setListenerOnEdtSchool();
			break;
		case R.id.spinnerTeacher:
			Intent teacherIntent = new Intent(this,SearchTeacherActivity.class);
			teacherIntent.putExtra("schoolId", schoolIdFromFind);
			startActivityForResult(teacherIntent, 2);
			break;
		/*case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,AddTempPlayerStep1Activity.class);
			startActivity(intentMain);
			break;*/
		}
	}
	
	/*
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
			return false;
		return super.onKeyDown(keyCode, event);
	}*/
	
	/**
	 * This method call when user click on school edit text
	 */
	private void setListenerOnEdtSchool()
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside setListenerOnEdtSchool()");
		
		Intent schoolIntent = new Intent(this,SearchYourSchoolActivity.class);
		
		if(AddTempPlayerStep1Activity.countryName.equals("United States") || 
				AddTempPlayerStep1Activity.countryName.equals("Canada"))
		{		
			schoolIntent.putExtra("country", AddTempPlayerStep1Activity.countryName);
			schoolIntent.putExtra("state", AddTempPlayerStep1Activity.stateName);
			schoolIntent.putExtra("city", AddTempPlayerStep1Activity.city);
			schoolIntent.putExtra("zip", AddTempPlayerStep1Activity.zipCode);
		}
		else
		{	
			schoolIntent.putExtra("country", AddTempPlayerStep1Activity.countryName);
			schoolIntent.putExtra("state", "");
			schoolIntent.putExtra("city", AddTempPlayerStep1Activity.city);
			schoolIntent.putExtra("zip", AddTempPlayerStep1Activity.zipCode);
		}
		startActivityForResult(schoolIntent, 1);
				
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside setListenerOnEdtSchool()");
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside onActivityResult()");
		
		/*if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         edtSchool.setText(schoolInfo.get(0));
		         schoolIdFromFind = schoolInfo.get(1);
		     }
		    
		 }*/
		
		if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         edtSchool.setText(schoolInfo.get(0));
		         schoolIdFromFind = schoolInfo.get(1);
		         
		         if(schoolInfo.get(0).equals("Home School"))
		         {
		        	 edtTeacher.setEnabled(false);
		        	 edtTeacher.setText("N/A");
		        	 teacherFirstName = "N/A";
		        	 teacherLastName  = "";
		        	 teacherId        = "-2";
		         }
		         else
		         {
		        	 edtTeacher.setEnabled(true);
		         }
		     }
	    }
		
		if (requestCode == 2) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         teacherInfo = data.getStringArrayListExtra("schoolInfo");
		         edtTeacher.setText(teacherInfo.get(0) + " " + teacherInfo.get(1));
		         
		         teacherFirstName = teacherInfo.get(0);
		         teacherLastName  = teacherInfo.get(1);
		         teacherId        = teacherInfo.get(2);
		     }
		    
		 }
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside onActivityResult()");
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Before adding temp player into database check for empty validation
	 */
	private void checkEmptyValidation()
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside checkEmptyValidation()");
				
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		
		if(edtSchool.getText().toString().equals("") || edtTeacher.getText().toString().equals(""))
		{
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
		}
		else
		{
			if(this.saveTempPlayer())
			{
				Intent intent = new Intent(this,MainActivity.class);
				startActivity(intent);
			}
			else
				Log.e("", "Problem in creating player");
		}
		
		transeletion.closeConnection();	
		
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside checkEmptyValidation()");
	}
	
	/**
	 * Save temp player into database
	 * @return
	 */
	private boolean saveTempPlayer()
	{
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "inside saveTempPlayer()");
		
		TempPlayer tempPlayerObj = new TempPlayer();
		
		tempPlayerObj.setCity(AddTempPlayerStep1Activity.city);
		tempPlayerObj.setCoins(0);
		tempPlayerObj.setCompeteLevel(1);
		tempPlayerObj.setFirstName(AddTempPlayerStep1Activity.firstName);
		tempPlayerObj.setGrade(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
		tempPlayerObj.setLastName(AddTempPlayerStep1Activity.lastName);
		tempPlayerObj.setParentUserId(0);
		tempPlayerObj.setPlayerId(0);
		tempPlayerObj.setPoints(0);
		tempPlayerObj.setProfileImageName(AddTempPlayerStep1Activity.imageName);
		
		if(AddTempPlayerStep1Activity.profileImageBitmap != null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			AddTempPlayerStep1Activity.profileImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			byte[] bitMapData = stream.toByteArray();
			tempPlayerObj.setProfileImage(bitMapData);
		}
		
		
		tempPlayerObj.setSchoolId(Integer.parseInt(schoolIdFromFind));
		
		tempPlayerObj.setSchoolName(edtSchool.getText().toString());
				
		tempPlayerObj.setTeacherFirstName(teacherFirstName);
		
		tempPlayerObj.setTeacherLastName(teacherLastName);
		tempPlayerObj.setTeacherUserId(Integer.parseInt(teacherId));
			
		tempPlayerObj.setUserName(AddTempPlayerStep1Activity.userName);
		tempPlayerObj.setZipCode(AddTempPlayerStep1Activity.zipCode);
		
		if(AddTempPlayerStep1Activity.stateName != null)
			tempPlayerObj.setState(AddTempPlayerStep1Activity.stateName);
		else
			tempPlayerObj.setState("");
				
		tempPlayerObj.setCountry(AddTempPlayerStep1Activity.countryName);
		
		TempPlayerOperation tempObj = new TempPlayerOperation(this);
		tempObj.deleteFromTempPlayer();
			
		if(ADD_TEMP_PLAYER_STEP2_FLAG)
			Log.e(TAG, "outside saveTempPlayer()");
		
		return (tempObj.insertTempPlayer(tempPlayerObj));
	}
}
