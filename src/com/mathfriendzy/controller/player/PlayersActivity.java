package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ACTIVITY_LOG;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

/**
 * This Activity shows the Player
 * @author Yashwant Singh
 *
 */
public class PlayersActivity extends AdBaseActivity implements OnClickListener
{
	private ListView playerListView 			 = null;
	private ArrayList<TempPlayer> tempPlayerList = null;

	
	private TextView mfTitleHomeScreen 		= null;
	private TextView lblPleaseSelectPlayer 	= null;
	private TextView textResult  			= null;
	private TextView btnTitleEdit 			= null;
	private Button   lblAddPlayer 			= null;
	private Button   playTitle 				= null;
	private Button   btnTitleTop100         = null;
	private TextView lblRegisteredUser      = null;
	private TextView btnTitlePlayers        = null;
	private Button   alertBtnLogin          = null;
	private Button   btnTitleBack      		= null;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_players);
		
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "inside onCreate()");
		
		this.setWidgetsReferences();
		this.getTempPlayerData();
		this.setWidgetsTextValue();
		this.setListenerOnWidgets();
		PlayerAdapter adapter = new PlayerAdapter(this,R.layout.list_players,tempPlayerList);
		playerListView.setAdapter(adapter);
		
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "outside onCreate()");
	}
	
	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		lblAddPlayer.setOnClickListener(this);
		playTitle.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
		alertBtnLogin.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
	}

	/**
	 * This method set the widgets references from the layout to the widgets object references
	 */
	private void setWidgetsReferences()
	{
		playerListView 			= (ListView) findViewById(R.id.playerListView);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblPleaseSelectPlayer 	= (TextView) findViewById(R.id.lblPleaseSelectPlayer);
		textResult 				= (TextView) findViewById(R.id.textResult);
		btnTitleEdit 			= (TextView) findViewById(R.id.btnTitleEdit);
		lblAddPlayer 			= (Button)   findViewById(R.id.lblAddPlayer);
		playTitle    			= (Button)   findViewById(R.id.playTitle);
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		lblRegisteredUser       = (TextView) findViewById(R.id.lblRegisteredUser);
		btnTitlePlayers         = (TextView) findViewById(R.id.btnTitlePlayers);
		alertBtnLogin           = (Button)   findViewById(R.id.alertBtnLogin);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);
	}
	
	/**
	 * This method get temp player data from the database for displaying player
	 */
	private void getTempPlayerData()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "inside getTempPlayerData()");
		
		TempPlayerOperation tempPlayerObj = new TempPlayerOperation(this);
		tempPlayerList = tempPlayerObj.getTempPlayerData();
		
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "outside getTempPlayerData()");
	}
	
	/**
	 * This method set the widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "inside setWidgetsTextValue()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		textResult.setText(transeletion.getTranselationTextByTextIdentifier("textResult"));
		btnTitleEdit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEdit"));
		lblAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayer"));
		playTitle.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
		          			+ " " + transeletion.getTranselationTextByTextIdentifier("lblNow") + "!");
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		lblRegisteredUser.setText(transeletion.getTranselationTextByTextIdentifier("lblRegisteredUser"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
		alertBtnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		if(PLAYER_ACTIVITY_LOG)
			Log.e("PlayersActivity", "outside setWidgetsTextValue()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.lblAddPlayer:
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegitserToAddPlayer"));
			transeletion.closeConnection();	
			break;
		case R.id.playTitle:
			startActivity(new Intent(this,MainActivity.class));
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.alertBtnLogin:
			Intent intentLogin = new Intent(this, LoginActivity.class);
			intentLogin.putExtra("callingActivity", "PlayersActivity");
			startActivity(intentLogin);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion1 = new Translation(this);
				transeletion1.openConnection();
				DialogGenerator dg1 = new DialogGenerator(this);
				dg1.generateRegisterOrLogInDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion1.closeConnection();	
			}
			else
			{
			   startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
