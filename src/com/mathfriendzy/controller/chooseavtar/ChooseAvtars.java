package com.mathfriendzy.controller.chooseavtar;

import static com.mathfriendzy.utils.ICommonUtils.CHOOSE_AVTAR_LOG;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.coinsdistribution.CoinsDistributionActivity;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.player.AddPlayer;
import com.mathfriendzy.controller.player.AddTempPlayerStep1Activity;
import com.mathfriendzy.controller.player.EditPlayer;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.AvtarDTO;
import com.mathfriendzy.model.chooseAvtar.AvtarServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.inapp.InAppServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;


/**
 * This class get avtar from database and show on the screen user can select one from that
 * @author Yashwant Singh
 *
 */
public class ChooseAvtars extends AdBaseActivity implements OnClickListener
{	
	private TextView mfTitleHomeScreen 		= null;
	private TextView lblChooseAnAvatar 		= null;
	private TextView lblYouHave        		= null;
	private Button   btnTitleGetMoreCoins 	= null;

	private ArrayList<AvtarDTO> avtarDataList 	= null;
	private GridView avtarGridView    			= null;
	private final String TAG 					= this.getClass().getSimpleName();
	private String dialogTitle                  = null;
	private String dialogMsg                    = null;
	private String yesText						= null;
	private String noText 						= null;

	//for new registration
	private boolean isForNewRegistration = false;
	private boolean isForNewRegistrationSelectAvatar = false;
	private String lblPleaseRegisterBeforeBuy = null;
	private String lblPleaseRegisterBeforeGetCoins = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_avtar);

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " inside onCreate()");

		this.getIntetValues();
		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getAvtarDataFromDatabase();
		this.setWidgetsValue();
		this.setListenerOnWidgets();

		this.setCoins();
		this.setGetMoreCoinButtonVisibility();

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " outside onCreate()");
	}

	@SuppressWarnings("unused")
	private void setGetMoreCoinButtonVisibility(){
		//if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
		btnTitleGetMoreCoins.setVisibility(Button.GONE);
		//}
	}
	
	private void getIntetValues(){
		isForNewRegistration = this.getIntent().getBooleanExtra("isForNewRegistration", false);
		isForNewRegistrationSelectAvatar = this.getIntent()
				.getBooleanExtra("isForNewRegistrationSelectAvatar", false);
	}

	/**
	 * Set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{	
		btnTitleGetMoreCoins.setOnClickListener(this);
		avtarGridView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,long arg3) 
			{			

				if(isForNewRegistration){
					if(avtarDataList.get(position).getPrice().equals("0")){
						Intent intent = new Intent();
						intent.putExtra("selectedAvatarName", 
								avtarDataList.get(position).getName());
						setResult(RESULT_OK, intent);
						finish();
					}else{
						MathFriendzyHelper.showWarningDialog(ChooseAvtars.this, 
								lblPleaseRegisterBeforeBuy);
					}
					return;
				}

				if(!isForNewRegistrationSelectAvatar){
					EditPlayer.IS_IMAGE_FROM_CHOOSE_AVTAR 					= true;
					AddTempPlayerStep1Activity.IS_IMAGE_FROM_CHOOSE_AVTAR   = true;
					EditRegisteredUserPlayer.IS_IMAGE_FROM_CHOOSE_AVTAR     = true;
					AddPlayer.IS_IMAGE_FROM_CHOOSE_AVTAR        = true;
				}

				if(avtarDataList.get(position).getPrice().equals("0"))
				{

					if(isForNewRegistrationSelectAvatar){
						Intent intent = new Intent();
						intent.putExtra("selectedAvatarName", 
								avtarDataList.get(position).getName());
						setResult(RESULT_OK, intent);
						finish();
						return;
					}

					EditRegisteredUserPlayer.imageName 	= avtarDataList.get(position).getName();
					EditPlayer.imageName          		= avtarDataList.get(position).getName();
					AddTempPlayerStep1Activity.imageName= avtarDataList.get(position).getName();
					AddPlayer.imageName     = avtarDataList.get(position).getName();

					/*EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
					AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
					EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
					AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);*/
					EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage());
					AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
					EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
					AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage());
					finish();
				}
				else 
				{					
					int buyCoins = Integer.parseInt(avtarDataList.get(position).getPrice());

					if(getStatus(avtarDataList.get(position).getId()) != 1)
					{	
						if(getPlayerCoins() >= buyCoins)
						{							
							updatePlayerCoins(getPlayerCoins() - buyCoins ,
									avtarDataList.get(position));


							if(isForNewRegistrationSelectAvatar){
								Intent intent = new Intent();
								intent.putExtra("selectedAvatarName", 
										avtarDataList.get(position).getName());
								setResult(RESULT_OK, intent);
								finish();
								return;
							}

							EditRegisteredUserPlayer.imageName 	= avtarDataList.get(position).getName();
							EditPlayer.imageName          		= avtarDataList.get(position).getName();
							AddTempPlayerStep1Activity.imageName= avtarDataList.get(position).getName();
							AddPlayer.imageName     = avtarDataList.get(position).getName();

							/*EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
							AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
							EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
							AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);*/
							EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
							AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
							EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
							AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
							finish();
						}
						else
						{
							AlertDialog.Builder builder = new AlertDialog.Builder(ChooseAvtars.this);
							builder
							.setTitle(dialogTitle)
							.setMessage(dialogMsg)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setPositiveButton(yesText, new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog, int which) 
								{	
									getUserDataFromServer();
								}
							})
							.setNegativeButton(noText,new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog, int which) 
								{			      	

								}
							})						
							.show();
						}
					}
					else
					{						
						if(isForNewRegistrationSelectAvatar){
							Intent intent = new Intent();
							intent.putExtra("selectedAvatarName", 
									avtarDataList.get(position).getName());
							setResult(RESULT_OK, intent);
							finish();
							return;
						}

						EditRegisteredUserPlayer.imageName 	= avtarDataList.get(position).getName();
						EditPlayer.imageName          		= avtarDataList.get(position).getName();
						AddTempPlayerStep1Activity.imageName= avtarDataList.get(position).getName();
						AddPlayer.imageName     = avtarDataList.get(position).getName();

						/*EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
						AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
						EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);
						AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() , ChooseAvtars.this);*/
						EditPlayer.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage());
						AddTempPlayerStep1Activity.profileImageBitmap = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
						EditRegisteredUserPlayer.profileImageBitmap   = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
						AddPlayer.profileImageBitmap      = MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(position).getImage() );
						finish();
					}
				}
			}
		});
	}


	private int getStatus(String avterId)
	{
		int status = 0;
		try{
			TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
			if(!tempPlayer.isTempPlayerDeleted())
			{
				ChooseAvtarOpration opr = new ChooseAvtarOpration();
				opr.openConn(ChooseAvtars.this);
				status = opr.getStatus("0", "0" , avterId);
				opr.closeConn();
			}
			else
			{
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr
						.getUserPlayerDataById(EditRegisteredUserPlayer.playerId);

				ChooseAvtarOpration opr = new ChooseAvtarOpration();
				opr.openConn(ChooseAvtars.this);
				status = opr.getStatus(userPlayerData.getParentUserId(), 
						EditRegisteredUserPlayer.playerId , avterId);
				opr.closeConn();
			}
		}catch(Exception e){
			e.printStackTrace(); 
		}
		return status;
	}

	/**
	 * Get the user coins
	 * @return
	 */
	private int getPlayerCoins()
	{
		int playerCoins = 0;
		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(!tempPlayer.isTempPlayerDeleted())
		{			
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			tempPlayer = new TempPlayerOperation(this);
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(tempPlayer.getTempPlayerData().get(0).getPlayerId() +"");
			tempPlayer.closeConn();
			learningCenterimpl.closeConn();	

			playerCoins = playerObj.getCoins();
		}
		else
		{
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			tempPlayer = new TempPlayerOperation(this);
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(EditRegisteredUserPlayer.playerId);
			tempPlayer.closeConn();
			learningCenterimpl.closeConn();

			playerCoins = playerObj.getCoins();
		}

		return playerCoins;
	}

	/**
	 * This method update player coins
	 * @param avtarDTO 
	 */
	private void updatePlayerCoins(int updatedCoins, AvtarDTO avtarDTO)
	{		
		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(!tempPlayer.isTempPlayerDeleted())
		{
			LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
			learningCenterObj.openConn();
			learningCenterObj.updateCoinsForPlayer(updatedCoins, "0", "0");				
			learningCenterObj.closeConn();

			ChooseAvtarOpration opr = new ChooseAvtarOpration();
			opr.openConn(ChooseAvtars.this);
			opr.insertIntoPlayerActarStatus("0", "0", avtarDTO.getId(), 1);
			opr.closeConn();

			//this.setCoins();
		}
		else
		{
			UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
			UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(EditRegisteredUserPlayer.playerId);

			LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
			learningCenterObj.openConn();
			learningCenterObj.updateCoinsForPlayer(updatedCoins, userPlayerData.getParentUserId(), EditRegisteredUserPlayer.playerId);				
			learningCenterObj.closeConn();

			ChooseAvtarOpration opr = new ChooseAvtarOpration();
			opr.openConn(ChooseAvtars.this);
			opr.insertIntoPlayerActarStatus(userPlayerData.getParentUserId(), EditRegisteredUserPlayer.playerId, avtarDTO.getId(), 1);
			opr.closeConn();

			//this.setCoins();

			String userId = userPlayerData.getParentUserId();
			String playerId = EditRegisteredUserPlayer.playerId;
			String avtarId = avtarDTO.getId();
			int status = 1;

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new SaveAvtarStatus(userId, playerId, avtarId, status).execute(null,null,null);
				new SaveCoinsForPlayer(userId, playerId, updatedCoins).execute(null,null,null);
			}
		}
	}

	/**
	 * This method set widgets value to the grid adapter
	 */
	private void setWidgetsValue() 
	{	
		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " inside setWidgetsValue()");

		int layout = 0;
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  
		if(metrics.heightPixels == MathFriendzyHelper.TAB_HEIGHT && 
				metrics.widthPixels == MathFriendzyHelper.TAB_WIDTH && 
				metrics.densityDpi == MathFriendzyHelper.TAB_DENISITY)
		{
			layout = R.layout.avtarlayout_low_denisity;
		}else{
			layout = R.layout.avtarlayout;
		}

		GridAdapter adapter = new GridAdapter(ChooseAvtars.this , layout ,avtarDataList);
		avtarGridView.setAdapter(adapter);

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " outside setWidgetsValue()");
	}

	/**
	 * This method get data from database for displaying avtar images
	 */
	private void getAvtarDataFromDatabase() 
	{
		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " inside getAvtarDataFromDatabase()");

		ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
		chooseAvtarObj.openConn(this);
		avtarDataList = chooseAvtarObj.getAvtarData();
		chooseAvtarObj.closeConn();

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " outside getAvtarDataFromDatabase()");
	}

	/**
	 * This method set the widgets references from the layout 
	 */
	private void setWidgetsReferences() 
	{
		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " inside setWidgetsReferences()");

		avtarGridView 		= (GridView) findViewById(R.id.avtarGridView);
		mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblChooseAnAvatar 	= (TextView) findViewById(R.id.lblChooseAnAvatar);
		lblYouHave        	= (TextView) findViewById(R.id.lblYouHave);
		btnTitleGetMoreCoins= (Button) findViewById(R.id.btnTitleGetMoreCoins);

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * This method set the text value from the transelation
	 */
	private void setWidgetsTextValues()
	{
		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " inside setWidgetsTextValues()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dialogTitle = transeletion.getTranselationTextByTextIdentifier("alertMsgSorryYouDoNotHaveThatManyCoins");
		dialogMsg   = transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins");
		noText      = transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks");
		yesText     = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");

		mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
		lblChooseAnAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseAnAvatar"));

		btnTitleGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));

		lblPleaseRegisterBeforeBuy = transeletion.getTranselationTextByTextIdentifier("lblPleaseRegisterBeforeBuy");
		lblPleaseRegisterBeforeGetCoins = transeletion.getTranselationTextByTextIdentifier("lblPleaseRegisterBeforeGetCoins");
		transeletion.closeConnection();

		if(CHOOSE_AVTAR_LOG)
			Log.e(TAG, " outside setWidgetsTextValues()");
	}

	private void setCoins()
	{
		if(MathFriendzyHelper.isTempraroryPlayerCreated(this)){
			TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
			if(!tempPlayer.isTempPlayerDeleted())
			{			
				PlayerTotalPointsObj playerObj = null;
				LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
				learningCenterimpl.openConn();
				tempPlayer = new TempPlayerOperation(this);
				playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(tempPlayer.getTempPlayerData().get(0).getPlayerId() +"");
				tempPlayer.closeConn();
				learningCenterimpl.closeConn();	

				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				lblYouHave.setText("(" + transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " + 
						CommonUtils.setNumberString(playerObj.getCoins() + "") + 
						" " + transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")");
				transeletion.closeConnection();
			}
			else
			{
				PlayerTotalPointsObj playerObj = null;
				LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
				learningCenterimpl.openConn();
				tempPlayer = new TempPlayerOperation(this);
				playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(EditRegisteredUserPlayer.playerId);
				tempPlayer.closeConn();
				learningCenterimpl.closeConn();

				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				lblYouHave.setText("(" + transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " + CommonUtils.setNumberString(playerObj.getCoins() + "") + 
						" " + transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")");
				transeletion.closeConnection();
			}
		}else{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			lblYouHave.setText("(" + transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " + 0 + 
					" " + transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")");
			transeletion.closeConnection();
		}
	}

	@Override
	protected void onRestart() 
	{
		//Log.e(TAG, "onRestart");
		this.setCoins();
		super.onRestart();
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{		
		case R.id.btnTitleGetMoreCoins:
			/*GetMoreCoins.isFromEditRegisteredUserPlayer = true;
				startActivity(new Intent(this , GetMoreCoins.class));*/
			if(isForNewRegistration){
				MathFriendzyHelper.warningDialog(this,lblPleaseRegisterBeforeGetCoins);
			}else{
				this.getUserDataFromServer();
			}
			break;
		}
	}

	/**
	 * This method get data from server 
	 */
	private void getUserDataFromServer()
	{
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			GetMoreCoins.isFromEditRegisteredUserPlayer = true;
			startActivity(new Intent(this , GetMoreCoins.class));
		}
		else
		{			
			UserRegistrationOperation userOprObj = new UserRegistrationOperation(ChooseAvtars.this);
			RegistereUserDto regUserObj = userOprObj.getUserData();

			if(regUserObj.getEmail().length() > 1)
			{
				new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
			}
			else
			{
				new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
			}
		}
	}

	/**
	 * This class save avtar status on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveAvtarStatus extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private String playerId;
		private String avterId;
		private int status;
		//private ProgressDialog pd;

		SaveAvtarStatus(String userId , String playerId , String avtarId , int status)
		{
			this.userId = userId;
			this.playerId = playerId;
			this.avterId = avtarId;
			this.status = status;
		}

		@Override
		protected void onPreExecute() {
			/*pd = CommonUtils.getProgressDialog(ChooseAvtars.this);
			pd.show();*/
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			AvtarServerOperation serverObj = new AvtarServerOperation();
			serverObj.saveAvtarOperationOnserver(userId, playerId, avterId, status);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//pd.cancel();
			super.onPostExecute(result);
		}
	}

	/**
	 * This class update coins on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveCoinsForPlayer extends AsyncTask<Void, Void, Void>
	{
		private String userId ; 
		private int coins;
		private String playerId;
		//ProgressDialog pd;

		SaveCoinsForPlayer(String userId , String playerId , int coins)
		{
			this.userId = userId;
			this.coins  = coins;
			this.playerId = playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			/*pd = CommonUtils.getProgressDialog(GetMoreCoins.this);
			pd.show();*/
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			InAppServerOperation serverObj = new InAppServerOperation();
			serverObj.saveCoinsForPlayer(userId, playerId, coins);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			//pd.cancel();
			super.onPostExecute(result);
		}
	}

	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByEmail extends AsyncTask<Void, Void, Void>
	{
		private ProgressDialog pd 	= null;
		private String email 		= null;
		private String pass 		= null;

		GetUserDetailByEmail(String email, String pass)
		{
			this.email = email;
			this.pass  = pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(ChooseAvtars.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(ChooseAvtars.this);
			login.getUserDetailByEmail(email, pass);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			UserRegistrationOperation userOprObj = new UserRegistrationOperation(ChooseAvtars.this);
			RegistereUserDto regUserObj = userOprObj.getUserData();

			if(regUserObj.getCoins().length() > 0){
				if(Integer.parseInt(regUserObj.getCoins()) > 0){
					GetMoreCoins.isFromEditRegisteredUserPlayer = true;
					Intent intent = new Intent(ChooseAvtars.this,CoinsDistributionActivity.class);
					startActivity(intent);
				}
				else{
					GetMoreCoins.isFromEditRegisteredUserPlayer = true;
					startActivity(new Intent(ChooseAvtars.this , GetMoreCoins.class));
				}
			}
			else{
				GetMoreCoins.isFromEditRegisteredUserPlayer = true;
				startActivity(new Intent(ChooseAvtars.this , GetMoreCoins.class));
			}

			super.onPostExecute(result);
		}
	}


	/**
	 * This Asyncktask get User Detail from server
	 * @author Yashwant Singh
	 *
	 */
	class GetUserDetailByUserId extends AsyncTask<Void, Void, Void>
	{
		private String userId 	= null;
		private String pass     	= null;
		private ProgressDialog pd 	= null;

		GetUserDetailByUserId(String userId , String pass)
		{
			this.userId = userId;
			this.pass     =  pass;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(ChooseAvtars.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Login login = new Login(ChooseAvtars.this);
			login.getUserDetailByUserId(userId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			pd.cancel();

			UserRegistrationOperation userOprObj = new UserRegistrationOperation(ChooseAvtars.this);
			RegistereUserDto regUserObj = userOprObj.getUserData();

			if(regUserObj.getCoins().length() > 0){
				if(Integer.parseInt(regUserObj.getCoins()) > 0){
					GetMoreCoins.isFromEditRegisteredUserPlayer = true;
					Intent intent = new Intent(ChooseAvtars.this,CoinsDistributionActivity.class);
					startActivity(intent);
				}
				else{
					GetMoreCoins.isFromEditRegisteredUserPlayer = true;
					startActivity(new Intent(ChooseAvtars.this , GetMoreCoins.class));
				}
			}
			else{
				GetMoreCoins.isFromEditRegisteredUserPlayer = true;
				startActivity(new Intent(ChooseAvtars.this , GetMoreCoins.class));
			}
			super.onPostExecute(result);
		}
	}

}
