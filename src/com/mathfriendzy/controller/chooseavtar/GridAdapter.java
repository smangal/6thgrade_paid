package com.mathfriendzy.controller.chooseavtar;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.AvtarDTO;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.sixthgradepaid.R;

/**
 * Grid Adapter class for set images of avtars which are taken from database are display by the grid adapter in 
 * the grid form
 * @author Yashwant Singh
 *
 */
public class GridAdapter extends BaseAdapter 
{
	private int resource 			 = 0;
	private LayoutInflater mInflater = null;
	//private Context context 		 = null;
	private ViewHolder vholder 		 = null;
	private ArrayList<AvtarDTO> avtarDataList = null;
	private ArrayList<String> avtarIdList = null;

	private String userId = "0";
	private String playerId = "0";

	public static boolean isAddNewPlayer = false;

	public GridAdapter(Context context,  int resource , ArrayList<AvtarDTO> avtarDataList)
	{	
		this.resource 	= resource;
		mInflater 		= LayoutInflater.from(context);
		//this.context 	= context;
		this.avtarDataList = avtarDataList;

		avtarIdList  = new ArrayList<String>();

		if(!isAddNewPlayer)
		{		
			try{
				if(MathFriendzyHelper.isTempraroryPlayerCreated(context)){
					TempPlayerOperation tempPlayer = new TempPlayerOperation(context);
					if(!tempPlayer.isTempPlayerDeleted()){
						userId   = "0";
						playerId = "0";
					}
					else{
						UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
						UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(EditRegisteredUserPlayer.playerId);

						if(userPlayerData != null){
							userId   = userPlayerData.getParentUserId();
							playerId = EditRegisteredUserPlayer.playerId;
						}
					}

					ChooseAvtarOpration opr = new ChooseAvtarOpration();
					opr.openConn(context);
					avtarIdList = opr.getAvtarIds(userId , playerId);
					opr.closeConn();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else
		{
			isAddNewPlayer = false;
		}
	}

	@Override
	public int getCount() 
	{
		return avtarDataList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{

		return 0;
	}

	@Override
	public View getView(int index, View view, ViewGroup viewGroup) 
	{		
		if(view == null)
		{
			vholder 			   = new ViewHolder();
			view 				   = mInflater.inflate(resource, null);
			vholder.txtAvtarName   = (TextView) view.findViewById(R.id.txtAvtarName);
			vholder.txtAvtarPrice  = (TextView) view.findViewById(R.id.txtPrice);
			vholder.imgAvtarImage  = (ImageView) view.findViewById(R.id.imgAvtarImage);
			view.setTag(vholder);			
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}

		vholder.txtAvtarName.setText(avtarDataList.get(index).getName());
		if(avtarDataList.get(index).getPrice().equals("0"))
			vholder.txtAvtarPrice.setText("FREE");
		else
		{
			if(isAvtarPurchased(avtarDataList.get(index).getId()))
			{
				vholder.txtAvtarPrice.setText("Purchased");
			}
			else
			{
				vholder.txtAvtarPrice.setText(avtarDataList.get(index).getPrice() + " coins");
			}
			/*vholder.txtAvtarPrice.setText(avtarDataList.get(index).getPrice() + " coins");*/
		}
		//vholder.imgAvtarImage.setImageBitmap(CommonUtils.getBitmapFromByte(avtarDataList.get(index).getImage() , context));
		vholder.imgAvtarImage.setImageBitmap(MathFriendzyHelper.getBitmapFromByte(avtarDataList.get(index).getImage()));

		return view;
	}


	static class ViewHolder
	{
		TextView  txtAvtarName ;
		ImageView imgAvtarImage;
		TextView  txtAvtarPrice;
	}

	private boolean isAvtarPurchased(String avtarId)
	{
		for(int i = 0 ; i < avtarIdList.size() ; i ++ )
		{
			if(avtarIdList.get(i).equals(avtarId))
				return true;
		}
		return false;
	}
}
