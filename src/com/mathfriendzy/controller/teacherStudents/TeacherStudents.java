package com.mathfriendzy.controller.teacherStudents;

import static com.mathfriendzy.utils.ICommonUtils.TAECHER_STUDENT_FLAG;

import java.util.ArrayList;
import java.util.Date;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.friendzy.TeacherChallengeActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.result.JsonAsyncTaskForScore;
import com.mathfriendzy.model.teacherStudents.GetTeacherStudents;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.sixthgradepaid.R;

/**
 * This Activity Shows the teacher user player when user login as a teacher
 * @author Yashwant Singh
 *
 */
public class TeacherStudents extends AdBaseActivity implements OnClickListener
{
	private TextView resultTitleMyStudents = null;
	private Button   btnTitleBack          = null;
	//private ListView lstTeacherStudents    = null;
	private String callingActivity         = null;
	
	private ProgressDialog progressDialog  = null;
	
	private TextView txtStudentdName   = null;
	private ImageView imgStudentDetail = null;
	private TextView  txtLine          = null;
	private ArrayList<RelativeLayout> layoutList = null;
	private final String TAG = this.getClass().getSimpleName();
	private int offSet = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teacher_students);
		
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside onCreate()");
		
		callingActivity = this.getIntent().getStringExtra("callingActivity");
		
		this.setWidgetsReferences();
		this.setTextValues();
		this.setListenerOnWidgets();
		
		layoutList = new ArrayList<RelativeLayout>();
		
		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto regUserObj = userObj.getUserData();
		
		if(CommonUtils.isInternetConnectionAvailable(this))
			new GetTeacherStudentsAsyncTask(regUserObj.getUserId(),offSet).execute(null,null,null);
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setWidgetsReferences()");
		
		resultTitleMyStudents = (TextView) findViewById(R.id.resultTitleMyStudents);
		btnTitleBack          = (Button)   findViewById(R.id.btnTitleBack);
		//lstTeacherStudents    = (ListView) findViewById(R.id.lstTeacherStudents);
		
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the widgets text values from the Traslation
	 */
	private void setTextValues()
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		resultTitleMyStudents.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setTextValues()");
	}

	/**
	 * this method set the listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		btnTitleBack.setOnClickListener(this);	
		
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
			case R.id.btnTitleBack :
				if(callingActivity.equals("TeacherPlayer"))
				{
					Intent intent = new Intent(this,TeacherPlayer.class);
					startActivity(intent);
				}
				else if(callingActivity.equals("CreateTeacherPlayerActivity"))
				{
					Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
					startActivity(intent);
				}
				else if(callingActivity.equals("TeacherChallengeActivity"))
				{
					Intent intent = new Intent(this,TeacherChallengeActivity.class);
					startActivity(intent);
					finish();
				}
				break;
		}
	}
	
		
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			if(callingActivity.equals("TeacherPlayer"))
			{
				Intent intent = new Intent(this,TeacherPlayer.class);
				startActivity(intent);
				finish();
			}
			else if(callingActivity.equals("CreateTeacherPlayerActivity"))
			{
				Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
				startActivity(intent);
				finish();
			}else if(callingActivity.equals("TeacherChallengeActivity"))
			{
				Intent intent = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intent);
				finish();
			}
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}



	/**
	 * This AsyncTask Get Teacher Student from the server 
	 * @author Yashwant Singh
	 *
	 */
	class GetTeacherStudentsAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String userId = null;
		private int offset    = 0;
		private ArrayList<UserPlayerDto> studentList = null;
		
		GetTeacherStudentsAsyncTask(String userId,int offset)
		{
			this.userId = userId;
			this.offset = offset;
		}
		
		@Override
		protected void onPreExecute() 
		{
			progressDialog = CommonUtils.getProgressDialog(TeacherStudents.this);
			progressDialog.show();
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			GetTeacherStudents teacherStudentObj = new GetTeacherStudents(TeacherStudents.this);
			studentList = teacherStudentObj.getStudents(userId, offset);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			progressDialog.cancel();
			
			ArrayList<String> studentnameList = new ArrayList<String>();
			for(int i = 0 ; i < studentList.size() ; i ++ )
			{ 
				studentnameList.add(studentList.get(i).getFirstname() + " " + studentList.get(i).getLastname());
			}
						
			/*StudentAdapter adapter = new StudentAdapter(TeacherStudents.this,R.layout.studentslistlayout,studentnameList);
			lstTeacherStudents.setAdapter(adapter);*/
			createDynamicLayoutForStudents(studentnameList , studentList);
			super.onPostExecute(result);
		}
	}
	
	
	/**
	 * This method creates the dyanmic layout to shoe the teacher user
	 * @param studentNameList
	 */
	private  void createDynamicLayoutForStudents(ArrayList<String> studentNameList , final ArrayList<UserPlayerDto> studentList)
	{
		int textSize;
		int height;
		if(MainActivity.isTab)
		{
			textSize = 22;
			height = 70;
		}
		else
		{
			textSize = 16;
			height	 = 50;
		}
		LinearLayout students = (LinearLayout) findViewById(R.id.lstTeacherStudents);
		
		for(int i = 0 ; i < studentNameList.size() ; i++)
		{
			RelativeLayout layout = new RelativeLayout(this);
			
			RelativeLayout.LayoutParams paramstxtName = new RelativeLayout.LayoutParams(
														RelativeLayout.LayoutParams.WRAP_CONTENT,
														height);
			paramstxtName.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
			//paramstxtName.setMargins(5, 0, 0, 0);	
			
			txtStudentdName = new TextView(this);
			txtStudentdName.setText(studentNameList.get(i));
			txtStudentdName.setTextColor(Color.BLACK);
			txtStudentdName.setId(1);
			txtStudentdName.setTextSize(textSize);
			
			layout.addView(txtStudentdName,paramstxtName);
			
			RelativeLayout.LayoutParams imgDetail = new RelativeLayout.LayoutParams(
														RelativeLayout.LayoutParams.WRAP_CONTENT,
														RelativeLayout.LayoutParams.WRAP_CONTENT);
			imgDetail.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
			//paramstxtName.setMargins(0, 0, 5, 0);
			
			imgStudentDetail = new ImageView(this);
			imgStudentDetail.setBackgroundResource(R.drawable.studentdetail);
			imgStudentDetail.setId(2);
						
			layout.addView(imgStudentDetail,imgDetail);
			
			RelativeLayout.LayoutParams imgLine = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,1);
			imgLine.addRule(RelativeLayout.BELOW,imgStudentDetail.getId());
			imgLine.setMargins(0, 2, 0, 0);
			
			txtLine = new TextView(this);
			txtLine.setBackgroundColor(Color.BLACK);
			layout.addView(txtLine,imgLine);
			layoutList.add(layout);
			
			layout.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{		
					for( int  j = 0 ; j < layoutList.size() ; j ++ )
					{
						if(v == layoutList.get(j))
						{
						   int playerid = Integer.parseInt(studentList.get(j).getPlayerid());
					       int userid  = Integer.parseInt(studentList.get(j).getParentUserId());
					       String playerName  = studentList.get(j).getFirstname()+" "
					         +studentList.get(j).getLastname();
					       Date date  = new Date();
					       
					       String zero1 = "";
					       String zero2 = "";
					       if(date.getMonth() < 9)
					        zero1 = "0";
					       if(date.getDate() < 9)
					        zero2 = "0";
					       String playDate  = (date.getYear()+1900) + "-"+zero1 + (date.getMonth() + 1)
					       +zero2+ "-"+date.getDate();
					       new JsonAsyncTaskForScore(TeacherStudents.this, playDate, userid, playerid, playerName, false)
					       .execute(null,null,null);
						}
					}
				}
			});
			
			students.addView(layout);
		}
		
		if(studentNameList.size() >= 30)//login for is student greater then 30 then show show more button
		{
			RelativeLayout.LayoutParams viewMore = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
																					RelativeLayout.LayoutParams.WRAP_CONTENT);
			viewMore.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
			
			RelativeLayout layout = new RelativeLayout(this);
			Button viewMoreButton = new Button(this);
			viewMoreButton.setText("Show More");
			viewMoreButton.setBackgroundResource(R.drawable.buttonalert);
			layout.addView(viewMoreButton);
			students.addView(layout);
			
			viewMoreButton.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(TeacherStudents.this);
					RegistereUserDto regUserObj = userObj.getUserData();
					
					if(CommonUtils.isInternetConnectionAvailable(TeacherStudents.this))
					{					
						offSet = offSet + 30;
						new GetTeacherStudentsAsyncTask(regUserObj.getUserId(),offSet).execute(null,null,null);
					}
					else
					{
						DialogGenerator dg = new DialogGenerator(TeacherStudents.this);
						Translation transeletion = new Translation(TeacherStudents.this);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
					
				}
			});
		}
	}
}
