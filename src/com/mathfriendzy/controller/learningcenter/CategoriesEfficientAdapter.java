package com.mathfriendzy.controller.learningcenter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.sixthgradepaid.R;

/**
 * This adapter set the operation categories to the list View in main learning center
 * @author Yashwant Singh
 *
 */
@SuppressLint("DefaultLocale")
public class CategoriesEfficientAdapter extends BaseAdapter  
{	
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	private boolean isTabSmall				   = false;
	private boolean[] isCheck;
	//private boolean isTabLarge				   = false;
	
	private ArrayList<LearningCenterTransferObj> categoryList 		= null;
	private ArrayList<LearningCenterTransferObj> newCategoryList 	= null;

	public CategoriesEfficientAdapter(Context context,  int resource , ArrayList<LearningCenterTransferObj> categoryList
			,ArrayList<LearningCenterTransferObj> newCategoryList, boolean[] isCheck)
	{
		this.resource 			= resource;
		mInflater 				= LayoutInflater.from(context);
		this.context 			= context;
		this.categoryList 		= categoryList;
		this.newCategoryList 	= newCategoryList;
		this.isCheck			= isCheck;
		
		isTabSmall				= context.getResources().getBoolean(R.bool.isTabSmall);
		//isTabLarge				= context.getResources().getBoolean(R.bool.isTabLarge);
	}
	
	@Override
	public int getCount() 
	{
		return categoryList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return 0;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public View getView(int index, View view, ViewGroup viewGroup) 
	{
		vholder 			   = new ViewHolder();
		if(view == null)
		{			
			view 				   = mInflater.inflate(resource, null);
			vholder.txtFunctionName = (TextView) view.findViewById(R.id.txtFunction);
			vholder.imgCheck		= (ImageView) view.findViewById(R.id.imgCheck);
			vholder.categoryLayout  = (RelativeLayout)view.findViewById(R.id.listLayoutForLearningCenter);
			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
		int bg_id = 0;
		int checkId = 0;
		
		if(isTabSmall)
		{
			bg_id = context.getResources().getIdentifier
					(categoryList.get(index).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_ipad", "drawable", 
							context.getPackageName());
			
			checkId = context.getResources().getIdentifier
					(categoryList.get(index).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_chk_ipad", "drawable", 
							context.getPackageName());
		}
		/*else if(isTabLarge)
		{
			bg_id = context.getResources().getIdentifier
					(categoryList.get(index).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_ipad2", "drawable", 
							context.getPackageName());
		}*/
		else
		{
			bg_id = context.getResources().getIdentifier
					(categoryList.get(index).getLearningCenterOperation().toLowerCase().replace(" ","_"), "drawable", 
							context.getPackageName());
			
			checkId = context.getResources().getIdentifier
					(categoryList.get(index).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_chk", "drawable", 
							context.getPackageName());
		}		
		
		if(isCheck[index])
		{
			vholder.imgCheck.setBackgroundResource(checkId);
		}
		else
		{
			vholder.imgCheck.setBackgroundResource(0);
		}
		vholder.categoryLayout.setBackgroundResource(bg_id);		
		
		vholder.txtFunctionName.setText(newCategoryList.get(index).getLearningCenterOperation());
	
		return view;
	}
	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtFunctionName ;
		ImageView imgCheck;
		RelativeLayout categoryLayout;
	}
}
