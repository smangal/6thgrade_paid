package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_decimal.Measurement;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_fraction.Money;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_negative_number.SocialStudies;
import com.mathfriendzy.controller.learningcenter.division.Time;
import com.mathfriendzy.controller.learningcenter.equationsolve.LearningCentreEquationForGrade;
import com.mathfriendzy.controller.learningcenter.multiplication.Language;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_decimals.Geography;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_fraction.Phonics;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_negative_number.Science;
import com.mathfriendzy.controller.learningcenter.reading.Reading;
import com.mathfriendzy.controller.learningcenter.subtraction.Volume;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyEquationActivity;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

public class SeeAnswerActivity extends AdBaseActivity implements OnClickListener, OnGestureListener
{
	private TextView mfTitleHomeScreen							= null;
	private TextView txtQueNo									= null;
	private TextView txtQue										= null;
	private TextView txtAnswer									= null;
	private TextView txtOption1									= null;
	private TextView txtOption2									= null;
	private TextView txtOption3									= null;

	private RelativeLayout ansLayout1							= null;
	private RelativeLayout ansLayout2							= null;
	private RelativeLayout ansLayout3							= null;
	private RelativeLayout showLayout							= null;

	private Button   	   btnPlayAgain							= null;
	private Button   	   btnPrevious 							= null;
	private Button   	   btnNext	 							= null;

	private ArrayList<LearningCenterTransferObj> equationList	= null;
	private LearningCenterTransferObj		transferObj			= null;
	private ArrayList<String>				userAnswerList		= null;		
	private SeeAnswerTransferObj 			seeAnswerDataObj	= null;

	private int 	MAX											= 0;
	private int 	questionNumber								= 0;
	private String	userAnswer									= null;
	private String	correctAnswer								= null;
	public static int isCallFromActivity						= 0;

	private GestureDetector gDetector							= null;
	private boolean isTab										= false;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see_answer);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{
			
			setContentView(R.layout.activity_see_answer_activity_low_tab);

		}
		

		isTab	= getResources().getBoolean(R.bool.isTabSmall);

		if(isCallFromActivity == 1)//1 for Learning center
		{
			seeAnswerDataObj = LearningCentreEquationForGrade.seeAnswerDataObj;
		}
		else if(isCallFromActivity == 2)//2 for Single Friendzy
		{			
			seeAnswerDataObj = SingleFriendzyEquationActivity.seeAnswerDataObj;
		}

		equationList			= seeAnswerDataObj.getEquationList();
		userAnswerList			= seeAnswerDataObj.getUserAnswerList();

		gDetector 	 = new GestureDetector(this); 

		setWidgetId();
		setTextOnWidgets();
		Translation translate = new Translation(this);
		translate.openConnection();		
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+
				translate.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));	
		translate.closeConnection();
	}//END onCreate method




	private void setTextOnWidgets() 
	{		
		if(userAnswerList != null)
		{
			if(questionNumber < userAnswerList.size() && userAnswerList.size() > 0)
			{
				MAX						= userAnswerList.size();
				showLayout.setVisibility(View.VISIBLE);
				if(userAnswerList.size() != 1)
				{
					btnNext.setVisibility(View.VISIBLE);
				}
				transferObj				= new LearningCenterTransferObj();
				transferObj				= equationList.get(questionNumber);
				correctAnswer 			= transferObj.getAnswer();

				txtQue.setText(transferObj.getQuestion());
				txtQueNo.setText(""+(questionNumber+1));
				txtAnswer.setText(transferObj.getAnswer());
				txtOption1.setText(transferObj.getOption1());
				txtOption2.setText(transferObj.getOption2());
				txtOption3.setText(transferObj.getOption3());

				if(isTab)
				{
					ansLayout1.setBackgroundResource(R.drawable.option2_ipad);
					ansLayout2.setBackgroundResource(R.drawable.option3_ipad);
					ansLayout3.setBackgroundResource(R.drawable.option4_ipad);
				}
				else
				{
					ansLayout1.setBackgroundResource(R.drawable.option2);
					ansLayout2.setBackgroundResource(R.drawable.option3);
					ansLayout3.setBackgroundResource(R.drawable.option4);
				}			

				userAnswer	= userAnswerList.get(questionNumber);
				if(!isAnswerCorrectByUser())
				{
					setWrongAnswerBackground();
				}			
			}
		}


	}//END setTextOnWidget method



	/**
	 * use to setBackgroung of wrong answer if given by user and tells about which answer user selected
	 */
	private void setWrongAnswerBackground()
	{		
		int wrong_bg;
		if(isTab)
		{
			wrong_bg = R.drawable.wrong_ipad;
		}
		else
		{
			wrong_bg = R.drawable.wrong;
		}
		if(txtOption1.getText().equals(userAnswer))
		{
			ansLayout1.setBackgroundResource(wrong_bg);
		}
		else if(txtOption2.getText().equals(userAnswer))
		{
			ansLayout2.setBackgroundResource(wrong_bg);
		}
		else if(txtOption3.getText().equals(userAnswer))
		{
			ansLayout3.setBackgroundResource(wrong_bg);
		}


	}//END setWrongAnswerBackground method


	private void clickOnPlay() 
	{
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0);

		switch(operationId)
		{
		case 1:
			startActivity(new Intent(this,Reading.class));
			break;
		case 2:
			startActivity(new Intent(this,Volume.class));
			break;
		case 3:
			startActivity(new Intent(this,Language.class));
			break;
		case 4:
			startActivity(new Intent(this,Time.class));
			break;
		case 5:
			startActivity(new Intent(this,Money.class));
			break;
		case 6:
			startActivity(new Intent(this,Phonics.class));
			break;
		case 7:
			startActivity(new Intent(this,Measurement.class));
			break;
		case 8:
			startActivity(new Intent(this,Geography.class));
			break;
		case 9:
			startActivity(new Intent(this,SocialStudies.class));
			break;
		case 10:
			startActivity(new Intent(this,Science.class));
			break;
		}

	}

	/**
	 * use to check answer tick by user when playing
	 * @return true if correct answer is checked otherwise false
	 */
	private boolean isAnswerCorrectByUser()
	{
		if(userAnswer.equals(correctAnswer))
			return true;
		else 
			return false;		

	}//END checkAnswer method




	private void setWidgetId() 
	{
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtAnswer			= (TextView) findViewById(R.id.txtOption1);
		txtOption1			= (TextView) findViewById(R.id.txtOption2);
		txtOption2			= (TextView) findViewById(R.id.txtOption3);
		txtOption3			= (TextView) findViewById(R.id.txtOption4);
		txtQue				= (TextView) findViewById(R.id.txtQue);
		txtQueNo			= (TextView) findViewById(R.id.txtQueNo);

		ansLayout1			= (RelativeLayout) findViewById(R.id.ansLayout2);
		ansLayout2			= (RelativeLayout) findViewById(R.id.ansLayout3);
		ansLayout3			= (RelativeLayout) findViewById(R.id.ansLayout4);
		showLayout			= (RelativeLayout) findViewById(R.id.showLayout);

		btnPlayAgain		= (Button) findViewById(R.id.btnPlayAgain);
		btnPrevious			= (Button) findViewById(R.id.btnPrevious);
		btnNext				= (Button) findViewById(R.id.btnNext);

		btnPrevious.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPlayAgain.setOnClickListener(this);

	}//END setWidgetId method




	@Override
	public void onClick(View v) 
	{		
		switch(v.getId())
		{		
		case R.id.btnPlayAgain :			
			clickOnPlayAgain();
			break;

		case R.id.btnPrevious :		
			btnNext.setVisibility(View.VISIBLE);			
			if(questionNumber != 0)
			{		
				questionNumber--;
				setTextOnWidgets();				
			}	
			if(questionNumber == 0)
			{
				btnPrevious.setVisibility(View.INVISIBLE);
			}
			break;

		case R.id.btnNext :	
			btnPrevious.setVisibility(View.VISIBLE);
			if(questionNumber != MAX - 1)
			{			
				questionNumber++;
				setTextOnWidgets();
			}
			if(questionNumber == MAX - 1)
			{
				btnNext.setVisibility(View.INVISIBLE);
			}
			break;
		}

	}

	/**
	 * This method call when click on play again
	 */
	private void clickOnPlayAgain()
	{
		switch(isCallFromActivity)
		{
		case 1:
			clickOnPlay();
			break;
		case 2:
			startActivity(new Intent(this,SingleFriendzyMain.class));
			break;
		}
	}



	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this, MainActivity.class));		
		super.onBackPressed();
	}


	/**************************|
	 Gesture Detector Methods
	 |*************************/
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY)
	{
		if (Math.abs(e1.getY() - e2.getY()) > 250)
			return false;
		// right to left swipe
		if(e1.getX() - e2.getX() > 120 && Math.abs(velocityX) > 200 )
		{
			btnPrevious.setVisibility(View.VISIBLE);
			if(questionNumber != MAX - 1)
			{			
				questionNumber++;
				setTextOnWidgets();
			}
			if(questionNumber == MAX - 1)
			{
				btnNext.setVisibility(View.INVISIBLE);
			}
			//Animation aniLeft = AnimationUtils.loadAnimation(this, R.anim.splash_animation_right);        	
			//view.setAnimation(aniLeft);

		}//Right Swipe
		else if (e2.getX() - e1.getX() > 120 && Math.abs(velocityX) > 200) 		
		{	
			btnNext.setVisibility(View.VISIBLE);			
			if(questionNumber != 0)
			{		
				questionNumber--;
				setTextOnWidgets();				
			}	
			if(questionNumber == 0)
			{
				btnPrevious.setVisibility(View.INVISIBLE);
			}			
			//Animation aniRight = AnimationUtils.loadAnimation(this, R.anim.splash_animation_left);			

		}		
		return true;

	}




	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}//END gesture detector methods


	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		return gDetector.onTouchEvent(event);
	}

}