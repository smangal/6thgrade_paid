package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.learningcenter.equationsolve.LearningCentreEquationForGrade;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusResponse;
import com.mathfriendzy.newinappclasses.OnPurchaseDone;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * Learning Center Base class for operation like as Addition,Subtraction,division etc
 * @author Yashwant Singh
 *
 */
abstract public class LearningCenterBase extends AdBaseActivity 
{ 
	protected Button btnBack 		  = null;
	protected TextView topBarText     = null;
	protected Button btnTop100        = null;
	protected ImageView imgPlayer     = null;
	protected TextView txtPlayerName  = null;
	protected TextView txtPlayerAddress= null;
	protected TextView txtPlayerPoints = null;
	protected TextView txtReachLavel  = null;
	protected TextView txtBonus       = null;

	protected ImageView ViewSnakes = null;
	protected Button imgLevel1     = null;
	protected Button imgLevel2     = null;
	protected Button imgLevel3     = null;
	protected Button imgLevel4     = null;
	protected Button imgLevel5     = null;
	protected Button imgLevel6     = null;
	protected Button imgLevel7     = null;
	protected Button imgLevel8     = null;
	protected Button imgLevel9     = null;
	protected Button imgLevel10    = null;
	protected Button imgLock    = null;

	protected TextView txtLavel1      = null;
	protected TextView txtLavel2      = null;
	protected TextView txtLavel3      = null;
	protected TextView txtLavel4      = null;
	protected TextView txtLavel5      = null;
	protected TextView txtLavel6      = null;
	protected TextView txtLavel7      = null;
	protected TextView txtLavel8      = null;
	protected TextView txtLavel9      = null;
	protected TextView txtLavel10     = null;

	protected RelativeLayout layout   	= null;
	protected Bitmap profileImageBitmap = null;
	protected ArrayList<PlayerEquationLevelObj> playerDataList = null;

	public static  ArrayList<LearningCenterTransferObj> categoriesList = null;
	public static  ArrayList<Integer> defaultCategoryIdList            = null;

	//private ArrayList<Integer> mathOperationIdList		= null;
	protected int highestLevel 	= 0;
	protected int appStatus    	= 0;


	//new InApp Change
	private ProgressDialog progressDialogForNewInApp = null;

	protected void setLayoutId()
	{
		setContentView(R.layout.layout_learning_base);
		this.init();
		/*boolean isTabSmall = getResources().getBoolean(R.bool.isTabSmall);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(isTabSmall)
		{
			if (metrics.densityDpi >= 300){
				setContentView(R.layout.activity_learning_center_operation_ipad);
			}
			else if((metrics.densityDpi < 300 && metrics.densityDpi > 160) || metrics.heightPixels <= 1000)
			{
				setContentView(R.layout.activity_learning_center_operation);
			}
			else
			{
				setContentView(R.layout.activity_learning_center_operation_for_tab_ten);
			}
		}
		else
		{
			if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels >= CommonUtils.TAB_WIDTH
					&& metrics.densityDpi == CommonUtils.TAB_DENISITY)  {

				setContentView(R.layout.activity_learning_center_operation_tab_low_denisity);
			}
			else if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi >= CommonUtils.SCREEN_DENSITY && metrics.widthPixels > CommonUtils.SCREEN_WIDTH)
			{
				setContentView(R.layout.activity_learning_center_operation_galaxy);
			}
		}*/
	}

	private void init(){
		try{
			progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this , "");
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	protected void checkInternetConnection()
	{
		if(!CommonUtils.isInternetConnectionAvailable(this))
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
					("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}


	/**
	 * This method set tha background of the learningCenter operation
	 */
	protected abstract void setBackGround();

	/**
	 * This method set the topbar text and topbar sign
	 */
	protected abstract void setTopBar();

	/**
	 * This method set the player detail
	 */
	protected void setPlayerDetail()
	{		
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		PlayerTotalPointsObj playerObj = null;
		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();
		if(sharedPreffPlayerInfo.getString("playerId", "0").equals(""))
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints("0");
		}
		else
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
					(sharedPreffPlayerInfo.getString("playerId", "0"));
		}
		learningCenterimpl.closeConn();			

		txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));

		if(sharedPreffPlayerInfo.getString("state", "").equals(""))
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", ""));
		}
		else
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", "")
					+ ", " + sharedPreffPlayerInfo.getString("state", ""));
		}

		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();

		txtPlayerPoints.setText(CommonUtils.setNumberString(playerObj.getTotalPoints()+"")
				+ " " + transeletion1.getTranselationTextByTextIdentifier("btnTitlePoints"));
		transeletion1.closeConnection();


		this.getHighestEquationLevel();

		if(highestLevel == 10)
		{
			txtReachLavel.setVisibility(TextView.INVISIBLE);
			txtBonus.setVisibility(TextView.INVISIBLE);
		}
		else
		{
			txtReachLavel.setVisibility(TextView.VISIBLE);			
			txtBonus.setVisibility(TextView.VISIBLE);
		}


		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl).execute(null,null,null);
			}/*
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}*/
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}

	/**
	 * This method set Widgets references from the layout to the widgets objects
	 */
	protected void setWidgetsReferences() 
	{		
		btnBack 			= (Button) 		findViewById(R.id.btnTitleBack);
		topBarText 			= (TextView) 	findViewById(R.id.labelTop);
		btnTop100 			= (Button) 		findViewById(R.id.btnTop100);
		imgPlayer 			= (ImageView) 	findViewById(R.id.imgSmiley);
		txtPlayerName 		= (TextView) 	findViewById(R.id.txtPlayerName);
		txtPlayerAddress 	= (TextView) 	findViewById(R.id.txtPlayerAddress);
		txtPlayerPoints 	= (TextView) 	findViewById(R.id.txtPlayerCoins);
		txtReachLavel 		= (TextView) 	findViewById(R.id.txtReachLevel);
		txtBonus			= (TextView) 	findViewById(R.id.txtBonus);

		ViewSnakes          = (ImageView)   findViewById(R.id.ViewSnakes);
		imgLevel1           = (Button) 	findViewById(R.id.imgViewLevel1);
		imgLevel2           = (Button) 	findViewById(R.id.imgViewLevel2);
		imgLevel3           = (Button) 	findViewById(R.id.imgViewLevel3);
		imgLevel4           = (Button) 	findViewById(R.id.imgViewLevel4);
		imgLevel5           = (Button) 	findViewById(R.id.imgViewLevel5);
		imgLevel6           = (Button) 	findViewById(R.id.imgViewLevel6);
		imgLevel7           = (Button) 	findViewById(R.id.imgViewLevel7);
		imgLevel8           = (Button) 	findViewById(R.id.imgViewLevel8);
		imgLevel9           = (Button) 	findViewById(R.id.imgViewLevel9);
		imgLevel10          = (Button) 	findViewById(R.id.imgViewLevel10);
		imgLock             = (Button)  findViewById(R.id.imgViewLock);

		txtLavel1           = (TextView)    findViewById(R.id.txtViewLevel1);
		txtLavel2           = (TextView)    findViewById(R.id.txtViewLevel2);
		txtLavel3           = (TextView)    findViewById(R.id.txtViewLevel3);
		txtLavel4           = (TextView)    findViewById(R.id.txtViewLevel4);
		txtLavel5           = (TextView)    findViewById(R.id.txtViewLevel5);
		txtLavel6           = (TextView)    findViewById(R.id.txtViewLevel6);
		txtLavel7           = (TextView)    findViewById(R.id.txtViewLevel7);
		txtLavel8           = (TextView)    findViewById(R.id.txtViewLevel8);
		txtLavel9           = (TextView)    findViewById(R.id.txtViewLevel9);
		txtLavel10          = (TextView)    findViewById(R.id.txtViewLevel10);

		layout              = (RelativeLayout) findViewById(R.id.layoutImgBackground);
	}

	/**
	 * This method set the Text values from the Translation
	 */
	protected void setTextValuesFromTranselation() 
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		//btnBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		btnTop100.setText(transeletion.getTranselationTextByTextIdentifier("lblRateMe"));
		txtReachLavel.setText(transeletion.getTranselationTextByTextIdentifier("lblReachLevel10AndGet5000Points"));
		txtLavel1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel3.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel4.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel5.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel6.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel7.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel8.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel9.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());
		txtLavel10.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel").toUpperCase());

		//btnTitleBonus, lblCoins
		txtBonus.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+"\n200 "+transeletion.getTranselationTextByTextIdentifier("lblCoins"));

		transeletion.closeConnection();
	}

	/**
	 * This method call when user click on level
	 * and start play
	 * @param level, operationId
	 */
	protected void clickOnLevel(int level, int operationId)
	{

		if(level >= 4)
		{
			if(appStatus == 1)
			{
				if(((level == 1 ) || (level <= highestLevel + 1)))
				{
					Intent i = new Intent(this, LearningCentreEquationForGrade.class);
					i.putExtra("operationId", operationId);
					i.putExtra("level", level);
					/*i.putExtra("queId", db.getTriviaEquationId(operationId, level));*/					
					startActivityForResult(i, RESULT_FIRST_USER);
				}
				else
				{					
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					DialogGenerator dg = new DialogGenerator(this);
					dg.generateWarningDialog(transeletion
							.getTranselationTextByTextIdentifier("alertMsgPlayThePreviousLevelToUnlockThis"));
					transeletion.closeConnection();
				}	
			}
			else
			{				
				SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
				operationId = sharedPreferences.getInt("operationId", 0);

				/*TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
				if(tempPlayer.isTempPlayerDeleted())
				{
					api = "itemId=" + operationId + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
							+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "")+"&appId=7";
				}
				else
				{
					api = "itemId=" + operationId+"&appId=7";
				}

				new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);*/ 	
				this.getRequiredCoinsForPurchase();
			}
		}
		else
		{
			if(((level == 1 ) || (level <= highestLevel + 1)))
			{	
				Intent i = new Intent(this, LearningCentreEquationForGrade.class);
				i.putExtra("operationId", operationId);
				i.putExtra("level", level);
				/*i.putExtra("queId", db.getTriviaEquationId(operationId, level));*/

				startActivityForResult(i, RESULT_FIRST_USER);
			}
			else
			{					
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPlayThePreviousLevelToUnlockThis"));
				transeletion.closeConnection();
			}	
		}
	}

	/*
	 *//**
	 * This method call when user click on level
	 * and start play
	 * @param level
	 */
	/*protected void clickOnLevel(int level)
	{
		startActivity(new Intent(this, LearningCentreEquationForGrade.class));

	 *//*******************Level conditions**************//*

		//highestLevel = this.getHighestEquationLevel();

		//Log.e("Learnign center base","inseide click on level " + highestLevel);

		if(level >= 4)
		{
			if(appStatus == 1)
			{
				if(((level == 1 ) || (level <= highestLevel + 1)))
				{
					SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
					int operationId = sharedPreferences.getInt("operationId", 0); 

					LearningCenterimpl learningCenter = new LearningCenterimpl(this);
					learningCenter.openConn();
					mathOperationIdList = learningCenter.getCategoryIdsFromMathEquationLeveCategory(operationId, level);
					learningCenter.closeConn();

					Intent intent = new Intent(this,LearningCenterEquationSolveWithTimer.class);
					intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);
					intent.putExtra("level", level);
					startActivityForResult(intent, 1);
				}
				else
				{					
						Translation transeletion = new Translation(this);
						transeletion.openConnection();
						DialogGenerator dg = new DialogGenerator(this);
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPlayThePreviousLevelToUnlockThis"));
						transeletion.closeConnection();
				}	
			}
			else
			{
				String api = null;
				SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
				int operationId = sharedPreferences.getInt("operationId", 0);

				SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

				TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
				if(tempPlayer.isTempPlayerDeleted())
				{
					api = "itemId=" + operationId + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
							+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");
				}
				else
				{
					api = "itemId=" + operationId;
				}

				new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null); 	
				this.getRequiredCoinsForPurchase();
			}
		}
		else
		{
			if(((level == 1 ) || (level <= highestLevel + 1)))
			{
				SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
				int operationId = sharedPreferences.getInt("operationId", 0); 

				LearningCenterimpl learningCenter = new LearningCenterimpl(this);
				learningCenter.openConn();
				mathOperationIdList = learningCenter.getCategoryIdsFromMathEquationLeveCategory(operationId, level);
				learningCenter.closeConn();

				Intent intent = new Intent(this,LearningCenterEquationSolveWithTimer.class);
				intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);
				intent.putExtra("level", level);
				startActivityForResult(intent, 1);
			}
			else
			{					
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					DialogGenerator dg = new DialogGenerator(this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPlayThePreviousLevelToUnlockThis"));
					transeletion.closeConnection();
			}	
		}
	}*/



	/**
	 * This method get required coins from server
	 */
	protected void getRequiredCoinsForPurchase()
	{
		/*String api = null;
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		String userId = sharedPreffPlayerInfo.getString("userId", "");
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");

		if(userId != "0" && playerId != "0")
		{
			api = "itemId=" + operationId + "&userId="+userId
					+"&playerId="+playerId;

		}
		else
		{
			api = "itemId=" + operationId;
		}

		new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);*/ 

		this.getAppUnlockCategoryPurchaseStatus();
	}

	/**
	 * This method get player level data from database
	 */
	protected void getPlayerLevelData() 
	{	
		int level = 0 ; 
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0); 

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		playerDataList = learningCenterObj.getPlayerEquationLevelDataByPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
		learningCenterObj.closeConn();

		for( int i  = 0 ; i < playerDataList.size() ; i ++ )
		{
			if(operationId == playerDataList.get(i).getEquationType())
			{
				level = playerDataList.get(i).getLevel();
				int star = playerDataList.get(i).getStars();
				setStarImageForLevel(level, star);
			}
		}

		/*if(playerDataList.size() > 0 )
		{
			if(operationId == playerDataList.get(playerDataList.size() - 1).getEquationType())
				this.setStarImageForLevel(playerDataList.size(), 0);
		}*/

		this.setStarImageForLevel(level + 1, 0);
		this.getHighestEquationLevel();
	}

	/**
	 * This method return the highest level for playing it
	 * @param playerId
	 * @param equationType
	 * @return
	 */
	protected void  getHighestEquationLevel()
	{
		//int highestLevel = 0;
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0); 

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();
		playerEquationObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
		playerEquationObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));
		playerEquationObj.setEquationType(operationId);

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		highestLevel =  learningCenterObj.getHighetsLevel(playerEquationObj);
		//Log.e("Learning : getHighLevel", "Highest Level "+highestLevel);
		learningCenterObj.closeConn();

		//return highestLevel;
	}


	protected int getApplicationUnLockStatus(int itemId,String userId)
	{	
		if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
			appStatus = 1;
			return appStatus;
		}

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	/**
	 * This method set the background snake image based on unlock application item id
	 * @param itemId
	 */
	protected void setBackGroundSnakeImage(int itemId)
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
		{
			if(highestLevel == 10)
				ViewSnakes.setBackgroundResource(R.drawable.lc_level_nobonus);
			else
				ViewSnakes.setBackgroundResource(R.drawable.lc_level_nolock);
		}
		else
		{	//Log.e(TAG, " inside set background status "  + this.getApplicationUnLockStatus(1));
			if(this.getApplicationUnLockStatus(itemId ,  sharedPreffPlayerInfo.getString("userId", "")) == 1)
			{
				if(highestLevel == 10)
					ViewSnakes.setBackgroundResource(R.drawable.lc_level_nobonus);
				else
					ViewSnakes.setBackgroundResource(R.drawable.lc_level_nolock);
			}
			else
			{
				if(highestLevel == 10)
					ViewSnakes.setBackgroundResource(R.drawable.lc_lock);
				else
					ViewSnakes.setBackgroundResource(R.drawable.lc_level);
			}
		}
	}


	/**
	 * This method call when click on top 100
	 */
	protected void clickonTop100()
	{
		/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
			transeletion.closeConnection();	
		}
		else
		{
			startActivity(new Intent(this,Top100Activity.class));
		}*/
		MathFriendzyHelper.rateUs(this);
	}

	/**
	 * This method set background of stars
	 * @param level
	 * @param stars
	 */
	protected void setStarImageForLevel(int level,int stars)
	{
		switch(level)
		{
		case 1 : 
			this.setBackGroundImages(level, stars, imgLevel1);
			break;
		case 2 : 
			this.setBackGroundImages(level, stars, imgLevel2);
			break;
		case 3 : 
			this.setBackGroundImages(level, stars, imgLevel3);
			break;
		case 4 : 
			this.setBackGroundImages(level, stars, imgLevel4);
			break;
		case 5 : 
			this.setBackGroundImages(level, stars, imgLevel5);
			break;
		case 6 : 
			this.setBackGroundImages(level, stars, imgLevel6);
			break;
		case 7 : 
			this.setBackGroundImages(level, stars, imgLevel7);
			break;
		case 8 : 
			this.setBackGroundImages(level, stars, imgLevel8);
			break;
		case 9 : 
			this.setBackGroundImages(level, stars, imgLevel9);
			break;
		case 10 : 
			this.setBackGroundImages(level, stars, imgLevel10);
			break;
		}

	}

	private void setBackGroundImages(int level, int stars , Button imgObj)
	{
		/*try 
		{
			imgObj.setBackgroundDrawable(Drawable.createFromStream(getAssets().open("starimages/star"+ level + 
					"_" + stars + ".png"), null));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}*/
		switch(stars)
		{
		case 1:
			imgObj.setBackgroundResource(R.drawable.lc_green_level_1star);
			break;
		case 2:
			imgObj.setBackgroundResource(R.drawable.lc_green_level_2star);
			break;
		case 3:	
			imgObj.setBackgroundResource(R.drawable.lc_green_level_3star);
			break;
		case 0:				
			imgObj.setBackgroundResource(R.drawable.lc_green_level);
			break;
		}

	}


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;

		GetRequiredCoinsForPurchaseItem(String apiValue)
		{
			this.apiString = apiValue;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(LearningCenterBase.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();			
			/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					coindFromServer.getCoinsPurchase() == 0){*/

			SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
			int operationId = sharedPreferences.getInt("operationId", 0); //itme id

			if(coindFromServer.getCoinsEarned() == -1)
			{				
				LearningCenterimpl learnignCenterObj = new LearningCenterimpl(LearningCenterBase.this);
				learnignCenterObj.openConn();
				DialogGenerator dg = new DialogGenerator(LearningCenterBase.this);
				dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
						coindFromServer , operationId, null);
				learnignCenterObj.closeConn();
			}
			else
			{					
				DialogGenerator dg = new DialogGenerator(LearningCenterBase.this);
				dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
						coindFromServer , operationId, null);
			}
			//}
			/*else
			{
				DialogGenerator dg = new DialogGenerator(LearningCenterBase.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/

			super.onPostExecute(result);
		}
	}

	private void getAppUnlockCategoryPurchaseStatus() {
		//new InApp Changes
		if(!CommonUtils.isInternetConnectionAvailable(this)){
			CommonUtils.showInternetDialog(this);
			return ;
		}

		MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
		MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
				MathFriendzyHelper.getUserId(this) , false , new HttpResponseInterface() {
			@Override
			public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
				if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
					MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
					return ;
				}
				GetAppUnlockStatusResponse response =
						(GetAppUnlockStatusResponse) httpResponseBase;
				if(response.getResult()
						.equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
						&& response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
					MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
					updateUIAfterPurchaseSuccess();
					return ;
				}

				MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
						MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER ,
						new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase,
							int requestCode) {
						MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
						if(requestCode ==
								MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
							return ;
						}
						updateUIAfterPurchaseSuccess();
					}
				} , false , response , new OnPurchaseDone() {
					@Override
					public void onPurchaseDone(boolean isDone) {
						if(isDone) {
							updateUIAfterPurchaseSuccess();
						}
					}
				});
			}
		});		
	}

	private void updateUIAfterPurchaseSuccess(){
		this.setBackGroundSnakeImage(100);
	}

	private Activity getCurrentActivityObj(){
		return this;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch (requestCode){                
			case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
				this.updateUIAfterPurchaseSuccess();
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
