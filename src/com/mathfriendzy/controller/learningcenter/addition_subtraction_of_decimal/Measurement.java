package com.mathfriendzy.controller.learningcenter.addition_subtraction_of_decimal;

import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_OPR;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.mathfriendzy.controller.learningcenter.LearningCenterBase;
import com.mathfriendzy.controller.learningcenter.LearningCenterMain;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

/**
 * This Class perform the addition subtraction operation on decimals 
 * @author Yashwant Singh
 *
 */
public class Measurement  extends LearningCenterBase implements OnClickListener
{
	private boolean isTabSmall				   = false;
	private boolean isTabLarge				   = false;
	
	private final String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setLayoutId();
		isTabSmall = getResources().getBoolean(R.bool.isTabSmall);
		
		
		this.checkInternetConnection();
		this.setWidgetsReferences();
		this.setTextValuesFromTranselation();
		this.setPlayerDetail();
		this.setBackGround();
		this.setTopBar();
		this.setListenerOnWidgets();
		this.getPlayerLevelData();
		
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "outside onCreate()");
	}

	@Override
	protected void setBackGround() 
	{
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "inside setBackGround()");
				
		if(isTabSmall || isTabLarge)
		{
			layout.setBackgroundResource(R.drawable.science_bg_ipad);
		}
		/*else if(isTabLarge)
		{
			layout.setBackgroundResource(R.drawable.measurement_bg_ipad2);
		}*/
		else
		{
			layout.setBackgroundResource(R.drawable.science_bg);
		}
		
		this.setBackGroundSnakeImage(7);
				
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "outside setBackGround()");
	}

	@Override
	protected void setTopBar() 
	{
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "inside setTopBar()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		topBarText.setText(transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_MEASUREMENT));
		transeletion.closeConnection();
		
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "outside setTopBar()");
	}

	/**
	 * This method set listener on Widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		btnBack.setOnClickListener(this);
		btnTop100.setOnClickListener(this);
		imgLevel1.setOnClickListener(this);
		imgLevel2.setOnClickListener(this);
		imgLevel3.setOnClickListener(this);
		imgLevel4.setOnClickListener(this);
		imgLevel5.setOnClickListener(this);
		imgLevel6.setOnClickListener(this);
		imgLevel7.setOnClickListener(this);
		imgLevel8.setOnClickListener(this);
		imgLevel9.setOnClickListener(this);
		imgLevel10.setOnClickListener(this);
		imgLock.setOnClickListener(this);
		
		if(LEARNING_CENTER_OPR)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intent = new Intent(this,LearningCenterMain.class);
			startActivity(intent);
			return false;
		}
		return super.onKeyDown(keyCode, event);
		
	}
	
	@Override
	public void onClick(View v) 
	{	
			switch(v.getId())
			{
			case R.id.btnTitleBack:
				Intent intent = new Intent(this,LearningCenterMain.class);
				startActivity(intent);
				break;
			case R.id.btnTop100:
				this.clickonTop100();
				break;
			
			case R.id.imgViewLevel1:
				this.clickOnLevel(1,7);
				break;
			case R.id.imgViewLevel2:
				this.clickOnLevel(2,7);
				break;
			case R.id.imgViewLevel3:
				this.clickOnLevel(3,7);
				break;
			case R.id.imgViewLevel4:
				this.clickOnLevel(4,7);
				break;
			case R.id.imgViewLevel5:
				this.clickOnLevel(5,7);
				break;
			case R.id.imgViewLevel6:
				this.clickOnLevel(6,7);
				break;
			case R.id.imgViewLevel7:
				this.clickOnLevel(7,7);
				break;
			case R.id.imgViewLevel8:
				this.clickOnLevel(8,7);
				break;
			case R.id.imgViewLevel9:
				this.clickOnLevel(9,7);
				break;
			case R.id.imgViewLevel10:
				this.clickOnLevel(10,7);
				break;
			case R.id.imgViewLock :
				this.getRequiredCoinsForPurchase();
				break;
			}
	}
	
	/**
	 * This method call when user click on level
	 * and start play
	 * @param level
	 *//*
	private void clickOnLevel(int level)
	{
		ProgressDialog pg = new ProgressDialog(this);
		pg.show();
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0); 
		
		LearningCenterimpl learningCenter = new LearningCenterimpl(this);
		learningCenter.openConn();
		mathOperationIdList = learningCenter.getCategoryIdsFromMathEquationLeveCategory(operationId, level);
		learningCenter.closeConn();
		
		pg.cancel();
		
		Intent intent = new Intent(this,LearningCenterEquationSolveWithTimer.class);
		intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);
		startActivityForResult(intent, 1);
				
	}
	*/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generatetimeSpentDialog();
				
			}
		}
		this.setPlayerDetail();
		super.onActivityResult(requestCode, resultCode, data);
	}
}
