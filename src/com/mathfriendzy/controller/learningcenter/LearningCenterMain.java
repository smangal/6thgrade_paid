package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_MAIN_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.LBL_GEOGRAPHY;
import static com.mathfriendzy.utils.ITextIds.LBL_LANGUAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_MATH;
import static com.mathfriendzy.utils.ITextIds.LBL_MEASUREMENT;
import static com.mathfriendzy.utils.ITextIds.LBL_MONEY;
import static com.mathfriendzy.utils.ITextIds.LBL_PHONICS;
import static com.mathfriendzy.utils.ITextIds.LBL_READING;
import static com.mathfriendzy.utils.ITextIds.LBL_SCIENCE;
import static com.mathfriendzy.utils.ITextIds.LBL_SOCIAL;
import static com.mathfriendzy.utils.ITextIds.LBL_VOCABULARY;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_decimal.Measurement;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_fraction.Money;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_negative_number.SocialStudies;
import com.mathfriendzy.controller.learningcenter.division.Time;
import com.mathfriendzy.controller.learningcenter.multiplication.Language;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_decimals.Geography;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_fraction.Phonics;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_negative_number.Science;
import com.mathfriendzy.controller.learningcenter.reading.Reading;
import com.mathfriendzy.controller.learningcenter.subtraction.Volume;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.database.LocalPlayerDatabase;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.sixthgradepaid.R;

/**
 * this is main Activity for learning center where all the categiries are display 
 * user can select any ne of them go to next for play
 * @author Yashwant Singh
 *
 */
public class LearningCenterMain extends AdBaseActivity implements OnClickListener
{
	private Button btnBack 							= null;
	private Button btnTop100 						= null;
	private TextView txtLearningCenterTop 			= null;
	private TextView chooseCategory         		= null;
	private ListView lstLearningCenterCategories 	= null;
	private int selectedPossition                   = 0;
	private String playerId;
	private String userId;
	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = null;

	private final String TAG = this.getClass().getSimpleName();

	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList = null;

	SharedPreferences sharedPreferences = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning_center_main);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside onCreate()");
		LocalPlayerDatabase db = new LocalPlayerDatabase();
		db.createLocalPlayerTable(this);
		
		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getFunctionalList();
		this.setListenerOnWidgets();

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets Objects 
	 */
	private void setWidgetsReferences()
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		btnBack   				= (Button) findViewById(R.id.btnTitleBack);
		btnTop100 				= (Button) findViewById(R.id.btnTop100);
		txtLearningCenterTop    = (TextView) findViewById(R.id.learningCenterTop);
		chooseCategory          = (TextView) findViewById(R.id.labelChooseCategory);
		lstLearningCenterCategories = (ListView) findViewById(R.id.listLearningCenter);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * This method set the textValues from the etranslation
	 */
	private void setWidgetsTextValues()
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		btnBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		btnTop100.setText(transeletion.getTranselationTextByTextIdentifier("lblRateMe"));
		txtLearningCenterTop.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
		chooseCategory.setText(transeletion.getTranselationTextByTextIdentifier("mfLblSelectEquationsType") + ".");
		transeletion.closeConnection();

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setListenerOnWidgets()");	

		btnBack.setOnClickListener(this);
		btnTop100.setOnClickListener(this);

		lstLearningCenterCategories.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,long id) 
			{				
				sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);

				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.clear();
				editor.putString("bgImageName", laernignCenterFunctionsList.get(position)
						.getLearningCenterOperation());
				editor.putString("topBarText", laernignCenterFunctionsList1.get(position)
						.getLearningCenterOperation());
				editor.putString("topBarSign", laernignCenterFunctionsList.get(position)
						.getLearningCenterOperation().toLowerCase().replace(" ","_")+"_sign");
				editor.putInt("operationId", laernignCenterFunctionsList.get(position)
						.getLearningCenterMathOperationId());
				editor.commit();

				selectedPossition = position;
				//TempPlayerOperation tempPlayer = new TempPlayerOperation(LearningCenterMain.this);
				SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

				if(sheredPreference.getBoolean(IS_LOGIN, false))
				{	
					if(CommonUtils.isInternetConnectionAvailable(LearningCenterMain.this))
					{
						saveLevelStartToServer();
						//Log.e("LearningCentermain", "Inside Server");
						/*new PlayerDataFromServer(sharedPreffPlayerInfo.getString("userId", ""), 
								sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);*/
					}
					else
					{
						/*DialogGenerator dg = new DialogGenerator(LearningCenterMain.this);
						Translation transeletion = new Translation(LearningCenterMain.this);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();*/
						operationSelectShowActivity();
					}

				}
				else
				{
					operationSelectShowActivity();
				}

			}
		});

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setListenerOnWidgets()");
	}


	private void operationSelectShowActivity()
	{
		switch(laernignCenterFunctionsList.get(selectedPossition).getLearningCenterMathOperationId())
		{
		case 1:
			Intent intentAddition = new Intent(LearningCenterMain.this,Reading.class);
			startActivity(intentAddition);
			break;
		case 2:
			Intent intentSubtraction = new Intent(LearningCenterMain.this,Volume.class);
			startActivity(intentSubtraction);
			break;
		case 3:
			Intent intentMultiPlication = new Intent(LearningCenterMain.this,Language.class);
			startActivity(intentMultiPlication);
			break;
		case 4:
			Intent intentDivision = new Intent(LearningCenterMain.this,Time.class);
			startActivity(intentDivision);
			break;
		case 5:
			Intent intentAdditionSubtractionOffraction = new Intent(LearningCenterMain.this,Money.class);
			startActivity(intentAdditionSubtractionOffraction);
			break;
		case 6:
			Intent intentMultiPlicationDivisionOfFraction = new Intent(LearningCenterMain.this,Phonics.class);
			startActivity(intentMultiPlicationDivisionOfFraction);
			break;
		case 7:
			Intent intentadditionSubtractionOfDecimal = new Intent(LearningCenterMain.this,Measurement.class);
			startActivity(intentadditionSubtractionOfDecimal);
			break;
		case 8:
			Intent intentMultiplicatioDivisionOFDecimal = new Intent(LearningCenterMain.this,Geography.class);
			startActivity(intentMultiplicatioDivisionOFDecimal);
			break;
		case 9:
			Intent intentAdditionSubtractionOfNegatives = new Intent(LearningCenterMain.this,SocialStudies.class);
			startActivity(intentAdditionSubtractionOfNegatives);
			break;
		case 10:
			Intent intentMultiplicatioDivisionOfNegatives = new Intent(LearningCenterMain.this,Science.class);
			startActivity(intentMultiplicatioDivisionOfNegatives);
			break;
		}
	}


	/**
	 * This method get function categories list from the database and set it to the list view
	 */
	private void getFunctionalList() 
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside getFunctionalList()");	

		laernignCenterFunctionsList = new ArrayList<LearningCenterTransferObj>();
		laernignCenterFunctionsList1 = new ArrayList<LearningCenterTransferObj>();
		LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
		laerningCenter.openConn();
		laernignCenterFunctionsList = laerningCenter.getLearningCenterFunctions();
		laerningCenter.closeConn();

		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for(int i = 0; i < laernignCenterFunctionsList.size() ; i ++ )
		{
			LearningCenterTransferObj lc = new LearningCenterTransferObj();

			if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Geography"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_READING));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Geometry and Fractions"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MATH));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Language Arts"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_LANGUAGE));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Math"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_VOCABULARY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Measurement and Data"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MONEY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Reading"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_PHONICS));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Science"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MEASUREMENT));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Vocabulary"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_GEOGRAPHY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("World Cultures"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SOCIAL));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Miscellaneous"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SCIENCE));

			laernignCenterFunctionsList1.add(lc);
		}

		transeletion.closeConnection();
		/**
		 * For checking is level completed or not till 10th level
		 */
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();
		
		playerEquationObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
		playerEquationObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));
		
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		boolean[] isCheck = new boolean[10];
		isCheck = learningCenterObj.getHighestLevelForCheck(playerEquationObj);
		learningCenterObj.closeConn();
		
		//set function list to the adapter
		CategoriesEfficientAdapter adapter = new CategoriesEfficientAdapter(this, R.layout.list_choose_categories, 
				laernignCenterFunctionsList, laernignCenterFunctionsList1, isCheck);
		
		lstLearningCenterCategories.setAdapter(adapter);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside getFunctionalList()");	
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnTitleBack:
			Intent intent = new Intent(this,MainActivity.class);
			startActivity(intent);
			break;
		case R.id.btnTop100:
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			MathFriendzyHelper.rateUs(this);
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intent = new Intent(this,MainActivity.class);
			startActivity(intent);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This class get player data from server
	 * @author Yashwant Singh
	 *
	 */
	class PlayerDataFromServer extends AsyncTask<Void, Void, PlayerDataFromServerObj>
	{
		private ProgressDialog pd;
		private String userId;
		private String playerId;

		PlayerDataFromServer(String userId , String playeId)
		{
			this.playerId = playeId;
			this.userId   = userId;

		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(LearningCenterMain.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected PlayerDataFromServerObj doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenter = new LearningCenteServerOperation();
			PlayerDataFromServerObj playerDataFromServer = learnignCenter.getPlayerDetailFromServer(userId, playerId);
			return playerDataFromServer;
		}

		@Override
		protected void onPostExecute(PlayerDataFromServerObj result) 
		{
			pd.cancel();
			if(result != null)
			{
				String itemIds = "";
				if(result.getItems().length() > 0)
					itemIds = result.getItems().replace(",", "");

				ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();

				for( int i = 0 ; i < itemIds.length() ; i ++ )
				{
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(Integer.parseInt(itemIds.charAt(i) + ""));
					purchseObj.setStatus(1);
					purchaseItem.add(purchseObj);
				}

				if(result.getLockStatus() != -1)
				{
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(100);
					purchseObj.setStatus(result.getLockStatus());
					purchaseItem.add(purchseObj);
				}

				LearningCenterimpl learningCenter = new LearningCenterimpl(LearningCenterMain.this);
				learningCenter.openConn();
				learningCenter.deleteFromPlayerEruationLevel();

				for( int i = 0 ; i < result.getPlayerLevelList().size() ; i ++ )
				{
					learningCenter.insertIntoPlayerEquationLevel(result.getPlayerLevelList().get(i));
				}

				if(learningCenter.isPlayerRecordExits(playerId))
				{
					learningCenter.updatePlayerPoints(result.getPoints(), playerId);
				}
				else
				{
					PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
					playerTotalPoints.setUserId(userId);
					playerTotalPoints.setPlayerId(playerId);
					playerTotalPoints.setCoins(0);
					playerTotalPoints.setCompleteLevel(0);
					playerTotalPoints.setPurchaseCoins(0);
					playerTotalPoints.setTotalPoints(result.getPoints());
					learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
				}

				learningCenter.deleteFromPurchaseItem();
				learningCenter.insertIntoPurchaseItem(purchaseItem);		
				learningCenter.closeConn();
			}
			operationSelectShowActivity();
			super.onPostExecute(result);
		}
	}
	
	/**
	 * use to save local data to server and fetch new data from server
	 */
	private void saveLevelStartToServer()
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerId = sharedPreffPlayerInfo.getString("playerId", "");
		userId	 = sharedPreffPlayerInfo.getString("userId", "");
		LocalPlayerDatabase db = new LocalPlayerDatabase();
		db.openConn(this);
		ArrayList<PlayerEquationLevelObj> playerquationData = db.getPlayerEquationLevelDataByPlayerId(playerId);
		db.deleteFromLocalTable();
		db.closeConn();
		if(playerquationData.size() != 0)
		{
			new AddLevelAsynTask(playerquationData).execute(null, null, null);
		}
		else
		{
			new PlayerDataFromServer(sharedPreffPlayerInfo.getString("userId", ""), 
					sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
		}

	}

	/**
	 * Update Player level data
	 * @author Yashwant Singh
	 *
	 */
	private class AddLevelAsynTask extends AsyncTask<Void, Void, Void>
	{
		private ArrayList<PlayerEquationLevelObj> playerquationData = null;

		AddLevelAsynTask(ArrayList<PlayerEquationLevelObj> playerquationData)
		{
			this.playerquationData = playerquationData;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
			serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");	
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			new PlayerDataFromServer(userId,playerId).execute(null,null,null);
			super.onPostExecute(result);
		}
	}

	private String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData)
	{
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ; i < playerquationData.size() ; i ++ )
		{
			xml.append("<userLevel>" +
					"<uid>"+playerquationData.get(i).getUserId()+"</uid>"+
					"<pid>"+playerquationData.get(i).getPlayerId()+"</pid>"+
					"<eqnId>"+playerquationData.get(i).getEquationType()+"</eqnId>"+
					"<level>"+playerquationData.get(i).getLevel()+"</level>"+
					"<stars>"+playerquationData.get(i).getStars()+"</stars>"+
					"</userLevel>");
		}

		return xml.toString();
	}

}
