package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_MAIN_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.MULTIFRIENDZY_PROBLEM_TYPE;
import static com.mathfriendzy.utils.ITextIds.LBL_GEOGRAPHY;
import static com.mathfriendzy.utils.ITextIds.LBL_LANGUAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_MATH;
import static com.mathfriendzy.utils.ITextIds.LBL_MEASUREMENT;
import static com.mathfriendzy.utils.ITextIds.LBL_MONEY;
import static com.mathfriendzy.utils.ITextIds.LBL_PHONICS;
import static com.mathfriendzy.utils.ITextIds.LBL_READING;
import static com.mathfriendzy.utils.ITextIds.LBL_SCIENCE;
import static com.mathfriendzy.utils.ITextIds.LBL_SOCIAL;
import static com.mathfriendzy.utils.ITextIds.LBL_VOCABULARY;

import java.util.ArrayList;
import java.util.Random;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyImpl;
import com.sixthgradepaid.R;
/**
 * This class set problem type with its difficulty
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyProblemType extends AdBaseActivity implements OnClickListener
{
	private TextView labelTop 						= null;
	private TextView txtChooseProblemType		 	= null;
	private Button   btnProblem1					= null;
	private Button   btnProblem2					= null;
	private Button   btnProblem3					= null;

	private ArrayList<LearningCenterTransferObj> operationList 		= null;
	private ArrayList<LearningCenterTransferObj> operationListNew	= null;

	private int operationId1						= 0;
	private int operationId2						= 0;
	private int operationId3						= 0;
	private boolean isTab							= false;

	private final String TAG = this.getClass().getSimpleName();	

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_problem_type);
		
		isTab  =  getResources().getBoolean(R.bool.isTabSmall);
		
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside onCreate()");		

		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.getFunctionalList();

		setRandomProblem(operationListNew);

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside onCreate()");

	}//END onCreate method



	/**
	 * use to set 3 problems randomly
	 * @param operationListNew2
	 */
	private void setRandomProblem(ArrayList<LearningCenterTransferObj> operationList) 
	{
		int nextVal;
		Random random = new Random();
		
		nextVal = random.nextInt(operationList.size());		
		//CommonUtils.setLearningTitle(operationList.get(nextVal).getOperationId());
		btnProblem1.setText(operationList.get(nextVal).getLearningCenterOperation());
		btnProblem1.setBackgroundResource(operationList.get(nextVal).getOperationBackgroundImg());
		operationId1 = operationList.get(nextVal).getLearningCenterMathOperationId();		
		operationList.remove(nextVal);

		nextVal = random.nextInt(operationList.size());		
		btnProblem2.setText(operationList.get(nextVal).getLearningCenterOperation());
		btnProblem2.setBackgroundResource(operationList.get(nextVal).getOperationBackgroundImg());
		operationId2 = operationList.get(nextVal).getLearningCenterMathOperationId();		
		operationList.remove(nextVal);

		nextVal = random.nextInt(operationList.size());		
		btnProblem3.setText(operationList.get(nextVal).getLearningCenterOperation());
		btnProblem3.setBackgroundResource(operationList.get(nextVal).getOperationBackgroundImg());
		operationId3 = operationList.get(nextVal).getLearningCenterMathOperationId();		
		operationList.remove(nextVal);
	}


	/**
	 * This  method set the widgets references from the layout
	 */
	private void setWidgetsReferences() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 		= (TextView) findViewById(R.id.labelTop);		
		btnProblem2		= (Button) findViewById(R.id.btnProblem2);
		btnProblem3		= (Button) findViewById(R.id.btnProblem3);
		btnProblem1		= (Button) findViewById(R.id.btnProblem1);

		txtChooseProblemType 	= (TextView) findViewById(R.id.txtChooseProblemType);		

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setWidgetsReferences()");
	}


	/**
	 * This method set the text from translation table
	 */
	private void setTextFromTranslation() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseProblemType"));		
		txtChooseProblemType.setText(transeletion.getTranselationTextByTextIdentifier("mfLblSelectEquationsType")+":");	

		transeletion.closeConnection();
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * This method set the widgets references from layout
	 */
	private void setListenerOnWidgets() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnProblem1.setOnClickListener(this);
		btnProblem2.setOnClickListener(this);
		btnProblem3.setOnClickListener(this);

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}



	/**
	 * This method get function categories list from the database and set it to the list view
	 */
	private void getFunctionalList() 
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside getFunctionalList()");	

		operationList			= new ArrayList<LearningCenterTransferObj>();
		operationListNew 		= new ArrayList<LearningCenterTransferObj>();
		LearningCenterimpl laerningCenter	= new LearningCenterimpl(this);
		laerningCenter.openConn();
		operationList = laerningCenter.getLearningCenterFunctions();
		laerningCenter.closeConn();
		operationList.get(0).getOperationId();
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for(int i = 0; i < operationList.size() ; i ++ )
		{
			LearningCenterTransferObj lc = new LearningCenterTransferObj();
			lc = operationList.get(i);
			int bg_id;
			
			if(isTab)
			{
				bg_id = getResources().getIdentifier
						(operationList.get(i).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_ipad",
								"drawable", 
								getPackageName());
			}			
			else
			{
				bg_id = getResources().getIdentifier
						(operationList.get(i).getLearningCenterOperation().toLowerCase().replace(" ","_"),
								"drawable", 
								getPackageName());
			}
			lc.setOperationBackgroundImg(bg_id);
			
			if(operationList.get(i).getLearningCenterOperation().contains("Geography"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_READING));
			else if(operationList.get(i).getLearningCenterOperation().contains("Geometry and Fractions"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MATH));
			else if(operationList.get(i).getLearningCenterOperation().contains("Language Arts"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_LANGUAGE));
			else if(operationList.get(i).getLearningCenterOperation().contains("Math"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_VOCABULARY));
			else if(operationList.get(i).getLearningCenterOperation().contains("Measurement and Data"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MONEY));
			else if(operationList.get(i).getLearningCenterOperation().contains("Reading"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_PHONICS));
			else if(operationList.get(i).getLearningCenterOperation().contains("Science"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MEASUREMENT));
			else if(operationList.get(i).getLearningCenterOperation().contains("Vocabulary"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_GEOGRAPHY));
			else if(operationList.get(i).getLearningCenterOperation().contains("World Cultures"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SOCIAL));
			else if(operationList.get(i).getLearningCenterOperation().contains("Miscellaneous"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SCIENCE));
			
			operationListNew.add(lc);
		}

		transeletion.closeConnection();


		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside getFunctionalList()");	
	}


	@Override
	public void onClick(View v) 
	{
		Intent intent = new Intent(this, MultiFriendzyEquationActivity.class);
		switch(v.getId())
		{		
		case R.id.btnProblem1:
			//Log.e("ProblemType", "operation  "+operationId1);			
			intent.putExtra("operationId", operationId1);
			break;
			
		case R.id.btnProblem2:
			//Log.e("ProblemType", "operation  "+operationId2);
			intent.putExtra("operationId", operationId2);
			break;
			
		case R.id.btnProblem3:
			//Log.e("ProblemType", "operation  "+operationId3);
			intent.putExtra("operationId", operationId3);
			break;
			
		}
		startActivity(intent);
	}




	/**
	 * This method call when player click on go button to play round
	 */
	private void clickOnGo()
	{

		MultiFriendzyImpl multifrinedzyImpl = new MultiFriendzyImpl(this);
		multifrinedzyImpl.openConn();
		/*ArrayList<Integer> mathOperationIdList = multifrinedzyImpl
				.getEquationCategoryId(categoryIdList, this.getDifficultyId());
		multifrinedzyImpl.closeConn();

		Intent intent = new Intent(this , MultiFriendzyEquationSolve.class);
		intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);*/

	}


}
