package com.mathfriendzy.controller.multifriendzy.findbyuser;

import static com.mathfriendzy.utils.ITextIds.BTN_GO;
import static com.mathfriendzy.utils.ITextIds.LBL_SEARCH_USER;
import static com.mathfriendzy.utils.ITextIds.LBL_USERNAME;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

public class SearchByUserActivity extends AdBaseActivity implements OnClickListener
{
	private TextView txtTitleTopbar				= null;
	private TextView txtSearchFriend			= null;
	private TextView txtUsername				= null;
	private EditText edtSearchUser				= null;
	private Button btnGo						= null;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_by_user);

		getWidgetId();
		setTextOnWidgets();
	}//END onCreate method



	private void setTextOnWidgets()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		String text		= null;
		text = ITextIds.LEVEL+" "+translate.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE);
		txtTitleTopbar.setText(text);

		text = translate.getTranselationTextByTextIdentifier(BTN_GO);
		btnGo.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_USERNAME);
		txtUsername.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_SEARCH_USER);
		txtSearchFriend.setText(text);

		translate.closeConnection();

	}//END setTextOnWIdget method



	private void getWidgetId()
	{
		txtSearchFriend		= (TextView) findViewById(R.id.txtSearchFriend);	
		txtTitleTopbar		= (TextView) findViewById(R.id.txtTitleTopbar);
		txtUsername			= (TextView) findViewById(R.id.txtSearchFriend);		
		btnGo				= (Button) findViewById(R.id.btnGo);
		edtSearchUser		= (EditText) findViewById(R.id.edtSearchUser);
		btnGo.setOnClickListener(this);

	}//END getWidgetId method



	@Override
	public void onClick(View v) 
	{
		String edtText;
		switch(v.getId())
		{
		case R.id.btnGo:
			edtText = edtSearchUser.getText().toString();
						
			if(edtText.length() == 0)
			{
				DialogGenerator dialog = new DialogGenerator(this);
				Translation translate = new Translation(this);
				translate.openConnection();
				dialog.generateWarningDialog(translate.getTranselationTextByTextIdentifier("alertMsgFillFirstAndLastName"));
				translate.closeConnection();
			}
			else
			{
				Intent intent = new Intent(this, SelectedPlayerActivity.class);
				intent.putExtra("username", edtText);
				startActivity(intent);
			}
			break;
		}

	}


}
