package com.mathfriendzy.controller.multifriendzy.findbyuser;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.LBL_FOUND;
import static com.mathfriendzy.utils.ITextIds.LBL_PLAYERS;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.top100.JsonFileParser;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.sixthgradepaid.R;

public class SelectedPlayerActivity extends AdBaseActivity implements OnClickListener 
{
	private TextView txtTitleTopbar					= null;
	private TextView txtDisplayPlayer				= null;
	private TextView txtFoundPlayer					= null;
	private Button   btnTop100						= null;
	
	private String username							= null;
	private Bitmap profileImageBitmap				= null;
	
	private ImageView imgPlayer						= null;
	private TextView txtPlayerName					= null;
	private TextView txtPlayerCity					= null;
	private TextView txtPlayerGrade					= null;
	
	private RelativeLayout layoutPlayerInformation	= null;
	
	private String playerId							= null;
	private String userId							= null;
	private String txtGrade							= null;
	
	public static Player player = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selected_player);
		
		username	= getIntent().getStringExtra("username");
		getWidgetId();
		setTextOnWidgets();
		setPlayerData();
	
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new AsyncUserFormServer(this, username).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}//END onCreate method
	

	
	private void setTextOnWidgets()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		String text		= null;
		//text = translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN);
		text = MathFriendzyHelper.getAppName(translate);
		txtTitleTopbar.setText(text);		
		text = translate.getTranselationTextByTextIdentifier("lblRateMe");
		btnTop100.setText(text);		
		text = translate.getTranselationTextByTextIdentifier(LBL_PLAYERS);
		txtDisplayPlayer.setText(text);
				
		text = translate.getTranselationTextByTextIdentifier(LBL_FOUND)	+ " 1 " + text + ":";
		txtFoundPlayer.setText(text);

		txtGrade = translate.getTranselationTextByTextIdentifier("lblAddPlayerGrade");
		translate.closeConnection();
		
	}//END setTextOnWIdget method
	


	private void getWidgetId()
	{
		txtTitleTopbar		= (TextView) findViewById(R.id.txtTitleHomeScreen);
		txtDisplayPlayer	= (TextView) findViewById(R.id.txtDisplayPlayer);
		txtFoundPlayer		= (TextView) findViewById(R.id.txtFoundPlayer);
		btnTop100			= (Button) findViewById(R.id.btnTop100);
		imgPlayer			= (ImageView) findViewById(R.id.imgPlayer);
		txtPlayerName		= (TextView) findViewById(R.id.txtPlayerName);
		txtPlayerCity		= (TextView) findViewById(R.id.txtPlayerCity);
		txtPlayerGrade		= (TextView) findViewById(R.id.txtPlayerGrade);
		
		layoutPlayerInformation = (RelativeLayout) findViewById(R.id.layoutPlayerInformation);
		btnTop100.setOnClickListener(this);
		layoutPlayerInformation.setOnClickListener(this);
		
	}//END getWidgetId method



	@Override
	public void onClick(View v) 
	{
		switch(v.getId()){
		case R.id.btnTop100:
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier(ALERT_LOGIN_REGISTER));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			MathFriendzyHelper.rateUs(this);
			break;
			
		case R.id.layoutPlayerInformation:
			this.clickOnlayoutPlayerInformation();
			break;
			
		}	
	}//END onClick method
	
	
	/**
	 * This method call when user click on layoutPlayerInformation layout
	 */
	 private void clickOnlayoutPlayerInformation()
	 {		 
		/* ArrayList<MathFriendzysRoundDTO> roundList = new ArrayList<MathFriendzysRoundDTO>();
		 MultiFriendzysFromServerDTO serverDataobj = new MultiFriendzysFromServerDTO();
		 	 
		 OppnentDataDTO opponentData = new OppnentDataDTO();
		 opponentData.setProfileImageNameId( player.getProfileImageName());
		 opponentData.setfName(player.getFirstName());
		 opponentData.setlName(player.getLastName());
		 serverDataobj.setOpponentData(opponentData);
		 
		 serverDataobj.setRoundList(roundList);
		 serverDataobj.setFriendzysId("");
		 serverDataobj.setType("your friendzy");*/
		 
		 //previous
		 	MultiFriendzyRound.opponentImageId = player.getProfileImageName();
			MultiFriendzyRound.oppenentPlayerName = player.getFirstName()
					                                + " " + player.getLastName().charAt(0);
			
			ArrayList<MathFriendzysRoundDTO> roundList = new ArrayList<MathFriendzysRoundDTO>();
			
			MultiFriendzyRound.roundList = roundList;
			
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
			transeletion.closeConnection();
			
			MultiFriendzyRound.type 		= "your friendzy";
			MultiFriendzyRound.friendzyId 	= "-1";
		//end
						
			Intent intent = new Intent(this , MultiFriendzyRound.class);
			//intent.putExtra("datafromServer", serverDataobj);
			startActivity(intent);
	 }
	
	/**
	 * use to check user exist or not if exist then go for its detail
	 * @author Shilpi Mangal
	 *
	 */
	class AsyncUserFormServer extends AsyncTask<Void, Void, Void>
	{
		Context context;
		ProgressDialog pd;
		String strUrl;
				
		public AsyncUserFormServer(Context context, String username) 
		{			
			this.context = context;
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			player = new Player();
			
			strUrl = ICommonUtils.COMPLETE_URL + "action=getPlayersByUserNameAndroid&uname="+username +
					"&userId="+userId+"&playerId="+playerId+"&appId=" + CommonUtils.APP_ID;
			
			/*String action = "getPlayersByUserNameAndroid";
			strUrl = ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION + "action=" + action + "&"
					 + "uname=" + username + "&"
					 + "userId="+ userId   + "&"
					 + "playerId=" + playerId + "&"
					 + "appId=" + CommonUtils.APP_ID;*/
		}
		
		@Override
		protected Void doInBackground(Void... arg0)
		{
			//Log.e("SelectedPlayerActivity", "url : " + strUrl);
			String jsonStr = CommonUtils.readFromURL(strUrl);
			//Log.e("SelectedPlayerActivity", jsonStr);
			player = JsonFileParser.getUserDetail(jsonStr, context);			
			return null;
		}
		
		
		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			if(player == null)
			{
				DialogGenerator dialog = new DialogGenerator(context);
				Translation translate = new Translation(context);
				translate.openConnection();
				dialog.generateDateWarningDialog(translate.getTranselationTextByTextIdentifier("alertMsgWeCouldNotFindThatPlayer"));
				translate.closeConnection();
			}
			else
			{
				layoutPlayerInformation.setVisibility(View.VISIBLE);
				txtFoundPlayer.setVisibility(View.VISIBLE);
				setUserInformation(player);
			}
			super.onPostExecute(result);
		}
	}
	
	private void setPlayerData() 
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);			
		playerId = sharedPreffPlayerInfo.getString("playerId", "");
		userId	 = sharedPreffPlayerInfo.getString("userId", "");
		
	}
	
	
	/**
	 * use to set user information on view
	 * @param userPlayer
	 */
	private void setUserInformation(Player userPlayer)
	{
		txtPlayerCity.setText(userPlayer.getCity());
		txtPlayerName.setText(userPlayer.getFirstName()+" "+ userPlayer.getLastName());
		
		if(userPlayer.getGrade() > 12)
		{
			txtPlayerGrade.setText("Adult");
		}
		else 
		{
			setGrade(userPlayer.getGrade());
		}
		
		String imageName = userPlayer.getProfileImageName();
		imgPlayer.setBackgroundResource(0);
		
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
					new FacebookImageLoaderTask(strUrl).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
	    }
	}//END setUserInformation method
	
	
	/**
	 * use to set standard with grade
	 * @param grade
	 */
	private void setGrade(int grade)
	{
		String alias;
		switch(grade)
		{
		case 1:
			alias = "st";
			break;
		case 2:
			alias = "nd";
			break;
		case 3:
			alias = "rd";
			break;
		default :
			alias = "th";
		}
		
		txtPlayerGrade.setText(grade + alias+ " "+ txtGrade);
	}//END setGrade method




	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}
}
