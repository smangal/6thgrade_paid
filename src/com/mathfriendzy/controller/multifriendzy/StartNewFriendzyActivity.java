package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.LBL_EMAIL;
import static com.mathfriendzy.utils.ITextIds.LBL_FB;
import static com.mathfriendzy.utils.ITextIds.LBL_FIND_FRIEND;
import static com.mathfriendzy.utils.ITextIds.LBL_FIND_INVITE;
import static com.mathfriendzy.utils.ITextIds.LBL_FRIEND;
import static com.mathfriendzy.utils.ITextIds.LBL_INVITE;
import static com.mathfriendzy.utils.ITextIds.LBL_MESSAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_USER;
import static com.mathfriendzy.utils.ITextIds.LIKE_URL;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.multifriendzy.contacts.ContactNameActivity;
import com.mathfriendzy.controller.multifriendzy.facebookfriends.FacebookFriendActivity;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SearchByUserActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;


public class StartNewFriendzyActivity extends AdBaseActivity implements OnClickListener 
{
	private TextView txtTobBar						= null;
	private TextView txtFindfriend					= null;
	private TextView txtInviteFriends				= null;
	private TextView txtFindOrInvite				= null;
	private TextView txtFb							= null;
	private TextView txtUser						= null;
	private TextView txtMessage						= null;
	private TextView txtMail						= null;

	private RelativeLayout findFbLayout				= null;
	private RelativeLayout findUserLayout			= null;
	private RelativeLayout inviteViaMsgLayout		= null;
	private RelativeLayout inviteViaEmailLayout		= null;

	private Button btnTop100						= null;

	private String subject							= null;
	private String inviteMsg						= null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_new_friendzy);

		getWidgetId();
		setTextOnWidget();

	}//END onCreate method



	private void getWidgetId() 
	{
		txtFb				= (TextView) findViewById(R.id.txtFb);
		txtFindfriend		= (TextView) findViewById(R.id.txtFindFriends);
		txtInviteFriends	= (TextView) findViewById(R.id.txtInviteFriends);
		txtFindOrInvite		= (TextView) findViewById(R.id.txtFindOrInvite);
		txtMail				= (TextView) findViewById(R.id.txtMail);
		txtMessage			= (TextView) findViewById(R.id.txtMessage);
		txtTobBar			= (TextView) findViewById(R.id.labelTop);
		txtUser				= (TextView) findViewById(R.id.txtUser);

		btnTop100			= (Button) findViewById(R.id.btnTop100);

		findFbLayout			= (RelativeLayout) findViewById(R.id.findFbLayout);
		findUserLayout			= (RelativeLayout) findViewById(R.id.findUserLayout);
		inviteViaEmailLayout	= (RelativeLayout) findViewById(R.id.inviteViaEmailLayout);
		inviteViaMsgLayout		= (RelativeLayout) findViewById(R.id.inviteViaMsgLayout);

		findFbLayout.setOnClickListener(this);
		findUserLayout.setOnClickListener(this);
		inviteViaEmailLayout.setOnClickListener(this);
		inviteViaMsgLayout.setOnClickListener(this);
		btnTop100.setOnClickListener(this);

	}//END getWidgetId method



	private void setTextOnWidget()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		String text;
		//text = translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN);
		text = MathFriendzyHelper.getAppName(translate);
		txtTobBar.setText(text);
		subject = "Check Out 1st Grade Friendzy!";

		text = translate.getTranselationTextByTextIdentifier("lblRateMe");
		btnTop100.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_EMAIL);
		txtMail.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_MESSAGE);
		txtMessage.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_FB);
		txtFb.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_FIND_FRIEND);
		txtFindfriend.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_FIND_INVITE);
		txtFindOrInvite.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_INVITE) + " " 
				+ translate.getTranselationTextByTextIdentifier(LBL_FRIEND);
		txtInviteFriends.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_USER);
		txtUser.setText(text);
		translate.closeConnection();

	}//END setTextOnWIdget method



	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.btnTop100:
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion
						.getTranselationTextByTextIdentifier(ALERT_LOGIN_REGISTER));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			MathFriendzyHelper.rateUs(this);
			break;

		case R.id.inviteViaEmailLayout:
			Intent emailIntent = new Intent(Intent.ACTION_SEND);

			inviteMsg = getInviteMessage();

			emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(inviteMsg));
			emailIntent.setType("text/html");
			startActivity(Intent.createChooser(emailIntent, "Send email using"));
			break;

		case R.id.findUserLayout:
			startActivity(new Intent(this, SearchByUserActivity.class));

			break;

		case R.id.inviteViaMsgLayout:
			Intent sms_intent = new Intent(this, ContactNameActivity.class);
			sms_intent.putExtra("msg", Html.fromHtml(getInviteSms())+"");
			startActivity(sms_intent);
			break;

		case R.id.findFbLayout:
			startActivity(new Intent(this, FacebookFriendActivity.class));			
			break;
		}//End switch case		

	}//END onClick Method



	private String getInviteMessage() 
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String playerUserName = sharedPreffPlayerInfo.getString("userName", "");
		Translation translate = new Translation(this);
		translate.openConnection();
		String text = null;		
		//msg = translate.getTranselationTextByTextIdentifier(MSG_INVITE_1);

		text =  "Dear Friend: <p> I just downloaded this awesome app and it is really helping me with my school work." +
				" It is so fun and exciting!" +
				" Now we can learn all our school subjects by competing with each other. Think you can beat me? "+
				" <a href=\""+ITextIds.RATE_URL+"\">"+"Click Here"+"</a>"+" "
				+" to visit the Google Play Store and download 1st Grade Friendzy. " 
				+"Once you get it, click on \"Multi-Friendzy\" and \"Start New Friendzy\". Then search '"
				+playerUserName+"'. Then click on Go to start a Friendzy. Once you are done, I will get the same questions " +
				"as you and we'll see who wins."
				+"<p><p><a href=\""+LIKE_URL+"\">Like</a>"
				+ " us on the official 1st Grade Friendzy page.";

		translate.closeConnection();
		return text;
	}

	private String getInviteSms() 
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String playerUserName = sharedPreffPlayerInfo.getString("userName", "");
		Translation translate = new Translation(this);
		translate.openConnection();
		String text = null;		
		//msg = translate.getTranselationTextByTextIdentifier(MSG_INVITE_1);

		text =  "Dear Friend: <p> I just downloaded this awesome app and it is really helping me with my school work." +
				" It is so fun and exciting!" +
				" Now we can learn all our school subjects by competing with each other. Think you can beat me? "+
				" <a href=\""+ITextIds.RATE_URL+"\">"+"Click Here"+"</a>"+" "
				+" to visit the Google Play Store and download 1st Grade Friendzy. " 
				+"Once you get it, click on \"Multi-Friendzy\" and \"Start New Friendzy\". Then search '"
				+playerUserName+"'. Then click on Go to start a Friendzy. Once you are done, I will get the same questions " +
				"as you and we'll see who wins.";

		translate.closeConnection();
		return text;
	}


}
