package com.mathfriendzy.controller.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.mathfriendzy.controller.base.MyApplication;
import com.mathfriendzy.controller.moreapp.ActMoreApp;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.ICommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sixthgradepaid.R;

/**
 * Created by root on 22/9/15.
 */
public class HouseAds {

	private static HouseAds houseAds = null;
	private InterstitialAd mInterstitialAd = null;
	private static int NUMBER_OF_SCREEN_WIDGETS = 0;
	private static int MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS = 25;
	private static Context myContext = null;	
	
	public static HouseAds getInstance(){
		if(houseAds == null) {
			houseAds = new HouseAds();
		}
		return houseAds;
	}

	public void showAds(Context context){   
		myContext = context;
		NUMBER_OF_SCREEN_WIDGETS ++ ;
		//if(MathFriendzyHelper.isShowFullScreenAds()){
		MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS = this.getFrequency(context); 
		if(NUMBER_OF_SCREEN_WIDGETS == MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS){		
			if(mInterstitialAd != null)
				mInterstitialAd = null;
			mInterstitialAd = new InterstitialAd(context);
			mInterstitialAd.setAdUnitId(ICommonUtils.HOUSE_AD_ID);
			AdRequest adRequest = new AdRequest.Builder()                    
			.build();
			mInterstitialAd.loadAd(adRequest);
			mInterstitialAd.setAdListener(new MyAdListener());
		}
	}

	private void showAds(){
		if (mInterstitialAd != null) {
			if (mInterstitialAd.isLoaded()) {
				NUMBER_OF_SCREEN_WIDGETS = 0;
				mInterstitialAd.show();
			}
		}
	}

	private class MyAdListener extends AdListener  {
		@Override
		public void onAdClosed() {
			super.onAdClosed();
		}

		@Override
		public void onAdFailedToLoad(int errorCode) {        	
			mInterstitialAd = null;
			NUMBER_OF_SCREEN_WIDGETS
			=  MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS - 1;
			//MathFriendzyHelper.showToast(myContext, "House Ads Loading fail...");
			super.onAdFailedToLoad(errorCode);
		}

		@Override
		public void onAdLeftApplication(){
			super.onAdLeftApplication();
		}

		@Override
		public void onAdLoaded() {
			HouseAds.this.showAds();
			super.onAdLoaded();
		}

		@Override
		public void onAdOpened() {
			super.onAdOpened();
		}
	}

	private int getFrequency(Context context){
		int frequency = MathFriendzyHelper.getIntValueFromPreff
				(context, MathFriendzyHelper.HOUSE_ADS_FREQUENCY_KEY);     	
		return frequency > 0 ? frequency : MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS;
	}


	//change for custom ads
	private WindowManager windowManage = null;

	public void showCustomeAds(final Context context){
		myContext = context;
		/*myContext = context;
		NUMBER_OF_SCREEN_WIDGETS ++ ;        
		//MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS = this.getFrequency(context); 
		if(NUMBER_OF_SCREEN_WIDGETS == MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS){*/		
		/*MathFriendzyHelper.downloadCustomeHouseAdsData(context,
				new OnAdsDataFromServerSuccess() {			
			@Override
			public void onAdsData(CustomeHouseAdsData customeAdsData) {
				if(customeAdsData != null){
					showAdsInView(context , customeAdsData);
				}
			}
		});*/
		
		showAdsInView(context /*, customeAdsData*/);
		//}		
	}

	@SuppressLint("InflateParams")
	private void showAdsInView(final Context context /*,final CustomeHouseAdsData customeAdsData*/){		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);
		final View dialogView = inflater.inflate(R.layout.house_ads_custome_dialog, null);
		ImageView imgHouseAdCustomeImage = (ImageView) dialogView.findViewById
				(R.id.imgHouseAdCustomeImage);
		Button btnCloseAd = (Button) dialogView.findViewById(R.id.btnCloseAd);

		this.setImageFromUrl(imgHouseAdCustomeImage ,
				MathFriendzyHelper.getPNGFormateImageName(MathFriendzyHelper
						.getHouseAdsImageName(context))
				, new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String s, View view) {
			}

			@Override
			public void onLoadingFailed(String s, View view, FailReason failReason) {
				NUMBER_OF_SCREEN_WIDGETS = MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS - 1;
			}

			@Override
			public void onLoadingComplete(String s, View view, Bitmap bitmap) {		
				NUMBER_OF_SCREEN_WIDGETS = 0;
				addViewInWM(context, dialogView);
			}

			@Override
			public void onLoadingCancelled(String s, View view) {
				NUMBER_OF_SCREEN_WIDGETS = MAX_NUMBER_OF_SCREEN_TO_SHOW_ADS - 1;
			}
		});

		btnCloseAd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				removeViewFromWindoManage(windowManage, dialogView);
				ifUserRateAppBefore(context);
			}
		});

		imgHouseAdCustomeImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeViewFromWindoManage(windowManage, dialogView);
				ifUserRateAppBefore(context);
				MathFriendzyHelper.openUrlWithoutHttps(context, 
						MathFriendzyHelper.getHouseAdsUrlLink(context));
			}
		});			
	}

	private void setImageFromUrl(ImageView imgView , String imageName , 
			ImageLoadingListener listener){
		ImageLoader loader = MathFriendzyHelper.getImageLoaderInstance();
		DisplayImageOptions options = MathFriendzyHelper.getDisplayOptionWithoutCaching();
		loader.displayImage(ICommonUtils.HOUSE_ADS_IMAGE_URL + imageName,
				imgView, options , listener);
	}

	private void addViewInWM(Context context , View view){
		try{
			WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.MATCH_PARENT,
					0,
					WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
					| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT);
			params.gravity = Gravity.CENTER;
			if(windowManage == null)
				windowManage = (WindowManager) MyApplication.getAppContext()
				.getSystemService(Context.WINDOW_SERVICE);			
			windowManage.addView(view, params);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void removeViewFromWindoManage(WindowManager wm , View view){
		if(wm != null && view != null){			
			wm.removeView(view);
		}
	}
	
	private void ifUserRateAppBefore(Context context){
		/*if(MathFriendzyHelper.getBooleanValueFromPreff(myContext, 
				MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY)){
			openMoreGreatAppScreen(context);
		}*/
	}
	
	private void openMoreGreatAppScreen(Context context){
		Intent intent = new Intent(context , ActMoreApp.class);		
		context.startActivity(intent);
	}
}
