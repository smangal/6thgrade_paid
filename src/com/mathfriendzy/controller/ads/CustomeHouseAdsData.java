package com.mathfriendzy.controller.ads;

public class CustomeHouseAdsData {
	
	private String imageName;
	private String link;
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}	
}
