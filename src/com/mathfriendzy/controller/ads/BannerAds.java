package com.mathfriendzy.controller.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mathfriendzy.controller.base.MyApplication;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.utils.AppVersion;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

/**
 * Created by root on 29/9/15.
 */
public class BannerAds {
	private static BannerAds bannerAds = null;
	private int MAX_COUNTER = 10;
	private int screenCounter = 0;
	private static Context myContext = null;
	private View view = null;
	private WindowManager wm = null;

	//for display purchase dialog
	private View dialogView	= null;
	private WindowManager wmDialog  = null;
	private int numberOfAdsDisplayed = 0;
	private int MAX_NUMBER_ADS_AFTER_PURCHASE_DIALOG_OPEN = 3;
	private UserPlayerDto selectedPlayer = null;

	public static BannerAds getInstance(Context context){
		myContext = context;
		if(bannerAds == null)
			bannerAds = new BannerAds();
		return bannerAds;
	}

	@SuppressWarnings("unused")
	@SuppressLint("InflateParams")
	public void showAds(){	
		if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
			return ;
		}

		if(!CommonUtils.isUserLogin(myContext)) {
			//return;
		}else{			
			selectedPlayer = MathFriendzyHelper.getPlayerData(myContext);
			if(MathFriendzyHelper.isStudentRegisterByTeacher(selectedPlayer)){
				return;
			}
			if(this.getIsAdDisable(myContext)
					|| /*this.isAppUnlock(myContext)*/
					MathFriendzyHelper.isSubscriptionPurchase()){
				return ;
			}
		}

		screenCounter ++;
		MAX_COUNTER = this.getAdsFrequency(myContext);
		if(screenCounter == MAX_COUNTER){
			//if(screenCounter == MAX_COUNTER){
			LayoutInflater inflater = (LayoutInflater)myContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.add_dialog, null);
			AdView adView   = (AdView) view.findViewById(R.id.ad);
			Button btnClose = (Button) view.findViewById(R.id.btnCloseAd);
			adView.loadAd(new AdRequest.Builder().build());
			adView.setAdListener(new MyAdListener());
			btnClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(wm != null && view != null){
						wm.removeView(view);
						screenCounter = 0;
					}
					//for showing purchase dialog after max ads to be displayed
					if(numberOfAdsDisplayed == MAX_NUMBER_ADS_AFTER_PURCHASE_DIALOG_OPEN){
						showDialogAfterMaxAdsDisplayed(myContext);
					}
				}
			});
		}
	}

	/**
	 * This is the dialog listener which is load the ad on dialo
	 * @author Yashwant Singh
	 *
	 */
	private class MyAdListener extends AdListener {
		@Override
		public void onAdClosed() {
			super.onAdClosed();
		}

		@Override
		public void onAdFailedToLoad(int errorCode) {
			screenCounter = MAX_COUNTER - 1;
			//MathFriendzyHelper.showToast(myContext, "Banner Ads Loading fail");
			super.onAdFailedToLoad(errorCode);
		}

		@Override
		public void onAdLeftApplication() {
			if(wm != null && view != null){
				wm.removeView(view);
				screenCounter = 0;
			}
			super.onAdLeftApplication();
		}

		@Override
		public void onAdLoaded() {
			showWindowManagerDialog();
			super.onAdLoaded();
		}

		@Override
		public void onAdOpened() {
			super.onAdOpened();
		}
	}

	/**
	 * This method show the ad dialog on window manager
	 */
	private void showWindowManagerDialog(){
		try{
			WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					0,
					WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
					| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT);
			params.gravity = Gravity.CENTER;
			wm = (WindowManager) MyApplication.getAppContext()
					.getSystemService(Context.WINDOW_SERVICE);
			wm.addView(view, params);
			//for number of ads displayed
			numberOfAdsDisplayed ++ ;
		}catch(Exception e){
			e.printStackTrace();
			screenCounter = 0;
		}
	}


	/**
	 * This method call after the maximun dialog are diaplayes , max_dialog = 10
	 * @param context
	 */
	@SuppressLint("InflateParams")
	private void showDialogAfterMaxAdsDisplayed(final Context context){
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);
		dialogView = inflater.inflate(R.layout.dialog_click_on_proceed_for_coins, null);

		TextView txtMsg  = (TextView) dialogView.findViewById(R.id.txtYouneedMoreCoins);
		Button btnCancel = (Button) dialogView.findViewById(R.id.btnNoThanks);
		Button btnOk     = (Button) dialogView.findViewById(R.id.btnYes);

		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblMakeAppAdsFree"));
		btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		final String lblYouMustRegisterLoginToAccess = transeletion
				.getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
		transeletion.closeConnection();

		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(wmDialog != null && dialogView != null){
					wmDialog.removeView(dialogView);
					numberOfAdsDisplayed = 0;
				}
			}
		});

		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(wmDialog != null && dialogView != null){
					wmDialog.removeView(dialogView);
					numberOfAdsDisplayed = 0;
					//if(MathFriendzyHelper.isUserLogin(context)){						
					MathFriendzyHelper.showUnlockMathCategoryDialog(context,
							MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER);
					/*}else{
						MathFriendzyHelper.showLoginRegistrationDialog(context, 
								lblYouMustRegisterLoginToAccess);
					}*/
				}
			}
		});
		showWindowManagerDialogAfterMAxAdsDialogDisplayed();
		//if(MathFriendzyHelper.isUserLogin(context)){
		btnOk.performClick();	
		//}
	}

	/**
	 * This method show the ad dialog on window manager
	 */
	private void showWindowManagerDialogAfterMAxAdsDialogDisplayed(){

		try{
			WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					0,
					WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
					| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT);
			params.gravity = Gravity.CENTER;
			wmDialog = (WindowManager) MyApplication.getAppContext()
					.getSystemService(Context.WINDOW_SERVICE);
			wmDialog.addView(dialogView, params);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private boolean getIsAdDisable(Context context){
		SharedPreferences sharedPreff = context.getSharedPreferences
				(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
		if(sharedPreff.getInt(ICommonUtils.ADS_IS_ADS_DISABLE, 0) == 1)
			return true;
		return false;
	}

	/*private boolean isAppUnlock(Context context){
		UserRegistrationOperation userObj = new UserRegistrationOperation(context);
		String userId = userObj.getUserId();
		if(CommonUtils.getApplicationStatus(context, userId, 100) == 1)
			return true;
		return false;
	}*/

	private int getAdsFrequency(Context context){
		int adsFrequency = 0;
		SharedPreferences sharedPreff = context.getSharedPreferences
				(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
		adsFrequency = sharedPreff.getInt(ICommonUtils.ADS_FREQUENCIES, 0);
		if(adsFrequency == 0)
			adsFrequency = MAX_COUNTER;
		return adsFrequency;
		//return 3;
	}
}
