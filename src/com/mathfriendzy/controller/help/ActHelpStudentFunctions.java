package com.mathfriendzy.controller.help;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;

public class ActHelpStudentFunctions extends ActBase {

    private TextView txtTopbar = null;
    private ListView lstTeacherFunction = null;
    private ArrayList<TeacherStudentFunctionDTO> teacherStudentFunctionDTOs = null;
    private TeacherStudentFunctionAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_help_student_functions);

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setFunctionAdapter(teacherStudentFunctionDTOs);
    }

    private void init(){
        this.initializeFunctionList();
    }

    private void initializeFunctionList(){
        this.teacherStudentFunctionDTOs = new ArrayList<TeacherStudentFunctionDTO>();
        Translation translation = new Translation(this);
        translation.openConnection();
        for(int i =  0 ; i < 2 ; i ++ ){
            this.teacherStudentFunctionDTOs.add(this.getTeacherFunction(translation , i));
        }
        translation.closeConnection();
    }

    @Override
    protected void setWidgetsReferences() {
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        lstTeacherFunction = (ListView) findViewById(R.id.lstTeacherFunction);
    }

    @Override
    protected void setListenerOnWidgets() {

    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText("Student's Functions");
        transeletion.closeConnection();
    }

    private void setFunctionAdapter(ArrayList<TeacherStudentFunctionDTO> functionDTOArrayList){
        if(functionDTOArrayList != null && functionDTOArrayList.size() > 0) {
            if(functionDTOArrayList != null && functionDTOArrayList.size() > 0) {
                adapter = new TeacherStudentFunctionAdapter(this,
                        functionDTOArrayList , new FunctionClickListener() {
                    @Override
                    public void onFunctionClick(int tag, int index) {
                        openVideoUrl(adapter.getUrl(index));
                    }
                });
                lstTeacherFunction.setAdapter(adapter);
            }
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {

    }

    private TeacherStudentFunctionDTO getTeacherFunction(Translation translation , int id){
        TeacherStudentFunctionDTO teacherStudentFunctionDTO = new TeacherStudentFunctionDTO();
        switch(id){
            case 0:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblDoingAssignments"));
                //teacherStudentFunctionDTO.setTxtName("Doing Assignments");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.doing_assignments_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=0lVOlNic_sU");
                break;
            case 1:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblGettingHelp"));
                //teacherStudentFunctionDTO.setTxtName("Getting Help");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.getting_help_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=0q8jRg1JKng");
                break;
        }
        return teacherStudentFunctionDTO;
    }

    private String getTranslation(Translation translation , String id){
        return translation.getTranselationTextByTextIdentifier(id);
    }
    
    /**
     * Open the video
     * @param url
     */
    private void openVideoUrl(String url){
        if(MathFriendzyHelper.isEmpty(url)){
            MathFriendzyHelper.showWarningDialog(this , "Coming Soon!!!");
            return ;
        }
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this, url);
    }
}
