package com.mathfriendzy.controller.help;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;

public class ActHelpTeacherFunctions extends ActBase {

    private TextView txtTopbar = null;
    private ListView lstTeacherFunction = null;
    private ArrayList<TeacherStudentFunctionDTO> teacherStudentFunctionDTOs = null;
    private TeacherStudentFunctionAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_help_teacher_functions);

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setFunctionAdapter(teacherStudentFunctionDTOs);
    }

    private void init(){
        this.initializeFunctionList();
    }

    private void initializeFunctionList(){
        this.teacherStudentFunctionDTOs = new ArrayList<TeacherStudentFunctionDTO>();
        Translation translation = new Translation(this);
        translation.openConnection();
        for(int i =  0 ; i < 10 ; i ++ ){
            this.teacherStudentFunctionDTOs.add(this.getTeacherFunction(translation , i));
        }
        translation.closeConnection();
    }

    @Override
    protected void setWidgetsReferences() {
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        lstTeacherFunction = (ListView) findViewById(R.id.lstTeacherFunction);
    }

    @Override
    protected void setListenerOnWidgets() {

    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText("Teacher's Functions");
        transeletion.closeConnection();
    }

    private void setFunctionAdapter(ArrayList<TeacherStudentFunctionDTO> functionDTOArrayList){
        if(functionDTOArrayList != null && functionDTOArrayList.size() > 0) {
            adapter = new TeacherStudentFunctionAdapter(this,
                    functionDTOArrayList , new FunctionClickListener() {
                @Override
                public void onFunctionClick(int tag, int index) {
                    openVideoUrl(adapter.getUrl(index));
                }
            });
            lstTeacherFunction.setAdapter(adapter);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {

    }

    private TeacherStudentFunctionDTO getTeacherFunction(Translation translation , int id){
        TeacherStudentFunctionDTO teacherStudentFunctionDTO = new TeacherStudentFunctionDTO();
        switch(id){
            case 0:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblCreateAnAccount"));
                //teacherStudentFunctionDTO.setTxtName("Creating an Account");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.creating_account_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=wVWllu62eWo");
                break;
            case 1:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblRegisteringStudent"));
                //teacherStudentFunctionDTO.setTxtName("Registering Students");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.registering_students_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=AdRZyfma5ik");
                break;
            case 2:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblManagingStudents"));
                //teacherStudentFunctionDTO.setTxtName("Managing Students");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.managing_students_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=5z7cot31bVY");
                break;
            case 3:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblEditStdAcc"));
                //teacherStudentFunctionDTO.setTxtName("Edit Students' Accounts");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.editing_students_accounts_button_sf_help);
                teacherStudentFunctionDTO.setUrl("");
                break;
            case 4:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblCreatingAssignment"));
                //teacherStudentFunctionDTO.setTxtName("Creating Assignments");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.creating_assignments_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=qSL47-cRcMM");
                break;
            case 5:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblCheckingAssignment"));
                //teacherStudentFunctionDTO.setTxtName("Checking Assignments");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.checking_assignments_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=P4-EWjTZWRM");
                break;
            case 6:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblEditAssignment"));
                //teacherStudentFunctionDTO.setTxtName("Editing Assignments");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.editing_assignments_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=fGqpnVR4gKE");
                break;
            case 7:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblRemedialAssignment"));
                //teacherStudentFunctionDTO.setTxtName("Remedial Assignments");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.remedial_assignments_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=o6KPD2xKQT8");
                break;
            case 8:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblAssigFreeTutor"));
                //teacherStudentFunctionDTO.setTxtName("Assigning Free Tutors");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.assigning_tutors_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=HjBxHOCJGio");
                break;
            case 9:
                teacherStudentFunctionDTO.setTxtName(this.getTranslation(translation , "lblManageFreeTutor"));
                //teacherStudentFunctionDTO.setTxtName("Managing Free Tutors");
                teacherStudentFunctionDTO.setBackGroundImageId(R.drawable.managing_tutors_button_sf_help);
                teacherStudentFunctionDTO.setUrl("https://www.youtube.com/watch?v=dek1G8Qv3os");
                break;
        }
        return teacherStudentFunctionDTO;
    }

    private String getTranslation(Translation translation , String id){
        return translation.getTranselationTextByTextIdentifier(id);
    }

    /**
     * Open the video
     * @param url
     */
    private void openVideoUrl(String url){
        if(MathFriendzyHelper.isEmpty(url)){
            MathFriendzyHelper.showWarningDialog(this , "Coming Soon!!!");
            return ;
        }
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this, url);
    }
}
