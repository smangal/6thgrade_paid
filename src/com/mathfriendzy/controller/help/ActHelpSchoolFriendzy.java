package com.mathfriendzy.controller.help;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.sixthgradepaid.R;

public class ActHelpSchoolFriendzy extends ActBase {

    private TextView txtTopbar = null;
    private TextView txtExploreAndEnjoy = null;
    private TextView lblTheGameOfMathPlus = null;

    private RelativeLayout rlIntroduction = null;
    private RelativeLayout rlTeacherFunctions = null;
    private RelativeLayout rlStudentFunctions = null;
    private RelativeLayout rlFreeTutorFunctions = null;
    private RelativeLayout rlSeeHowItWork = null;
    private TextView txtIntroduction = null;
    private TextView txtTeacherFunction = null;
    private TextView txtStudentFunction = null;
    private TextView txtFreeTutorFunctions = null;
    private TextView txtSeeHowItWork = null;

    private TextView txtSchoolDesigned = null;
    private Button btnTryItFreeNow = null;
    private Button btnInviteFriends = null;
    		
    private final String SCHOOL_APP_LINK = "https://play.google.com/store/apps/details?id=com.homework_friendzy";
    
    private String lblOurSchWill , lblInviteSchEmail = null; 
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_help_school_friendzy);

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
    }


    @Override
    protected void setWidgetsReferences() {
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtExploreAndEnjoy = (TextView) findViewById(R.id.txtExploreAndEnjoy);
        lblTheGameOfMathPlus = (TextView) findViewById(R.id.lblTheGameOfMathPlus);
        txtIntroduction = (TextView) findViewById(R.id.txtIntroduction);
        txtTeacherFunction = (TextView) findViewById(R.id.txtTeacherFunction);
        txtStudentFunction = (TextView) findViewById(R.id.txtStudentFunction);
        txtFreeTutorFunctions = (TextView) findViewById(R.id.txtFreeTutorFunctions);
        txtSeeHowItWork = (TextView) findViewById(R.id.txtSeeHowItWork);

        rlIntroduction = (RelativeLayout) findViewById(R.id.rlIntroduction);
        rlTeacherFunctions = (RelativeLayout) findViewById(R.id.rlTeacherFunctions);
        rlStudentFunctions = (RelativeLayout) findViewById(R.id.rlStudentFunctions);
        rlFreeTutorFunctions = (RelativeLayout) findViewById(R.id.rlFreeTutorFunctions);
        rlSeeHowItWork = (RelativeLayout) findViewById(R.id.rlSeeHowItWork);
        txtSchoolDesigned = (TextView) findViewById(R.id.txtSchoolDesigned);
        btnTryItFreeNow = (Button) findViewById(R.id.btnTryItFreeNow);
        btnInviteFriends = (Button) findViewById(R.id.btnInviteFriends);
    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblInstructionalVideo"));
        txtExploreAndEnjoy.setText(transeletion.getTranselationTextByTextIdentifier("lblExploreAndEnjoy"));
        lblTheGameOfMathPlus.setText(transeletion.getTranselationTextByTextIdentifier("lblTheGameOfMath"));
        txtIntroduction.setText(transeletion.getTranselationTextByTextIdentifier("lblQuick")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblIntroduction"));
        txtTeacherFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher") + "'s "
                    + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
        txtStudentFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent") + "'s "
                + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
        txtFreeTutorFunctions.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTryFree")
        + " " + transeletion.getTranselationTextByTextIdentifier("lblTutorInfo")
        + " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
        txtSeeHowItWork.setText(transeletion.getTranselationTextByTextIdentifier("lblSee")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblHowItWorks"));
        txtSchoolDesigned.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolDesined"));
        btnTryItFreeNow.setText(transeletion.getTranselationTextByTextIdentifier("lblTryNowItFree"));
        btnInviteFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblInviteSchool"));
        lblOurSchWill = transeletion.getTranselationTextByTextIdentifier("lblOurSchWill");
        lblInviteSchEmail = transeletion.getTranselationTextByTextIdentifier("lblInviteSchEmail");
        transeletion.closeConnection();
    }

    @Override
    protected void setListenerOnWidgets() {
        rlIntroduction.setOnClickListener(this);
        rlTeacherFunctions.setOnClickListener(this);
        rlStudentFunctions.setOnClickListener(this);
        rlFreeTutorFunctions.setOnClickListener(this);
        rlSeeHowItWork.setOnClickListener(this);
        btnTryItFreeNow.setOnClickListener(this);
        btnInviteFriends.setOnClickListener(this);
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.rlIntroduction:
                this.openVideoUrl("https://www.youtube.com/watch?v=Up0SfHafeN4");
                break;
            case R.id.rlTeacherFunctions:
                startActivity(new Intent(this , ActHelpTeacherFunctions.class));
                break;
            case R.id.rlStudentFunctions:
                startActivity(new Intent(this , ActHelpStudentFunctions.class));
                break;
            case R.id.rlFreeTutorFunctions:
                this.openVideoUrl("https://www.youtube.com/watch?v=vUYFdK4RlGI");
                break;
            case R.id.rlSeeHowItWork:
                this.openVideoUrl("https://www.youtube.com/watch?v=yJxc0-cno0Q");
                break;
            case R.id.btnTryItFreeNow:
            	MathFriendzyHelper.openUrl(this, SCHOOL_APP_LINK);
            	break;
            case R.id.btnInviteFriends:
            	MathFriendzyHelper.sendEmail(this, "", lblOurSchWill, lblInviteSchEmail, "Send Email...");
            	break;            	
        }
    }
    
    /**
     * Open the video
     * @param url
     */
    private void openVideoUrl(String url){
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this, url);
    }
}
