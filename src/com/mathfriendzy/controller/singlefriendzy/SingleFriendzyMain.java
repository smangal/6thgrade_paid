package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SINGLE_FRIENDZY_MAIN;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusResponse;
import com.mathfriendzy.newinappclasses.OnPurchaseDone;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

public class SingleFriendzyMain extends AdBaseActivity implements OnClickListener
{
	private TextView lblSingleFriendzy 				= null;
	private TextView  mfLblTextCompeteWithOthers 	= null;
	private TextView txtPlayerName  				= null;
	private TextView txtPlayerAddress				= null;
	private TextView txtPlayerPoints 				= null;		
	private ImageView imgPlayer     				= null;
	private Button  btnTop100 						= null;
	private RelativeLayout levelLayout              = null;
	private Button btnLevelPlay                     = null;
	private Button btnBonous1                     	= null;
	private Button btnBonous2                     	= null;
	private Button btnBonous3                     	= null;

	private Bitmap profileImageBitmap = null;
	private int completeLevel = 0;
	private int playLevelbuttonWidth = 150;
	private int playLevelButtonMarginHieght = 90;//set the top margin
	private int marginGap = 50;
	private int levelStartingIndex = 0 ;//this will contain the value of starting index which is divisivle by 30
	private int MAX_LEVEL_SHOW = 30 ; // this is used for maximum 30 level on the screen
	private int appStatus    = 0;
	private int UNLOCK_ITEM_ID = 11 ; // unlock item id for single friendzy
	private boolean isUnlockSingleFriendzy = false;//hold the lock/unlock status of single friendzy

	private String bonusCoins = " 200 ";

	//private final int MAX_CONIS = 10000;
	private final int MAX_CONIS = 100;
	private ArrayList<Button> btnList = null;

	private  int SCREEN_DENISITY   = 240;

	private final String TAG = this.getClass().getSimpleName();

	private int max_width_for_level_layout  = 90;
	private int TAB_HEIGHT_1024				= 1024;
	private int textSize					= 0;
	private boolean isTab = false;

	private LinearLayout lnlevelLayout = null;
	private String bonusCoinsText = "";
	private String levelText = "";
	private String playAgainLevel = "";

	//new InApp Change
	private ProgressDialog progressDialogForNewInApp = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_friendzy_main);

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside onCreate()");

		//change for Add ads
		//adFeature();
		//edit done
		this.init();
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTabSmall);
		if(tabletSize)
		{
			//Log.e(TAG, "Tab");
			isTab = true;
			textSize = 30;

			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				max_width_for_level_layout = 150;
				playLevelButtonMarginHieght = 110;
				marginGap = 110;
				playLevelbuttonWidth  = 150;
			}
			else
			{ 
				if (metrics.densityDpi > 160){
					max_width_for_level_layout = 180;
					playLevelButtonMarginHieght = 150;
					marginGap = 146;
					playLevelbuttonWidth  = 200;
				}
				else{
					max_width_for_level_layout = 150;
					playLevelButtonMarginHieght = 110;
					marginGap = 110;
					playLevelbuttonWidth  = 150;
				}
			}
		}
		else
		{
			//Log.e(TAG, "Phone");
			isTab = false;

			textSize = 15;
			if(metrics.heightPixels <= CommonUtils.TAB_HEIGHT && metrics.widthPixels >= CommonUtils.TAB_WIDTH &&
					metrics.densityDpi == CommonUtils.TAB_DENISITY)
			{
				setContentView(R.layout.activity_single_friendzy_main_tab_low_denisity);

				max_width_for_level_layout = 90;
				playLevelbuttonWidth       = 150;
				playLevelButtonMarginHieght = 90;
				marginGap = 55;
			}
			else if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				max_width_for_level_layout = 180;
				playLevelbuttonWidth = 300;
				playLevelButtonMarginHieght  = 180;
				marginGap = 110;
			}
			else
			{
				if ((getResources().getConfiguration().screenLayout &
						Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
						metrics.densityDpi > 160
						&&
						(getResources().getConfiguration().screenLayout &
								Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
								metrics.densityDpi <= SCREEN_DENISITY)
				{
					max_width_for_level_layout = 135;
					playLevelbuttonWidth       = 225;
					playLevelButtonMarginHieght= 135;
					marginGap = 83;
				}
				else
				{
					max_width_for_level_layout = 90;
					playLevelbuttonWidth       = 150;
					playLevelButtonMarginHieght = 90;
					marginGap = 55;
				}
			}
		}	

		this.setWidgetsReferences();
		this.setTextFromTranselation();
		this.setListenerOnWidgets();
		this.setPlayerData();
		this.createLevelLayout();
		//this.setPlayLevelPosition();


		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(!CommonUtils.isInternetConnectionAvailable(this))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside onCreate()");
	}

	private void init(){
		try{
			progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this , "");
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * This method set player data
	 */
	private void setPlayerData() 
	{		
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 1);//get played complete level
		if(completeLevel <= 0)
			completeLevel = 1;
		//completeLevel = 7;

		this.invibleBonusImageButton(this.getInvisibleBonousButton(completeLevel));

		levelStartingIndex = completeLevel / MAX_LEVEL_SHOW ; 

		PlayerTotalPointsObj playerObj = null;
		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();
		if(sharedPreffPlayerInfo.getString("playerId", "0").equals(""))
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints("0");
		}
		else
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
					(sharedPreffPlayerInfo.getString("playerId", "0"));
		}
		learningCenterimpl.closeConn();			

		txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));

		if(sharedPreffPlayerInfo.getString("state", "").equals(""))
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", ""));
		}
		else
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", "")
					+ ", " + sharedPreffPlayerInfo.getString("state", ""));
		}

		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();

		DateTimeOperation numberformat = new DateTimeOperation();
		txtPlayerPoints.setText(numberformat.setNumberString(playerObj.getTotalPoints() + "")
				+ " " + transeletion1.getTranselationTextByTextIdentifier("btnTitlePoints"));

		transeletion1.closeConnection();

		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}


	/**
	 * This method return the index on which the bonus button is invisivle
	 * @param level
	 * @return
	 */
	private int getInvisibleBonousButton(int level)
	{		
		/*if(level % 10 == 0)
		{			
			return (level % MAX_LEVEL_SHOW) / 10;

		}*/

		//return MAX_LEVEL_SHOW / 10 + 1;
		int retValue = 0;
		if(level % MAX_LEVEL_SHOW >= 10 && level % MAX_LEVEL_SHOW < 20)
			retValue = 1;
		else if(level % MAX_LEVEL_SHOW >= 20 && level % MAX_LEVEL_SHOW < 30)
			retValue = 2;
		else if(level % MAX_LEVEL_SHOW == 0 )
			retValue = 0;
		else
			retValue = 4;

		return retValue;

	} 

	/**
	 * This method invisible the bonus images
	 */
	private void invibleBonusImageButton(int index)
	{
		switch(index)
		{
		case 1:
			btnBonous1.setVisibility(Button.INVISIBLE);
			break;
		case 2:
			btnBonous1.setVisibility(Button.INVISIBLE);
			btnBonous2.setVisibility(Button.INVISIBLE);
			break;
		case 0:
			btnBonous1.setVisibility(Button.INVISIBLE);
			btnBonous2.setVisibility(Button.INVISIBLE);
			btnBonous3.setVisibility(Button.INVISIBLE);
			break;
		}
	}


	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside setWidgetsReferences()");

		lblSingleFriendzy 			= (TextView) findViewById(R.id.lblSingleFriendzy);
		mfLblTextCompeteWithOthers 	= (TextView) findViewById(R.id.mfLblTextCompeteWithOthers);
		txtPlayerName 				= (TextView) findViewById(R.id.txtPlayerName);
		txtPlayerAddress 			= (TextView) findViewById(R.id.txtPlayerAddress);
		txtPlayerPoints 			= (TextView) findViewById(R.id.txtPlayerCoins);
		imgPlayer 					= (ImageView) findViewById(R.id.imgPlayer);
		btnTop100 					= (Button) findViewById(R.id.btnTop100);
		levelLayout 				= (RelativeLayout) findViewById(R.id.levelLayout);

		btnLevelPlay                = (Button) findViewById(R.id.btnLevelPlay);
		btnBonous1               	= (Button) findViewById(R.id.btnBonous1);
		btnBonous2                	= (Button) findViewById(R.id.btnBonous2);
		btnBonous3                	= (Button) findViewById(R.id.btnBonous3);

		lnlevelLayout = (LinearLayout) findViewById(R.id.lnlevelLayout);

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside setWidgetsReferences()");
	}


	/**
	 * This method set the text value from translation
	 */
	private void setTextFromTranselation() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside setTextFromTranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
		mfLblTextCompeteWithOthers.setText(transeletion.getTranselationTextByTextIdentifier("lblCompeteWithChildrenAroundWorld") + "!");
		btnTop100.setText(transeletion.getTranselationTextByTextIdentifier("lblRateMe"));
		txtPlayerPoints.setText("0"
				+ " " + transeletion.getTranselationTextByTextIdentifier("btnTitlePoints"));

		btnLevelPlay.setText(transeletion.getTranselationTextByTextIdentifier("playTitle") + " "
				+ transeletion.getTranselationTextByTextIdentifier("mfLblLevel"));

		btnBonous1.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		btnBonous2.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		btnBonous3.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		bonusCoinsText = transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins");
		playAgainLevel = transeletion.getTranselationTextByTextIdentifier("playTitle") + " "
				+ transeletion.getTranselationTextByTextIdentifier("mfLblLevel");
		levelText = transeletion.getTranselationTextByTextIdentifier("mfLblLevel");
		transeletion.closeConnection();

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside setTextFromTranselation()");
	}


	/** 
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		btnTop100.setOnClickListener(this);
		btnLevelPlay.setOnClickListener(this);
	}


	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	private int getApplicationUnLockStatus(int itemId,String userId)
	{	
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}


	/**
	 * This method check application unlock status for unlock the application
	 * @return
	 */
	private boolean checkUnlockStatus()
	{
		if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
			appStatus = 1;
			return true;
		}

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
			return true;
		else if(this.getApplicationUnLockStatus(UNLOCK_ITEM_ID , 
				sharedPreffPlayerInfo.getString("userId", "")) == 1)
			return true;
		else
			return false;
	}


	/**
	 * This method create level layout 
	 */
	@SuppressLint("InflateParams")
	private void createLevelLayout() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside createLevelLayout()");


		/*btnList = new ArrayList<Button>();

		isUnlockSingleFriendzy = this.checkUnlockStatus();

		RelativeLayout top3Layout = new RelativeLayout(this);
		top3Layout.setId(2000);//set id to the layout

		RelativeLayout otherLevelLayout   = new RelativeLayout(this);

		if(!isUnlockSingleFriendzy)
			otherLevelLayout.setBackgroundResource(R.drawable.buttons_orange_bck);


		for( int i = 0 ; i <= 30 ; i ++ )
		{	
			Button btnLevel = new Button(this);
			if(isTab)
			{
				btnLevel.setTextSize(textSize);				
			}

			RelativeLayout.LayoutParams layoutParam = new RelativeLayout.LayoutParams(max_width_for_level_layout,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			layoutParam.addRule(RelativeLayout.CENTER_HORIZONTAL , RelativeLayout.TRUE);

			if(i == 0)
			{				
				btnLevel.setText("Level");
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);

				if(isTab)
					btnLevel.setBackgroundResource(R.drawable.tab_sf_level_ipad);
				else
				btnLevel.setBackgroundResource(R.drawable.sf_level);
				btnLevel.setLayoutParams(layoutParam);
				top3Layout.addView(btnLevel);
			}
			else if( i > 0 && i < 4)
			{
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (i + 1)) - 1));
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);
				if( i == 1 )
				{
					if(isTab)
						btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
					else
						btnLevel.setBackgroundResource(R.drawable.sf_green);

				}
				else
				{
					if(isTab)
						btnLevel.setBackgroundResource(R.drawable.tab_sf_gray_ipad);
					else
						btnLevel.setBackgroundResource(R.drawable.sf_gray);
				}

				layoutParam.addRule(RelativeLayout.BELOW , i + 1 - 1);

				btnLevel.setLayoutParams(layoutParam);

				btnList.add(btnLevel);

				top3Layout.addView(btnLevel);
			}
			else
			{
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (i + 1)) - 1));
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);
				//btnLevel.setBackgroundResource(R.drawable.sf_gray);

				if(isTab)
					btnLevel.setBackgroundResource(R.drawable.tab_sf_gray_ipad);
				else
					btnLevel.setBackgroundResource(R.drawable.sf_gray);

				if( i > 4) // if i < 4 then does not set the below property , because it is the first element in this layout 
					layoutParam.addRule(RelativeLayout.BELOW , i + 1 - 1);

				btnLevel.setLayoutParams(layoutParam);

				otherLevelLayout.addView(btnLevel);

				if( i == 4 && (!isUnlockSingleFriendzy)) //  if i == 4 then add lock to the layout
				{
					layoutParam.setMargins(0, 10, 0, 0);
					RelativeLayout.LayoutParams layoutParamLock = new RelativeLayout.LayoutParams(max_width_for_level_layout / 3 , max_width_for_level_layout / 3);
					layoutParamLock.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
					layoutParamLock.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE);

					Button btnLock = new Button(this);
					btnLock.setBackgroundResource(R.drawable.sf_lock);
					btnLock.setLayoutParams(layoutParamLock);

					btnLock.setOnClickListener(new OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{						
							clickOnLevel(4);
						}
					});

					otherLevelLayout.addView(btnLock);
				}

				btnList.add(btnLevel);
			}

			btnLevel.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					for(int i = 0 ; i < btnList.size() ; i ++ )
					{
						if(v == btnList.get(i))
						{
							//Log.e(TAG, btnList.get(i).getHeight() + "");
							clickOnLevel(i + 1);
						}
					}
				}
			});
		}

		RelativeLayout.LayoutParams layoutParamForTop = new RelativeLayout.LayoutParams(max_width_for_level_layout + 10,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		top3Layout.setLayoutParams(layoutParamForTop);
		levelLayout.addView(top3Layout);

		RelativeLayout.LayoutParams layoutParamForOther = new RelativeLayout.LayoutParams(max_width_for_level_layout + 10,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		layoutParamForOther.addRule(RelativeLayout.BELOW ,top3Layout.getId());
		otherLevelLayout.setLayoutParams(layoutParamForOther);

		levelLayout.addView(otherLevelLayout);*/


		//change for the level layout
		int numberOfCompleleve = completeLevel - (levelStartingIndex * MAX_LEVEL_SHOW);
		Log.e(TAG, "completeLevel "+ completeLevel);
		isUnlockSingleFriendzy = this.checkUnlockStatus();
		//numberOfCompleleve = 5;
		final LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.HORIZONTAL);
		final LinearLayout levelLayout = new LinearLayout(this);
		levelLayout.setOrientation(LinearLayout.VERTICAL);
		levelLayout.setBackgroundResource(R.drawable.buttons_bck);
		final LinearLayout topLayout = new LinearLayout(this);
		topLayout.setOrientation(LinearLayout.VERTICAL);
		final LinearLayout belowTopLayout = new LinearLayout(this);
		belowTopLayout.setOrientation(LinearLayout.VERTICAL);
		final LinearLayout playAgainBonusLayout = new LinearLayout(this);
		playAgainBonusLayout.setOrientation(LinearLayout.VERTICAL);		
		if(!isUnlockSingleFriendzy)
			belowTopLayout.setBackgroundResource(R.drawable.buttons_orange_bck);

		for(int levelIndex = 0 ; levelIndex <= 30 ; levelIndex ++ ){
			View view = LayoutInflater.from(this).inflate(R.layout.single_friendzy_level_layout, 
					null);
			Button btnPlayLevel = (Button) view.findViewById(R.id.btnPlayLevel);
			Button btnLevel = (Button) view.findViewById(R.id.btnLevel);
			ImageView imgLock = (ImageView) view.findViewById(R.id.imgLock);

			if(levelIndex == 0){
				btnLevel.setText(levelText);
				btnLevel.setBackgroundResource(R.drawable.sf_level);
				btnLevel.getLayoutParams().height = (int) getResources()
						.getDimension(R.dimen.play_level_first_index_button_height);
				btnLevel.getLayoutParams().width = (int) getResources()
						.getDimension(R.dimen.play_level_first_index_button_width);
				btnLevel.requestLayout();
				topLayout.addView(view);
			}else if( levelIndex > 0 && levelIndex < 4){				
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (levelIndex + 1)) - 1));				
				topLayout.addView(view);
			}else{
				if( levelIndex == 4 && (!isUnlockSingleFriendzy)){
					imgLock.setVisibility(ImageView.VISIBLE);
				}
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (levelIndex + 1)) - 1));				
				belowTopLayout.addView(view);
			}

			if(numberOfCompleleve == 1 && levelIndex == 1){
				btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
			}else{
				if( levelIndex != 0  && levelIndex <= numberOfCompleleve){
					btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
				}
			}

			/*if(levelIndex == numberOfCompleleve){
				btnPlayLevel.setVisibility(Button.VISIBLE);
			}*/

			btnPlayLevel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					clickOnLevel(completeLevel);					
				}
			});

			btnLevel.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					for(int i = 0 ; i < topLayout.getChildCount() ; i ++ ){
						View view = topLayout.getChildAt(i);
						if((Button)v == ((Button)view.
								findViewById(R.id.btnLevel))){
							clickOnLevel(i);
							return;
						}
					}

					for(int i = 0 ; i < belowTopLayout.getChildCount() ; i ++ ){
						View view = belowTopLayout.getChildAt(i);
						if((Button)v == ((Button)view.
								findViewById(R.id.btnLevel))){
							clickOnLevel(topLayout.getChildCount() + i);
							return ;
						}
					}
				}
			});
		}


		for(int i = 0 ; i <= 30 ; i ++ ){
			View view = LayoutInflater.from(this)
					.inflate(R.layout.single_friendzy_play_again_bonus_layout, null);
			Button btnPlayLevel = (Button) view.findViewById(R.id.btnPlayLevel);
			if(i  == 0){
				btnPlayLevel.getLayoutParams().height = (int) getResources()
						.getDimension(R.dimen.play_level_first_index_button_height);
				btnPlayLevel.getLayoutParams().width = (int) getResources()
						.getDimension(R.dimen.play_level_first_index_button_width);
				btnPlayLevel.requestLayout();
			}

			if(i == numberOfCompleleve){
				btnPlayLevel.setText(playAgainLevel);
				btnPlayLevel.setVisibility(Button.VISIBLE);
				btnPlayLevel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						clickOnLevel(completeLevel);					
					}
				});
			}else if( i > 0 && i % 10 == 0){
				btnPlayLevel.setBackgroundResource(R.drawable.tab_sf_bonus_ipad);
				btnPlayLevel.setText(bonusCoinsText);
				btnPlayLevel.setVisibility(Button.VISIBLE);
			}
			playAgainBonusLayout.addView(view);

		}

		levelLayout.addView(topLayout);
		levelLayout.addView(belowTopLayout);


		mainLayout.addView(playAgainBonusLayout);
		mainLayout.addView(levelLayout);
		lnlevelLayout.addView(mainLayout);

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside createLevelLayout()");
	}


	private int getCurrectLevel(int level){
		int deviceCompleteLevel = completeLevel / 30;
		return deviceCompleteLevel * 30 + level;
	}
	/**
	 * This method call when click on level in single friendzy
	 * @param level
	 */
	private void clickOnLevel(int level)
	{			
		if(level == 0){
			return ;
		}

		//set level for playing at single friendzy equation soleve screen
		SingleFriendzyEquationActivity.completeLevel = this.getCurrectLevel(level);

		if( level < 4)
		{
			this.showPopUpForWarningForPlayWrongLevel(level);
		}
		else
		{
			if(isUnlockSingleFriendzy)
			{
				this.showPopUpForWarningForPlayWrongLevel(level);
			}
			else
			{
				//this.getRequiredCoinsForPurchase();
				this.getAppUnlockCategoryPurchaseStatus();
			}
		}
	}


	/**
	 * This method call when user click on the level, which is not open fro him
	 */
	private void showPopUpForWarningForPlayWrongLevel(int level)
	{
		if(this.getCurrectLevel(level) <= completeLevel)
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				startActivity(new Intent(this,ChooseAchallengerActivity.class));
			}
			else
			{
				this.generateIntenetWarningDialog();
			}
		}
		else
		{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog(transeletion
					.getTranselationTextByTextIdentifier
					("alertMsgCompeteThePreviousLevelToUnlockThis"));
			transeletion.closeConnection();
		}
	}

	/**
	 * This method get coins from server
	 */
	private void getRequiredCoinsForPurchase()
	{
		String api = null;
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(tempPlayer.isTempPlayerDeleted())
		{
			api = "itemId=" + UNLOCK_ITEM_ID + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
					+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");
		}
		else
		{
			api = "itemId=" + UNLOCK_ITEM_ID;
		}

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);
		}
		else
		{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**This method generate warming dialog when intenet connection is not available
	 * 
	 */
	private void generateIntenetWarningDialog()
	{
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateWarningDialogForSingleFriendzy(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
		transeletion.closeConnection();
	}

	/**
	 * This method set the play level Button position based on the complete level
	 */
	private void setPlayLevelPosition()
	{           
		for( int i = 0 ; i < completeLevel - (levelStartingIndex * MAX_LEVEL_SHOW); i ++ )
		{
			if(isTab)
				btnList.get(i).setBackgroundResource(R.drawable.tab_sf_green_ipad);
			else
				btnList.get(i).setBackgroundResource(R.drawable.sf_green);
		}

		RelativeLayout.LayoutParams layoutParamForPlayLevelButton = new RelativeLayout.LayoutParams(playLevelbuttonWidth,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		layoutParamForPlayLevelButton.addRule(RelativeLayout.BELOW , R.id.layoutNowPlaying);
		layoutParamForPlayLevelButton.addRule(RelativeLayout.LEFT_OF , R.id.levelLayout);

		int margintop = playLevelButtonMarginHieght + ((completeLevel % MAX_LEVEL_SHOW - 1) * marginGap);
		layoutParamForPlayLevelButton.setMargins(0, margintop, 0, 0);

		btnLevelPlay.setLayoutParams(layoutParamForPlayLevelButton);
	}


	@Override
	public void onClick(View v) 
	{	
		switch(v.getId())
		{
		case R.id.btnTop100 :
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			MathFriendzyHelper.rateUs(this);
			break;
		case R.id.btnLevelPlay :
			this.clickOnLevel(completeLevel);
			break;
		}

	}

	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this,MainActivity.class));
		super.onBackPressed();
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;

		GetRequiredCoinsForPurchaseItem(String apiValue)
		{
			this.apiString = apiValue;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(SingleFriendzyMain.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();

			/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					|| coindFromServer.getCoinsPurchase() == 0)
			{*/
			if(coindFromServer.getCoinsEarned() == -1)
			{
				LearningCenterimpl learnignCenterObj = new LearningCenterimpl(SingleFriendzyMain.this);
				learnignCenterObj.openConn();
				DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
				dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
						coindFromServer , UNLOCK_ITEM_ID, null);
				learnignCenterObj.closeConn();
			}
			else
			{

				DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
				dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
						coindFromServer , UNLOCK_ITEM_ID, null);
			}
			//}
			/*else
			{
				DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/
			super.onPostExecute(result);
		}
	}

	private void getAppUnlockCategoryPurchaseStatus() {
		//new InApp Changes
		if(!CommonUtils.isInternetConnectionAvailable(this)){
			CommonUtils.showInternetDialog(this);
			return ;
		}

		MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
		MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
				MathFriendzyHelper.getUserId(this) , false , new HttpResponseInterface() {
			@Override
			public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
				if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
					MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
					return ;
				}
				GetAppUnlockStatusResponse response =
						(GetAppUnlockStatusResponse) httpResponseBase;
				if(response.getResult()
						.equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
						&& response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
					MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
					updateUIAfterPurchaseSuccess();
					return ;
				}

				MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
						MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER ,
						new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase,
							int requestCode) {
						MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
						if(requestCode ==
								MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
							return ;
						}
						updateUIAfterPurchaseSuccess();
					}
				} , false , response , new OnPurchaseDone() {
					@Override
					public void onPurchaseDone(boolean isDone) {
						if(isDone) {
							updateUIAfterPurchaseSuccess();
						}
					}
				});
			}
		});		
	}

	private void updateUIAfterPurchaseSuccess(){
		this.onCreate(null);
	}

	private Activity getCurrentActivityObj(){
		return this;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch (requestCode){                
			case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
				this.updateUIAfterPurchaseSuccess();
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
