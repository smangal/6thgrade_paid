package com.mathfriendzy.controller.singlefriendzy;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.controller.learningcenter.SeeAnswerActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;


public class CongratulationScreenForSingleFriendzy extends AdBaseActivity implements OnClickListener
{
	private TextView txtTileScreen				= null;
	private TextView txtGoodEffort				= null;
	private TextView lblTheGameOfLearning		= null;
	private TextView txtEarned					= null;
	private TextView txtEarnedPoints			= null;
	private TextView txtPlayAgain				= null;
	private Button	 btnAnswer					= null;
	private Button	 btnPlay					= null;
	private String   lblPts						= null;
	
	private int isWin 							= 0;
	private String points						= "0";
	private int point							= 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulation_screen_for_single_friendzy);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{
			
			setContentView(R.layout.activity_congratulation_screen_for_single_friendzy_tab_low_denisity);
		}
		
		
		this.getWidgetId();
		this.getIntentValue();
		this.setWidgetText();
	}
	
	private void getIntentValue() 
	{	
		point  = this.getIntent().getIntExtra("points", 0);
		isWin  = this.getIntent().getIntExtra("isWin", 0);
		points = CommonUtils.setNumberString(String.valueOf(point));
	}
	
	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE);
		txtTileScreen.setText(ITextIds.LEVEL+" "+text);
		
		text = translate.getTranselationTextByTextIdentifier("lblTheGameOfLearning");
		lblTheGameOfLearning.setText(text);
		
		if(isWin == 0)
			text = translate.getTranselationTextByTextIdentifier("lblGoodEffort");
		else
			text = translate.getTranselationTextByTextIdentifier("lblYouWon");
		
		txtGoodEffort.setText(text + "!");
		
		text = translate.getTranselationTextByTextIdentifier("lblYouEraned");
		txtEarned.setText(text);
		
		lblPts = translate.getTranselationTextByTextIdentifier("lblPts");
		txtEarnedPoints.setText(points + " " + lblPts);
		
		if(isWin == 1)
		{
			text = translate.getTranselationTextByTextIdentifier("lblYouCanNowPlayTheNextLevel");
		}
		else
		{
			text = translate.getTranselationTextByTextIdentifier("lblYouHaveGoneBackOneLevel");			
		}		
		txtPlayAgain.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("mfLblAnswers");
		btnAnswer.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("mfBtnTitleCompete");
		btnPlay.setText(text);
		
		
		translate.closeConnection();
		
	}//END setWidgetText method
	
	private void getWidgetId() 
	{
		txtTileScreen		= (TextView) findViewById(R.id.txtTitleScreen);		
		txtGoodEffort		= (TextView) findViewById(R.id.txtGoodEffort);
		txtEarned			= (TextView) findViewById(R.id.txtEarned);
		txtEarnedPoints		= (TextView) findViewById(R.id.txtEarnedPoints);
		txtPlayAgain        = (TextView) findViewById(R.id.txtPlayAgain);
		lblTheGameOfLearning= (TextView) findViewById(R.id.lblTheGameOfMath);
		
		btnAnswer			= (Button) findViewById(R.id.btnAnswer);
		btnPlay				= (Button) findViewById(R.id.btnPlay);
			
		btnAnswer.setOnClickListener(this);
		btnPlay.setOnClickListener(this);
	}//END getWidgetId method
	
	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.btnAnswer:	
			startActivity(new Intent(this,SeeAnswerActivity.class));
			break;
		
		case R.id.btnPlay:
			startActivity(new Intent(this,SingleFriendzyMain.class));
			break;
			
		}
		
	}//END onClick Method
	
	@Override
	public void onBackPressed()
	{
		Intent intent = new Intent(this,SingleFriendzyMain.class);
		startActivity(intent);
		super.onBackPressed();
	}

}
