package com.mathfriendzy.controller.webview;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;
import com.sixthgradepaid.R;

public class MyWebView extends ActBase {

    private WebView webView = null;
    private ProgressBar progressBar = null;

    private final String TAG = this.getClass().getSimpleName();
    private String url = null;
    private int initialZoomLevel = 130;
    private RelativeLayout topbar = null;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_you_tube_playe);
        
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setWebViewSetting();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void getIntentValues(){
        url = getIntent().getStringExtra("url");
        if(url.contains("youtube")){
            initialZoomLevel = 1;
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        topbar = (RelativeLayout) findViewById(R.id.topbar);
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        webView = (android.webkit.WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        topbar.setVisibility(RelativeLayout.VISIBLE);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");
    }


    @Override
    protected void setListenerOnWidgets() {

    }


    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(MathFriendzyHelper.getAppName(transeletion));
        transeletion.closeConnection();
    }

    @SuppressWarnings("deprecation")
    private void setWebViewSetting(){

        WebSettings webSettings = webView.getSettings();
        webSettings.setAppCacheEnabled(false);
        webSettings.setJavaScriptEnabled(true);
        webView.setInitialScale(initialZoomLevel);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new MyWebViewClient());
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setEnableSmoothTransition(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setSupportZoom(true);
        webSettings.setUseWideViewPort(true);
        //webSettings.setPluginsEnabled(true);
        //webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
    }

    //load url from onResume
    @Override
    protected void onResume() {
        webView.loadUrl(url);
        super.onResume();
    }

    /**
     * Load blank page to the webview
     */
    private void loadBlankPage(){
        webView.loadUrl("about:blank");
    }

    //load blank url to stop the previous one
    @Override
    public void onBackPressed() {
        this.loadBlankPage();
        Intent intent = new Intent();
        setResult(RESULT_OK , intent);
        super.onBackPressed();
    }

    //load blank url to stop the previous one
    @Override
    protected void onStop() {
        super.onStop();
        this.loadBlankPage();
    }

    class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(ProgressBar.GONE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            progressBar.setVisibility(ProgressBar.GONE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }
}
