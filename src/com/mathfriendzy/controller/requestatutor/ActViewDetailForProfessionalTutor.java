package com.mathfriendzy.controller.requestatutor;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.sixthgradepaid.R;

public class ActViewDetailForProfessionalTutor extends AdBaseActivity implements OnClickListener{

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtMathOfferAPatentPanding = null;
    private Button btnWatchOurVideos = null;
    private TextView txtIfYourChildSchoolNotParticipating = null;
    private Button btnSendEmailToYourPrincipal = null;
    private String lblEmailToPrincipal = "Email to Principal";
    private String inviteEmailSubject = null;
    private String lblEmailContentToPrincipal = null;
    private TextView txtTopbar = null;
    private Button BtnHowItWork = null;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_view_detail_for_professional_tutor);

        CommonUtils.printLog(TAG , "inside onCreate()");

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtMathOfferAPatentPanding = (TextView) findViewById(R.id.txtMathOfferAPatentPanding);
        txtIfYourChildSchoolNotParticipating = (TextView) findViewById(R.id.txtIfYourChildSchoolNotParticipating);
        btnWatchOurVideos = (Button) findViewById(R.id.btnWatchOurVideos);
        btnSendEmailToYourPrincipal = (Button) findViewById(R.id.btnSendEmailToYourPrincipal);
        BtnHowItWork = (Button) findViewById(R.id.BtnHowItWork);
        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnWatchOurVideos.setOnClickListener(this);
        btnSendEmailToYourPrincipal.setOnClickListener(this);
        BtnHowItWork.setOnClickListener(this);
        
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

   
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(MathFriendzyHelper.getAppName(transeletion));
        txtMathOfferAPatentPanding.
                setText(transeletion.getTranselationTextByTextIdentifier("lblOurSisterApp")
                		+ " " + transeletion.getTranselationTextByTextIdentifier("lblMFOfferPatent"));
        txtIfYourChildSchoolNotParticipating.
                setText(transeletion.getTranselationTextByTextIdentifier("lblIfYourChildSchool"));
        btnWatchOurVideos.setText(transeletion.getTranselationTextByTextIdentifier("lblOverview"));
        btnSendEmailToYourPrincipal.
                setText(transeletion.getTranselationTextByTextIdentifier("lblEmailToPrincipal"));
        inviteEmailSubject = transeletion.getTranselationTextByTextIdentifier("inviteEmailSubject");
        lblEmailContentToPrincipal = transeletion.getTranselationTextByTextIdentifier("lblEmailContentToPrincipal");
        BtnHowItWork.setText(transeletion.getTranselationTextByTextIdentifier("lblHowItWorks"));
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }
    
    private void clickOnWatchOurVideo(){
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this
                , MathFriendzyHelper.PROFESSIONAL_TUTORING_OVERVIEW);
    }

    private void clickOnHowItWorkVideo(){
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this
                , MathFriendzyHelper.PROFESSIONAL_TUTORING_WATCH_VEDIO_URL);
    }
    
    private void clickOnSendEmailToPrincipal(){
        MathFriendzyHelper.sendEmailUsingEmailChooser(this , "",
                inviteEmailSubject , lblEmailContentToPrincipal ,
                "Send Email...");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnWatchOurVideos:
                this.clickOnWatchOurVideo();
                break;
            case R.id.btnSendEmailToYourPrincipal:
                this.clickOnSendEmailToPrincipal();
                break;
            case R.id.BtnHowItWork:
            	this.clickOnHowItWorkVideo();
            	break;
        }
    }
}
