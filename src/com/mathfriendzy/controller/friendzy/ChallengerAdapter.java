package com.mathfriendzy.controller.friendzy;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.friendzy.ChallengerDTO;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.sixthgradepaid.R;

public class ChallengerAdapter extends BaseAdapter
{
	Context context;
	public static ArrayList<ChallengerDTO> challengerList;
	String userId;
	private Bitmap profileImageBitmap = null;
	boolean isEdit;
	boolean isTeacher;
	String challengeId;
	ListView listChallenger;
	boolean isPast;
	FriendzyDTO friendzyDTO;

	public ChallengerAdapter(Context context, ArrayList<ChallengerDTO> challengerList, boolean isEdit,
			boolean isTeacher, String challengeId, ListView listChallenger, boolean isPast,FriendzyDTO friendzyDTO){

		this.context = context;
		this.isEdit	= isEdit;
		this.isTeacher = isTeacher;
		this.challengeId = challengeId;
		ChallengerAdapter.challengerList = challengerList;
		this.isPast			= isPast;
		this.listChallenger = listChallenger;
		this.friendzyDTO	= friendzyDTO;
		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
		userId 	= sharedPreffPlayerInfo.getString("userId", "3556");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return challengerList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View row, ViewGroup parent)
	{
		int index = position + 1;
		String time = challengerList.get(position).getTime();
		String points = challengerList.get(position).getPoints();

		ViewHolder holder = null;
		if (row == null)
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.activity_layout_challenger, parent, false);

			holder				= new ViewHolder();		
			holder.txtPos		= (TextView) row.findViewById(R.id.txtIndex);
			holder.name			= (TextView) row.findViewById(R.id.txtname);
			holder.points		= (TextView) row.findViewById(R.id.txtpoints);
			holder.time			= (TextView) row.findViewById(R.id.txttime);
			holder.img			= (ImageView) row.findViewById(R.id.img);
			holder.btnDelete	= (Button) row.findViewById(R.id.btnDelete);

			row.setTag(holder);
		}
		else 
		{
			holder = (ViewHolder) row.getTag();
		}

		holder.txtPos.setText(index+".");
		holder.name.setText(challengerList.get(position).getName());
		setImage(holder.img, challengerList.get(position).getImageId());

		if(challengerList.get(position).getUserId().equals(userId) || isTeacher || isPast)
		{
			holder.points.setText(CommonUtils.setNumberString(points));
			holder.time.setText(DateTimeOperation.setTimeInMinAndSec(Long.parseLong(time)));			
		}
		else
		{
			holder.points.setText("---");
			holder.time.setText("-:-");
		}

		if(isEdit)
		{
			holder.btnDelete.setVisibility(View.VISIBLE);			
		}
		else
		{
			holder.btnDelete.setVisibility(View.GONE);
		}

		holder.btnDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				if(CommonUtils.isInternetConnectionAvailable(context))
				{
					new DeleteChallenge(challengeId, challengerList.get(position).getUserId(),
							challengerList.get(position).getPlayerId(),challengerList.get(position).getName())
					.execute(null, null,null);
				}
				else
				{
					CommonUtils.showInternetDialog(context);
				}
			}
		});

		return row;
	}


	class ViewHolder
	{
		TextView txtPos;
		TextView name;
		TextView time;
		TextView points;		
		ImageView img;
		Button btnDelete;

	}


	/**
	 * This method set the opponent image
	 * @param imgChallenger
	 * @param imageName
	 */
	private void setImage(ImageView imgChallenger , String imageName)
	{
		try
		{
			imgChallenger.setBackgroundResource(0);
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(context))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl , imgChallenger).execute(null,null,null);
			}
			else
			{
				imgChallenger.setBackgroundResource(R.drawable.smiley);

			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(context);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), context);
				imgChallenger.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		private ImageView imgPlayer = null;
		public FacebookImageLoaderTask(String strUrl , ImageView imgPlayer)
		{
			this.strUrl = strUrl;
			this.imgPlayer = imgPlayer;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}


	private class DeleteChallenge extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog pd;	
		String challengeId;
		String userId;
		String playerId;
		String pName;

		public DeleteChallenge(String challengeId, String userId, String playerId, String pName)
		{
			this.challengeId = challengeId;
			this.userId = userId;
			this.playerId = playerId;
			this.pName	  = pName;
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			String json	= friendzy.deletePlayerChallenge(userId, playerId, challengeId);
			challengerList = friendzy.getFriendzyForUser(challengeId);
			try {
				sendDeleteNotification(json, pName);
			} catch (JSONException e) {				
				Log.e("ChallengerAdapter", ""+e.toString());
			}		
			
			return null;
		}


		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			ChallengerAdapter adapter = new ChallengerAdapter(context, challengerList,
					isEdit, isTeacher,challengeId, listChallenger, isPast, friendzyDTO);
			listChallenger.setAdapter(adapter);
			//FriendzyServerOperation friendzy = new FriendzyServerOperation();
			/*if(challengerList.size() != index)
				challengerList.remove(index);*/
			super.onPostExecute(result);
		}

	}
	
	
	private void sendDeleteNotification(String json, String pName) throws JSONException
	{
		//Log.e("ChallengerAdapter", ""+json);
		JSONObject jObj 	= new JSONObject(json);
		JSONObject obj		= jObj.getJSONObject("data");
		String devicePid	= obj.getString("androidPids");
		String uid			= obj.getString("uid");
		String pid			= obj.getString("pid");
		
		FriendzyServerOperation friendzy = new FriendzyServerOperation();
		friendzy.createNotificationForDeleteChallenge(friendzyDTO, devicePid, pName, uid, pid );
		
	}//END sendDeleteNotification Method
}
