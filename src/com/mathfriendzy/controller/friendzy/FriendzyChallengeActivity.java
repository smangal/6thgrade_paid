package com.mathfriendzy.controller.friendzy;

import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore.Images;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.friendzy.ChallengerDTO;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.FriendzyUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.sixthgradepaid.R;
import com.sixthgrade.facebookconnect.ShareActivity;

public class FriendzyChallengeActivity extends AdBaseActivity implements OnClickListener
{
	private FriendzyDTO friendzyDTO					= null;
	private String challengerId						= null;
	private ArrayList<ChallengerDTO> challengerList	= null;

	private TextView mfTitleHomeScreen				= null;
	private TextView txtCountdown					= null;
	private TextView txtDays						= null;
	private TextView txtHours						= null;
	private TextView txtMins						= null;
	private TextView txtSecs						= null;
	private TextView day							= null;
	private TextView hour							= null;
	private TextView min							= null;
	private TextView sec							= null;

	private TextView txtClassname					= null;
	private TextView txtTeachername					= null;
	private TextView txtDuration					= null;
	private TextView txtLeaderboard					= null;
	private TextView txtPos							= null;
	private TextView txtStudentname					= null;
	private TextView txtTime						= null;
	private TextView txtPoints						= null;
	private Button btnReward						= null;
	private Button btnViewreward					= null;
	private Button btnPlaynow						= null;
	private Button btnEdit							= null;

	private LinearLayout layoutCountdown			= null;
	private RelativeLayout layoutActivePast			= null;

	private static String startTime					= null;
	private long interval							= 1000;

	private CountDownTimer timer 					= null;
	private ListView listChallenger					= null;
	private boolean isActive						= false;
	private boolean isSchedule						= false;
	private boolean isTeacher						= false;
	//SharedPreferences sharedPrefFriendzy			= null;	
	SharedPreferences sharedPreffPlayerInfo			= null;
	String reward									= null;
	

	private Button btnCloseShareTool		= null;
	private RelativeLayout layoutShare		= null;
	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;

	private String subject					= null;
	private String body						= null;
	private String screenText				= null;
	
	String edit;
	String done;
	boolean isEdit	= false;
	boolean isPast	= false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friendzy_challenge);

		friendzyDTO 		= StudentChallengeActivity.friendzyObj;
		challengerId 		= getIntent().getStringExtra("challengerId");		
		isActive			= getIntent().getBooleanExtra("isActive", false);
		isSchedule			= getIntent().getBooleanExtra("isSchedule", false);
		isTeacher			= getIntent().getBooleanExtra("isTeacher", false);
		
		getWidgetId();
		setWidgetText();
		//sharedPrefFriendzy 		= getSharedPreferences(ITextIds.FRIENDZY, 0);
		sharedPreffPlayerInfo 	= getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0);
		
		if(isActive)
		{
			if(isTeacher)
			{
				btnEdit.setVisibility(View.VISIBLE);
				btnReward.setVisibility(View.VISIBLE);
				btnViewreward.setVisibility(View.GONE);
				btnPlaynow.setVisibility(View.GONE);
			}
			startTime = friendzyDTO.getEndDate();
			timer = new MyCountDownTimer(DateTimeOperation.getDuration(startTime), interval);
			timer.start();
		}
		else if(isSchedule)
		{
			startTime = friendzyDTO.getStartDate();
			timer = new MyCountDownTimer(DateTimeOperation.getDuration(startTime), interval);

			layoutActivePast.setVisibility(View.GONE);
			btnReward.setVisibility(View.VISIBLE);
			btnViewreward.setVisibility(View.GONE);
			btnPlaynow.setVisibility(View.GONE);		
		}
		else
		{
			isPast	= true;
			btnReward.setVisibility(View.VISIBLE);
			btnViewreward.setVisibility(View.GONE);
			btnPlaynow.setVisibility(View.GONE);
			layoutCountdown.setVisibility(View.GONE);				

		}//End if else condition		

		if(!isSchedule)
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new FindFriendzyForUser(this).execute(null, null, null);
			}
			else
			{
				CommonUtils.showInternetDialog(this);

			}//End if else for Intenet connection
		}

	}//END onCreate method



	private void getWidgetId()
	{
		mfTitleHomeScreen		= (TextView) findViewById(R.id.mfTitleHomeScreen);

		txtClassname			= (TextView) findViewById(R.id.txtClassname);
		txtCountdown			= (TextView) findViewById(R.id.txtCountdown);
		txtDays					= (TextView) findViewById(R.id.txtDays);
		txtDuration				= (TextView) findViewById(R.id.txtDuration);
		txtHours				= (TextView) findViewById(R.id.txtHours);
		txtMins					= (TextView) findViewById(R.id.txtMins);
		txtSecs					= (TextView) findViewById(R.id.txtSecs);
		txtTeachername			= (TextView) findViewById(R.id.txtTeachername);
		txtLeaderboard			= (TextView) findViewById(R.id.txtLeaderboard);
		txtTime					= (TextView) findViewById(R.id.txtTime);
		txtPos					= (TextView) findViewById(R.id.txtPos);
		txtPoints				= (TextView) findViewById(R.id.txtPoints);
		txtStudentname			= (TextView) findViewById(R.id.txtName);

		listChallenger			= (ListView) findViewById(R.id.listChallenger);
		day						= (TextView) findViewById(R.id.day);
		hour					= (TextView) findViewById(R.id.hour);
		min						= (TextView) findViewById(R.id.min);
		sec						= (TextView) findViewById(R.id.sec);
		btnEdit					= (Button) findViewById(R.id.btnEdit);

		layoutActivePast		= (RelativeLayout) findViewById(R.id.layoutActivePast);
		layoutCountdown			= (LinearLayout) findViewById(R.id.layoutCountdown);
		btnReward				= (Button) findViewById(R.id.btnReward);
		btnPlaynow				= (Button) findViewById(R.id.btnPlay);
		btnViewreward			= (Button) findViewById(R.id.btnViewReward);
		
		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);

		btnViewreward.setOnClickListener(this);
		btnPlaynow.setOnClickListener(this);
		btnReward.setOnClickListener(this);
		btnEdit.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);

	}//END getWidgetId method


	private void setWidgetText()
	{
		Translation translate = new Translation(this);
		translate.openConnection();

		mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier("lblFriendzyChallenge"));

		if(isSchedule)
		{
			txtCountdown.setText(translate.getTranselationTextByTextIdentifier("lblCountdownToStart")+":");
		}
		else if(isActive)
		{
			txtCountdown.setText(translate.getTranselationTextByTextIdentifier("lblCountdownToFinish")+":");
		}

		txtDays.setText(translate.getTranselationTextByTextIdentifier("lblDays"));
		txtHours.setText(translate.getTranselationTextByTextIdentifier("lblHours"));
		txtMins.setText(translate.getTranselationTextByTextIdentifier("lblMinutes"));
		txtSecs.setText(translate.getTranselationTextByTextIdentifier("lblSeconds"));

		txtClassname.setText(translate.getTranselationTextByTextIdentifier("lblClass")+" : "+friendzyDTO.getClassName());
		txtTeachername.setText(translate.getTranselationTextByTextIdentifier("pickerTitleTeacher")+
				" : "+friendzyDTO.getTeacherName());
		txtDuration.setText(translate.getTranselationTextByTextIdentifier("lblDuration")+" : "+friendzyDTO.getStartDate()+
				" to "+friendzyDTO.getEndDate());

		btnPlaynow.setText(translate.getTranselationTextByTextIdentifier("playTitle")+" "+
				translate.getTranselationTextByTextIdentifier("lblNow"));
		btnViewreward.setText(translate.getTranselationTextByTextIdentifier("lblViewRewards"));
		btnReward.setText(translate.getTranselationTextByTextIdentifier("lblViewRewards"));
		txtLeaderboard.setText(translate.getTranselationTextByTextIdentifier("lblLeaderboard")+":");

		txtPos.setText(translate.getTranselationTextByTextIdentifier("mfLblPos")+":");
		txtPoints.setText(translate.getTranselationTextByTextIdentifier("btnTitlePoints")+":");
		txtStudentname.setText(translate.getTranselationTextByTextIdentifier("mfLblStudentsName")+":");
		txtTime.setText(translate.getTranselationTextByTextIdentifier("lblTime")+":");
		
		edit = translate.getTranselationTextByTextIdentifier("btnTitleEdit");
		done = translate.getTranselationTextByTextIdentifier("btnTitleDone");
		
		btnEdit.setText(edit);
		reward		= translate.getTranselationTextByTextIdentifier("alertMsgNoRewardsAssigned");
		screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");
		translate.closeConnection();

	}//END setWidgetText method	



	class MyCountDownTimer extends CountDownTimer
	{

		public MyCountDownTimer(long millisInFuture, long countDownInterval)
		{
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() 
		{		
			day.setText("0");
			hour.setText("0");
			min.setText("0");
			sec.setText("0");
		}

		@Override
		public void onTick(long millisUntilFinished)
		{		
			setCountdownTimer(millisUntilFinished/1000);
		}		
	}//END MyCountDownTimer Class


	private void setCountdownTimer(long diff)
	{
		long days = diff/(24*60*60);
		long hour = ((diff)/(60*60)- 24*days);
		long min = ((diff)/(60)- (hour+24*days)*60);
		long sec = (diff - (min+(hour+24*days)*60)*60);

		this.day.setText(days+"");
		this.hour.setText(hour+"");
		this.min.setText(min+"");
		this.sec.setText(sec+"");  

	}//END setCountDownTimer method


	@Override
	protected void onStop() 
	{
		if(timer != null)
			timer.cancel();
		super.onStop();
	}

	
	@Override
	public void onClick(View v)
	{
		Intent intent = null;	
		Translation transeletion = null;
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap b = view.getDrawingCache();
		Top100Activity.b = b;
		DialogGenerator dg = new DialogGenerator(this);
		
		switch(v.getId())
		{
		case R.id.btnPlay:
			if(FriendzyUtils.isChallengerActiveOrNot(challengerId, sharedPreffPlayerInfo.getString("playerId", ""))){
				
				startActivity(new Intent(this, MainActivity.class));
				finish();
			}	
			else
			{
				dg.generateFriendzyCheckDialog(null, challengerId, friendzyDTO);
			}

			break;

		case R.id.btnViewReward:
		case R.id.btnReward:
			if(friendzyDTO.getRewards().length() > 1)
			{
				reward = friendzyDTO.getRewards();
			}
			dg.generateWarningDialog(""+reward);
			break;
			
		case R.id.btnEdit:
			if(!isEdit)
			{
				isEdit = true;
				btnEdit.setText(done);
			}
			else
			{
				isEdit = false;
				btnEdit.setText(edit);
			}
			challengerList = ChallengerAdapter.challengerList;
			ChallengerAdapter adapter = new ChallengerAdapter(FriendzyChallengeActivity.this, challengerList,
					isEdit, isTeacher,challengerId, listChallenger, isPast, friendzyDTO);
			listChallenger.setAdapter(adapter);
			break;
		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);
			break;		

		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			screenText 	= transeletion.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+RATE_URL);
			emailIntent.setType("image/png");
			startActivity(Intent.createChooser(emailIntent, "Send email using"));

			break;

		case R.id.btnFbSahre:	

			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);
			break;
			
		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
			
		}//End switch case 		

	}//END onClick method



	class FindFriendzyForUser extends AsyncTask<Void, Void, Void>
	{	
		ProgressDialog pd = null;
		public FindFriendzyForUser(Context context)
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();			
		}


		@Override
		protected Void doInBackground(Void... params)
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			challengerList = friendzy.getFriendzyForUser(challengerId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			ChallengerAdapter adapter = new ChallengerAdapter(FriendzyChallengeActivity.this, challengerList,
					isEdit, isTeacher,challengerId, listChallenger, isPast, friendzyDTO);
			listChallenger.setAdapter(adapter);
			super.onPostExecute(result);
		}

	}//END FindFriendzyForUser class

}
