package com.mathfriendzy.controller.friendzy.calender;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.sixthgradepaid.R;


@SuppressLint("SimpleDateFormat")
public class GridCellAdapter extends BaseAdapter 
{	
	private Context _context;

	private List<String> list;
	private static final int DAY_OFFSET = 1;
	//private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December"};
	private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	//private int month, year;
	private int daysInMonth;
	private int currentDayOfMonth;
	private int currentWeekDay;
	private TextView gridcell;
	//private RelativeLayout layBg;
	private GridView grid;
	private int month, year;
	//private EfficientAdapter adapter;
	//private ArrayList<ContactBeans> record;
	private int selectedItem;
	private GridCellAdapter adap;
	
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
	
	public GridCellAdapter(Context context, int month, int year, GridView grid, int selectedItem)
	{
		super();
		this._context = context;
		this.list = new ArrayList<String>();
		this.month = month;
		this.year = year;
		this.grid = grid;
		this.selectedItem = selectedItem;

		Calendar calendar = Calendar.getInstance();
		setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
		setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

		// Print Month
		printMonth(month, year);	
		//record = new ArrayList<ContactBeans>();
	}
	private String getMonthAsString(int i)
	{
		return months[i];
	}

	/*private String getWeekDayAsString(int i)
		{
			return weekdays[i];
		}*/

	private int getNumberOfDaysOfMonth(int i)
	{
		return daysOfMonth[i];
	}

	public String getItem(int position)
	{
		return list.get(position);
	}

	@Override
	public int getCount()
	{
		return list.size();
	}

	/**
	 * Prints Month
	 * 
	 * @param mm
	 * @param yy
	 */
	private void printMonth(int mm, int yy)
	{			
		// The number of days to leave blank at
		// the start of this month.
		int trailingSpaces = 0;			
		int daysInPrevMonth = 0;
		int prevMonth = 0;
		int prevYear = 0;
		int nextMonth = 0;
		int nextYear = 0;

		int currentMonth = mm - 1;

		daysInMonth = getNumberOfDaysOfMonth(currentMonth);

		// Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
		GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);

		if (currentMonth == 11)
		{
			prevMonth = currentMonth - 1;
			daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
			nextMonth = 0;
			prevYear = yy;
			nextYear = yy + 1;					
		}
		else if (currentMonth == 0)
		{
			prevMonth = 11;
			prevYear = yy - 1;
			nextYear = yy;
			daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
			nextMonth = 1;					
		}
		else
		{
			prevMonth = currentMonth - 1;
			nextMonth = currentMonth + 1;
			nextYear = yy;
			prevYear = yy;
			daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);					
		}

		// Compute how much to leave before before the first day of the
		// month.
		// getDay() returns 0 for Sunday.
		int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
		trailingSpaces = currentWeekDay;

		if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 2)
		{
			++daysInMonth;
		}

		// Trailing Month days
		for (int i = 0; i < trailingSpaces; i++)
		{					
			list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) +
					"-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
		}

		// Current Month Days
		for (int i = 1; i <= daysInMonth; i++)
		{
			if (i == getCurrentDayOfMonth())
			{
				list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
			}
			else if((list.size() % 7 == 6)||(list.size() % 7 == 0))
			{
				list.add(String.valueOf(i) + "-RED" + "-" + getMonthAsString(currentMonth) + "-" + yy);
			}
			else
			{
				list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
			}
		}

		// Leading Month days
		for (int i = 0; i < (list.size() % 7); i++)
		{
			list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
		}
	}


	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View row, ViewGroup parent)
	{
		if (row == null)
		{
			LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.activity_grid_cell, parent, false);
		}

		// Get a reference to the Day gridcell
		gridcell =  (TextView) row.findViewById(R.id.day_gridcell);
		//layBg	 = (RelativeLayout) row.findViewById(R.id.layBg);
		
		// ACCOUNT FOR SPACING
		String[] day_color = list.get(position).split("-");
		String theday = day_color[0];

		if (!day_color[1].equals("GREY"))
		{
			gridcell.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) 
				{		
					String[] date_month_year = (String[]) v.getTag();
					
					String theDay	= date_month_year[0];
					String themonth = date_month_year[2];
					String theyear = date_month_year[3];
					
					if(Integer.parseInt(theDay) < 10)
					{
						theDay = "0"+theDay;
					}
					String date = getMonth(themonth)+"/"+theDay+"/"+theyear;					
						
					adap = new GridCellAdapter(_context, month, year, grid, position);
					adap.notifyDataSetChanged();
					grid.setAdapter(adap);							
					
					Intent i = new Intent();
					i.putExtra("date", date);					
					((Activity) _context).setResult(1, i);
					((Activity)_context).finish();
					
				}
			});

		}

		// Set the Day GridCell
		gridcell.setText(theday);
		gridcell.setTag(day_color);	
		
		gridcell.setTextColor(Color.parseColor("#000000"));		
		
		if (day_color[1].equals("GREY"))
		{
			gridcell.setTextColor(Color.LTGRAY);
		}
		if (day_color[1].equals("WHITE"))
		{
			gridcell.setTextColor(Color.BLACK);
		}
		if(day_color[1].equals("BLUE"))
		{
			gridcell.setBackgroundResource(R.drawable.kal_tile_today_selected);
		}
			

		if(selectedItem == position)
			gridcell.setBackgroundResource(R.drawable.kal_tile_today_selected);
		else if(selectedItem != -1)
			gridcell.setBackgroundResource(0);

		return row;
	}

	public int getCurrentDayOfMonth()
	{
		return currentDayOfMonth;
	}

	private void setCurrentDayOfMonth(int currentDayOfMonth)
	{
		this.currentDayOfMonth = currentDayOfMonth;
	}
	public void setCurrentWeekDay(int currentWeekDay)
	{
		this.currentWeekDay = currentWeekDay;
	}
	public int getCurrentWeekDay()
	{
		return currentWeekDay;
	}	
	
	
	String getMonth(String month)
	{
		if(month.contains("Jan"))
		{
			return "01";
		}
		else if(month.contains("Feb"))
		{
			return "02";
		}
		else if(month.contains("Mar"))
		{
			return "03";
		}
		else if(month.contains("Apr"))
		{
			return "04";
		}
		else if(month.contains("May"))
		{
			return "05";
		}
		else if(month.contains("Jun"))
		{
			return "06";
		}
		else if(month.contains("Jul"))
		{
			return "07";
		}
		else if(month.contains("Aug"))
		{
			return "08";
		}
		else if(month.contains("Sep"))
		{
			return "09";
		}
		else if(month.contains("Oct"))
		{
			return "10";
		}
		else if(month.contains("Nov"))
		{
			return "11";
		}
		else
		{
			return "12";
		}
	}
}
