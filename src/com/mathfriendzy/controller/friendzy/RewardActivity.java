package com.mathfriendzy.controller.friendzy;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.model.language.translation.Translation;
import com.sixthgradepaid.R;

public class RewardActivity extends AdBaseActivity
{
	private TextView txtReward				= null;
	private TextView mfTitleHomeScreen		= null;
	private EditText edtReward				= null;
	
	private Button btnSubmit				= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reward);
		
		getWidgetId();	
		setTextOnWidget();
	}

	private void getWidgetId()
	{
		txtReward			= (TextView) findViewById(R.id.txtReward);
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		edtReward			= (EditText) findViewById(R.id.edtReward);
		btnSubmit			= (Button) findViewById(R.id.btnSubmit);
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
				Intent i = new Intent();
				i.putExtra("reward", edtReward.getText()+"");
				setResult(RESULT_OK, i);
				finish();
			}
		});
	}
	
	
	
	private void setTextOnWidget()
	{
		Translation tr = new Translation(this);
		tr.openConnection();
		
		mfTitleHomeScreen.setText(tr.getTranselationTextByTextIdentifier("btnTitleCreateChallenge"));
		txtReward.setText(tr.getTranselationTextByTextIdentifier("lblEnterRewardsThatYouWouldLikeToOffer"));
		edtReward.setHint(tr.getTranselationTextByTextIdentifier("lblExampleRewardsOffer"));
		btnSubmit.setText(tr.getTranselationTextByTextIdentifier("btnTitleSubmit"));
		
		tr.closeConnection();
	}

}
