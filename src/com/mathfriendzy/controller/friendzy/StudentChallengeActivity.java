package com.mathfriendzy.controller.friendzy;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.FriendzyUtils;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

public class StudentChallengeActivity extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen			= null;
	private TextView txtActiveTitle				= null;
	private TextView txtPlaying					= null;
	private TextView txtPastTitle				= null;
	private TextView txtActiveChallengerMsg		= null;
	private TextView txtPastChallengerMsg		= null;

	private TextView txtType						= null;
	private TextView txtTimePeriod					= null;	
	private ImageButton imgCheck					= null;
	private RelativeLayout layoutFriendzy			= null;

	private Button btnCreateChallenge				= null;
	private RelativeLayout layoutActiveStrip		= null;
	private RelativeLayout layoutActiveChallenge	= null;
	private RelativeLayout layoutActive				= null;
	private RelativeLayout layoutPast				= null;
	private RelativeLayout layoutActiveFriendzy		= null;
	private RelativeLayout layoutPastFriendzy		= null;

	private ImageButton imgInfo1					= null;
	private ImageButton imgInfo2					= null;
	private String		infoMsg						= "";

	public static FriendzyDTO friendzyObj			= null;

	boolean isPast 									= false;
	boolean isActive 								= false;
	ArrayList<ImageButton> imgChekedList			= null;
	SharedPreferences sharedPreffPlayerInfo			= null;

	private boolean isTeacher						= false;	
	private ArrayList<FriendzyDTO> frndzyList;	
	private int pos									= 0;
	private String playerId							= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_challenge);
		isTeacher		= getIntent().getBooleanExtra("isTeacher", false);

		getWidgetId();
		setTextOnWidget();	

		sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);

		playerId = sharedPreffPlayerInfo.getString("playerId", "1");
		String userId 	= sharedPreffPlayerInfo.getString("userId", "3");

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new FriendzyChallengesForPlayer(this, playerId, userId, isTeacher).execute(null,null,null);
		}
		else
		{
			CommonUtils.showInternetDialog(this);
		}		

	}//END onCreate method


	private void getWidgetId()
	{
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtActiveChallengerMsg	= (TextView) findViewById(R.id.txtActiveChallengerMsg);
		txtActiveTitle			= (TextView) findViewById(R.id.txtActiveTitle);
		txtPastChallengerMsg	= (TextView) findViewById(R.id.txtPastChallengerMsg);
		txtPastTitle			= (TextView) findViewById(R.id.txtPastTitle);
		txtPlaying				= (TextView) findViewById(R.id.txtPlaying);
		/*
		txtType					= (TextView) findViewById(R.id.txtType);
		txtTimePeriod			= (TextView) findViewById(R.id.txtTimePeriod);
		txtPastType				= (TextView) findViewById(R.id.txtPastType);
		txtPastTime				= (TextView) findViewById(R.id.txtPastTimePeriod);*/

		imgInfo1				= (ImageButton) findViewById(R.id.imgInfo1);
		imgInfo2				= (ImageButton) findViewById(R.id.imgInfo2);
		btnCreateChallenge		= (Button) findViewById(R.id.btnCreateChallenge);

		layoutActiveStrip		= (RelativeLayout) findViewById(R.id.layoutActiveStrip);
		layoutActive			= (RelativeLayout) findViewById(R.id.layoutActive);
		layoutPast				= (RelativeLayout) findViewById(R.id.layoutPast);
		layoutActiveFriendzy	= (RelativeLayout) findViewById(R.id.layoutActiveFriendzy);
		layoutPastFriendzy		= (RelativeLayout) findViewById(R.id.layoutPastFriendzy);
		layoutActiveChallenge	= (RelativeLayout) findViewById(R.id.layoutActiveChallenge);
		imgInfo1.setOnClickListener(this);
		imgInfo2.setOnClickListener(this);
		btnCreateChallenge.setOnClickListener(this);
	}//END getWidgetId method



	private void setTextOnWidget()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier("btnTitleChallenges"));

		txtPastTitle.setText(""+translate.getTranselationTextByTextIdentifier("lblPastChallenges")+":");

		btnCreateChallenge.setText(""+translate.getTranselationTextByTextIdentifier("btnTitleCreateChallenge"));

		String pastMsg;
		String activeMsg;
		String playtext = "btnTitlePlaying";
		if(isTeacher)
		{
			txtActiveTitle.setText(translate.getTranselationTextByTextIdentifier("lblActiveScheduledChallenges")+":");	
			if(MainActivity.isTab)
			{
				layoutActiveStrip.setBackgroundResource(R.drawable.active_scheduled_challange_bg_ipad);	
				layoutActiveChallenge.setBackgroundResource(R.drawable.green_arrow_ipad);
			}
			else
			{
				layoutActiveStrip.setBackgroundResource(R.drawable.active_scheduled_challange_bg);
				layoutActiveChallenge.setBackgroundResource(R.drawable.green_round_arrow);
			}

			playtext	= "btnTitleEdit";
			activeMsg = "lblTeacherYouDontHaveAnyActiveChallenge";
			pastMsg	  = "lblTeacherYouDontHaveAnyPastChallenge";


			btnCreateChallenge.setVisibility(View.VISIBLE);
			imgInfo1.setVisibility(View.INVISIBLE);
			imgInfo2.setVisibility(View.INVISIBLE);
		}
		else
		{
			txtActiveTitle.setText(translate.getTranselationTextByTextIdentifier("lblActiveChallenges")+":");
			activeMsg = "lblPlayerYouDontHaveAnyActiveChallenge";
			pastMsg	  = "lblPlayerYouDontHaveAnyPastChallenge";
			infoMsg = translate.getTranselationTextByTextIdentifier("lblFriendzyChallengeWeek")+" \n\n "+
					translate.getTranselationTextByTextIdentifier("lblAskYourTeacherToDownload")+" "+
					ITextIds.LEVEL+" "+translate.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE)+" "+
					translate.getTranselationTextByTextIdentifier("lblAndCreateAnAccountAsTeacher")+" \n\n "+
					translate.getTranselationTextByTextIdentifier("lblYourTeacherWillBeableToChooseDurationOfChallenge");
		}
		txtPlaying.setText(translate.getTranselationTextByTextIdentifier(playtext));
		txtActiveChallengerMsg.setText(translate.getTranselationTextByTextIdentifier(activeMsg));
		txtPastChallengerMsg.setText(translate.getTranselationTextByTextIdentifier(pastMsg));	

		translate.closeConnection();

	}//END setTextonWidget method


	@Override
	public void onClick(View v)
	{				
		switch(v.getId())
		{
		case R.id.imgInfo1:
		case R.id.imgInfo2:
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateFriendzyWarningDialog(infoMsg);
			break;
		case R.id.btnCreateChallenge:
			Intent intent = new Intent(StudentChallengeActivity.this, CreateChallengeActivity.class);
			intent.putExtra("challengerId", "0");
			startActivityForResult(intent, RESULT_FIRST_USER);
		}
	}


	class FriendzyChallengesForPlayer extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog pd;
		String playerId;
		String userId;
		boolean isTeacher;		

		public FriendzyChallengesForPlayer(Context context, String playerId, String userId, boolean isTeacher)
		{
			this.playerId = playerId;
			this.userId	  = userId;
			this.isTeacher	  = isTeacher;

			pd = CommonUtils.getProgressDialog(context);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			FriendzyServerOperation frndObj = new FriendzyServerOperation();
			frndzyList = frndObj.getFriendzyForPlayer(userId, playerId, isTeacher);

			return null;
		}
		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			if(frndzyList.size() > 0)
			{				
				createDynamicActiveLayout(frndzyList);				
			}

			super.onPostExecute(result);
		}

	}


	/**
	 * use to create Active or Past Challenges...
	 * @param frndzyList
	 */
	private void createDynamicActiveLayout(final ArrayList<FriendzyDTO> frndzyList)
	{	
		final LinearLayout activeLay = new LinearLayout(this);
		activeLay.setOrientation(LinearLayout.VERTICAL);

		final LinearLayout pastLay = new LinearLayout(this);
		pastLay.setOrientation(LinearLayout.VERTICAL);
		imgChekedList		= new ArrayList<ImageButton>();

		RelativeLayout child = new RelativeLayout(this);
		for(int i = 0 ; i < frndzyList.size() ; i ++ )
		{
			final int j = i;
			final String challengerId = frndzyList.get(j).getChallengerId();
			@SuppressWarnings("static-access")
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_friendzy, null);

			layoutFriendzy	= (RelativeLayout) child.findViewById(R.id.layoutFriendzy);
			txtType 		= (TextView) child.findViewById(R.id.txtType);
			txtTimePeriod 	= (TextView) child.findViewById(R.id.txtTimePeriod);
			imgCheck		= (ImageButton) child.findViewById(R.id.btncheck);

			if(isTeacher)
			{
				if(MainActivity.isTab)
				{
					layoutFriendzy.setBackgroundResource(R.drawable.green_arrow_ipad);
					imgCheck.setBackgroundResource(R.drawable.bttn_edit_box_ipad);
				}
				else
				{
					layoutFriendzy.setBackgroundResource(R.drawable.green_round_arrow);
					imgCheck.setBackgroundResource(R.drawable.bttn_edit_box);
				}
			}
			else
			{
				//SharedPreferences sf	 = getSharedPreferences(ITextIds.FRIENDZY, 0);
				//Log.e("", ""+sf.getString("challengerId", "-1"));
				/*if(sf.getString("challengerId", "-1").equals(challengerId)
						&& sf.getString("playerId", "").equals(sharedPreffPlayerInfo.getString("playerId", "")))*/
				if(FriendzyUtils.isChallengerActiveOrNot(challengerId, playerId)){					
					if(MainActivity.isTab)
					{
						imgCheck.setBackgroundResource(R.drawable.tab_checkbox_checked_ipad);
					}
					else
					{
						imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					}
				}
			}

			imgChekedList.add(imgCheck);
			if(frndzyList.get(i).getStatus().equals("1") || frndzyList.get(i).getStatus().equals("2") )
			{
				isActive = true;
				layoutActive.setVisibility(View.GONE);
				activeLay.addView(child);	
			}

			if(frndzyList.get(i).getStatus().equals("3"))
			{
				isPast = true;
				layoutPast.setVisibility(View.GONE);
				imgCheck.setVisibility(View.INVISIBLE);
				if(MainActivity.isTab)
				{
					layoutFriendzy.setBackgroundResource(R.drawable.blue_arrow_ipad);
				}
				else
				{
					layoutFriendzy.setBackgroundResource(R.drawable.blue_round_arrow);
				}
				pastLay.addView(child);	
			}	

			txtType.setText(frndzyList.get(i).getClassName());
			txtTimePeriod.setText(frndzyList.get(i).getStartDate()+" to "+frndzyList.get(i).getEndDate());

			layoutFriendzy.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{	
					friendzyObj = frndzyList.get(j);
					Intent intent = new Intent(StudentChallengeActivity.this, FriendzyChallengeActivity.class);
					intent.putExtra("challengerId", challengerId);
					intent.putExtra("isTeacher", isTeacher);
					if(frndzyList.get(j).getStatus().equals("1"))
					{
						intent.putExtra("isActive", true);
					}	
					else if(frndzyList.get(j).getStatus().equals("2"))
					{
						intent.putExtra("isSchedule", true);
					}
					else
					{
						intent.putExtra("isActive", false);
					}					
					startActivity(intent);
				}
			});			

			imgCheck.setOnClickListener(new OnClickListener() 
			{
				//SharedPreferences sharedPreffriendzy	 = getSharedPreferences(ITextIds.FRIENDZY, 0);
				@Override
				public void onClick(View v) 
				{
					for( int  i = 0 ; i < imgChekedList.size() ; i ++ )
					{
						if(v == imgChekedList.get(i))
						{
							if(isTeacher) //For student
							{								
								friendzyObj = frndzyList.get(i);
								pos = i;
								Intent intent = new Intent(StudentChallengeActivity.this, CreateChallengeActivity.class);
								intent.putExtra("challengerId", challengerId);
								startActivityForResult(intent, RESULT_FIRST_USER);
							}
							else
							{

								if(!FriendzyUtils.isChallengerActiveOrNot(challengerId, playerId)){

									DialogGenerator dg = new DialogGenerator(StudentChallengeActivity.this);
									dg.generateFriendzyCheckDialog(imgChekedList.get(i), challengerId, frndzyList.get(i));
								}
								else{

									FriendzyUtils.deActivatePlayer(challengerId, playerId);
									if(MainActivity.isTab)
									{
										imgChekedList.get(i).setBackgroundResource(R.drawable.tab_checkbox_unchecked_ipad);
									}
									else
									{
										imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
									}
								}
							}
						}
						else if(!isTeacher)
						{
							if(!FriendzyUtils.isChallengerActiveOrNot(frndzyList.get(i)
									.getChallengerId(), playerId)){

								if(MainActivity.isTab){
									imgChekedList.get(i).setBackgroundResource(R.drawable.tab_checkbox_unchecked_ipad);
								}
								else{
									imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
								}
							}

						}//End if Else for click check
					}//End for loop
				}//End on Click method
			});
		}

		if(isActive)
			layoutActiveFriendzy.addView(activeLay);
		if(isPast)
			layoutPastFriendzy.addView(pastLay);
	}//END createDynamicActiveLayout Method


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(requestCode == RESULT_FIRST_USER && resultCode == 1)
		{
			if(data.getStringExtra("challengerId").equals("0"))
			{
				frndzyList.add(CreateChallengeActivity.friendzyObj);				
			}
			else if(data.getStringExtra("challengerId").equals("-1"))
			{
				frndzyList.remove(pos);
			}				
			else
			{
				frndzyList.remove(pos);
				frndzyList.add(pos, CreateChallengeActivity.friendzyObj);
			}
			createDynamicActiveLayout(frndzyList);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
