package com.mathfriendzy.controller.school;

import static com.mathfriendzy.utils.ICommonUtils.SEARCH_YOUR_SCHOOL_FLAG;

import java.util.ArrayList;
import java.util.Collections;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.schools.Schools;
import com.mathfriendzy.model.schools.SortByScoolName;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

/**
 * This Activity is open when user want to saelect your school
 * @author Yashwant Singh
 *
 */
public class SearchYourSchoolActivity extends AdBaseActivity implements OnClickListener //, OnKeyListener
{
	private TextView mfTitleHomeScreen 				= null;
	private EditText SearchSchoolEdtSearch 			= null;
	private ListView SearchSchoolListViewSchool 	= null;
	private Button SearchSchoolBtnCannotFindSchool 	= null;
	private Button SearchSchoolBtnHomeSchool 		= null;
	
	protected ArrayList<String>    schoolNameList	  = null;
	protected ArrayList<SchoolDTO> schoolList		  = null;
	protected ArrayList<String>    schoolIdList	  	  = null;
	
	protected final String SCHOOL_TEXT 		= "Cannot find my school";
	protected final String SCHOOL_ID 		= "0000";
	protected final String TEACHAER_TEXT  	= "Cannot find my teacher";
	protected String countryIso					  	  = null;
	protected String stateCodeName				      = null;
	
	ProgressDialog prgressDailog 			= null;
	private SimpleListAdapter adapter 		= null;
	ArrayList<String> newSchoolNameList     = null;
	ArrayList<String> newSchoolIdList       = null;
	int textSize							= 0;
	
	private final String TAG = this.getClass().getSimpleName(); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_your_school);
		
		if(getResources().getBoolean(R.bool.isTabSmall))
		{
			textSize = 24;
		}
		else
		{
			textSize = 14;
		}
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.setTextValues();
		this.getValueFromIntent();
		this.setListenerOnWidgets();
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		SearchSchoolListViewSchool.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) 
			{
				if(newSchoolNameList.get(position).equals(SCHOOL_TEXT))
				{
					addSchool();
				}
				else
				{
					/*Log.e(TAG, "inside setListenerOnWidgets SchoolName , schoolId " + newSchoolNameList.get(position)
							+ " , " + newSchoolIdList.get(position));*/
					
					ArrayList<String> schoolIdAndName = new ArrayList<String>();
					schoolIdAndName.add(newSchoolNameList.get(position));//add school name at 0th position
					schoolIdAndName.add(newSchoolIdList.get(position));//add school id at 1st position
					
					Intent returnIntent = new Intent();
					returnIntent.putExtra("schoolInfo",schoolIdAndName);
					setResult(RESULT_OK,returnIntent);     
					finish();
				}
			}
		});
		
		//SearchSchoolEdtSearch.setOnKeyListener(this);
		SearchSchoolBtnCannotFindSchool.setOnClickListener(this);
		SearchSchoolBtnHomeSchool.setOnClickListener(this);
		
		SearchSchoolEdtSearch.addTextChangedListener(new TextWatcher() 
		{			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				//Log.e(TAG, "inside onTextChanged");			
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) 
			{				
				//Log.e(TAG, "inside beforeTextChanged");	
			}
			
			@Override
			public void afterTextChanged(Editable s) 
			{
				newSchoolNameList = new ArrayList<String>();
				
				/* This logic for the searching according to the given string in search Bar(EditText)
				 and display which match
				 */
				for(int i = 0 ; i < schoolNameList.size() ; i++)
				{
					if(schoolNameList.get(i).toLowerCase().contains(SearchSchoolEdtSearch.getText().toString()
							.toLowerCase())) 
						newSchoolNameList.add(schoolNameList.get(i));
				}
				adapter = new SimpleListAdapter(SearchYourSchoolActivity.this, newSchoolNameList, textSize);
				SearchSchoolListViewSchool.setAdapter(adapter);
				
				//Log.e(TAG, "inside afterTextChanged");				
			}
		});
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	/**
	 * This method get values from Intent 
	 */
	private void getValueFromIntent() 
	{
	
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside getValueFromIntent()");
		
		//changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new GetSchoolsData(this.getIntent().getStringExtra("country"), 
					this.getIntent().getStringExtra("state"), 
					this.getIntent().getStringExtra("city"), 
					this.getIntent().getStringExtra("zip"), 
					"", "", this).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside getValueFromIntent()");
	}

	/**
	 * This method set the widgets references from the layout to widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		mfTitleHomeScreen 				= (TextView) findViewById(R.id.mfTitleHomeScreen);
		SearchSchoolEdtSearch 			= (EditText) findViewById(R.id.SearchSchoolEdtSearch);
		SearchSchoolListViewSchool 		= (ListView) findViewById(R.id.SearchSchoolListViewSchool);
		SearchSchoolBtnCannotFindSchool = (Button) 	 findViewById(R.id.SearchSchoolBtnCannotFindSchool);
		SearchSchoolBtnHomeSchool       = (Button)	 findViewById(R.id.SearchSchoolBtnHomeSchool);
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the widgets Text values from the Translation
	 */
	private void setTextValues()
	{
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside setTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		SearchSchoolBtnCannotFindSchool.setText(transeletion.getTranselationTextByTextIdentifier("pickerValSchoolNotFound"));
		SearchSchoolBtnHomeSchool.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleHome")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblRegSchool"));
		transeletion.closeConnection();
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside setTextValues()");
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.SearchSchoolBtnCannotFindSchool:
			this.addSchool();
			break;
		case R.id.SearchSchoolBtnHomeSchool:
			ArrayList<String> schoolIdAndName = new ArrayList<String>();
			schoolIdAndName.add("Home School");//add school name at 0th position
			schoolIdAndName.add("1");//add school id at 1st position
			
			Intent returnIntent = new Intent();
			returnIntent.putExtra("schoolInfo",schoolIdAndName);
			setResult(RESULT_OK,returnIntent);     
			finish();
			break;
		}
	}
	
	/**
	 * This method add School on the server when user can not find your school in the given list
	 */
	private void addSchool()
	{	
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside addSchool()");
		
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			
			if(this.getIntent().getStringExtra("country").equals("United States")
					||this.getIntent().getStringExtra("country").equals("Canada"))
			{
				 ArrayList<String> schoolInfo = new ArrayList<String>();
				 schoolInfo.add(SCHOOL_TEXT);
				 schoolInfo.add(SCHOOL_ID);
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateSchoolDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgDidNotFindSchool"),schoolInfo,RESULT_OK);
			}
			else
			{
				SchoolInfoSubmitter.countryName   = this.getIntent().getStringExtra("country");
				SchoolInfoSubmitter.stateName     = this.getIntent().getStringExtra("state");
				SchoolInfoSubmitter.city          = this.getIntent().getStringExtra("city");
				SchoolInfoSubmitter.zip           = this.getIntent().getStringExtra("zip");
				
				Intent intent = new Intent(this,SchoolInfoSubmitter.class);
				startActivityForResult(intent, 1);
			}
			transeletion.closeConnection();
			
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "outside addSchool()");
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "inside onActivityResult()");
		
		if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         //Log.e(TAG, "school Name" + schoolInfo.get(0));
		         Intent returnIntent = new Intent();
				 returnIntent.putExtra("schoolInfo",schoolInfo);
				 setResult(RESULT_OK,returnIntent);     
				 finish();
		     }
		    
		 }
		
		if(SEARCH_YOUR_SCHOOL_FLAG)
			Log.e(TAG, "outside onActivityResult()");
		
		super.onActivityResult(requestCode, resultCode, data);
	}


	/**
	 * Get School From server based on country,state,city,zip code
	 * @author Yashwant Singh
	 *
	 */
	public class GetSchoolsData extends AsyncTask<Void, Void, ArrayList<SchoolDTO>>
	{
		private String country  	= null;
		private String state    	= null;
		private String city     	= null;
		private String zip			= null;
		private String schoolName 	= null;
		private String teacherName  = null;
		private Context context 	= null;
		
		public GetSchoolsData(String country, String state , String city , String zipCode ,String schoolName, 
				String teacherName,Context context)
		{
			this.country 	= country;
			this.state 		= state;
			this.city  		= city;
			this.zip   		= zipCode;
			this.schoolName = schoolName;
			this.teacherName= teacherName;
			this.context 	= context;
		}
				
		@Override
		protected void onPreExecute() 
		{
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "inside GetSchoolsData onPreExecute()");
			
			prgressDailog = CommonUtils.getProgressDialog(context);
			prgressDailog.show();
			
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "outside GetSchoolsData onPreExecute()");
			
			super.onPreExecute();
		}
		
		@Override
		protected ArrayList<SchoolDTO> doInBackground(Void... params) 
		{	
			
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "inside GetSchoolsData doInBackground()");
			
			Country countryObj = new Country();
			schoolList = new ArrayList<SchoolDTO>();
			schoolNameList = new ArrayList<String>();
			schoolIdList   = new ArrayList<String>();
			
			countryIso = countryObj.getCountryIsoByCountryName(country , context);
			
			Schools schoolObj = new Schools();
			
			if(country.equals("United States") || country.equals("Canada"))
			{		
				States stateObj = new States();
				stateCodeName = stateObj.getStateCodeNameByStateNameFromUS(state, context);
				schoolList = schoolObj.getSchoolList(countryIso, stateCodeName, city, zip);
			}
			else
			{
				schoolList = schoolObj.getSchoolListOtherThanUSandCanada(countryIso);
			}
			
			
			//changes
			SchoolDTO schoolDataObj = new SchoolDTO();
			schoolDataObj.setSchoolId(SCHOOL_ID);
			schoolDataObj.setSchoolName(SCHOOL_TEXT);
			schoolList.add(schoolDataObj);
			//end changes
			
			/*for( int i = 0 ; i < schoolList.size() ; i++)
			{
				schoolNameList.add(schoolList.get(i).getSchoolName());
				schoolIdList.add(schoolList.get(i).getSchoolId());
			}*/
			
			Collections.sort(schoolList, new SortByScoolName());
			
			for( int i = 0 ; i < schoolList.size() ; i++)
			{
				schoolNameList.add(schoolList.get(i).getSchoolName());
				schoolIdList.add(schoolList.get(i).getSchoolId());
			}
			/*schoolNameList.add(SCHOOL_TEXT);
			schoolIdList.add(SCHOOL_ID);*/
			
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "outside GetSchoolsData doInBackground()");
			
			return schoolList;
		}

		@Override
		protected void onPostExecute(final ArrayList<SchoolDTO> result) 
		{	
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "inside GetSchoolsData onPostExecute()");
			
			prgressDailog.cancel();
			
			newSchoolNameList = schoolNameList;
			newSchoolIdList   = schoolIdList;
			
			/*for(int i = 0 ; i < newSchoolNameList.size() ; i ++ )
			{
				Log.e(TAG, "SchoolName " + newSchoolNameList.get(i) + " school id " + newSchoolIdList.get(i));
			}
			*/
			
			//Collections.sort(newSchoolNameList);
						
			adapter = new SimpleListAdapter(context, newSchoolNameList, textSize);
			SearchSchoolListViewSchool.setAdapter(adapter);
			
			if(SEARCH_YOUR_SCHOOL_FLAG)
				Log.e(TAG, "outside GetSchoolsData onPostExecute()");
			
			super.onPostExecute(result);
		}
	}
}
