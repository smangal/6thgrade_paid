package com.mathfriendzy.controller.registration;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MODIFY_USER_FLAG;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.sixthgradepaid.R;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.player.AddPlayer;
import com.mathfriendzy.controller.player.BasePlayer;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.customview.DynamicLayout;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.Validation;

/**
 * This Activity is used for modify registered user
 * @author Yashwant Singh
 *
 */
public class ModifyRegistration extends BasePlayer implements OnClickListener , EditTextFocusChangeListener {

	private TextView regTextAreYouA 	    = null;
	private TextView mfBtnTitleStudents     = null;
	private TextView lblRegParent 		    = null;
	private TextView lblRegEmail 		    = null;
	private TextView lblRegPassword 	    = null;
	private TextView textAccount		    = null;
	private TextView lblRegLanguage 		= null;
	private TextView lblRegVolume 			= null;
	private SeekBar  seekbarVolume 		    = null;
	private EditText edtEmail               = null;
	private EditText edtPass                = null;
	private Spinner spinnerLanguage 		= null;
	private TextView btnTitlePlayers        = null;
	private Button   btnTitleAddPlayer      = null;
	private TextView lblPleaseSelectPlayer  = null;
	private TextView textResult             = null;
	private TextView btnTitleEdit           = null;
	private ImageButton RegModifyImgBtnStudent = null;
	private ImageButton RegModifyImgBtnParent  = null;
	private ImageButton RegModifyImgBtnTeacher = null;
	private LinearLayout linearLayout 		   = null;

	private String languageCode 			= null;
	private String languageId   			= null;
	private Button btnTitleLogOut			= null;
	private Button playTitle      			= null;

	private RegistereUserDto regUserObj 			= null;
	private ArrayList<UserPlayerDto> userPlayerList = null;
	private ProgressDialog progressDialog 			= null;
	private ArrayList<String>     languageList    	= null;

	private boolean isTeacher = false;
	private boolean isStudent = false;
	private boolean isParent  = false;

	private final String TAG = this.getClass().getSimpleName();
	/*private boolean isSetSchool = true;*/
	/*private String schoolIdFromFind   = null;
    private String schoolNameFromFind = null;*/
	private Button  btnTitleBack      = null;

	private boolean isClickOnLogout = false;

	//for new registration changes
	private RelativeLayout studentLayout = null;
	private RelativeLayout parentLayout  = null;
	private RelativeLayout teacherLayout = null;
	private RelativeLayout stateSpinnerLayout = null;
	//for account type
	private final int TEACHER = 1;
	private final int STUDENT = 2;
	private final int PARENT  = 3;

	//for new registration changes
	private String profileImageName = MathFriendzyHelper.DEFAULT_PROFILE_IMAGE;
	private LinearLayout avtarGradeLayout = null;
	private ImageView avtarImage = null;
	private Spinner edtGrade = null;
	private ArrayList<String> gradeList = null;
	private String zipText = null;
	private String cityText = null;
	private TextView playerChildernAccount = null;
	private RelativeLayout avtarLayout = null;
	private final int SELECT_AVATAR_REQUEST = 10001;
	private TextView txtAvtar = null;
	private String lblYouCantChangeAccount = null;
	private SchoolDTO school = null;
	private String alertYouCanNotAddMoreThanMaxLimitPlayer = null;

	//for teacher new registration
	private EditText edtUserName = null;

	private int numberOfPlayer = 0;
	private EditText spinnerTeacher = null;
	private String studentInfoModificationNotice = null;

	private String lblStudent = null;
	private String lblTeacher = null;
	private String lblParent = null;
	private String lblAccount = null;

	//for again new changes on 18 jun 2015
	private TextView lblTextTeacher = null;
	private LinearLayout avatarGradeLavelLayout = null;
	private TextView lblAvatar = null;
	private TextView lblGrade = null;
	private TextView lblRegUserName = null;
	//private String lblEnterEmailToReceiveWeeklyReport = "Enter an email to receive weekly progress reports";
	private TextView lblRegEmailDescription = null;
	//private boolean isAdultDialogShown = false;
	private boolean onFocusLostIsValidString = true;
	private String stateText = null;

	//for weekly report
	private RelativeLayout recieveWeeklyReporLayout = null;
	private ImageView imgRecieveWeeklyReport = null;
	private TextView txtReciveWeeklyReport = null;
	private int emailSubscription = 0;
	private boolean isEmailValid = false;
	private String previousUserEmail = null;
	private String alertOnlyTeacherCanChange = null;

	//private String lblOnlyLastInitial = "Only the last initial is allowed";

	private String lblLastInitial = "Last Initial";
    private String txtLastName = "Last Name";
    private String lblOnlyLastInitial = null;
    
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_registration);

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside onCreate()");

		this.init();
		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getRegisterUserData();
		this.getUserPlayerData();
		this.setWidgesDataFromDatabase();
		this.getLanguages();

		this.setListenerOnWidgets();
		this.setWeeklyReport();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method return the user player data
	 * @return
	 */
	private UserPlayerDto getPlayerData(){
		try{
			return MathFriendzyHelper.getSelectedPlayerDataById
					(this, MathFriendzyHelper.getSelectedPlayerID(this));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	private void init(){
		gradeList = MathFriendzyHelper.getUpdatedGradeList(this);
		this.initilizeSchoolWithDefaultValue();
	}

	private void initilizeSchoolWithDefaultValue(){
		school = new SchoolDTO();
		school.setSchoolId("0");
		school.setSchoolName("");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");

		edtFirstName 	= (EditText) findViewById(R.id.edtFirstName);
		edtLastName  	= (EditText) findViewById(R.id.edtLastName);
		edtCity 	 	= (EditText) findViewById(R.id.edtCity);
		edtZipCode 	 	= (EditText) findViewById(R.id.edtZip);
		edtEmail 	 	= (EditText) findViewById(R.id.edtEmail);
		edtPass 	 	= (EditText) findViewById(R.id.edtPass);
		spinnerCountry	= (Spinner) findViewById(R.id.spinnerCountry);
		spinnerState 	= (Spinner) findViewById(R.id.spinnerState);
		spinnerLanguage	= (Spinner) findViewById(R.id.spinnerLanguage);
		lblRegState     = (TextView)findViewById(R.id.lblRegState);
		mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblFirstName 		=  (TextView) findViewById(R.id.lblFirstName);
		lblLastName 		= (TextView) findViewById(R.id.lblLastName);
		lblRegCountry 		= (TextView) findViewById(R.id.lblRegCountry);
		lblRegCity 			= (TextView) findViewById(R.id.lblRegCity);
		lblRegZip 			= (TextView) findViewById(R.id.lblRegZip);
		seekbarVolume 		= (SeekBar) findViewById(R.id.seekbarVolume);
		textAccount         = (TextView) findViewById(R.id.textAccount);
		regTextAreYouA      = (TextView) findViewById(R.id.regTextAreYouA);
		mfBtnTitleStudents  = (TextView) findViewById(R.id.mfBtnTitleStudents);
		lblRegParent        = (TextView) findViewById(R.id.lblRegParent);
		lblRegTeacher       = (TextView) findViewById(R.id.lblRegTeacher);
		lblRegEmail       	= (TextView) findViewById(R.id.lblRegEmail);
		lblRegPassword      = (TextView) findViewById(R.id.lblRegPassword);
		lblRegLanguage      = (TextView) findViewById(R.id.lblRegLanguage);
		lblRegVolume        = (TextView) findViewById(R.id.lblRegVolume);
		btnTitlePlayers     = (TextView) findViewById(R.id.btnTitlePlayers);
		lblPleaseSelectPlayer=(TextView) findViewById(R.id.lblPleaseSelectPlayer);
		textResult          = (TextView) findViewById(R.id.textResult);
		btnTitleEdit        = (TextView) findViewById(R.id.btnTitleEdit);
		btnTitleLogOut      = (Button) findViewById(R.id.btnTitleLogOut);
		playTitle           = (Button) findViewById(R.id.playTitle);
		btnTitleAddPlayer   = (Button) findViewById(R.id.btnTitleAddPlayer);
		RegModifyImgBtnStudent = (ImageButton) findViewById(R.id.RegModifyImgBtnStudent);
		RegModifyImgBtnParent  = (ImageButton) findViewById(R.id.RegModifyImgBtnParent);
		RegModifyImgBtnTeacher = (ImageButton) findViewById(R.id.RegModifyImgBtnTeacher);

		pickerTitleSchool      = (TextView) findViewById(R.id.lblRegSchool);
		/*spinnerSchool          = (Spinner) findViewById(R.id.spinnerSchool);*/
		edtSchool 			   = (EditText) findViewById(R.id.spinnerSchool);//changes

		linearLayout           = (LinearLayout) findViewById(R.id.userPlayerLayout);

		btnTitleBack           = (Button)   findViewById(R.id.btnTitleBack);

		studentLayout          = (RelativeLayout) findViewById(R.id.studentLayout);
		parentLayout           = (RelativeLayout) findViewById(R.id.parentLayout);
		teacherLayout          = (RelativeLayout) findViewById(R.id.teacherLayout);
		stateSpinnerLayout     = (RelativeLayout) findViewById(R.id.stateSpinnerLayout);
		spinnerTeacher         = (EditText) findViewById(R.id.spinnerTeacher);

		//set Done button to keyboard
		MathFriendzyHelper.setDoneButtonToEditText(edtFirstName);
		MathFriendzyHelper.setDoneButtonToEditText(edtLastName);
		MathFriendzyHelper.setDoneButtonToEditText(edtEmail);
		MathFriendzyHelper.setDoneButtonToEditText(edtPass);
		MathFriendzyHelper.setDoneButtonToEditText(edtCity);
		MathFriendzyHelper.setDoneButtonToEditText(edtZipCode);
		//end changes


		//for new registration changes
		avtarGradeLayout = (LinearLayout) findViewById(R.id.avtarGradeLayout);
		avtarImage = (ImageView) findViewById(R.id.avtarImage);
		txtAvtar = (TextView) findViewById(R.id.txtAvtar);
		edtGrade = (Spinner) findViewById(R.id.edtGrade);
		playerChildernAccount = (TextView) findViewById(R.id.playerChildernAccount);
		avtarLayout = (RelativeLayout) findViewById(R.id.avtarLayout);
		edtUserName = (EditText) findViewById(R.id.edtUserName);

		lblTextTeacher = (TextView) findViewById(R.id.lblTextTeacher);
		avatarGradeLavelLayout = (LinearLayout) findViewById(R.id.avatarGradeLavelLayout);
		lblAvatar = (TextView) findViewById(R.id.lblAvatar);
		lblGrade = (TextView) findViewById(R.id.lblGrade);
		lblRegUserName = (TextView) findViewById(R.id.lblRegUserName);
		lblRegEmailDescription = (TextView) findViewById(R.id.lblRegEmailDescription);

		//for weekly report
		recieveWeeklyReporLayout = (RelativeLayout) findViewById(R.id.recieveWeeklyReporLayout);
		imgRecieveWeeklyReport = (ImageView) findViewById(R.id.imgRecieveWeeklyReport);
		txtReciveWeeklyReport = (TextView) findViewById(R.id.txtReciveWeeklyReport);

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the widgets text values from the translation
	 */
	private void setWidgetsTextValues()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setWidgetsTextValues()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
		lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName"));
		lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial"));
		lblRegCountry.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry"));
		lblRegState.setText(transeletion.getTranselationTextByTextIdentifier("lblRegState"));
		lblRegCity.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCity"));
		lblRegZip.setText(transeletion.getTranselationTextByTextIdentifier("lblRegZip"));
		mfBtnTitleStudents.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent"));
		lblRegParent.setText(transeletion.getTranselationTextByTextIdentifier("lblRegParent"));
		lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"));
		lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
		lblRegPassword.setText(transeletion.getTranselationTextByTextIdentifier("lblRegPassword"));
		lblRegLanguage.setText(transeletion.getTranselationTextByTextIdentifier("lblRegLanguage"));
		lblRegVolume.setText(transeletion.getTranselationTextByTextIdentifier("lblRegVolume"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
		btnTitleAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleAddPlayer"));
		//btnTitleAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblAddAnotherChild"));
		//lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		textResult.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		btnTitleEdit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEdit"));
		btnTitleLogOut.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleLogOut"));
		playTitle.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblNow") + "!");
		textAccount.setText(transeletion.getTranselationTextByTextIdentifier("textAccount"));
		regTextAreYouA.setText(transeletion.getTranselationTextByTextIdentifier("regTextAreYouA"));
		pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblRegSchool"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));

		//for new registration
		playerChildernAccount.setText(transeletion.getTranselationTextByTextIdentifier("lblplayerChildrenAccount"));
		txtAvtar.setText(transeletion.getTranselationTextByTextIdentifier("lblAvatar"));
		zipText = transeletion.getTranselationTextByTextIdentifier("lblRegZip");
		cityText = transeletion.getTranselationTextByTextIdentifier("lblRegCity");
		stateText = transeletion.getTranselationTextByTextIdentifier("lblRegState");
		alertYouCanNotAddMoreThanMaxLimitPlayer = transeletion
				.getTranselationTextByTextIdentifier("lblYouCantAddMorePlayers");

		lblYouCantChangeAccount = transeletion.getTranselationTextByTextIdentifier("lblYouCantChangeAccount");
		studentInfoModificationNotice = transeletion
				.getTranselationTextByTextIdentifier("studentInfoModificationNotice");

		lblStudent = transeletion.getTranselationTextByTextIdentifier("lblStudent");
		lblParent = transeletion.getTranselationTextByTextIdentifier("lblRegParent");
		lblTeacher = transeletion.getTranselationTextByTextIdentifier("lblRegTeacher");
		lblAccount = transeletion.getTranselationTextByTextIdentifier("textAccount");

		try {
			if (this.getPlayerData() != null) {
				lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblNowPlaying"));
			} else {
				lblPleaseSelectPlayer.setText(transeletion
						.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
			}
		}catch(Exception e){
			lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		}
		lblTextTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"));
		lblAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblAvatar"));
		lblGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
		lblRegUserName.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName"));
		lblRegEmailDescription.setText(transeletion.getTranselationTextByTextIdentifier
				("lblAddYourEmailToReciveReport"));

		edtZipCode.setHint(zipText);
		edtCity.setHint(cityText);
		txtReciveWeeklyReport.setText(transeletion.getTranselationTextByTextIdentifier("lblReceiveWeeklyReports"));
		alertOnlyTeacherCanChange = transeletion
				.getTranselationTextByTextIdentifier("alertOnlyTeacherCanChange");
		//lblOnlyLastInitial = transeletion.getTranselationTextByTextIdentifier("lblOnlyLastInitial");
		this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblFirstName"), edtFirstName);
		this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial"), edtLastName);
		this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblUserName"), edtUserName);
		
		lblLastInitial = transeletion.getTranselationTextByTextIdentifier("lblLastInitial");
        txtLastName = transeletion.getTranselationTextByTextIdentifier("lblLastName");
        lblOnlyLastInitial = transeletion.getTranselationTextByTextIdentifier("lblOnlyLastInitial");
		transeletion.closeConnection();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setWidgetsTextValues()");
	}

	protected void setHintToEditText(String text ,  EditText edtText){
		edtText.setHint(text);
	}

	/**
	 * This method get Registered user data from th database
	 */
	private void getRegisterUserData()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside getRegisterUserData()");

		regUserObj = new RegistereUserDto();
		UserRegistrationOperation userOprObj = new UserRegistrationOperation(this);
		regUserObj = userOprObj.getUserData();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside getRegisterUserData()");
	}

	/**
	 * Check for teacher, student and parent
	 * @return
	 */
	private int getYouAre(){
		try{
			if(regUserObj.getIsParent().equals("2")){//2 for student
				return STUDENT;
			}else if(regUserObj.getIsParent().equals("1")){// 1 for parent
				return PARENT ;
			}else if(regUserObj.getIsParent().equals("0")){//0 for teacher
				return TEACHER;
			}else{
				return TEACHER;
			}
		}catch(Exception e){
			e.printStackTrace();
			return TEACHER;
		}
	}

	/**
	 * This method get Temp Player data from database
	 */
	private void getUserPlayerData()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside getUserPlayerData()");

		userPlayerList = new ArrayList<UserPlayerDto>();
		UserPlayerOperation userOprObj = new UserPlayerOperation(this);
		userPlayerList = userOprObj.getUserPlayerData();

		//for new registration changes
		numberOfPlayer = userPlayerList.size();

		if(userPlayerList.size() > 0){
			btnTitleEdit.setVisibility(TextView.VISIBLE);
			lblPleaseSelectPlayer.setVisibility(TextView.VISIBLE);
			textResult.setVisibility(TextView.VISIBLE);

			DynamicLayout dynamicLayout = new DynamicLayout(this);
			/*dynamicLayout.createDyanamicLayoutForDisplayUserPlayer
			(userPlayerList, linearLayout , true);*/
			//the new one
			/*dynamicLayout.createDyanamicLayoutForDisplayUserPlayer
                    (userPlayerList, linearLayout, true, this.getYouAre());*/
			//changes for single player
			if(numberOfPlayer == 1) {
				dynamicLayout.createDyanamicLayoutForDisplayUserPlayer
				(userPlayerList, linearLayout, true, TEACHER);
			}else{
				dynamicLayout.createDyanamicLayoutForDisplayUserPlayer
				(userPlayerList, linearLayout, true, PARENT);
			}
		}
		else{
			btnTitleEdit.setVisibility(TextView.GONE);
			lblPleaseSelectPlayer.setVisibility(TextView.GONE);
			textResult.setVisibility(TextView.GONE);
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside getUserPlayerData()");
	}

	/**
	 * This method set the data to the widgets from the database and user can edit this.
	 */
	private void setWidgesDataFromDatabase()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setWidgesDataFromDatabase()");

		this.setYouAreA();
		this.setEditTextData();
		this.getCountries(this, this.getCountryNameById(regUserObj.getCountryId()));

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setWidgesDataFromDatabase()");
	}

	/**
	 * This method return the country name by the country id
	 * @param countryId
	 * @return
	 */
	private String getCountryNameById(String countryId)
	{
		Country countryObj = new Country();
		return countryObj.getCountryNameByCountryId(countryId, this);
	}

	private void setAccountTypeText(String accountType){
		textAccount.setText(accountType + "'s" + " " + lblAccount);
	}

	/**
	 * This method set the check image for the student,teacher,and parent
	 */
	private void setYouAreA()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setYouAreA()");

		if(regUserObj.getIsParent().equals("2"))//2 for student
		{
			this.setAccountTypeText(lblStudent);

			RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			this.setEmailText(1);
			edtSchool.setVisibility(Spinner.GONE);
			spinnerTeacher.setVisibility(EditText.GONE);
			lblTextTeacher.setVisibility(TextView.GONE);
			pickerTitleSchool.setVisibility(TextView.GONE);
			isStudent = true;
			this.setBackToSelectedAccount(STUDENT);
			this.setAvatarGradeLayoutVisibility(false);
			this.setVisibilityUserName(false);
			if(userPlayerList.size() > 0){
				btnTitleEdit.setVisibility(TextView.VISIBLE);
				//for student only single player
				if(numberOfPlayer == 1){
					if(MathFriendzyHelper.parseInt(userPlayerList.get(0).getSchoolId()) > 0){
						pickerTitleSchool.setVisibility(TextView.VISIBLE);
						edtSchool.setVisibility(Spinner.VISIBLE);
						edtSchool.setText(userPlayerList.get(0).getSchoolName());
					}

					if(MathFriendzyHelper.parseInt(userPlayerList.get(0).getTeacherUserId()) > 0){
						spinnerTeacher.setText(userPlayerList.get(0).getTeacherFirstName() + " "
								+ userPlayerList.get(0).getTeacheLastName());
						lblTextTeacher.setVisibility(TextView.VISIBLE);
						spinnerTeacher.setVisibility(EditText.VISIBLE);
					}
				}
			}
			btnTitleAddPlayer.setVisibility(Button.GONE);			
			this.setHintAndListenerOnLastName(STUDENT);
		}
		else if(regUserObj.getIsParent().equals("1"))// 1 for parent
		{
			this.setAccountTypeText(lblParent);

			RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			this.setEmailText(0);
			edtSchool.setVisibility(Spinner.GONE);
			lblTextTeacher.setVisibility(TextView.GONE);
			spinnerTeacher.setVisibility(EditText.GONE);
			pickerTitleSchool.setVisibility(TextView.GONE);
			isParent = true;
			this.setBackToSelectedAccount(PARENT);
			this.setAvatarGradeLayoutVisibility(false);
			this.setVisibilityUserName(false);
			if(userPlayerList.size() > 0){
				btnTitleEdit.setVisibility(TextView.VISIBLE);
			}
			btnTitleAddPlayer.setVisibility(Button.VISIBLE);
			this.setHintAndListenerOnLastName(PARENT);
		}
		else if(regUserObj.getIsParent().equals("0"))//0 for teacher
		{
			this.setAccountTypeText(lblTeacher);

			RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			this.setEmailText(0);
			pickerTitleSchool.setVisibility(TextView.VISIBLE);
			edtSchool.setVisibility(Spinner.VISIBLE);
			lblTextTeacher.setVisibility(TextView.GONE);
			spinnerTeacher.setVisibility(EditText.GONE);
			//pickerTitleSchool.setVisibility(TextView.VISIBLE);
			isTeacher = true;
			this.setBackToSelectedAccount(TEACHER);
			this.setAvatarGradeLayoutVisibility(true);
			this.setVisibilityUserName(true);
			btnTitleEdit.setVisibility(TextView.GONE);
			if(userPlayerList.size() > 0){
				/*this.onAvatarSelection(userPlayerList.get(0).getImageName());
                this.setGrade(userPlayerList.get(0).getGrade());
                edtUserName.setText(userPlayerList.get(0).getUsername());*/
				btnTitleEdit.setVisibility(TextView.VISIBLE);
			}
			btnTitleAddPlayer.setVisibility(Button.GONE);
			 this.setHintAndListenerOnLastName(TEACHER);
		}

		this.setDataIfOnlyOnePlayerInAccount();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setYouAreA()");
	}

	private void setDataIfOnlyOnePlayerInAccount(){
		if(numberOfPlayer > 0) {
			if (numberOfPlayer == 1) {
				this.setAvatarGradeLayoutVisibility(true);
				this.setVisibilityUserName(true);
				btnTitleEdit.setVisibility(TextView.GONE);

				this.onAvatarSelection(userPlayerList.get(0).getImageName());
				this.setGrade(userPlayerList.get(0).getGrade());
				edtUserName.setText(userPlayerList.get(0).getUsername());
				this.checkForStudentCreatedByTeacher(userPlayerList.get(0));
			} else {
				this.setAvatarGradeLayoutVisibility(false);
				this.setVisibilityUserName(false);
				btnTitleEdit.setVisibility(TextView.VISIBLE);
			}
		}
	}

	private void setAvatarGradeLayoutVisibility(boolean isVisible){
		if(isVisible) {
			avtarGradeLayout.setVisibility(RelativeLayout.VISIBLE);
			avatarGradeLavelLayout.setVisibility(LinearLayout.VISIBLE);
		}else {
			avtarGradeLayout.setVisibility(RelativeLayout.GONE);
			avatarGradeLavelLayout.setVisibility(LinearLayout.GONE);
		}
	}

	private void setVisibilityUserName(boolean isVisible){
		if(isVisible){
			edtUserName.setVisibility(EditText.VISIBLE);
			lblRegUserName.setVisibility(TextView.VISIBLE);
		}else{
			edtUserName.setVisibility(EditText.GONE);
			lblRegUserName.setVisibility(TextView.GONE);
		}
	}

	/**
	 * This method set the edit text value from tha database for editing
	 */
	private void setEditTextData()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setEditTextData()");

		edtFirstName.setText(regUserObj.getFirstName());
		//edtLastName.setText(MathFriendzyHelper.getLastInitialName(regUserObj.getLastName()));
		edtEmail.setText(regUserObj.getEmail());
		edtPass.setText(regUserObj.getPass());
		edtCity.setText(regUserObj.getCity());
		edtZipCode.setText(regUserObj.getZip());
		previousUserEmail = regUserObj.getEmail();
		this.setVolume();

		//for new changes based on gaurav email on 17 jun 2015
		if(getYouAre() == STUDENT){
			if(numberOfPlayer == 1){
				String schoolName = userPlayerList.get(0).getSchoolName();
				if (schoolName.equals(""))
					edtSchool.setText(SCHOOL_TEXT);
				else
					edtSchool.setText(schoolName);
			}
			edtLastName.setText(MathFriendzyHelper.getLastInitialName(regUserObj.getLastName()));
		}else {
			if (regUserObj.getSchoolName().equals(""))
				edtSchool.setText(SCHOOL_TEXT);
			else
				edtSchool.setText(regUserObj.getSchoolName());
			edtLastName.setText(regUserObj.getLastName());
		}
		/*schoolIdFromFind 	= regUserObj.getSchoolId();
		schoolNameFromFind  = regUserObj.getSchoolName();*/
		school.setSchoolId(regUserObj.getSchoolId());
		school.setSchoolName(regUserObj.getSchoolName());

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setEditTextData()");
	}

	/**
	 * Set user volume
	 */
	private void setVolume(){
		//Log.e(TAG , "Volume " + regUserObj.getVolume());
		try{
			int progress = (int) (Float.parseFloat(regUserObj.getVolume()) * 100);
			seekbarVolume.setProgress(progress);
		}
		catch(NumberFormatException ee){
			seekbarVolume.setProgress(0);
		}
	}

	/**
	 * On Avatar selection
	 * @param avatarName
	 */
	private void onAvatarSelection(String avatarName){
		if(avatarName != null){
			profileImageName = avatarName;
			//avtarImage.setImageResource(0);
			avtarImage.setImageBitmap(MathFriendzyHelper
					.getAvatarImageByName(this, avatarName));
		}
	}

	/**
	 * Set adapter to grade
	 * @param selectedGrade
	 */
	private void setGrade(String selectedGrade){
		MathFriendzyHelper.setAdapterToSpinner(this, MathFriendzyHelper
				.getGradeForSelection(selectedGrade),
				gradeList, edtGrade);
	}

	private void setZipCodeOrCity(String zipOrCity){
		edtZipCode.setText(zipOrCity);
	}

	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnWidgets(){

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");

		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3){
				String selectedCountry = spinnerCountry.getSelectedItem().toString();
				if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)
						|| selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
					lblRegState.setTextColor(Color.BLACK);
					lblRegZip.setTextColor(Color.BLACK);
					edtZipCode.setText(regUserObj.getZip());
					spinnerState.setEnabled(true);
					edtZipCode.setEnabled(true);
					setStates(selectedCountry);
				}else{
					lblRegState.setTextColor(Color.GRAY);
					lblRegZip.setTextColor(Color.GRAY);
					edtZipCode.setText("");
					spinnerState.setEnabled(false);
					edtZipCode.setEnabled(false);
					setStates("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0){
			}
		});

		RegModifyImgBtnStudent.setOnClickListener(this);
		RegModifyImgBtnParent.setOnClickListener(this);
		RegModifyImgBtnTeacher.setOnClickListener(this);
		btnTitleLogOut.setOnClickListener(this);
		playTitle.setOnClickListener(this);
		btnTitleAddPlayer.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);

		edtSchool.setOnClickListener(this);//changes

		avtarLayout.setOnClickListener(this);

		studentLayout.setOnClickListener(this);
		parentLayout.setOnClickListener(this);
		teacherLayout.setOnClickListener(this);
		spinnerTeacher.setOnClickListener(this);
		imgRecieveWeeklyReport.setOnClickListener(this);

		MathFriendzyHelper.setFocusChangeListener(this , edtFirstName , this);
		MathFriendzyHelper.setFocusChangeListener(this , edtLastName , this);
		MathFriendzyHelper.setFocusChangeListener(this , edtUserName , this);
		MathFriendzyHelper.setFocusChangeListener(this , edtEmail , this);
		MathFriendzyHelper.setFocusChangeListener(this , edtPass , this);
		MathFriendzyHelper.setFocusChangeListener(this , edtZipCode , this);


		//if(){
		//edtGrade.setOnTouchListener(Spinner_OnTouch);
		//}

		//this.setTextWatcherToLastName();
		//MathFriendzyHelper.setEditTextWatcherToEditTextForLastInitialName(this , edtLastName);
		edtLastName.addTextChangedListener(textWatcher);
		
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	/**
	 * Select avatar
	 */
	private void clickToSelectAvatar(){
		try{
			EditRegisteredUserPlayer.playerId = userPlayerList.get(0).getPlayerid();
			MathFriendzyHelper.selectAvatar(this, SELECT_AVATAR_REQUEST , true);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v)
	{
		MathFriendzyHelper.clearFocus(this);
		if(onFocusLostIsValidString) {
			switch (v.getId()) {
			case R.id.RegModifyImgBtnStudent:
				RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
				isTeacher = false;
				isStudent = true;
				isParent = false;
				this.setEmailText(1);
				edtSchool.setVisibility(Spinner.GONE);
				//pickerTitleSchool.setVisibility(TextView.GONE);
				break;

			case R.id.RegModifyImgBtnParent:
				RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
				isTeacher = false;
				isStudent = false;
				isParent = true;
				this.setEmailText(0);
				edtSchool.setVisibility(Spinner.GONE);
				//pickerTitleSchool.setVisibility(TextView.GONE);
				break;

			case R.id.RegModifyImgBtnTeacher:
				RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				isTeacher = true;
				isStudent = false;
				isParent = false;
				this.setEmailText(0);
				edtSchool.setVisibility(Spinner.VISIBLE);
				//pickerTitleSchool.setVisibility(TextView.VISIBLE);
				break;

			case R.id.btnTitleLogOut:
				isClickOnLogout = true;
				this.logoutAction();
				break;
			case R.id.playTitle:
				this.checkEmptyValidation();
				break;
			case R.id.btnTitleAddPlayer:
				if (userPlayerList != null &&
				userPlayerList.size() == MathFriendzyHelper.MAX_NUMBER_OF_PLAYER) {
					MathFriendzyHelper.warningDialog(this, alertYouCanNotAddMoreThanMaxLimitPlayer);
				} else {
					startActivity(new Intent(this, AddPlayer.class)
					.putExtra("callingActivity", "ModifyRegistration"));
				}
				break;
			case R.id.spinnerSchool:
				if (getYouAre() == STUDENT) {
					if (numberOfPlayer == 1) {
						MathFriendzyHelper.showWarningDialog(this, studentInfoModificationNotice);
					}
				} else {
					this.setListenerOnEdtSchool();
				}
				break;
			case R.id.btnTitleBack:
				this.checkEmptyValidation();
				break;
			case R.id.studentLayout:
				if (this.getYouAre() == TEACHER) {
					MathFriendzyHelper.showWarningDialog(this, lblYouCantChangeAccount);
					return;
				}
				RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
				isTeacher = false;
				isStudent = true;
				isParent = false;
				this.setEmailText(1);
				//edtSchool.setVisibility(Spinner.GONE);
				this.setBackToSelectedAccount(STUDENT);
				//pickerTitleSchool.setVisibility(TextView.GONE);
				break;
			case R.id.parentLayout:
				if (this.getYouAre() == TEACHER) {
					MathFriendzyHelper.showWarningDialog(this, lblYouCantChangeAccount);
					return;
				}
				RegModifyImgBtnStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
				RegModifyImgBtnParent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				RegModifyImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
				isTeacher = false;
				isStudent = false;
				isParent = true;
				this.setEmailText(0);
				//edtSchool.setVisibility(Spinner.GONE);
				this.setBackToSelectedAccount(PARENT);
				//pickerTitleSchool.setVisibility(TextView.GONE);
				break;
			case R.id.teacherLayout:
				if (this.getYouAre() != TEACHER) {
					MathFriendzyHelper.showWarningDialog(this, lblYouCantChangeAccount);
					return;
				}
				break;
			case R.id.avtarLayout:
				this.clickToSelectAvatar();
				break;
			case R.id.spinnerTeacher:
				if (numberOfPlayer == 1) {
					MathFriendzyHelper.showWarningDialog(this, studentInfoModificationNotice);
				}
				break;
			case R.id.imgRecieveWeeklyReport:
				emailSubscription = (emailSubscription + 1) % 2;
				this.setEmailSubscription(emailSubscription);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Change the selection type account background
	 * @param accountFor
	 */
	private void setBackToSelectedAccount(int accountFor){
		if(accountFor == TEACHER){
			studentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
			teacherLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
			parentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
		}else if(accountFor == STUDENT){
			studentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
			teacherLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
			parentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
		}else if(accountFor == PARENT){
			studentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
			teacherLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
			parentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
		}
	}

	/**
	 * This method is call when the cliakc perform on the school edit text, and the school form the server is loaded
	 * and set it
	 */
	private void setListenerOnEdtSchool()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setListenerOnEdtSchool()");

		String seletedCountry = spinnerCountry.getSelectedItem().toString();
		if(seletedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)
				|| seletedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
			MathFriendzyHelper.selectSchool(this, seletedCountry ,
					"", edtZipCode.getText().toString(), "");
		}else{
			MathFriendzyHelper.selectSchool(this, seletedCountry ,
					edtCity.getText().toString(), "" , "");
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setListenerOnEdtSchool()");
	}

	/**
	 * On School Selected
	 * @param schoolInfo
	 */
	private void onSelectSchool(ArrayList<String> schoolInfo){
		edtSchool.setText(schoolInfo.get(0));
		school.setSchoolName(schoolInfo.get(0));
		school.setSchoolId(schoolInfo.get(1));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside onActivityResult()");

		if(resultCode == RESULT_OK){
			switch(requestCode){
			case 1:
				ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
				edtSchool.setText(schoolInfo.get(0));
				/*schoolNameFromFind = schoolInfo.get(0);
                    schoolIdFromFind = schoolInfo.get(1);*/
				school.setSchoolId(schoolInfo.get(1));
				school.setSchoolName(schoolInfo.get(0));
				break;
			case SELECT_AVATAR_REQUEST:
				EditRegisteredUserPlayer.playerId = null;
				this.onAvatarSelection(data.getStringExtra("selectedAvatarName"));
				break;
			case MathFriendzyHelper.SELECT_SCHOOL_REQUEST_CODE:
				this.onSelectSchool(data.getStringArrayListExtra("schoolInfo"));
				break;
			}
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside onActivityResult()");

		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Set email text
	 */
	private void setEmailText(int value)
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setEmailText()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		if(value == 1){//1 for registered as student
			lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
			edtEmail.setHint(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") +
					"(" + transeletion.getTranselationTextByTextIdentifier("lblOptional") + ")");
		}else{
			lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
			edtEmail.setHint(transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
		}

		if(this.getYouAre() == STUDENT || this.getYouAre() == PARENT){
			lblRegEmailDescription.setVisibility(TextView.VISIBLE);
		}else{
			lblRegEmailDescription.setVisibility(TextView.GONE);
		}
		transeletion.closeConnection();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setEmailText()");
	}

	/**
	 * This method is call when user want to logout from the appliaction
	 */
	private void logoutAction()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside logoutAction()");

		this.checkEmptyValidation();

		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		userObj.deleteFromUser();
		userObj.closeConn();

		UserPlayerOperation userPlayerPbj = new UserPlayerOperation(this);
		userPlayerPbj.deleteFromUserPlayer();
		userPlayerPbj.closeConn();

		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		SharedPreferences.Editor editor = sheredPreference.edit();
		editor.putBoolean(IS_LOGIN, false);
		editor.commit();

		SharedPreferences sp = this.getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0);
		SharedPreferences.Editor edit = sp.edit();
		edit.clear();
		edit.commit();

		MathFriendzyHelper.savePurchaseSubscriptionDate(this , null);
		//CommonUtils.isActiveHomeSchoolNotificationDownloaded = false;
		MathFriendzyHelper.saveIsAdDisble(this ,0);        
		MathFriendzyHelper.saveBooleanInSharedPreff(this ,
				MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , false);
		MathFriendzyHelper.saveBooleanInSharedPreff(this ,
				MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY ,
				false);
		//changes for friendzy
		//CommonUtils.activeFriendzyPlayerList = new ArrayList<FriendzyDTO>();//for remove all player from the list

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside logoutAction()");
	}

	/**
	 * This method set states when the user country either United States and Canada
	 * @param countryName
	 */
	@SuppressWarnings("unused")
	private void setStates(final String countryName){

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setStates()");

		int layoutId = R.layout.spinner_country_item;
		String stateName = null;
		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(countryName.equals("United States")){
			satateList = stateObj.getUSStates(this);
			stateObj = new States();
			stateName = stateObj.getUSStateNameById(regUserObj.getStateId(), this);
		}else if(countryName.equals("Canada")){
			satateList = stateObj.getCanadaStates(this);
			stateObj = new States();
			stateName = stateObj.getCanadaStateNameById(regUserObj.getStateId(), this);
		}else{
			satateList.add(stateText);
			layoutId = R.layout.spinner_country_item_with_grey_xolor_text;
		}

		ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this,layoutId,satateList);
		stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerState.setAdapter(stateAdapter);
		spinnerState.setSelection(stateAdapter.getPosition(stateName));
		stateAdapter.notifyDataSetChanged();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setStates()");
	}

	/**
	 * This method get Languages from the database and set it
	 */
	private void getLanguages()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside getLanguages()");

		Language languageObj = new Language(this);
		languageList = languageObj.getLanguages();
		this.setLanguageAdapter();

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside getLanguages()");
	}

	/**
	 * This method set the languages to the spinner
	 */
	private void setLanguageAdapter()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside setLanguageAdapter()");

		ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this, R.layout.spinner_country_item,
				languageList);
		languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerLanguage.setAdapter(languageAdapter);
		Language languageObj =  new Language(this);
		spinnerLanguage.setSelection(languageAdapter.getPosition
				(languageObj.getLanguageNameById(regUserObj.getPreferedLanguageId())));

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside setLanguageAdapter()");
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK){
			MathFriendzyHelper.clearFocus(this);
			if(onFocusLostIsValidString)
				this.checkEmptyValidation();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void checkEmptyValidation()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside checkEmptyValidation()");

		if(isStudent)
		{
			if(edtEmail.getText().toString().length() > 0 )
			{
				this.checkValidation();
			}
			else
			{
				this.checkValidationForStudentEmail();
			}
		}
		else
		{
			this.checkValidation();
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside checkEmptyValidation()");

	}

	/**
	 * Check validation
	 */
	private void checkValidation()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);

		if(spinnerCountry.getSelectedItem().toString().equals("United States")
				|| spinnerCountry.getSelectedItem().toString().equals("Canada")){
			if(edtFirstName.getText().toString().equals("")
					||edtLastName.getText().toString().equals("")
					||edtEmail.getText().toString().equals("")
					||edtPass.getText().toString().equals("")
					||edtCity.getText().toString().equals("")
					||edtZipCode.getText().toString().equals("")){
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
						("alertMsgPleaseEnterAllInfo"));
			}else{
				if(isTeacher){
					if(edtSchool.getText().toString().equals("")){
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
								("alertMsgPleaseEnterAllInfo"));
					}else{
						this.checkValidEmail();
					}
				}else{
					this.checkValidEmail();
				}
			}
		}else{
			if(edtFirstName.getText().toString().equals("")
					||edtLastName.getText().toString().equals("")
					||edtEmail.getText().toString().equals("")
					||edtPass.getText().toString().equals("")
					||edtCity.getText().toString().equals("")
					/*||edtZipCode.getText().toString().equals("")*/){
				dg.generateWarningDialog(transeletion.
						getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}else{
				if(isTeacher){
					if(edtSchool.getText().toString().equals("")){
						dg.generateWarningDialog(transeletion.
								getTranselationTextByTextIdentifier
								("alertMsgPleaseEnterAllInfo"));
					}else{
						this.checkValidEmail();
					}
				}else{
					this.checkValidEmail();
				}
			}
		}
		transeletion.closeConnection();
	}

	/**
	 * Check validation for student email
	 */
	private void checkValidationForStudentEmail()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);

		if(spinnerCountry.getSelectedItem().toString().equals("United States")
				|| spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			if(edtFirstName.getText().toString().equals("")
					||edtLastName.getText().toString().equals("")
					||edtPass.getText().toString().equals("")
					//||edtCity.getText().toString().equals("")
					||edtZipCode.getText().toString().equals(""))
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.checkValidPass();
			}
		}
		else
		{
			if(edtFirstName.getText().toString().equals("")
					||edtLastName.getText().toString().equals("")
					||edtPass.getText().toString().equals("")
					/*||edtCity.getText().toString().equals("")*/)
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.checkValidPass();
			}
		}
		transeletion.closeConnection();
	}

	/**
	 * Check valid email
	 */
	private void checkValidEmail()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);

		if(CommonUtils.isEmailValid(edtEmail.getText().toString()))
		{
			this.checkValidPass();
		}
		else
		{
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailIncorrectFormat"));
		}
		transeletion.closeConnection();
	}

	/**
	 * Check valid password
	 */
	private void checkValidPass()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);
		if(CommonUtils.isPasswordValid(edtPass.getText().toString()))
		{
			updateRegisterUser();
		}
		else
		{
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPasswordInvalid"));
		}
		transeletion.closeConnection();
	}

	/**
	 * Convert the volume value into fraction between 0 - 1
	 * @param progress
	 * @return
	 */
	private Float convertVolumeToSave(int progress){
		try {
			return progress / 100f;
		}catch(Exception e){
			return 0f;
		}
	}

	/**
	 * This method update the user information on server
	 */
	private void updateRegisterUser()
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside updateRegisterUser()");

		RegistereUserDto regUpdateObj = new RegistereUserDto();

		regUpdateObj.setFirstName(edtFirstName.getText().toString());
		regUpdateObj.setLastName(edtLastName.getText().toString());
		regUpdateObj.setEmail(edtEmail.getText().toString());
		regUpdateObj.setPass(edtPass.getText().toString());
		regUpdateObj.setSchoolName(edtSchool.getText().toString());

		Country countryObj = new Country();
		regUpdateObj.setCountryId(countryObj.getCountryIdByCountryName
				(spinnerCountry.getSelectedItem().toString(), this));
		regUpdateObj.setCountryIso(countryObj.getCountryIsoByCountryName
				(spinnerCountry.getSelectedItem().toString(), this));

		if(spinnerState.getSelectedItem() != null){
			States stateobj = new States();
			if(spinnerCountry.getSelectedItem().toString().equals("United States")){
				regUpdateObj.setStateId(stateobj.getUSStateIdByName
						(spinnerState.getSelectedItem().toString(), this));
				regUpdateObj.setStateCode
				(stateobj.getStateCodeNameByStateNameFromUS(spinnerState.getSelectedItem()
						.toString(), this));
			}else if(spinnerCountry.getSelectedItem().toString().equals("Canada")){
				regUpdateObj.setStateId(stateobj.getCanadaStateIdByName
						(spinnerState.getSelectedItem().toString(), this));
				regUpdateObj.setStateCode
				(stateobj.getStateCodeNameByStateNameFromCanada(spinnerState.getSelectedItem()
						.toString(), this));
			}else{
				regUpdateObj.setStateId("0");
				regUpdateObj.setStateCode("");
			}
		}else{
			regUpdateObj.setStateId("0");
			regUpdateObj.setStateCode("");
		}

		Language languageObj = new Language(this);
		languageId 		= languageObj.getLanguageIdByName(spinnerLanguage.getSelectedItem().toString());
		languageCode	= languageObj.getLanguageCodeByName(spinnerLanguage.getSelectedItem().toString());

		regUpdateObj.setPreferedLanguageId(languageId);
		if(isParent)
		{
			regUpdateObj.setIsParent("1");// 1 for parent
			regUpdateObj.setSchoolId("0");// 0 for parent
			regUpdateObj.setSchoolName("");
		}
		else if(isStudent)
		{
			regUpdateObj.setIsParent("2");// 2 for student
			regUpdateObj.setIsStudent("1");
			regUpdateObj.setSchoolId("0");// 0 for student
			regUpdateObj.setSchoolName("");
		}
		else if(isTeacher)
		{
			regUpdateObj.setIsParent("0");// 0 for teacher
			regUpdateObj.setIsTeacher("1");
			regUpdateObj.setSchoolId(school.getSchoolId());// school id for teacher
			regUpdateObj.setSchoolName(school.getSchoolName());
		}

		regUpdateObj.setVolume(convertVolumeToSave(seekbarVolume.getProgress()) + "");
		regUpdateObj.setCoins(regUserObj.getCoins());

		//changes for new registration
		/*String selectedCountry = spinnerCountry.getSelectedItem().toString();
        if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)
                || selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
            regUpdateObj.setCity("");
            regUpdateObj.setZip(edtZipCode.getText().toString());
        }else{
            regUpdateObj.setCity(edtZipCode.getText().toString());
            regUpdateObj.setZip("");
        }*/

		regUpdateObj.setCity(edtCity.getText().toString());
		regUpdateObj.setZip(edtZipCode.getText().toString());
		regUpdateObj.setUserId(regUserObj.getUserId());


		//changes for weekly report
		//regUpdateObj.setFromAccountScreen(true);
		/*if(isEmailValid){
			 regUpdateObj.setEmailSubscription(emailSubscription + "");
		 }else{
			 regUpdateObj.setEmailSubscription(MathFriendzyHelper.getEmailSubscription(this));
		 }*/
		//end weekly report changes

		UserPlayerOperation userObj = new UserPlayerOperation(this);
		if(userObj.isUserPlayersExist()){
			regUpdateObj.setPlayers("<AllPlayers></AllPlayers>");
		}
		else{
			//for new registration only for teacher
			//if(this.getYouAre() == TEACHER){
			if(numberOfPlayer == 1){
				if(userPlayerList.size() > 0){
					int selectedGrade = MathFriendzyHelper.getGrade
							(edtGrade.getSelectedItem().toString());
					if(selectedGrade != MathFriendzyHelper.NO_GRADE_SELETED){
						MathFriendzyHelper.updateTeacherplayer(this, profileImageName,
								selectedGrade + "" , userPlayerList.get(0).getPlayerid()
								, regUpdateObj.getFirstName() , regUpdateObj.getLastName());
					}else{
						MathFriendzyHelper.updateTeacherplayer(this, profileImageName,
								userPlayerList.get(0).getGrade() ,
								userPlayerList.get(0).getPlayerid()
								, regUpdateObj.getFirstName() , regUpdateObj.getLastName());
					}
				}
			}

			//change for teacher
			UserPlayerOperation userObj1 = new UserPlayerOperation(this);
			ArrayList<UserPlayerDto> userPlayerObj = userObj1.getUserPlayerData();
			regUpdateObj.setPlayers("<AllPlayers>"+this.getXmlPlayer(userPlayerObj)
					+"</AllPlayers>");
		}

		//changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new UpdateAsynTask(regUpdateObj).execute(null,null,null);

			if(isClickOnLogout){
				//unregistered device from server
				new UnRegisterDeviceForLogInUser().execute(null,null,null);
			}
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside updateRegisterUser()");
	}

	/**
	 * This method return the xml format of the user player data from th database
	 * @param userPlayerObj
	 * @return
	 */
	private String getXmlPlayer(ArrayList<UserPlayerDto> userPlayerObj)
	{
		if(MODIFY_USER_FLAG)
			Log.e(TAG, "inside getXmlPlayer()");

		//String xml = "";
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < userPlayerObj.size() ; i++)
		{
			xml.append("<player>" +
					"<playerId>"+userPlayerObj.get(i).getPlayerid()+"</playerId>"+
					"<fName>"+userPlayerObj.get(i).getFirstname()+"</fName>"+
					"<lName>"+userPlayerObj.get(i).getLastname()+"</lName>"+
					"<grade>"+userPlayerObj.get(i).getGrade()+"</grade>"+
					"<schoolId>"+userPlayerObj.get(i).getSchoolId()+"</schoolId>"+
					"<teacherId>"+userPlayerObj.get(i).getTeacherUserId()+"</teacherId>"+
					"<indexOfAppearance>-1</indexOfAppearance>"+
					"<profileImageId>"+userPlayerObj.get(i).getImageName()+"</profileImageId>"+
					"<coins>"+userPlayerObj.get(i).getCoin()+"</coins>"+
					"<points>"+userPlayerObj.get(i).getPoints()+"</points>"+
					"<userName>"+userPlayerObj.get(i).getUsername()+"</userName>"+
					"<competeLevel>" + userPlayerObj.get(i).getCompletelavel() + "</competeLevel>"+
					"</player>");
		}

		if(MODIFY_USER_FLAG)
			Log.e(TAG, "outside getXmlPlayer()");

		return xml.toString();
	}

	/**
	 * Check for email change or newly added
	 * @param newEmail
	 * @return
	 */
	private boolean isNewEmailAddedOrOldChange(String newEmail){
		try {
			if (!MathFriendzyHelper.isEmpty(newEmail)
					&& !previousUserEmail.equalsIgnoreCase(newEmail)) {
				return true;
			}
			return false;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Update user information on server
	 * @author Yashwant Singh
	 *
	 */
	class UpdateAsynTask extends AsyncTask<Void, Void, Void>
	{
		private RegistereUserDto regObj = null;
		private int registreationResult = 0;
		private boolean isShowPopUpForEmailChange = false;
		UpdateAsynTask(RegistereUserDto regObj){
			this.regObj = regObj;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			if(MODIFY_USER_FLAG)
				Log.e(TAG, "inside UpdateAsynTask onPreExecute()");

			progressDialog = CommonUtils.getProgressDialog(ModifyRegistration.this);
			progressDialog.show();

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "outside UpdateAsynTask onPreExecute()");
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			if(MODIFY_USER_FLAG)
				Log.e(TAG, "inside UpdateAsynTask doInBackground()");

			Register registerObj = new Register(ModifyRegistration.this);
			registreationResult = registerObj.updateUserOnserver(regObj);

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "outside UpdateAsynTask doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			//progressDialog.cancel();

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "inside UpdateAsynTask onPostExecute()");

			Translation transeletion = new Translation(ModifyRegistration.this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(ModifyRegistration.this);

			if(registreationResult == Register.SUCCESS)
			{
				Language languageObj =  new Language(ModifyRegistration.this);
				if(languageObj.getLanguageNameById(regUserObj.getPreferedLanguageId()).
						equals(spinnerLanguage.getSelectedItem().toString()))
				{
					progressDialog.cancel();
					openMainActivity();
				}
				else
				{
					if(CommonUtils.isInternetConnectionAvailable(ModifyRegistration.this)){
						MathFriendzyHelper.getLanguageTranslation(ModifyRegistration.this ,
								languageId , languageCode ,
								CommonUtils.APP_ID , new HttpServerRequest() {
							@Override
							public void onRequestComplete() {
								openMainActivity();
							}
						});
					}else{
						progressDialog.cancel();
						dg.generateWarningDialog(transeletion
								.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
					}
				}
			}
			else if(registreationResult == Register.INVALID_CITY)
			{
				progressDialog.cancel();
				//dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
			}
			else if(registreationResult == Register.INVALID_ZIP)
			{
				progressDialog.cancel();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
			}
			else if(registreationResult == Register.INVALID_EMAIL)
			{
				progressDialog.cancel();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailExist"));
			}
			else
			{
				progressDialog.cancel();
				openMainActivity();
				//dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			}

			transeletion.closeConnection();

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "outside UpdateAsynTask onPostExecute()");

			super.onPostExecute(result);
		}
	}

	/**
	 * @description get transelation from server
	 * @author Yashwant Singh
	 *
	 */
	class GetTranselationFromServer extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			if(MODIFY_USER_FLAG)
				Log.e(TAG, "inside GetTranselationFromServer doInBackground()");

			Translation traneselation = new Translation(ModifyRegistration.this);
			CommonUtils.LANGUAGE_CODE = languageCode;
			CommonUtils.LANGUAGE_ID   = languageId;
			traneselation.getTransalateTextFromServer(languageCode, languageId, "6");
			traneselation.saveTranslationText();

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "outside GetTranselationFromServer doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			if(MODIFY_USER_FLAG)
				Log.e(TAG, "inside GetTranselationFromServer onPostExecute()");

			progressDialog.cancel();
			openMainActivity();

			if(MODIFY_USER_FLAG)
				Log.e(TAG, "outside GetTranselationFromServer onPostExecute()");

			super.onPostExecute(result);
		}
	}

	private void openMainActivity(){
		String newEmail = edtEmail.getText().toString();
		Intent intent = new Intent(ModifyRegistration.this,MainActivity.class);
		intent.putExtra("isCallFromRegistration", this.isNewEmailAddedOrOldChange(newEmail));
		startActivity(intent);
	}

	/**
	 * @Description check for city is valid or not
	 * @author Yashwant Singh
	 *
	 */
	class CheckValidCityAsynTask extends AsyncTask<Void, Void, Void>
	{
		String countryId 	= null;
		String stateId 		= null;
		String city 		= null;
		String zip 			= null;
		String resultValue  = null;

		CheckValidCityAsynTask(String countryId,String stateId,String city,String zip)
		{
			progressDialog = CommonUtils.getProgressDialog(ModifyRegistration.this);
			progressDialog.show();
			this.countryId = countryId;
			this.stateId   = stateId;
			this.city      = city;
			this.zip       = zip;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			Validation validObj = new Validation();
			resultValue = validObj.validateCity(countryId, stateId, city, zip);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			progressDialog.cancel();

			Translation transeletion = new Translation(ModifyRegistration.this);
			transeletion.openConnection();

			if(resultValue != null){
				if(resultValue.equals("-9001"))//for invalid City
				{
					DialogGenerator dg = new DialogGenerator(ModifyRegistration.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
				}
				else if(resultValue.equals("-9002"))//for invalid ZipCode
				{
					DialogGenerator dg = new DialogGenerator(ModifyRegistration.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
				}
				else
				{
					Intent schoolIntent = new Intent(ModifyRegistration.this,SearchYourSchoolActivity.class);
					schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
					schoolIntent.putExtra("state", spinnerState.getSelectedItem().toString());
					schoolIntent.putExtra("city", edtCity.getText().toString());
					schoolIntent.putExtra("zip", edtZipCode.getText().toString());

					startActivityForResult(schoolIntent, 1);
				}
				transeletion.closeConnection();
			}else{
				CommonUtils.showInternetDialog(ModifyRegistration.this);
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * use to unregister device for logout user
	 * @author Yashwant Singh
	 *
	 */
	class UnRegisterDeviceForLogInUser extends AsyncTask<Void, Void, Void>
	{
		String deviceId;
		public UnRegisterDeviceForLogInUser()
		{
			SharedPreferences sharedPreff = getSharedPreferences(ICommonUtils.DEVICE_ID_PREFF, 0);
			deviceId = sharedPreff.getString(ICommonUtils.DEVICE_ID, "");
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			String strUrl = ICommonUtils.COMPLETE_URL+"action=deleteUserEntryForDevice&userId=" +regUserObj.getUserId()
					+"&udid="+deviceId+"&appId="+CommonUtils.APP_ID;
			CommonUtils.readFromURL(strUrl);
			return null;
		}
	}//END class UnRegisterDeviceForUser

	//check only for the last edittext which has current focus
	@Override
	public void onFocusLost(boolean isValidString) {
		onFocusLostIsValidString = isValidString;
	}

	@Override
	public void onFocusHas(boolean isValidString) {

	}

	/**
	 * Set the weekly report data
	 */
	private void setWeeklyReport() {
		if(this.getYouAre() == PARENT || this.getYouAre() == STUDENT) {//if part added , new change on 13 july 2015
			//new change on 13 july 2015
			if(MathFriendzyHelper.getEmailValid(this).equalsIgnoreCase(MathFriendzyHelper.YES + "")
					&& MathFriendzyHelper.getEmailSubscription(this)
					.equalsIgnoreCase(MathFriendzyHelper.YES + "")){
				isEmailValid = false;
				recieveWeeklyReporLayout.setVisibility(RelativeLayout.GONE);
				return ;
			}
			//end change

			if (MathFriendzyHelper.getEmailValid(this).equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
				isEmailValid = true;
				recieveWeeklyReporLayout.setVisibility(RelativeLayout.VISIBLE);
				if (MathFriendzyHelper.getEmailSubscription(this)
						.equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
					emailSubscription = 1;
				} else {
					emailSubscription = 0;
				}
				this.setEmailSubscription(emailSubscription);
			} else {
				isEmailValid = false;
				recieveWeeklyReporLayout.setVisibility(RelativeLayout.GONE);
			}
		}else{//else part added , new change on 13 july 2015
			isEmailValid = false;
			recieveWeeklyReporLayout.setVisibility(RelativeLayout.GONE);
		}
	}

	/**
	 * Set email subscription
	 * @param subscription
	 */
	private void setEmailSubscription(int subscription){
		if(subscription == MathFriendzyHelper.YES){
			imgRecieveWeeklyReport.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}else{
			imgRecieveWeeklyReport.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}
	}


	/**
	 *
	 */
	private void checkForStudentCreatedByTeacher(UserPlayerDto playerData){
		try{
			/*if(!MathFriendzyHelper.isEmpty(playerData.getStudentIdByTeacher())){
                edtGrade.setOnTouchListener(Spinner_OnTouch);
            }*/

			if(MathFriendzyHelper.isStudentRegisterByTeacher(playerData)){
				edtGrade.setOnTouchListener(Spinner_OnTouch);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				MathFriendzyHelper.showWarningDialog(ModifyRegistration.this ,
						alertOnlyTeacherCanChange);
			}
			return true;
		}
	};
	
	private void setHintAndListenerOnLastName(int accountType) {
        if (accountType == TEACHER || accountType == PARENT) {
            edtLastName.setHint(txtLastName);
            lblLastName.setText(txtLastName);
        } else {
            edtLastName.setHint(lblLastInitial);
            lblLastName.setText(lblLastInitial);
            //edtLastName.setText(MathFriendzyHelper.getLastInitialName(edtLastName.getText().toString()));
        }
    }
	
	TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = String.valueOf(s);
            if(isStudent) {
                if (text.length() > 1) {
                    edtLastName.setText(MathFriendzyHelper.getLastInitialName(text));
                    MathFriendzyHelper.showWarningDialog(ModifyRegistration.this, lblOnlyLastInitial);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
