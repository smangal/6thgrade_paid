package com.mathfriendzy.controller.registration.newregistration;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.controller.base.RegistartionBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.listener.OnRegistration;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.Registration;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.sixthgradepaid.R;

public class StudentRegistrationStep2 extends RegistartionBase implements EditTextFocusChangeListener {

	private TextView mfTitleHomeScreen = null;
	private TextView txtLastStep = null;
	private TextView txtStudentInformation = null;
	private Spinner edtCountry = null;
	private TextView edtSchool = null;
	private TextView edtTeacher = null;
	private EditText edtZipCode = null;
	private ImageView avtarImage = null;
	private TextView txtAvtar = null;
	private Spinner edtGrade = null;
	private Button btnTitleSubmit = null;

	private RelativeLayout countrySpinnerLayout = null;
	private RelativeLayout spinnerSchoolLayout 	= null;
	private RelativeLayout spinnerTeacherLayout = null;
	private RelativeLayout spinnerGradeLayout 	= null;
	private RelativeLayout avtarLayout = null;

	private final String TAG = this.getClass().getSimpleName();

	private ArrayList<String> countryList = null;
	private ArrayList<String> gradeList = null;
	private TempPlayer tempPlayer = null;

	private SchoolDTO school   = null;
	private TeacherDTO teacher = null;

	private String alertMsgPleaseEnterAllInfo = null;
	//private String alertMsgInvalidCity = null;
	private String alertMsgInvalidZipCode = null;
	private String alertPleaseSelectYourGrade = null;
	private String alertPleaseSelectYourSchoolFirst = null;
	private Registration registration = null;
	private ProgressDialog progressDialog = null;
	private String profileImageName = MathFriendzyHelper.DEFAULT_PROFILE_IMAGE;
	private final int SELECT_AVATAR_REQUEST = 10001;
	private String zipText = null;
	private String cityText = null;
	private boolean onFocusLostIsValidString = true;

	private LinearLayout stateCityLayout = null;
	private Spinner edtSpinner = null;//state
	private EditText edtCity = null;
	private String stateText = null;
	private String stateName = "";

	private RelativeLayout stateSpinnerLayout = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_registration_step2);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.init();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.tempPlayerData();
		this.setCountry();
		this.setGrade();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	private void getIntentValues() {
		registration = (Registration) this.getIntent().getSerializableExtra("registrationStep1");
	}

	/**
	 * Set the temp player data if temp player exist
	 */
	private void tempPlayerData() {
		if(tempPlayer != null){
			stateName = tempPlayer.getState();
			edtSchool.setText(tempPlayer.getSchoolName());
			school.setSchoolId(tempPlayer.getSchoolId() + "");
			school.setSchoolName(tempPlayer.getSchoolName());
			edtTeacher.setText(tempPlayer.getTeacherFirstName() + " "
					+ tempPlayer.getTeacherLastName());
			teacher.setfName(tempPlayer.getTeacherFirstName());
			teacher.setlName(tempPlayer.getTeacherFirstName());
			teacher.setTeacherUserId(tempPlayer.getTeacherUserId() + "");
		}
	}

	/**
	 * Initialize the initial variables
	 */
	private void init(){
		tempPlayer  = MathFriendzyHelper.getTempPlayer(this);
		countryList = MathFriendzyHelper.getCountryList(this);
		gradeList = MathFriendzyHelper.getUpdatedGradeList(this);

		this.initilizeSchoolWithDefaultValue();
		this.initializeTeacherWithDefaultValue();

		progressDialog = CommonUtils.getProgressDialog(this);
	}

	private void initilizeSchoolWithDefaultValue(){
		school = new SchoolDTO();
		school.setSchoolId("0");//default school id
		school.setSchoolName("");//default school name
	}

	private void initializeTeacherWithDefaultValue(){
		teacher = new TeacherDTO();
		teacher.setfName("N/A");//default teacher name
		teacher.setlName("");//default teacher last name
		teacher.setTeacherUserId("-2");//default teacher id
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");

		mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtLastStep = (TextView) findViewById(R.id.txtLastStep);
		txtStudentInformation = (TextView) findViewById(R.id.txtStudentInformation);
		edtCountry = (Spinner) findViewById(R.id.edtCountry);
		edtSchool = (TextView) findViewById(R.id.edtSchool);
		edtTeacher = (TextView) findViewById(R.id.edtTeacher);
		edtZipCode = (EditText) findViewById(R.id.edtZipCode);
		avtarImage = (ImageView) findViewById(R.id.avtarImage);
		txtAvtar = (TextView) findViewById(R.id.txtAvtar);
		edtGrade = (Spinner) findViewById(R.id.edtGrade);
		btnTitleSubmit = (Button) findViewById(R.id.btnTitleSubmit);

		countrySpinnerLayout = (RelativeLayout) findViewById(R.id.countrySpinnerLayout);
		spinnerSchoolLayout  = (RelativeLayout) findViewById(R.id.spinnerSchoolLayout);
		spinnerTeacherLayout = (RelativeLayout) findViewById(R.id.spinnerTeacherLayout);
		spinnerGradeLayout   = (RelativeLayout) findViewById(R.id.spinnerGradeLayout);
		avtarLayout          = (RelativeLayout) findViewById(R.id.avtarLayout);

		//for new changes for gaurav forward email 17 jun 2015
		spinnerSchoolLayout.setVisibility(RelativeLayout.GONE);
		spinnerTeacherLayout.setVisibility(RelativeLayout.GONE);

		stateCityLayout = (LinearLayout) findViewById(R.id.stateCityLayout);
		edtSpinner = (Spinner) findViewById(R.id.edtSpinner);
		edtCity = (EditText) findViewById(R.id.edtCity);
		stateCityLayout.setVisibility(LinearLayout.VISIBLE);
		stateSpinnerLayout = (RelativeLayout) findViewById(R.id.stateSpinnerLayout);
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	@Override
	protected void setListenerOnWidgets() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setListenerOnWidgets()");

		countrySpinnerLayout.setOnClickListener(this);
		spinnerSchoolLayout.setOnClickListener(this);
		spinnerTeacherLayout.setOnClickListener(this);
		spinnerGradeLayout.setOnClickListener(this);
		avtarLayout.setOnClickListener(this);
		btnTitleSubmit.setOnClickListener(this);

		//for spinner listener
		this.setListenerOnCountrySelection();
		//this.enableDisableListenerOntextView(spinnerTeacherLayout , false);
		MathFriendzyHelper.setFocusChangeListener(this , edtZipCode , this);
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	/**
	 * Country selection listener
	 */
	private void setListenerOnCountrySelection(){
		edtCountry.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3){
				String selectedCountry = edtCountry.getSelectedItem().toString();
				if(selectedCountry.equals(MathFriendzyHelper.UNITED_STATE)
						|| selectedCountry.equals(MathFriendzyHelper.CANADA)){
					/*edtZipCode.setText("");
                    edtZipCode.setHint(zipText);*/
					edtSpinner.setEnabled(true);
					edtZipCode.setEnabled(true);  
					setVisibilityOfStateAndZipCode(edtZipCode , edtSpinner , stateSpinnerLayout , true);
				}else{
					/*edtZipCode.setText("");
                    edtZipCode.setHint(cityText);*/
					edtSpinner.setEnabled(false);
					edtZipCode.setEnabled(false);
					edtZipCode.setText("");
					setVisibilityOfStateAndZipCode(edtZipCode , edtSpinner , stateSpinnerLayout , false);
				}
				setState(selectedCountry);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	/**
	 * Set State
	 * @param selectedCountry
	 */
	private void setState(String selectedCountry){
		int layoutId = R.layout.spinner_country_item;
		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)){
			satateList = stateObj.getUSStates(this);
		}else if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
			satateList = stateObj.getCanadaStates(this);
		}else{
			satateList.add(stateText);
			layoutId = R.layout.spinner_country_item_with_grey_xolor_text;
		}
		ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this,layoutId,satateList);
		stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		edtSpinner.setAdapter(stateAdapter);
		edtSpinner.setSelection(stateAdapter.getPosition(stateName));
		stateAdapter.notifyDataSetChanged();
	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
		txtLastStep.setText(transeletion.getTranselationTextByTextIdentifier("lblLastStep"));
		txtStudentInformation.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
				+ "'s " + transeletion.getTranselationTextByTextIdentifier("infoTitle"));
		/*this.setHintToText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry"), 
				edtCountry);*/
		this.setHintToEditText(edtZipCode
				, transeletion.getTranselationTextByTextIdentifier("lblRegZip"));
		this.setHintToEditText(edtCity, transeletion.getTranselationTextByTextIdentifier("lblRegCity"));
		this.setHintToText(transeletion.getTranselationTextByTextIdentifier("lblRegSchool"),
				edtSchool);
		this.setHintToText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"),
				edtTeacher);
		txtAvtar.setText(transeletion.getTranselationTextByTextIdentifier("lblAvatar"));
		/*this.setHintToText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"), 
				edtGrade);*/
		zipText = transeletion.getTranselationTextByTextIdentifier("lblRegZip");
		cityText = transeletion.getTranselationTextByTextIdentifier("lblRegCity");

		btnTitleSubmit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));

		alertMsgPleaseEnterAllInfo = transeletion
				.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo");
		/*alertMsgInvalidCity = transeletion
				.getTranselationTextByTextIdentifier("alertMsgInvalidCity");*/
		alertMsgInvalidZipCode = transeletion
				.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode");
		alertPleaseSelectYourGrade = transeletion
				.getTranselationTextByTextIdentifier("lblPleaseSelectGrade");
		alertPleaseSelectYourSchoolFirst = transeletion
				.getTranselationTextByTextIdentifier("lblPleaseSelectSchool");
		stateText = transeletion.getTranselationTextByTextIdentifier("lblRegState");
		transeletion.closeConnection();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * Set the default county
	 */
	private void setCountry(){
		if(tempPlayer != null){
			edtZipCode.setText(tempPlayer.getZipCode());
			this.getCountries(this, tempPlayer.getCountry());
		}else{
			this.getCountries(this, MathFriendzyHelper.UNITED_STATE);
		}
	}

	/**
	 * @Description getCountries from database and set it to the country adapter
	 * @param context
	 * @param countryName
	 */
	protected void getCountries(Context context , String countryName){
		this.setCountryAdapter(context, countryName);
	}

	/**
	 * @description Set country adapter
	 * @param context
	 * @param countryName
	 */
	private void setCountryAdapter(Context context,String countryName){
		MathFriendzyHelper.setCountryAdapter(this, countryName, countryList, edtCountry);
	}

	/**
	 * Set grade
	 */
	private void setGrade() {
		if(tempPlayer != null){
			MathFriendzyHelper.setAdapterToSpinner(this, tempPlayer.getGrade() + "",
					gradeList, edtGrade);
		}else{
			MathFriendzyHelper.setAdapterToSpinner(this, MathFriendzyHelper.GRADE_TEXT,
					gradeList, edtGrade);
		}
	}

	/**
	 * Click to select school
	 */
	private void clickToSelectSchool(){
		//how to select school
		String seletedCountry = edtCountry.getSelectedItem().toString();
		if(seletedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)
				|| seletedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
			MathFriendzyHelper.selectSchool(this, seletedCountry ,
					"", edtZipCode.getText().toString(), "");
		}else{
			MathFriendzyHelper.selectSchool(this, seletedCountry ,
					edtCity.getText().toString(), "" , "");//zip as city
		}       
	}

	private void clickToSelectTeacher(){
		String selectedSchool = school.getSchoolName();
		if( selectedSchool != null && selectedSchool.length() > 0){
			MathFriendzyHelper.selectTeacher(this, school.getSchoolId());
		}else{
			MathFriendzyHelper.warningDialog(this, alertPleaseSelectYourSchoolFirst);
		}
	}


	/**
	 * Select avatar
	 */
	private void clickToSelectAvatar(){
		MathFriendzyHelper.selectAvatar(this, SELECT_AVATAR_REQUEST);
	}

	/**
	 *
	 * Clicl to submit user data
	 */
	private void clickToSubmit(){
		String selectedCountry = edtCountry.getSelectedItem().toString();
		String zipCode = edtZipCode.getText().toString();
		String city = edtCity.getText().toString();
		int selectedGrade = MathFriendzyHelper.getGrade(edtGrade.getSelectedItem().toString());
		/*String schoolName = school.getSchoolName();
		String teacherName = teacher.getfName();*/

		//if(!MathFriendzyHelper.checkForEmptyFields(zipCode)){
		if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)
				|| selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
			if(MathFriendzyHelper.checkForEmptyFields(zipCode , city)){
				MathFriendzyHelper.showWarningDialog(this, alertMsgPleaseEnterAllInfo);
			}else if(selectedGrade == MathFriendzyHelper.NO_GRADE_SELETED){
				MathFriendzyHelper.showWarningDialog(this, alertPleaseSelectYourGrade);
			}else{
				this.registerUser();
			}
		}else{
			if(MathFriendzyHelper.checkForEmptyFields(city)){
				MathFriendzyHelper.showWarningDialog(this, alertMsgPleaseEnterAllInfo);
			}else if(selectedGrade == MathFriendzyHelper.NO_GRADE_SELETED)
				MathFriendzyHelper.showWarningDialog(this, alertPleaseSelectYourGrade);
			else{
				this.registerUser();
			}
		}
		/*}else{
			MathFriendzyHelper.showWarningDialog(this, alertMsgPleaseEnterAllInfo);
		}*/
	}

	private void showProgressDialog(){
		if(progressDialog != null && !progressDialog.isShowing()){
			progressDialog.show();
		}
	}

	private void hideProgressDialog(){
		if(progressDialog != null && progressDialog.isShowing()){
			progressDialog.cancel();
		}
	}

	private String getSelectedState(){
		try{
			String selectedState = 
					edtSpinner.getSelectedItem().toString();
			return selectedState;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Register user
	 */
	private void registerUser(){

		String selectedCountry = edtCountry.getSelectedItem().toString();
		String selectedState = this.getSelectedState();
		String countryId  = MathFriendzyHelper.getCountryIdByCountryName(selectedCountry, this);
		String countryIso = MathFriendzyHelper.getCountryIsoByCountryName(selectedCountry, this);
		int selectedGrade = MathFriendzyHelper.getGrade(edtGrade.getSelectedItem().toString());

		String stateId = "0";//default state id
		String stateCode = "";//default state code
		String state = "";
		String city = "";//default city
		String zipCode = "";

		if(selectedState != null){
			if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)){
				state = selectedState;
				stateId   = MathFriendzyHelper.getUsStateIdByName(state, this);
				stateCode = MathFriendzyHelper.getUsStateCodeByName(state, this);
				//city = MathFriendzyHelper.DEFAULT_US_CITY;
				//zipCode = edtZipCode.getText().toString();
			}else if(selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)){
				state = selectedState;
				stateId   = MathFriendzyHelper.getCanadaStateIdByName(state, this);
				stateCode = MathFriendzyHelper.getCanadaStateCodeByName(state, this);
				//city = MathFriendzyHelper.DEFAULT_CANADA_CITY;
				//zipCode = edtZipCode.getText().toString();
			}else{
				//city = edtZipCode.getText().toString();
			}
		}

		//new change for adding state and city
		city = edtCity.getText().toString();
		zipCode = edtZipCode.getText().toString();
		//end change

		String languageId   = MathFriendzyHelper.ENG_LANGUAGE_ID;//default language id
		//String languageCode = MathFriendzyHelper.ENG_LANGuAGE_CODE;//default language code
		String isParentValue    = "2";
		String schoolIdFromFind	  = school.getSchoolId();
		String schoolNameFromFind = school.getSchoolName();
		String volume = "0";
		String coins = "0";
		String players = "<AllPlayers></AllPlayers>";

		//String tempPlayerUserName = null;

		if(!MathFriendzyHelper.isTempraroryPlayerCreated(this)){
			MathFriendzyHelper.createBlankTempTable(this);
		}

		//if(MathFriendzyHelper.isTempraroryPlayerCreated(this)){
		if(tempPlayer != null){
			tempPlayer.setUserName(registration.getUserName());//replace the temp player user name with new one
			tempPlayer.setFirstName(registration.getfName());
			tempPlayer.setLastName(registration.getlName());
			tempPlayer.setProfileImageName(profileImageName);
			tempPlayer.setGrade(selectedGrade);
			ArrayList<TempPlayer> tempPlayerObj = new ArrayList<TempPlayer>();
			tempPlayerObj.add(tempPlayer);
			players =  "<AllPlayers>"+this.getXmlPlayer(tempPlayerObj) + "</AllPlayers>";
		}else{
			//create a new player based on given information
			TempPlayer defaultPlayer = MathFriendzyHelper.createDefaultPlayer(this,
					registration.getfName(),
					registration.getlName(), city, selectedGrade, school, teacher,
					registration.getUserName(), zipCode, state, selectedCountry,
					profileImageName);
			ArrayList<TempPlayer> tempPlayerObj = new ArrayList<TempPlayer>();
			tempPlayerObj.add(defaultPlayer);
			players =  "<AllPlayers>"+this.getXmlPlayer(tempPlayerObj) + "</AllPlayers>";
		}

		RegistereUserDto regObj = new RegistereUserDto();
		regObj.setFirstName(registration.getfName());
		regObj.setLastName(registration.getlName());
		regObj.setEmail(registration.getEmail());
		regObj.setPass(registration.getPassword());
		regObj.setCountryId(countryId);
		regObj.setCountryIso(countryIso);
		regObj.setStateId(stateId);
		regObj.setStateCode(stateCode);
		regObj.setCity(city);
		regObj.setPreferedLanguageId(languageId);
		regObj.setSchoolId(schoolIdFromFind);
		regObj.setSchoolName(schoolNameFromFind);
		regObj.setIsParent(isParentValue);
		regObj.setIsStudent("0");
		regObj.setIsTeacher("0");
		regObj.setVolume(volume);
		regObj.setZip(zipCode);
		regObj.setPlayers(players);
		regObj.setCoins(coins);

		if(CommonUtils.isInternetConnectionAvailable(this)){
			this.showProgressDialog();
			this.registerUser(regObj);
		}else{
			CommonUtils.showInternetDialog(this);
		}		
	}

	private StudentRegistrationStep2 getCurrentObj(){
		return this;
	}

	/**
	 * Register user
	 * @param regObj
	 */
	private void registerUser(RegistereUserDto regObj){
		MathFriendzyHelper.registerUser(this, regObj, new OnRegistration() {

			@Override
			public void onRegistrationComplete(int result) {
				if(result == Register.SUCCESS){
					afterRegisterSuccess();
				}else if(result == Register.INVALID_CITY){
					hideProgressDialog();
					//MathFriendzyHelper.showWarningDialog(getCurrentObj(), alertMsgInvalidCity);
					MathFriendzyHelper.showWarningDialog(getCurrentObj(), alertMsgInvalidZipCode);
				}else if(result == Register.INVALID_ZIP){
					hideProgressDialog();
					MathFriendzyHelper.showWarningDialog(getCurrentObj(), alertMsgInvalidZipCode);
				}else if(result == 0){
					hideProgressDialog();
					CommonUtils.showInternetDialog(getCurrentObj());
				}
			}
		});
	}

	/**
	 * Do the thing after registration success
	 */
	private void afterRegisterSuccess(){

		MathFriendzyHelper.saveUserLogin(this, true);
		MathFriendzyHelper.addUserOnServerWithAndroidDevice(this);

		if(tempPlayer != null){//If temp player exist then do the following
			String playerId = MathFriendzyHelper.getPlayerIdOfTempPlayerWhichIsNowRegister
					(this, tempPlayer);
			String userId = MathFriendzyHelper.getUserIdOfTempPlayerWhichIsNowRegister
					(this, tempPlayer);
			MathFriendzyHelper.updateTempPlayerDataWhichIsNotRegisteredPlayed(userId, playerId, this);

			//for avatar purchased by temp player
			ArrayList<String> avatarList = MathFriendzyHelper.updatePlayerAvterAndGetList
					(userId , playerId , this);
			if(avatarList.size() > 0){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					MathFriendzyHelper.saveAvatar(userId, playerId, avatarList);
				}
			}

			//item puchased by temmp player
			ArrayList<String> purchaseItemList = MathFriendzyHelper.
					getUserPurchaseItemList(this , userId);
			if(purchaseItemList.size() > 0){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					MathFriendzyHelper.savePurchasedItem(this, userId, purchaseItemList);
				}
			}
		}
		//for language
		if(tempPlayer != null){
			ArrayList<MathResultTransferObj> mathResultList = MathFriendzyHelper
					.getResultMathList(this);
			if(mathResultList.size() > 0){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					MathFriendzyHelper.saveTempPlayerScoreOnServer(this, mathResultList,
							new OnRequestComplete() {
						@Override
						public void onComplete() {
							MathFriendzyHelper.deleteFromMath(getCurrentObj());
							ArrayList<PlayerEquationLevelObj> playerquationData = MathFriendzyHelper
									.getPlayerEquationData(getCurrentObj(),
											MathFriendzyHelper
											.getPlayerIdOfTempPlayerWhichIsNowRegister
											(getCurrentObj(), tempPlayer));

							if(playerquationData.size() > 0){
								if(CommonUtils.isInternetConnectionAvailable(getCurrentObj())){
									MathFriendzyHelper.addLevelAsynckTask(playerquationData,
											new OnRequestComplete() {
										@Override
										public void onComplete() {
											hideProgressDialog();
											doneRegistration();
										}
									});
								}else{
									hideProgressDialog();
									CommonUtils.showInternetDialog(getCurrentObj());
								}
							}else{
								hideProgressDialog();
								doneRegistration();
							}
						}
					}, tempPlayer.getUserName());
				}else{
					hideProgressDialog();
					CommonUtils.showInternetDialog(this);
				}
			}else{
				hideProgressDialog();
				doneRegistration();
			}
		}else{
			hideProgressDialog();
			doneRegistration();
		}
	}

	/**
	 * Done after registration
	 */
	private void doneRegistration(){
		MathFriendzyHelper.setCheckPlayer(this);
		/*if(tempPlayer != null){
			MathFriendzyHelper.openEditRegisteredUserScreenAfterRegistration(this, 
					registration.getEmail());
		}else{*/
		MathFriendzyHelper.openMainScreenAfterRegistration(this, registration.getEmail());
		//}
	}

	@Override
	public void onClick(View v) {
		MathFriendzyHelper.clearFocus(this);
		if(onFocusLostIsValidString) {
			switch (v.getId()) {
			case R.id.spinnerSchoolLayout:
				this.clickToSelectSchool();
				break;
			case R.id.spinnerTeacherLayout:
				this.clickToSelectTeacher();
				break;
			case R.id.avtarLayout:
				this.clickToSelectAvatar();
				break;
			case R.id.btnTitleSubmit:
				this.clickToSubmit();
				break;
			}
		}
	}

	/**
	 * On School Selected
	 * @param schoolInfo
	 */
	private void onSelectSchool(ArrayList<String> schoolInfo){
		edtSchool.setText(schoolInfo.get(0));
		school.setSchoolName(schoolInfo.get(0));
		school.setSchoolId(schoolInfo.get(1));

		if(school.getSchoolName().
				equalsIgnoreCase(MathFriendzyHelper.HOME_SCHOOL)){
			this.enableDisableListenerOntextView(spinnerTeacherLayout, false);
			edtTeacher.setText("N/A");
			this.initializeTeacherWithDefaultValue();
		}else{
			this.enableDisableListenerOntextView(spinnerTeacherLayout, true);
		}
	}

	/**
	 * On Teacher selection
	 * @param teacherInfo
	 */
	private void onTeacherSelected(ArrayList<String> teacherInfo){
		edtTeacher.setText(teacherInfo.get(0) + " " + teacherInfo.get(1));
		teacher.setfName(teacherInfo.get(0));
		teacher.setlName(teacherInfo.get(1));
		teacher.setTeacherUserId(teacherInfo.get(2));
	}

	/**
	 * On Avatar selection
	 * @param avatarName
	 */
	private void onAvatarSelection(String avatarName){
		if(avatarName != null){
			profileImageName = avatarName;
			avtarImage.setImageBitmap(MathFriendzyHelper
					.getAvatarImageByName(this, avatarName));
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case MathFriendzyHelper.SELECT_SCHOOL_REQUEST_CODE:
				this.onSelectSchool(data.getStringArrayListExtra("schoolInfo"));
				break;
			case MathFriendzyHelper.SELECT_TEACHER_REQUEST_CODE:
				this.onTeacherSelected(data.getStringArrayListExtra("schoolInfo"));
				break;
			case SELECT_AVATAR_REQUEST :
				this.onAvatarSelection(data.getStringExtra("selectedAvatarName"));
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	//check only for the last edittext which has current focus
	@Override
	public void onFocusLost(boolean isValidString) {
		onFocusLostIsValidString = isValidString;
	}

	@Override
	public void onFocusHas(boolean isValidString) {

	}
}
